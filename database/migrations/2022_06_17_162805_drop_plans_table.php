<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plans', function (Blueprint $table) {
            //$table->dropColumn(['slug','features','features','monthly','annual']);
            $table->renameColumn('price', 'display_price');
            $table->string('product_name')->nullable();
            $table->integer('interval_count')->nullable();
            $table->string('apple_id')->nullable();
            $table->string('google_id')->nullable();
            $table->float('display_price_google')->nullable();
            $table->float('display_price_apple')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plans', function (Blueprint $table) {
            $table->string('slug')->nullable();
            $table->renameColumn('display_price', 'price');
            $table->json('features')->nullable();
            $table->boolean('is_active')->nullable();
            $table->boolean('monthly')->nullable();
            $table->boolean('annual')->nullable();
            $table->dropColumn('product_name');
            $table->dropColumn('interval_count');
            $table->dropColumn('apple_id');
            $table->dropColumn('google_id');
            $table->dropColumn('display_price_google');
            $table->dropColumn('display_price_apple');
        });
    }
}
