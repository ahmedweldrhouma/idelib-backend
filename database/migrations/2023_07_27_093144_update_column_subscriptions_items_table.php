<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColumnSubscriptionsItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subscription_items', function (Blueprint $table) {
            Schema::disableForeignKeyConstraints();
            $table->unsignedBigInteger('mobile_subscription_id')->nullable();
            $table->dropColumn('subscription_id');
            Schema::enableForeignKeyConstraints();

        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('subscription_items', function (Blueprint $table) {
            Schema::disableForeignKeyConstraints();

            $table->unsignedBigInteger('subscription_id');
            $table->dropColumn('mobile_subscription_id');
            Schema::enableForeignKeyConstraints();

        });
    }
}
