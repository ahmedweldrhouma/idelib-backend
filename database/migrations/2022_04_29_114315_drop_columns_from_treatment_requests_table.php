<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropColumnsFromTreatmentRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('treatment_requests', function (Blueprint $table) {
            $table->dropColumn(['start_date','end_date','treatment']);
            $table->longText('note')->nullable();
        });
    }

}
