<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTreatmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('treatments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('start_at')->nullable();
            $table->date('end_at')->nullable();
            $table->string('period')->nullable();
            $table->string('daily_frequency')->nullable();
            $table->string('monthly_frequency')->nullable();
            $table->string('type')->nullable();
            $table->string('classic_type')->nullable();
            $table->string('bsi_type')->nullable();
            $table->string('treatment')->nullable();
            $table->datetime('stoped_at')->nullable();
            $table->date('absent_from')->nullable();
            $table->date('absent_to')->nullable();
            $table->foreignId('patient_id')
                ->nullable()
                ->constrained('patients')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignId('nurse_id')
                ->nullable()
                ->constrained('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('treatments');
    }
}
