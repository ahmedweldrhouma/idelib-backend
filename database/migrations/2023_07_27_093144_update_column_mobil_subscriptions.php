<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColumnMobilSubscriptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mobile_subscriptions', function (Blueprint $table) {
            $table->string('stripe_id')->nullable();
           // $table->dropForeign('plan_id');

            $table->integer('plan_id')->nullable()->change();

            $table->string('provider')->nullable()->change();
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('mobile_subscriptions', function (Blueprint $table) {
            $table->dropColumn('stripe_id');
        });
    }
}
