<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->string('model')->nullable();
            $table->string('page')->nullable();
            $table->string('content')->nullable();
            $table->integer('model_id')->nullable();
            $table->foreignId('receiver_id')
                ->constrained('users')
                ->onUpdate('cascade')
                ->onDelete('cascade')
                ->cascadeOnUpdate()
                ->cascadeOnDelete()
                ->nullable();
            $table->string('subject')->nullable();
            $table->foreignId('user_id')
                ->constrained('users')
                ->onUpdate('cascade')
                ->onDelete('cascade')
                ->cascadeOnUpdate()
                ->cascadeOnDelete()
                ->nullable();
            $table->timestamps();
            $table->timestamp('sent_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
