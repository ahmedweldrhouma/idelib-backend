<?php

use App\Domains\Message\Models\Message;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->id();
            $table->text('content')->nullable();
             $table->enum('type', [
                 Message::TYPE_PICTURE,
                 Message::TYPE_FILE,
                 Message::TYPE_TEXT,
                 Message::TYPE_VOCAL
             ])->nullable();
            $table->string('attachment')->nullable();

            $table->foreignId('sender_id')
                ->constrained('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreignId('conversation_id')
                ->constrained('conversations')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            ;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
