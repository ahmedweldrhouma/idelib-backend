<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToTreatmentRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('treatment_requests', function (Blueprint $table) {
            $table->foreignId('nurse_id')
                ->nullable()
                ->constrained('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->boolean('is_available')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('treatment_requests', function (Blueprint $table) {
            $table->dropForeign('treatment_requests_nurse_id_foreign');
            $table->dropColumn('nurse_id');
            $table->dropColumn('is_available');
        });
    }
}
