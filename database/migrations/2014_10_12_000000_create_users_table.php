<?php

use App\Domains\Auth\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('type', [
                User::TYPE_SUPER_ADMIN,
                User::TYPE_ADMIN,
                User::TYPE_PATIENT,
                User::TYPE_NURSE,
                User::TYPE_SUBSTITUTE_NURSE,
                User::TYPE_BILLER
                ])->default(User::TYPE_ADMIN);
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique()->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->timestamp('password_changed_at')->nullable();
            $table->string('api_token')->nullable();
            $table->string('phone')->nullable();
            $table->string('app_user_id')->unique()->nullable();
            $table->string('adeli_number')->nullable();
            $table->string('address')->nullable();
            $table->date('birth_date')->nullable();
            $table->string('tour_sector')->nullable();
            $table->string('presentation')->nullable();
            $table->string('subscription_type')->nullable();
            $table->string('operating_system')->nullable();
            $table->string('avatar_type')->default('gravatar');
            $table->string('avatar_location')->nullable();
            $table->unsignedTinyInteger('active')->default(1);
            $table->string('timezone')->nullable();
            $table->timestamp('last_login_at')->nullable();
            $table->string('last_login_ip')->nullable();
            $table->boolean('to_be_logged_out')->default(false);
            $table->string('confirmation_code')->nullable()->default(null);
            $table->boolean('is_used')->nullable()->default(false);
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
