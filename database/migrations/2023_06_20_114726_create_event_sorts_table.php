<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventSortsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_sorts', function (Blueprint $table) {
            $table->id();
            $table->smallInteger('index')->nullable();
            $table->date('day');
            $table->time('time_from')->nullable();
            $table->time('time_to')->nullable();
            $table->foreignId('appointment_id')
                ->nullable()
                ->constrained('appointments')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignId('treatment_id')
                ->nullable()
                ->constrained('treatments')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignId('user_id')
                ->constrained('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_sorts');
    }
}
