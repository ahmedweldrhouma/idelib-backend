<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMobileSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mobile_subscriptions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('provider');
            $table->string('subscription_id')->nullable();
            $table->longText('receipt')->nullable();
            $table->foreignId('user_id')
                ->constrained('users')
                ->onUpdate('cascade');
            $table->foreignId('plan_id')
                ->constrained('plans')
                ->onUpdate('cascade');
            $table->boolean('is_active')->nullable();
            $table->string('transaction_id')->nullable();
            $table->string('original_transaction_id')->nullable();
            $table->string('operating_system')->nullable();
            $table->datetime('subscribed_at')->nullable();
            $table->datetime('expired_at')->nullable();
            $table->datetime('cancelled_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mobile_subscriptions');
    }
}
