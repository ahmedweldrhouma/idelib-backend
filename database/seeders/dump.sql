-- Adminer 4.8.1 PostgreSQL 13.7 dump

DROP TABLE IF EXISTS "activity_log";
DROP SEQUENCE IF EXISTS activity_log_id_seq;
CREATE SEQUENCE activity_log_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."activity_log" (
    "id" bigint DEFAULT nextval('activity_log_id_seq') NOT NULL,
    "log_name" character varying(255),
    "description" text NOT NULL,
    "subject_id" bigint,
    "subject_type" character varying(255),
    "causer_id" bigint,
    "causer_type" character varying(255),
    "properties" json,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "activity_log_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

CREATE INDEX "activity_log_log_name_index" ON "public"."activity_log" USING btree ("log_name");

CREATE INDEX "causer" ON "public"."activity_log" USING btree ("causer_id", "causer_type");

CREATE INDEX "subject" ON "public"."activity_log" USING btree ("subject_id", "subject_type");


DROP TABLE IF EXISTS "admin_subscriptions";
DROP SEQUENCE IF EXISTS admin_subscriptions_id_seq;
CREATE SEQUENCE admin_subscriptions_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."admin_subscriptions" (
    "id" bigint DEFAULT nextval('admin_subscriptions_id_seq') NOT NULL,
    "user_id" bigint NOT NULL,
    "plan_id" bigint NOT NULL,
    "subscribed_at" timestamp(0) NOT NULL,
    "expires_at" timestamp(0) NOT NULL,
    "iap" boolean DEFAULT false,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "admin_subscriptions_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "announcements";
DROP SEQUENCE IF EXISTS announcements_id_seq;
CREATE SEQUENCE announcements_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."announcements" (
    "id" bigint DEFAULT nextval('announcements_id_seq') NOT NULL,
    "start_date" date,
    "end_date" date,
    "sector" character varying(255),
    "has_transport" boolean,
    "announcement" text,
    "user_id" bigint NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "announcements_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "announcements" ("id", "start_date", "end_date", "sector", "has_transport", "announcement", "user_id", "created_at", "updated_at") VALUES
(1,	'2022-12-08',	'2022-12-10',	'98890',	'1',	'je suis une inf',	4,	'2022-12-02 17:43:50',	'2022-12-02 17:43:50'),
(2,	'2022-12-13',	'2022-12-14',	'98885,98884',	'0',	'hello',	4,	'2022-12-02 17:44:43',	'2022-12-02 17:44:43'),
(3,	'2022-12-05',	'2022-12-07',	',98889',	'1',	'hi',	9,	'2022-12-02 18:10:06',	'2022-12-02 18:10:06'),
(4,	'2022-12-19',	'2022-12-22',	',98884',	'0',	'hi test',	9,	'2022-12-02 18:10:31',	'2022-12-02 18:10:31'),
(5,	'2022-12-06',	'2022-12-31',	',13001,13002,13004',	'1',	'Infirmier D.E depuis plus de 10ans, je suis remplaçant en libéral depuis 3ans. véhiculé, je suis disponible tous les jours.',	21,	'2022-12-06 09:26:00',	'2022-12-06 09:26:00'),
(6,	'2022-12-06',	'2022-12-07',	',98890,98889',	'1',	'anonnce trés urgente',	3,	'2022-12-06 09:48:10',	'2022-12-06 09:48:10'),
(7,	'2022-12-12',	'2022-12-23',	'13180',	'1',	'djfkgjekgkcb',	16,	'2022-12-06 13:42:45',	'2022-12-06 13:42:45');

DROP TABLE IF EXISTS "applications";
DROP SEQUENCE IF EXISTS applications_id_seq;
CREATE SEQUENCE applications_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."applications" (
    "id" bigint DEFAULT nextval('applications_id_seq') NOT NULL,
    "user_id" bigint NOT NULL,
    "announcement_id" bigint NOT NULL,
    "accepted" boolean,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "applications_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "appointments";
DROP SEQUENCE IF EXISTS appointments_id_seq;
CREATE SEQUENCE appointments_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."appointments" (
    "id" bigint DEFAULT nextval('appointments_id_seq') NOT NULL,
    "title" character varying(255) NOT NULL,
    "address" character varying(255),
    "start_date" date NOT NULL,
    "end_date" date NOT NULL,
    "start_time" time(0) without time zone,
    "end_time" time(0) without time zone,
    "note" text,
    "nurse_id" bigint NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "appointments_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "appointments" ("id", "title", "address", "start_date", "end_date", "start_time", "end_time", "note", "nurse_id", "created_at", "updated_at") VALUES
(4,	'qqsdq',	'saint jean',	'2022-12-13',	'2022-12-15',	'15:55:00',	'20:15:00',	'qsdqsd',	3,	'2022-12-02 14:58:16',	'2022-12-02 14:58:16'),
(5,	'sdqsd',	'saint jean',	'2022-12-01',	'2022-12-02',	'15:55:00',	'20:00:00',	'qsdqsd',	3,	'2022-12-02 14:59:02',	'2022-12-02 14:59:02'),
(12,	'test pcr',	'saint jean',	'2022-12-07',	'2022-12-07',	'16:30:00',	'17:30:00',	'test',	4,	'2022-12-05 15:21:02',	'2022-12-05 15:32:22'),
(16,	'test',	'saint jean',	'2022-12-13',	'2022-12-15',	'16:45:00',	'18:45:00',	'test inff',	17,	'2022-12-05 15:49:01',	'2022-12-05 15:49:01'),
(17,	'test 2',	'saint jean',	'2022-12-27',	'2022-12-27',	'17:55:00',	'18:55:00',	'test 2',	17,	'2022-12-05 15:56:48',	'2022-12-05 15:56:48'),
(19,	'test',	'saint jean',	'2022-12-08',	'2022-12-09',	'16:55:00',	'18:55:00',	'testt',	4,	'2022-12-05 15:59:38',	'2022-12-05 15:59:38'),
(21,	'tyyhvx',	'saint jean',	'2022-12-14',	'2022-12-23',	'16:20:00',	'18:20:00',	'hcbndx',	5,	'2022-12-06 12:56:27',	'2022-12-06 12:56:27');

DROP TABLE IF EXISTS "covid_treatments";
DROP SEQUENCE IF EXISTS covid_treatments_id_seq;
CREATE SEQUENCE covid_treatments_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."covid_treatments" (
    "id" bigint DEFAULT nextval('covid_treatments_id_seq') NOT NULL,
    "type" character varying(255) NOT NULL,
    "date" date NOT NULL,
    "result" character varying(255),
    "prescription" character varying(255),
    "patient_id" bigint NOT NULL,
    "user_id" bigint NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "covid_treatments_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "covid_treatments" ("id", "type", "date", "result", "prescription", "patient_id", "user_id", "created_at", "updated_at") VALUES
(1,	'pcr',	'2022-12-14',	'negative',	NULL,	7,	4,	'2022-12-05 12:15:02',	'2022-12-05 12:15:02'),
(2,	'pcr',	'2022-12-08',	'positive',	NULL,	14,	4,	'2022-12-05 16:35:37',	'2022-12-05 16:35:37'),
(3,	'pcr',	'2022-12-14',	'positive',	NULL,	21,	5,	'2022-12-05 21:08:26',	'2022-12-05 21:08:26'),
(4,	'antigen',	'2022-12-14',	'positive',	NULL,	20,	5,	'2022-12-05 21:10:18',	'2022-12-05 21:10:18'),
(5,	'pcr',	'2022-12-01',	'negative',	NULL,	20,	5,	'2022-12-05 21:10:39',	'2022-12-05 21:10:39'),
(6,	'visite',	'2022-12-14',	NULL,	NULL,	20,	5,	'2022-12-05 21:10:55',	'2022-12-05 21:10:55'),
(7,	'pcr',	'2022-12-16',	'positive',	NULL,	18,	4,	'2022-12-06 09:12:09',	'2022-12-06 09:12:09'),
(10,	'pcr',	'2022-12-05',	'negative',	NULL,	22,	16,	'2022-12-06 13:32:15',	'2022-12-06 13:32:15'),
(8,	'pcr',	'2022-12-06',	'negative',	NULL,	22,	16,	'2022-12-06 09:33:47',	'2022-12-07 10:39:57');

DROP TABLE IF EXISTS "disponibilites";
DROP SEQUENCE IF EXISTS disponibilites_id_seq;
CREATE SEQUENCE disponibilites_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."disponibilites" (
    "id" bigint DEFAULT nextval('disponibilites_id_seq') NOT NULL,
    "start_date" date NOT NULL,
    "end_date" date NOT NULL,
    "start_time" time(0) without time zone,
    "end_time" time(0) without time zone,
    "nurse_id" bigint NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "disponibilites_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "disponibilites" ("id", "start_date", "end_date", "start_time", "end_time", "nurse_id", "created_at", "updated_at") VALUES
(2,	'2022-12-02',	'2022-12-02',	'08:00:00',	'18:00:00',	5,	'2022-12-02 14:37:51',	'2022-12-02 14:37:51'),
(7,	'2022-12-06',	'2022-12-08',	'08:00:00',	'18:00:00',	9,	'2022-12-02 17:38:26',	'2022-12-02 17:38:26'),
(10,	'2022-12-15',	'2022-12-17',	'13:00:00',	'18:00:00',	4,	'2022-12-02 17:39:33',	'2022-12-02 17:39:33'),
(11,	'2022-12-12',	'2022-12-15',	'08:00:00',	'12:00:00',	9,	'2022-12-02 17:40:02',	'2022-12-02 17:40:02'),
(12,	'2022-12-05',	'2022-12-09',	'08:00:00',	'18:00:00',	16,	'2022-12-04 17:08:16',	'2022-12-04 17:08:16'),
(13,	'2022-12-01',	'2022-12-03',	'08:00:00',	'18:00:00',	17,	'2022-12-06 09:33:13',	'2022-12-06 09:33:13'),
(14,	'2022-12-12',	'2022-12-16',	'08:00:00',	'18:00:00',	16,	'2022-12-06 10:55:34',	'2022-12-06 10:55:34'),
(15,	'2022-12-07',	'2022-12-09',	'08:00:00',	'12:00:00',	17,	'2022-12-06 15:19:29',	'2022-12-06 15:19:29');

DROP TABLE IF EXISTS "failed_jobs";
DROP SEQUENCE IF EXISTS failed_jobs_id_seq;
CREATE SEQUENCE failed_jobs_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."failed_jobs" (
    "id" bigint DEFAULT nextval('failed_jobs_id_seq') NOT NULL,
    "uuid" character varying(255) NOT NULL,
    "connection" text NOT NULL,
    "queue" text NOT NULL,
    "payload" text NOT NULL,
    "exception" text NOT NULL,
    "failed_at" timestamp(0) DEFAULT CURRENT_TIMESTAMP NOT NULL,
    CONSTRAINT "failed_jobs_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "failed_jobs_uuid_unique" UNIQUE ("uuid")
) WITH (oids = false);


DROP TABLE IF EXISTS "general_configs";
DROP SEQUENCE IF EXISTS general_configs_id_seq;
CREATE SEQUENCE general_configs_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."general_configs" (
    "id" bigint DEFAULT nextval('general_configs_id_seq') NOT NULL,
    "MaxNursesByDay" integer DEFAULT '3' NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "general_configs_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "invitations";
DROP SEQUENCE IF EXISTS invitations_id_seq;
CREATE SEQUENCE invitations_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."invitations" (
    "id" bigint DEFAULT nextval('invitations_id_seq') NOT NULL,
    "type" character varying(255) NOT NULL,
    "email" character varying(255) NOT NULL,
    "token" character varying(255) NOT NULL,
    "user_id" bigint NOT NULL,
    "team_id" bigint NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    "invited_id" bigint,
    CONSTRAINT "invitations_email_unique" UNIQUE ("email"),
    CONSTRAINT "invitations_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "migrations";
DROP SEQUENCE IF EXISTS migrations_id_seq;
CREATE SEQUENCE migrations_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;

CREATE TABLE "public"."migrations" (
    "id" integer DEFAULT nextval('migrations_id_seq') NOT NULL,
    "migration" character varying(255) NOT NULL,
    "batch" integer NOT NULL,
    CONSTRAINT "migrations_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "migrations" ("id", "migration", "batch") VALUES
(1,	'2014_10_12_000000_create_users_table',	1),
(2,	'2014_10_12_100000_create_password_resets_table',	1),
(3,	'2019_08_19_000000_create_failed_jobs_table',	1),
(4,	'2019_12_14_000001_create_personal_access_tokens_table',	1),
(5,	'2020_02_25_034148_create_permission_tables',	1),
(6,	'2020_05_29_020244_create_password_histories_table',	1),
(7,	'2020_07_06_215139_create_activity_log_table',	1),
(8,	'2021_04_05_153840_create_two_factor_authentications_table',	1),
(9,	'2022_01_03_141858_create_patients_table',	1),
(10,	'2022_01_05_164706_create_plans_table',	1),
(11,	'2022_01_06_122542_create_mobile_subscriptions_table',	1),
(12,	'2022_01_13_152712_create_treatments_table',	1),
(13,	'2022_01_14_133733_create_passages_table',	1),
(14,	'2022_01_21_151037_create_annoucements_table',	1),
(15,	'2022_02_07_140043_create_covid_treatments_table',	1),
(16,	'2022_02_09_114933_create_vaccines_table',	1),
(17,	'2022_02_14_132159_add_prescription_column_to_treatments_table',	1),
(18,	'2022_04_05_134744_create_requests_table',	1),
(19,	'2022_04_15_152444_add_sector_column_to_treatment_requests_table',	1),
(20,	'2022_04_25_145843_add_is_pending_column_to_treatment_requests_table',	1),
(21,	'2022_04_26_151843_add_zip_code_column_to_patients_table',	1),
(22,	'2022_04_27_135951_add_columns_to_treatment_requests_table',	1),
(23,	'2022_04_29_114315_drop_columns_from_treatment_requests_table',	1),
(24,	'2022_05_04_155553_create_teams_table',	1),
(25,	'2022_05_04_155709_create_invitations_table',	1),
(26,	'2022_05_23_155929_add_created_by_column_to_users_table',	1),
(27,	'2022_06_08_190512_create_ratings_table',	1),
(28,	'2022_06_14_222450_add_invited_id_column_to_invitations_table',	1),
(29,	'2022_06_17_162805_drop_plans_table',	1),
(30,	'2022_06_20_155231_create_admin_subscriptions_table',	1),
(31,	'2022_06_24_095830_create_applications_table',	1),
(32,	'2022_06_30_133820_add_fcm_token_to_users_table',	1),
(33,	'2022_07_26_132707_add_rc_id_columns_to_users_table',	2),
(34,	'2022_07_26_132708_add_new_columns_to_users_table',	2),
(35,	'2022_08_17_120117_create_appointments_table',	2),
(36,	'2022_10_18_093415_create_tracking_table',	2),
(37,	'2022_11_10_190432_create_disponibilites_table',	2),
(38,	'2022_11_10_215551_create_general_configs_table',	2),
(39,	'2022_11_17_002156_add_timezone_column_to_users_table',	2),
(40,	'2022_11_17_002156_upgrade_two_factor_authentications_table',	2);

DROP TABLE IF EXISTS "mobile_subscriptions";
DROP SEQUENCE IF EXISTS mobile_subscriptions_id_seq;
CREATE SEQUENCE mobile_subscriptions_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."mobile_subscriptions" (
    "id" bigint DEFAULT nextval('mobile_subscriptions_id_seq') NOT NULL,
    "provider" character varying(255) NOT NULL,
    "subscription_id" character varying(255),
    "receipt" text NOT NULL,
    "user_id" bigint NOT NULL,
    "plan_id" bigint NOT NULL,
    "is_active" boolean,
    "transaction_id" character varying(255),
    "original_transaction_id" character varying(255),
    "operating_system" character varying(255),
    "subscribed_at" timestamp(0),
    "expired_at" timestamp(0),
    "cancelled_at" timestamp(0),
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "mobile_subscriptions_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "model_has_permissions";
CREATE TABLE "public"."model_has_permissions" (
    "permission_id" bigint NOT NULL,
    "model_type" character varying(255) NOT NULL,
    "model_id" bigint NOT NULL,
    CONSTRAINT "model_has_permissions_pkey" PRIMARY KEY ("permission_id", "model_id", "model_type")
) WITH (oids = false);

CREATE INDEX "model_has_permissions_model_id_model_type_index" ON "public"."model_has_permissions" USING btree ("model_id", "model_type");


DROP TABLE IF EXISTS "model_has_roles";
CREATE TABLE "public"."model_has_roles" (
    "role_id" bigint NOT NULL,
    "model_type" character varying(255) NOT NULL,
    "model_id" bigint NOT NULL,
    CONSTRAINT "model_has_roles_pkey" PRIMARY KEY ("role_id", "model_id", "model_type")
) WITH (oids = false);

CREATE INDEX "model_has_roles_model_id_model_type_index" ON "public"."model_has_roles" USING btree ("model_id", "model_type");

INSERT INTO "model_has_roles" ("role_id", "model_type", "model_id") VALUES
(1,	'App\Domains\Auth\Models\User',	1);

DROP TABLE IF EXISTS "passages";
DROP SEQUENCE IF EXISTS passages_id_seq;
CREATE SEQUENCE passages_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."passages" (
    "id" bigint DEFAULT nextval('passages_id_seq') NOT NULL,
    "period" character varying(255),
    "time_slot" character varying(255),
    "ais" character varying(255),
    "patient_id" bigint,
    "treatment_id" bigint,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "passages_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "passages" ("id", "period", "time_slot", "ais", "patient_id", "treatment_id", "created_at", "updated_at") VALUES
(1,	'matin',	'<20',	'1',	5,	1,	'2022-12-04 17:10:46',	'2022-12-04 17:10:46'),
(2,	'matin',	'<20',	'1',	6,	2,	'2022-12-04 17:14:15',	'2022-12-04 17:14:15'),
(3,	'soir',	'>20',	'1',	6,	2,	'2022-12-04 17:14:15',	'2022-12-04 17:14:15'),
(4,	'matin',	'<20',	'1',	23,	3,	'2022-12-06 13:15:11',	'2022-12-06 13:15:11'),
(5,	'matin',	'<20',	'1',	27,	4,	'2022-12-06 13:31:32',	'2022-12-06 13:31:32');

DROP TABLE IF EXISTS "password_histories";
DROP SEQUENCE IF EXISTS password_histories_id_seq;
CREATE SEQUENCE password_histories_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."password_histories" (
    "id" bigint DEFAULT nextval('password_histories_id_seq') NOT NULL,
    "model_type" character varying(255) NOT NULL,
    "model_id" bigint NOT NULL,
    "password" character varying(255) NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "password_histories_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "password_histories" ("id", "model_type", "model_id", "password", "created_at", "updated_at") VALUES
(1,	'App\Domains\Auth\Models\User',	1,	'$2y$10$Nt8xDzJA0MT3A6Uwvpm5CeFKzFImMMfeBS3/FGplFDR1Iwb0FHgKi',	'2022-11-17 00:49:38',	'2022-11-17 00:49:38'),
(2,	'App\Domains\Auth\Models\User',	2,	'$2y$10$G.GjLW2ruzrY2tTJByWoMe5gt9BS3w/1K1AJvWOZ4SQecbxX8.csG',	'2022-11-21 13:42:12',	'2022-11-21 13:42:12'),
(3,	'App\Domains\Auth\Models\User',	3,	'$2y$10$SrK5t1Arpnw/rp7uZbf49OkkdOYph1x2bIi76cl1uFnzdOMO.OLz6',	'2022-12-02 11:58:00',	'2022-12-02 11:58:00'),
(4,	'App\Domains\Auth\Models\User',	4,	'$2y$10$uQhOzPt4HRSsReX0WsxaGeBpeY165qWfTNKpsKiWNnAgdhpWmpZXu',	'2022-12-02 12:02:27',	'2022-12-02 12:02:27'),
(5,	'App\Domains\Auth\Models\User',	5,	'$2y$10$zoSMgqk1oy8UQrW/8K7JoeQ6UWikBPPRIvTRo9DdgB2QpCL9.czzy',	'2022-12-02 12:07:36',	'2022-12-02 12:07:36'),
(6,	'App\Domains\Auth\Models\User',	6,	'$2y$10$ZbUtmoNdXxMVsZY2iWkBaOC7Gd60YLFU6m5p9UFhUlbpFfq9BNfK2',	'2022-12-02 13:56:55',	'2022-12-02 13:56:55'),
(7,	'App\Domains\Auth\Models\User',	7,	'$2y$10$0Z07XbdmCQwSfRM2XLo8yecknY3dyKLG4Tfs0aPCR/H5KjM8ImWB.',	'2022-12-02 14:55:56',	'2022-12-02 14:55:56'),
(8,	'App\Domains\Auth\Models\User',	8,	'$2y$10$FxiZSDlAA5hu4tEvo3d79O4C6EWcVekq.tgm5WmrdchPu3N8uZOCG',	'2022-12-02 15:00:17',	'2022-12-02 15:00:17'),
(9,	'App\Domains\Auth\Models\User',	9,	'$2y$10$f6FgTI7FjWF.lDWvqw6UtuCplNMDj8/A.Olhq2KQkNbo3ZFr.f9/.',	'2022-12-02 15:15:32',	'2022-12-02 15:15:32'),
(10,	'App\Domains\Auth\Models\User',	10,	'$2y$10$N8SBD4WRKqSj4nXqn7QHbeH59EO5Y7J0xzK5pm8nR1CP2JuONSyXS',	'2022-12-02 16:01:14',	'2022-12-02 16:01:14'),
(11,	'App\Domains\Auth\Models\User',	11,	'$2y$10$C.XdM.gvN6xZZArardRCVeOxEXZN48RLfs0fjZPTi3kA./56s9TJe',	'2022-12-02 16:05:16',	'2022-12-02 16:05:16'),
(12,	'App\Domains\Auth\Models\User',	12,	'$2y$10$YBPqEXtxorjV6F/pwPJG2.go34KouVkxkIzhtjNSggLJiV.DfLgIO',	'2022-12-02 18:32:21',	'2022-12-02 18:32:21'),
(13,	'App\Domains\Auth\Models\User',	13,	'$2y$10$pO24ZwevoQsPs.fNaMBUF.NkCwPxxD0bFkG.hnhYuzqwRhdfWKzvy',	'2022-12-02 18:44:55',	'2022-12-02 18:44:55'),
(14,	'App\Domains\Auth\Models\User',	14,	'$2y$10$fDDnu5geJoK4eHBcUyqKBOi/vL6KkK.cC31ddfB8BL8LEcIgEjzYy',	'2022-12-02 22:49:04',	'2022-12-02 22:49:04'),
(15,	'App\Domains\Auth\Models\User',	15,	'$2y$10$fB3CdHXtFQuQ.k681QaqUOw7mZ1s.JbuwX0zFZP9qMQx2ilfLQSDy',	'2022-12-03 00:13:38',	'2022-12-03 00:13:38'),
(16,	'App\Domains\Auth\Models\User',	16,	'$2y$10$qSKrtvw4ux/MW9ul5EDaAeeWsK1YgzH5S8Kdldq5KB/X5sfcyGOJK',	'2022-12-04 17:06:42',	'2022-12-04 17:06:42'),
(17,	'App\Domains\Auth\Models\User',	17,	'$2y$10$8lX2uykTz7Ziv3ST07G7Oun8v7pydmu/i9gLnPpe3ZtzQPnEHz/na',	'2022-12-05 15:39:30',	'2022-12-05 15:39:30'),
(18,	'App\Domains\Auth\Models\User',	18,	'$2y$10$AbSRLJXslLcWPcN8JFH3xeKfF4.BjOfpJLAxWvOYjQKvJOs5DSqzO',	'2022-12-05 15:39:34',	'2022-12-05 15:39:34'),
(19,	'App\Domains\Auth\Models\User',	19,	'$2y$10$wzwDMPR3PLGRRp4EJyKF8epegEUk3gRqkQK57zA9bO/TDZvB80Uaa',	'2022-12-05 15:47:32',	'2022-12-05 15:47:32'),
(20,	'App\Domains\Auth\Models\User',	20,	'$2y$10$hN.p5fGU8dlmye1l9G0u7.czoQPlFctWk65EB/agWmXnEGbBuNLVu',	'2022-12-05 20:50:51',	'2022-12-05 20:50:51'),
(21,	'App\Domains\Auth\Models\User',	21,	'$2y$10$PPWuumOKE5FPPDSOMdLQ.eju1jtycxnD/lO1c.JITnSaESMURednG',	'2022-12-06 09:23:47',	'2022-12-06 09:23:47'),
(22,	'App\Domains\Auth\Models\User',	22,	'$2y$10$ng9UrK5Pq1vShRz0d2xkoul8OVXM.0As3keFqlH4hqQtTrXdlV1te',	'2022-12-06 09:49:24',	'2022-12-06 09:49:24'),
(23,	'App\Domains\Auth\Models\User',	23,	'$2y$10$YnAmLawndDxfdJ19XcG6uutW9WDVH40JKqjL4DNKwIYW3yPMuZ1S.',	'2022-12-06 11:10:36',	'2022-12-06 11:10:36'),
(24,	'App\Domains\Auth\Models\User',	24,	'$2y$10$xOh8VEuAgAX2uaNlyFYtzuGYGBiOiRnVg5ljXitzQkpN6i8Xai/F.',	'2022-12-06 14:41:14',	'2022-12-06 14:41:14');

DROP TABLE IF EXISTS "password_resets";
CREATE TABLE "public"."password_resets" (
    "email" character varying(255) NOT NULL,
    "token" character varying(255) NOT NULL,
    "created_at" timestamp(0)
) WITH (oids = false);

CREATE INDEX "password_resets_email_index" ON "public"."password_resets" USING btree ("email");


DROP TABLE IF EXISTS "patients";
DROP SEQUENCE IF EXISTS patients_id_seq;
CREATE SEQUENCE patients_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."patients" (
    "id" bigint DEFAULT nextval('patients_id_seq') NOT NULL,
    "type" character varying(255),
    "first_name" character varying(255) NOT NULL,
    "last_name" character varying(255) NOT NULL,
    "email" character varying(255),
    "phone" character varying(255),
    "address" character varying(255),
    "birth_date" date,
    "documents" character varying(255),
    "social_security_number" character varying(255),
    "doctor" character varying(255),
    "doctor_phone" character varying(255),
    "antecedents" json,
    "vital_card" character varying(255),
    "mutual_card" character varying(255),
    "user_id" bigint,
    "nurse_id" bigint,
    "is_replacement" timestamp(0),
    "replaced_at" timestamp(0),
    "avatar_type" character varying(255) DEFAULT 'gravatar' NOT NULL,
    "avatar_location" character varying(255),
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    "zip_code" character varying(255),
    CONSTRAINT "patients_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "patients" ("id", "type", "first_name", "last_name", "email", "phone", "address", "birth_date", "documents", "social_security_number", "doctor", "doctor_phone", "antecedents", "vital_card", "mutual_card", "user_id", "nurse_id", "is_replacement", "replaced_at", "avatar_type", "avatar_location", "created_at", "updated_at", "zip_code") VALUES
(1,	NULL,	'samir',	'ib',	'samir@gmail.com',	'55819384',	'9-11 Pl. du Colonel Fabien, 75010',	'1994-11-25',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	10,	NULL,	NULL,	NULL,	'gravatar',	NULL,	'2022-12-02 16:01:14',	'2022-12-02 16:01:14',	'75010'),
(2,	'type',	'qsd',	'kk',	'dd@gmail.o',	'qsdqsd',	NULL,	'2022-12-14',	NULL,	NULL,	'qsd',	NULL,	'{"diabetic":true,"immunocompromised":false,"palliative_care":false}',	NULL,	NULL,	NULL,	5,	NULL,	NULL,	'gravatar',	NULL,	'2022-12-02 17:21:09',	'2022-12-02 17:21:09',	'98890'),
(3,	NULL,	'jean',	'philips',	'jean@gmail.com',	'4566785',	'rue jean 5',	'1984-10-02',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	12,	NULL,	NULL,	NULL,	'gravatar',	NULL,	'2022-12-02 18:32:21',	'2022-12-02 18:32:21',	'98885'),
(4,	NULL,	'dali',	'dali',	'dali@gmail.com',	'53543535',	'fdfsdfsdf',	'2011-12-15',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	15,	NULL,	NULL,	NULL,	'gravatar',	NULL,	'2022-12-03 00:13:38',	'2022-12-03 00:13:38',	'98890'),
(5,	'type',	'test',	'patient',	'azert.a@gmail.com',	'00000000',	NULL,	'1997-12-09',	NULL,	NULL,	NULL,	NULL,	'{"diabetic":false,"immunocompromised":false,"palliative_care":false}',	NULL,	NULL,	NULL,	16,	NULL,	NULL,	'gravatar',	NULL,	'2022-12-04 17:09:33',	'2022-12-04 17:09:33',	'13001'),
(6,	'type',	'pat',	'ient',	'pt.ient@idelib.com',	'0631214151',	NULL,	'1925-12-17',	NULL,	NULL,	NULL,	NULL,	'{"diabetic":true,"immunocompromised":false,"palliative_care":false}',	NULL,	NULL,	NULL,	16,	NULL,	NULL,	'gravatar',	NULL,	'2022-12-04 17:13:24',	'2022-12-04 17:13:24',	'13002'),
(7,	'covid',	'test',	'tes',	're@gmail.com',	'2563565',	'rue fi',	'2008-10-01',	NULL,	NULL,	NULL,	NULL,	NULL,	'5555555555',	NULL,	NULL,	4,	NULL,	NULL,	'gravatar',	NULL,	'2022-12-05 12:14:27',	'2022-12-05 12:14:27',	'8979'),
(8,	'covid',	'Mohamed',	'bouzid',	'tf@gmail.com',	'5366652',	'rue med 5',	'1900-01-01',	NULL,	NULL,	NULL,	NULL,	NULL,	'457657986',	NULL,	NULL,	4,	NULL,	NULL,	'gravatar',	NULL,	'2022-12-05 12:41:19',	'2022-12-05 15:03:25',	NULL),
(10,	'type',	'hgdh',	'gdfgf',	'6@gmail.com',	'5345455',	'GDFGFG',	'2022-12-09',	NULL,	NULL,	'hhhhh',	NULL,	'{"diabetic":false,"immunocompromised":false,"palliative_care":false}',	'4543545',	'656546',	NULL,	7,	NULL,	NULL,	'gravatar',	NULL,	'2022-12-05 15:36:59',	'2022-12-05 15:36:59',	'98890'),
(11,	NULL,	'gfdgdf',	'gfdgdfg',	'7@gmail.com',	'555555555',	'fggg',	'2022-12-03',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	18,	NULL,	NULL,	NULL,	'gravatar',	NULL,	'2022-12-05 15:39:34',	'2022-12-05 15:39:34',	'98890'),
(12,	NULL,	'gfdgdf',	'gfdgdfg',	'74@gmail.com',	'555555555444',	'fggg',	'2022-12-03',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	19,	NULL,	NULL,	NULL,	'gravatar',	NULL,	'2022-12-05 15:47:32',	'2022-12-05 15:47:32',	'98890'),
(13,	'type',	'test',	'test',	'ok@gmail.com',	'56788',	'rue',	'2022-12-07',	NULL,	NULL,	NULL,	NULL,	'{"diabetic":false,"immunocompromised":false,"palliative_care":false}',	NULL,	NULL,	NULL,	4,	NULL,	NULL,	'gravatar',	NULL,	'2022-12-05 16:27:13',	'2022-12-05 16:27:13',	'98890'),
(14,	'covid',	'Imen',	'Hasseni',	'imen@gmail.com',	'5467858',	'rue 5',	'1900-01-01',	NULL,	NULL,	NULL,	NULL,	NULL,	'9978 013 386',	NULL,	NULL,	4,	NULL,	NULL,	'gravatar',	NULL,	'2022-12-05 16:35:19',	'2022-12-05 16:42:10',	NULL),
(15,	'covid',	'zeineb',	'Hasseni',	'zizou@gmaiil.com',	'564675',	'rue Yoyo',	'1900-01-01',	NULL,	NULL,	NULL,	NULL,	NULL,	'3456264',	NULL,	NULL,	4,	NULL,	NULL,	'gravatar',	NULL,	'2022-12-05 16:41:18',	'2022-12-05 16:43:06',	'0'),
(16,	'type',	'sdfsdf',	'qsdqsd',	'dsfzer@dd.dd',	'123123',	'SDFSDFSdf',	'2022-12-15',	NULL,	NULL,	NULL,	NULL,	'{"diabetic":false,"immunocompromised":false,"palliative_care":true}',	'234234234',	'234234234',	NULL,	5,	NULL,	NULL,	'gravatar',	NULL,	'2022-12-05 16:53:25',	'2022-12-05 16:53:25',	'98890,98884'),
(19,	NULL,	'reter',	'gfgfd',	's@gmail',	'222222222',	'fsfdf',	'2022-12-03',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	20,	NULL,	NULL,	NULL,	'gravatar',	NULL,	'2022-12-05 20:50:51',	'2022-12-05 20:50:51',	'98890'),
(20,	'covid',	'sdfsdf',	'fsdf',	'gg@s.com',	'sdfsdf',	'qsdqsd',	'2022-12-02',	NULL,	NULL,	NULL,	NULL,	NULL,	'Une err umérisation',	NULL,	NULL,	5,	NULL,	NULL,	'gravatar',	NULL,	'2022-12-05 21:07:54',	'2022-12-05 21:07:54',	NULL),
(21,	'covid',	'sdfsdf',	'fsdf',	'gg@s.comd',	'sdfsdf',	'qsdqsd',	'2022-12-02',	NULL,	NULL,	NULL,	NULL,	NULL,	'Une err umérisation',	NULL,	NULL,	5,	NULL,	NULL,	'gravatar',	NULL,	'2022-12-05 21:08:12',	'2022-12-05 21:08:12',	NULL),
(9,	'type',	'bou',	'ahmed',	'tg@gmail.com',	'253625',	'rir',	'1900-01-01',	NULL,	NULL,	NULL,	NULL,	'{"diabetic":false,"immunocompromised":false,"palliative_care":false}',	NULL,	NULL,	NULL,	4,	NULL,	NULL,	'storage',	'avatars/uTXh7mO8PLRkNcxjQUzDAsfLTr35pRK1slB46F4R.jpg',	'2022-12-05 13:00:35',	'2022-12-06 09:07:31',	'98889'),
(22,	'covid',	'treza',	'azerty',	'azerty@gmail.com',	'0631465623',	'45 adresse',	'1993-12-15',	NULL,	NULL,	NULL,	NULL,	NULL,	'123456789',	NULL,	NULL,	16,	NULL,	NULL,	'gravatar',	NULL,	'2022-12-06 09:33:36',	'2022-12-06 09:33:36',	NULL),
(23,	'type',	'yvette',	'dubois',	'yvett.d@idelib.com',	'0612364568',	'87 rue sainte',	'1940-08-15',	NULL,	NULL,	NULL,	NULL,	'{"diabetic":false,"immunocompromised":false,"palliative_care":false}',	NULL,	NULL,	NULL,	16,	NULL,	NULL,	'gravatar',	NULL,	'2022-12-06 09:45:48',	'2022-12-06 09:45:48',	'13001'),
(24,	'chronic',	'Richard',	'Good',	'xekecix@mailinator.com',	'+1 (985) 584-4053',	NULL,	'1992-03-28',	NULL,	'689',	'Laborum perferendis enim quia veritatis ullamco facilis',	'+1 (131) 973-9611',	NULL,	NULL,	NULL,	NULL,	5,	NULL,	NULL,	'gravatar',	NULL,	'2022-12-06 12:06:38',	'2022-12-06 12:06:38',	NULL),
(25,	'chronic',	'lili',	'Test',	'lili@mail.com',	'258963224',	'21 rue gthyju',	'1999-05-22',	NULL,	NULL,	'azertty',	NULL,	'{"diabetic":true,"immunocompromised":true,"palliative_care":false}',	'25808552',	'25508852',	NULL,	5,	NULL,	NULL,	'gravatar',	NULL,	'2022-12-06 13:20:15',	'2022-12-06 13:20:15',	'13002'),
(26,	'covid',	'lili',	'Test',	'lili1@mail.com',	'258963224',	'21 rue gthyju',	'1999-05-22',	NULL,	NULL,	'azertty',	NULL,	'{"diabetic":true,"immunocompromised":true,"palliative_care":false}',	'25808552',	'25508852',	NULL,	5,	NULL,	NULL,	'gravatar',	NULL,	'2022-12-06 13:28:02',	'2022-12-06 13:28:02',	'13002'),
(27,	'type',	'thomas',	'patient',	'test@idelib.com',	'00121341',	'45 rue sainte',	'1980-02-13',	NULL,	NULL,	NULL,	NULL,	'{"diabetic":false,"immunocompromised":false,"palliative_care":false}',	'190 09 13 055 121 6161',	'1245685433355',	NULL,	16,	NULL,	NULL,	'gravatar',	NULL,	'2022-12-06 13:30:32',	'2022-12-06 13:30:32',	'13001'),
(17,	'covid',	'Chaker',	'Hasseni',	'chaker@gmail.com',	'456789',	'rue',	'2022-12-20',	NULL,	NULL,	NULL,	NULL,	NULL,	'34567890',	NULL,	NULL,	4,	NULL,	NULL,	'gravatar',	NULL,	'2022-12-05 16:55:21',	'2022-12-07 09:50:29',	NULL),
(18,	'covid',	'Ahmed',	'Bouzgarrouu',	'ahmed@gmail.com',	'45678',	'rue',	'1900-01-01',	NULL,	NULL,	NULL,	NULL,	NULL,	'346756',	NULL,	2,	4,	NULL,	NULL,	'gravatar',	NULL,	'2022-12-05 16:58:31',	'2022-12-07 09:51:17',	NULL);

DROP TABLE IF EXISTS "permissions";
DROP SEQUENCE IF EXISTS permissions_id_seq;
CREATE SEQUENCE permissions_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."permissions" (
    "id" bigint DEFAULT nextval('permissions_id_seq') NOT NULL,
    "type" character varying(255) NOT NULL,
    "guard_name" character varying(255) NOT NULL,
    "name" character varying(255) NOT NULL,
    "description" character varying(255),
    "parent_id" bigint,
    "sort" smallint DEFAULT '1' NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "permissions_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "permissions" ("id", "type", "guard_name", "name", "description", "parent_id", "sort", "created_at", "updated_at") VALUES
(1,	'super admin',	'web',	'admin.access.user',	'All User Permissions',	NULL,	1,	'2022-11-17 00:49:38',	'2022-11-17 00:49:38'),
(2,	'super admin',	'web',	'admin.access.user.list',	'View Users',	1,	1,	'2022-11-17 00:49:38',	'2022-11-17 00:49:38'),
(3,	'super admin',	'web',	'admin.access.user.deactivate',	'Deactivate Users',	1,	2,	'2022-11-17 00:49:38',	'2022-11-17 00:49:38'),
(4,	'super admin',	'web',	'admin.access.user.reactivate',	'Reactivate Users',	1,	3,	'2022-11-17 00:49:38',	'2022-11-17 00:49:38'),
(5,	'super admin',	'web',	'admin.access.user.clear-session',	'Clear User Sessions',	1,	4,	'2022-11-17 00:49:38',	'2022-11-17 00:49:38'),
(6,	'super admin',	'web',	'admin.access.user.impersonate',	'Impersonate Users',	1,	5,	'2022-11-17 00:49:38',	'2022-11-17 00:49:38'),
(7,	'super admin',	'web',	'admin.access.user.change-password',	'Change User Passwords',	1,	6,	'2022-11-17 00:49:38',	'2022-11-17 00:49:38');

DROP TABLE IF EXISTS "personal_access_tokens";
DROP SEQUENCE IF EXISTS personal_access_tokens_id_seq;
CREATE SEQUENCE personal_access_tokens_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."personal_access_tokens" (
    "id" bigint DEFAULT nextval('personal_access_tokens_id_seq') NOT NULL,
    "tokenable_type" character varying(255) NOT NULL,
    "tokenable_id" bigint NOT NULL,
    "name" character varying(255) NOT NULL,
    "token" character varying(64) NOT NULL,
    "abilities" text,
    "last_used_at" timestamp(0),
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "personal_access_tokens_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "personal_access_tokens_token_unique" UNIQUE ("token")
) WITH (oids = false);

CREATE INDEX "personal_access_tokens_tokenable_type_tokenable_id_index" ON "public"."personal_access_tokens" USING btree ("tokenable_type", "tokenable_id");


DROP TABLE IF EXISTS "plans";
DROP SEQUENCE IF EXISTS plans_id_seq;
CREATE SEQUENCE plans_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."plans" (
    "id" bigint DEFAULT nextval('plans_id_seq') NOT NULL,
    "name" character varying(255) NOT NULL,
    "display_price" double precision NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    "product_name" character varying(255),
    "interval_count" integer,
    "apple_id" character varying(255),
    "google_id" character varying(255),
    "display_price_google" double precision,
    "display_price_apple" double precision,
    CONSTRAINT "plans_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "ratings";
DROP SEQUENCE IF EXISTS ratings_id_seq;
CREATE SEQUENCE ratings_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."ratings" (
    "id" bigint DEFAULT nextval('ratings_id_seq') NOT NULL,
    "rating" double precision NOT NULL,
    "nurse_id" bigint NOT NULL,
    "user_id" bigint NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "ratings_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "role_has_permissions";
CREATE TABLE "public"."role_has_permissions" (
    "permission_id" bigint NOT NULL,
    "role_id" bigint NOT NULL,
    CONSTRAINT "role_has_permissions_pkey" PRIMARY KEY ("permission_id", "role_id")
) WITH (oids = false);


DROP TABLE IF EXISTS "roles";
DROP SEQUENCE IF EXISTS roles_id_seq;
CREATE SEQUENCE roles_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."roles" (
    "id" bigint DEFAULT nextval('roles_id_seq') NOT NULL,
    "type" character varying(255) NOT NULL,
    "name" character varying(255) NOT NULL,
    "guard_name" character varying(255) NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "roles_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "roles" ("id", "type", "name", "guard_name", "created_at", "updated_at") VALUES
(1,	'super admin',	'Administrator',	'web',	'2022-11-17 00:49:38',	'2022-11-17 00:49:38');

DROP TABLE IF EXISTS "team_user";
DROP SEQUENCE IF EXISTS team_user_id_seq;
CREATE SEQUENCE team_user_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."team_user" (
    "id" bigint DEFAULT nextval('team_user_id_seq') NOT NULL,
    "color" character varying(255),
    "is_admin" boolean DEFAULT false,
    "user_id" bigint NOT NULL,
    "team_id" bigint NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "team_user_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "team_user" ("id", "color", "is_admin", "user_id", "team_id", "created_at", "updated_at") VALUES
(1,	'#000',	'1',	4,	1,	'2022-12-02 12:48:52',	'2022-12-02 12:48:52'),
(4,	NULL,	'0',	9,	1,	'2022-12-02 15:18:15',	'2022-12-02 15:18:15'),
(5,	'#000',	'1',	16,	3,	'2022-12-04 17:07:32',	'2022-12-04 17:07:32'),
(6,	NULL,	'0',	17,	1,	'2022-12-05 15:45:53',	'2022-12-05 15:45:53'),
(7,	NULL,	'0',	21,	3,	'2022-12-06 09:50:31',	'2022-12-06 09:50:31'),
(9,	'#000',	'1',	5,	5,	'2022-12-06 12:11:31',	'2022-12-06 12:11:31'),
(10,	'#000',	'1',	3,	6,	'2022-12-06 13:46:09',	'2022-12-06 13:46:09'),
(11,	'#000',	'1',	24,	7,	'2022-12-06 14:47:24',	'2022-12-06 14:47:24'),
(12,	NULL,	'0',	3,	7,	'2022-12-06 15:02:13',	'2022-12-06 15:02:13'),
(13,	NULL,	'0',	7,	6,	'2022-12-06 15:06:21',	'2022-12-06 15:06:21');

DROP TABLE IF EXISTS "teams";
DROP SEQUENCE IF EXISTS teams_id_seq;
CREATE SEQUENCE teams_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."teams" (
    "id" bigint DEFAULT nextval('teams_id_seq') NOT NULL,
    "name" character varying(255),
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    "admin_id" bigint NOT NULL,
    CONSTRAINT "teams_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "teams" ("id", "name", "created_at", "updated_at", "admin_id") VALUES
(1,	'best nurse',	'2022-12-02 12:48:52',	'2022-12-02 12:48:52',	4),
(2,	'azerty',	'2022-12-02 13:51:26',	'2022-12-02 13:51:26',	5),
(3,	'napoleon',	'2022-12-04 17:07:32',	'2022-12-04 17:07:32',	16),
(4,	'qsdqsd',	'2022-12-06 12:03:07',	'2022-12-06 12:03:07',	5),
(5,	'aaa bb',	'2022-12-06 12:11:31',	'2022-12-06 12:11:31',	5),
(7,	'equipe',	'2022-12-06 14:47:24',	'2022-12-06 14:47:24',	24),
(6,	'équipe saturée',	'2022-12-06 13:46:09',	'2022-12-07 10:19:54',	3);

DROP TABLE IF EXISTS "trackings";
DROP SEQUENCE IF EXISTS trackings_id_seq;
CREATE SEQUENCE trackings_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."trackings" (
    "id" bigint DEFAULT nextval('trackings_id_seq') NOT NULL,
    "prescription_url" character varying(255) NOT NULL,
    "pickup_date" date,
    "duration" integer,
    "duration_unit" character varying(255),
    "daily_frequency_period" character varying(255),
    "daily_frequency_time" character varying(255),
    "absences" smallint,
    "patient_id" bigint NOT NULL,
    "user_id" bigint NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "trackings_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "treatment_requests";
DROP SEQUENCE IF EXISTS treatment_requests_id_seq;
CREATE SEQUENCE treatment_requests_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."treatment_requests" (
    "id" bigint DEFAULT nextval('treatment_requests_id_seq') NOT NULL,
    "patient_id" bigint NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    "tour_sector" character varying(255),
    "is_pending" boolean,
    "nurse_id" bigint,
    "is_available" boolean,
    "note" text,
    CONSTRAINT "treatment_requests_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "treatment_requests" ("id", "patient_id", "created_at", "updated_at", "tour_sector", "is_pending", "nurse_id", "is_available", "note") VALUES
(1,	3,	'2022-12-02 18:35:41',	'2022-12-02 18:35:41',	'98885',	'1',	NULL,	'1',	'hello'),
(2,	3,	'2022-12-02 18:35:55',	'2022-12-02 18:35:55',	'98885',	'1',	NULL,	'1',	'je suis malade'),
(3,	4,	'2022-12-03 00:47:40',	'2022-12-03 00:47:40',	'98890',	'1',	NULL,	'1',	'fsddf');

DROP TABLE IF EXISTS "treatments";
DROP SEQUENCE IF EXISTS treatments_id_seq;
CREATE SEQUENCE treatments_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."treatments" (
    "id" bigint DEFAULT nextval('treatments_id_seq') NOT NULL,
    "start_at" date,
    "end_at" date,
    "period" character varying(255),
    "daily_frequency" character varying(255),
    "monthly_frequency" character varying(255),
    "type" character varying(255),
    "classic_type" character varying(255),
    "bsi_type" character varying(255),
    "treatment" character varying(255),
    "stoped_at" timestamp(0),
    "absent_from" date,
    "absent_to" date,
    "patient_id" bigint,
    "nurse_id" bigint,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    "prescription" character varying(255),
    CONSTRAINT "treatments_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "treatments" ("id", "start_at", "end_at", "period", "daily_frequency", "monthly_frequency", "type", "classic_type", "bsi_type", "treatment", "stoped_at", "absent_from", "absent_to", "patient_id", "nurse_id", "created_at", "updated_at", "prescription") VALUES
(1,	'2022-12-05',	'2022-12-05',	'12mois',	'1',	'J',	'Classique',	'chronique',	NULL,	'Pansement Lourd',	NULL,	NULL,	NULL,	5,	16,	'2022-12-04 17:10:46',	'2022-12-04 17:10:46',	NULL),
(2,	'2022-12-01',	'2022-12-01',	'12mois',	'2',	'L, Ma, Me, J, V, S, D',	'bsi-90',	NULL,	NULL,	'Soins éffectués',	NULL,	NULL,	NULL,	6,	16,	'2022-12-04 17:14:15',	'2022-12-04 17:14:15',	NULL),
(3,	'2022-12-06',	'2022-12-06',	'12mois',	'1',	'L, Ma, Me, J, V, S, D',	'bsi-85',	NULL,	NULL,	'Soins éffectués',	NULL,	NULL,	NULL,	23,	16,	'2022-12-06 13:15:11',	'2022-12-06 13:15:11',	NULL),
(4,	'2022-12-07',	'2022-12-07',	'12mois',	'1',	'L, Ma, Me, J, V, S, D',	'Classique',	'chronique',	NULL,	'Pansement Lourd',	NULL,	NULL,	NULL,	27,	16,	'2022-12-06 13:31:32',	'2022-12-06 13:31:32',	NULL);

DROP TABLE IF EXISTS "two_factor_authentications";
DROP SEQUENCE IF EXISTS two_factor_authentications_id_seq;
CREATE SEQUENCE two_factor_authentications_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."two_factor_authentications" (
    "id" bigint DEFAULT nextval('two_factor_authentications_id_seq') NOT NULL,
    "authenticatable_type" character varying(255) NOT NULL,
    "authenticatable_id" bigint NOT NULL,
    "shared_secret" text NOT NULL,
    "enabled_at" timestamptz(0),
    "label" character varying(255) NOT NULL,
    "digits" smallint DEFAULT '6' NOT NULL,
    "seconds" smallint DEFAULT '30' NOT NULL,
    "window" smallint DEFAULT '0' NOT NULL,
    "algorithm" character varying(16) DEFAULT 'sha1' NOT NULL,
    "recovery_codes" text,
    "recovery_codes_generated_at" timestamptz(0),
    "safe_devices" json,
    "created_at" timestamptz(0),
    "updated_at" timestamptz(0),
    CONSTRAINT "two_factor_authentications_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

CREATE INDEX "2fa_auth_type_auth_id_index" ON "public"."two_factor_authentications" USING btree ("authenticatable_type", "authenticatable_id");


DROP TABLE IF EXISTS "users";
DROP SEQUENCE IF EXISTS users_id_seq;
CREATE SEQUENCE users_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."users" (
    "id" bigint DEFAULT nextval('users_id_seq') NOT NULL,
    "type" character varying(255) DEFAULT 'admin' NOT NULL,
    "first_name" character varying(255) NOT NULL,
    "last_name" character varying(255) NOT NULL,
    "email" character varying(255),
    "email_verified_at" timestamp(0),
    "password" character varying(255),
    "password_changed_at" timestamp(0),
    "api_token" character varying(255),
    "phone" character varying(255),
    "app_user_id" character varying(255),
    "adeli_number" character varying(255),
    "address" character varying(255),
    "birth_date" date,
    "tour_sector" character varying(255),
    "presentation" character varying(255),
    "subscription_type" character varying(255),
    "operating_system" character varying(255),
    "avatar_type" character varying(255) DEFAULT 'gravatar' NOT NULL,
    "avatar_location" character varying(255),
    "active" smallint DEFAULT '1' NOT NULL,
    "timezone" character varying(255),
    "last_login_at" timestamp(0),
    "last_login_ip" character varying(255),
    "to_be_logged_out" boolean DEFAULT false NOT NULL,
    "confirmation_code" character varying(255),
    "is_used" boolean DEFAULT false,
    "remember_token" character varying(100),
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    "deleted_at" timestamp(0),
    "created_by" bigint,
    "fcm_token" text,
    "color" character varying(255),
    "rpps_number" character varying(255),
    CONSTRAINT "users_app_user_id_unique" UNIQUE ("app_user_id"),
    CONSTRAINT "users_email_unique" UNIQUE ("email"),
    CONSTRAINT "users_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "users" ("id", "type", "first_name", "last_name", "email", "email_verified_at", "password", "password_changed_at", "api_token", "phone", "app_user_id", "adeli_number", "address", "birth_date", "tour_sector", "presentation", "subscription_type", "operating_system", "avatar_type", "avatar_location", "active", "timezone", "last_login_at", "last_login_ip", "to_be_logged_out", "confirmation_code", "is_used", "remember_token", "created_at", "updated_at", "deleted_at", "created_by", "fcm_token", "color", "rpps_number") VALUES
(23,	'nurse',	'dev',	'emilie',	'e.devrieux@idelib.com',	'2022-12-06 11:10:36',	'$2y$10$YnAmLawndDxfdJ19XcG6uutW9WDVH40JKqjL4DNKwIYW3yPMuZ1S.',	NULL,	'AEbdVnC0pF50S9AQJioOVRXKJOr47UPSBBB1Rz6dA4gShAU3K71Q6m0wTTe8ABldpXc3TVFlojYzX8Zc',	'00000000',	'RCd24766af',	'754313288',	'45 rue sainte',	'1996-05-06',	'13001,13006,13002',	NULL,	NULL,	NULL,	'gravatar',	NULL,	1,	NULL,	NULL,	NULL,	'0',	NULL,	'0',	NULL,	'2022-12-06 11:10:36',	'2022-12-06 11:10:36',	NULL,	NULL,	NULL,	NULL,	'75432781005'),
(11,	'substitute',	'amira',	'test22',	'sub@gmail.com',	'2022-12-02 16:05:16',	'$2y$10$C.XdM.gvN6xZZArardRCVeOxEXZN48RLfs0fjZPTi3kA./56s9TJe',	NULL,	'hEoCOuc7HwFrrphMgWXH3fSQKYOKTFzqMHO7vkX7aO1wzO3KAv8awX9P71H9n9UoZ7WfiNZaeVl6S0ZW',	'55819384',	'RC3cdeb4e3',	NULL,	'etestst',	'1993-10-05',	NULL,	NULL,	NULL,	NULL,	'gravatar',	NULL,	1,	NULL,	NULL,	NULL,	'0',	NULL,	'0',	NULL,	'2022-12-02 16:05:16',	'2022-12-06 12:20:47',	NULL,	NULL,	NULL,	NULL,	NULL),
(18,	'patient',	'gfdgdf',	'gfdgdfg',	'7@gmail.com',	'2022-12-05 15:39:34',	'$2y$10$AbSRLJXslLcWPcN8JFH3xeKfF4.BjOfpJLAxWvOYjQKvJOs5DSqzO',	NULL,	'o7IVDmSMVKvJakFEDjH5BgJsnVlX1pkhTCrHGrOI4CmkHX1wcKIqtHXEoU7IZNomocJ0g7WX3VTANSZl',	'555555555',	'RCc446984d',	NULL,	'fggg',	'2022-12-03',	NULL,	NULL,	NULL,	NULL,	'gravatar',	NULL,	1,	NULL,	NULL,	NULL,	'0',	NULL,	'0',	NULL,	'2022-12-05 15:39:34',	'2022-12-05 15:39:34',	NULL,	NULL,	NULL,	NULL,	NULL),
(14,	'nurse',	'hxhdbdhhh',	'hdhdj',	'hhh@gmail.com',	'2022-12-02 22:49:04',	'$2y$10$fDDnu5geJoK4eHBcUyqKBOi/vL6KkK.cC31ddfB8BL8LEcIgEjzYy',	NULL,	'nN5Tz7Vi2DiQYrY9e7qQDk18dW3R9oC0ASuotn77NS4BED3mXUF80oGJdO7K7Y2oVwikY7irrGrsl7Sy',	'6464545',	'RCc24adc0c',	NULL,	'gzhzgzv',	'2022-11-17',	'98889,98884',	NULL,	NULL,	NULL,	'gravatar',	NULL,	1,	NULL,	NULL,	NULL,	'0',	NULL,	'0',	NULL,	'2022-12-02 22:49:04',	'2022-12-02 23:45:53',	NULL,	NULL,	NULL,	NULL,	'65656979797'),
(8,	'nurse',	'fdsgdfg',	'fgsgd',	'nurse@gmail.com',	'2022-12-02 15:00:17',	'$2y$10$FxiZSDlAA5hu4tEvo3d79O4C6EWcVekq.tgm5WmrdchPu3N8uZOCG',	NULL,	'4wfIewYFwmaF3rmwPEX7LNJgh8qPa12iufffwLWrL6mwlMeByexmUIPCGeHzjmzDKbv2no6hi9n74luO',	'44543535345',	'RCb90a3999',	'523423432',	'gfsgfgfg',	'2008-12-12',	'98890',	NULL,	NULL,	NULL,	'gravatar',	NULL,	1,	NULL,	NULL,	NULL,	'0',	NULL,	'0',	NULL,	'2022-12-02 15:00:17',	'2022-12-02 15:00:17',	NULL,	NULL,	NULL,	NULL,	'43242423423'),
(12,	'patient',	'jean',	'philips',	'jean@gmail.com',	'2022-12-02 18:32:21',	'$2y$10$YBPqEXtxorjV6F/pwPJG2.go34KouVkxkIzhtjNSggLJiV.DfLgIO',	NULL,	'GpkIzDOisMYx6qhvtHDQqz5JW5m90bfwWcMfRTIwT1W8EbU8metMzswmFPiSXPA1hB7nwbWWRCYl60uA',	'4566785',	'RC420d9494',	NULL,	'rue jean 5',	'1984-10-02',	NULL,	NULL,	NULL,	NULL,	'gravatar',	NULL,	1,	NULL,	NULL,	NULL,	'0',	NULL,	'0',	NULL,	'2022-12-02 18:32:21',	'2022-12-02 18:32:35',	NULL,	NULL,	NULL,	NULL,	NULL),
(22,	'nurse',	'prud',	'thom',	't.prudent@napoleonbusinessdevelopment.fr',	'2022-12-06 09:49:24',	'$2y$10$ng9UrK5Pq1vShRz0d2xkoul8OVXM.0As3keFqlH4hqQtTrXdlV1te',	NULL,	'DU14bATzQIMgA1EWK6nPZdjKvvrUfwLPlFrCZOaq7sAmV9cZkaMrzIdF8V77TqoruymioPji5HTuWhVz',	'0665121341',	'RC4da28658',	NULL,	'adresse 123',	'1975-12-10',	'13001,13009',	NULL,	NULL,	NULL,	'gravatar',	NULL,	1,	NULL,	NULL,	NULL,	'0',	NULL,	'0',	NULL,	'2022-12-06 09:49:24',	'2022-12-06 09:49:24',	NULL,	NULL,	NULL,	NULL,	NULL),
(16,	'nurse',	'prudent',	'thomas',	'thomas.prudent13@gmail.com',	'2022-12-04 17:06:41',	'$2y$10$qSKrtvw4ux/MW9ul5EDaAeeWsK1YgzH5S8Kdldq5KB/X5sfcyGOJK',	NULL,	'PSw0f759UfZTwXFbLDOGp80dWu8wuaUSwXXSv3J5gT2tSdKTYtaNKenmHH1d3VuMopnlIcBqm3IZdVEf',	'0665317151',	'RC63c5e68c',	'123456789',	'45 rues sainte',	'1985-12-10',	'13001',	NULL,	NULL,	NULL,	'storage',	'avatars/qJMPXfJkWNwryqZSvvE9Op1mh00K8OSY15XbJrGj.jpg',	1,	'Europe/Paris',	'2022-12-07 10:32:55',	'86.211.158.52',	'0',	NULL,	'0',	NULL,	'2022-12-04 17:06:42',	'2022-12-07 10:32:55',	NULL,	NULL,	NULL,	NULL,	'12345678910'),
(13,	'substitute',	'test',	'test',	'test@gmail.com',	'2022-12-02 18:44:55',	'$2y$10$pO24ZwevoQsPs.fNaMBUF.NkCwPxxD0bFkG.hnhYuzqwRhdfWKzvy',	NULL,	'AcR93v3PCujicSPxRWGfHpWo4JzQa7xZUaLPZJp2T1djHo2qnHWTYgkyBHgp6rCZV041nDYA8iIKlXaW',	'45667457',	'RC88fa2ad0',	NULL,	'rue test',	'1900-01-01',	NULL,	NULL,	NULL,	NULL,	'gravatar',	NULL,	1,	NULL,	NULL,	NULL,	'0',	NULL,	'0',	NULL,	'2022-12-02 18:44:55',	'2022-12-02 18:44:55',	NULL,	NULL,	NULL,	NULL,	NULL),
(10,	'patient',	'samir',	'ib',	'samir@gmail.com',	'2022-12-02 16:01:14',	'$2y$10$N8SBD4WRKqSj4nXqn7QHbeH59EO5Y7J0xzK5pm8nR1CP2JuONSyXS',	NULL,	'1ivZIXAMnCp9OAOgodye3V80dz9PDya6JvilrXPGbVLSVWSVXKVxu7fp3B74BVFHTR3mNnTaLkq7VgbW',	'55819384',	'RCe5f1b47e',	NULL,	'9-11 Pl. du Colonel Fabien, 75010',	'1994-11-25',	NULL,	NULL,	NULL,	NULL,	'gravatar',	NULL,	1,	NULL,	NULL,	NULL,	'0',	NULL,	'0',	NULL,	'2022-12-02 16:01:14',	'2022-12-02 16:01:29',	NULL,	NULL,	NULL,	NULL,	NULL),
(2,	'nurse',	'marwa',	'test',	'ahmed@gmail.com',	'2022-11-21 13:42:12',	'$2y$10$G.GjLW2ruzrY2tTJByWoMe5gt9BS3w/1K1AJvWOZ4SQecbxX8.csG',	NULL,	'PQQ0yvqLGrvwiVzaE2u5stFLoBM8ckUZGhmTQRdpKw7s9kYTr3ELNkXqZe6Gcgpr6697ElH1Jvdfrd9Y',	'55819384',	'RCfa0041b7',	'0000',	'etestst',	'1994-01-01',	'13001,13002',	'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nostrum ut impedit animi a in vero asperiores rem sapiente? Vel incidunt assumenda, voluptatibus ullam consectetur minima rerum facilis iusto adipisci harum.',	NULL,	NULL,	'gravatar',	NULL,	1,	NULL,	NULL,	NULL,	'0',	NULL,	'0',	NULL,	'2022-11-21 13:42:12',	'2022-11-21 13:42:12',	NULL,	NULL,	NULL,	NULL,	NULL),
(6,	'nurse',	'marwa',	'test',	'damin@gmail.com',	'2022-12-02 13:56:55',	'$2y$10$ZbUtmoNdXxMVsZY2iWkBaOC7Gd60YLFU6m5p9UFhUlbpFfq9BNfK2',	NULL,	'D0MmxtbahuxAVRcx6RMDD0hBZziTet44t5lBbQzCpxD4RS9roethHWc7v8iUFIYKT9FEpxw5vAdI0vz1',	'55819384',	'RCf1265629',	'0000',	'etestst',	'1994-01-01',	'13001,13002',	'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nostrum ut impedit animi a in vero asperiores rem sapiente? Vel incidunt assumenda, voluptatibus ullam consectetur minima rerum facilis iusto adipisci harum.',	NULL,	NULL,	'gravatar',	NULL,	1,	NULL,	NULL,	NULL,	'0',	NULL,	'0',	NULL,	'2022-12-02 13:56:55',	'2022-12-02 16:54:46',	NULL,	NULL,	NULL,	NULL,	NULL),
(3,	'nurse',	'majdeddine',	'fazaa',	'fazaa@gmail.com',	'2022-12-02 11:58:00',	'$2y$10$SrK5t1Arpnw/rp7uZbf49OkkdOYph1x2bIi76cl1uFnzdOMO.OLz6',	NULL,	'G3qqBqrrZ80QyphPzBjpgtNLA6Elru4L1CdAP45Rf0HcjYdTFcbD0dcAbKVZd2ouA8bkKmKFVbXIXiUB',	'12345678',	'RC96a85500',	'111111111',	'rue',	'1900-01-01',	'98890',	'hajaj',	NULL,	NULL,	'gravatar',	NULL,	1,	NULL,	NULL,	NULL,	'0',	NULL,	'0',	NULL,	'2022-12-02 11:58:00',	'2022-12-07 08:05:01',	NULL,	NULL,	NULL,	'#ff9800',	'11111111111'),
(9,	'substitute',	'sarra',	'joé',	'sarra@gmail.com',	'2022-12-02 15:15:32',	'$2y$10$f6FgTI7FjWF.lDWvqw6UtuCplNMDj8/A.Olhq2KQkNbo3ZFr.f9/.',	NULL,	'hhipe3COTHSVSRetn5nZFzFDGsl4x9WSUva6q1nOa0LFZnzqYRXWGv8LMI0W1jpl12s4tfRQPEuyYaVm',	'4565677',	'RCb2640c55',	NULL,	'rue 10 med',	'1999-10-05',	NULL,	NULL,	NULL,	NULL,	'gravatar',	NULL,	1,	NULL,	NULL,	NULL,	'0',	NULL,	'0',	NULL,	'2022-12-02 15:15:32',	'2022-12-05 15:35:18',	NULL,	NULL,	NULL,	NULL,	NULL),
(17,	'nurse',	'Selim',	'Boujneh',	'selim@gmail.com',	'2022-12-05 15:39:30',	'$2y$10$8lX2uykTz7Ziv3ST07G7Oun8v7pydmu/i9gLnPpe3ZtzQPnEHz/na',	NULL,	'3dXQJ4Sa99wNNrN7nLTHlcGxvIf4sz7gaLAZImS5wwd6szXOznKKA8MJJzFsFGwzuQuW1AwDaOuqJozM',	'526365',	'RC8834d9e6',	'233445362',	'rue 5',	'1900-01-01',	'98882',	NULL,	NULL,	NULL,	'gravatar',	NULL,	1,	NULL,	NULL,	NULL,	'0',	NULL,	'0',	NULL,	'2022-12-05 15:39:30',	'2022-12-05 15:46:28',	NULL,	NULL,	NULL,	'#ab47bc',	'14353462462'),
(15,	'patient',	'dali',	'dali',	'dali@gmail.com',	'2022-12-03 00:13:38',	'$2y$10$fB3CdHXtFQuQ.k681QaqUOw7mZ1s.JbuwX0zFZP9qMQx2ilfLQSDy',	NULL,	'3jAjNNNMSa6TEpnOzfpNiT14g7N4z6s57eoS0SSz9OXg5EcWJnUlwUfjvnfh6ZkHLyGK9uCQVdnlSmN1',	'53543535',	'RC963370bc',	NULL,	'fdfsdfsdf',	'2011-12-15',	NULL,	NULL,	NULL,	NULL,	'gravatar',	NULL,	1,	NULL,	NULL,	NULL,	'0',	NULL,	'0',	NULL,	'2022-12-03 00:13:38',	'2022-12-07 00:20:00',	NULL,	NULL,	NULL,	NULL,	NULL),
(20,	'patient',	'reter',	'gfgfd',	's@gmail',	'2022-12-05 20:50:51',	'$2y$10$hN.p5fGU8dlmye1l9G0u7.czoQPlFctWk65EB/agWmXnEGbBuNLVu',	NULL,	'x6GgbNS5rxTSeOaro4V1vgYRQ8FWLtYPRnvg9x2Vpy2iBdx3dk2D0wnLAWVmFAGd7lOscGJBm98hKlXv',	'222222222',	'RCed8d7bf8',	NULL,	'fsfdf',	'2022-12-03',	NULL,	NULL,	NULL,	NULL,	'gravatar',	NULL,	1,	NULL,	NULL,	NULL,	'0',	NULL,	'0',	NULL,	'2022-12-05 20:50:51',	'2022-12-05 20:50:51',	NULL,	NULL,	NULL,	NULL,	NULL),
(1,	'super admin',	'Super ',	'Admin',	'admin@admin.com',	'2022-11-17 00:49:38',	'$2y$10$Nt8xDzJA0MT3A6Uwvpm5CeFKzFImMMfeBS3/FGplFDR1Iwb0FHgKi',	NULL,	'z8UHU6T5fqesbqqvPcSgshBBBcq7DDodNlO91eztYcIS0ms2IzA8IQlJiTmFUmAyHbV0JnEEhwqKUttn',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'gravatar',	NULL,	1,	'Europe/Paris',	'2022-12-07 10:04:26',	'86.211.158.52',	'0',	NULL,	'0',	NULL,	'2022-11-17 00:49:38',	'2022-12-07 10:04:26',	NULL,	NULL,	NULL,	NULL,	NULL),
(19,	'patient',	'gfdgdf',	'gfdgdfg',	'74@gmail.com',	'2022-12-05 15:47:32',	'$2y$10$wzwDMPR3PLGRRp4EJyKF8epegEUk3gRqkQK57zA9bO/TDZvB80Uaa',	NULL,	'x1qi2SbBuyS6GGZdpraLwHpMmt597ScoJRXlNBclCMmuDctnjBiydVMKakNWLUZjz3LxKbrjGykGm6Ir',	'555555555444',	'RC43a42640',	NULL,	'fggg',	'2022-12-03',	NULL,	NULL,	NULL,	NULL,	'gravatar',	NULL,	1,	NULL,	NULL,	NULL,	'0',	NULL,	'0',	NULL,	'2022-12-05 15:47:32',	'2022-12-05 15:47:32',	NULL,	NULL,	NULL,	NULL,	NULL),
(4,	'nurse',	'thomas',	'philips',	'thomas@gmail.com',	'2022-12-02 12:02:27',	'$2y$10$uQhOzPt4HRSsReX0WsxaGeBpeY165qWfTNKpsKiWNnAgdhpWmpZXu',	NULL,	'm6TPDutRUNRcAnioNzBkUfXbACmOCHT0Dp0VhsqlcXIUmAQ1xmsbcxB1ZbsLTv5Edbvin2K6VgXzTRoV',	'5655532',	'RC690514b4',	'980808779',	'rue jean 5',	'2022-11-08',	'98885,98884',	NULL,	NULL,	NULL,	'storage',	'avatars/myXBxNhitDoGw0kuJ6RxaEdJKLM99g2TULCseoa0.jpg',	1,	NULL,	NULL,	NULL,	'0',	NULL,	'0',	NULL,	'2022-12-02 12:02:27',	'2022-12-07 10:36:50',	NULL,	NULL,	NULL,	'#a5d6a7',	'22524346473'),
(7,	'nurse',	'dali',	'dali',	'5@gmail.com',	'2022-12-02 14:55:56',	'$2y$10$0Z07XbdmCQwSfRM2XLo8yecknY3dyKLG4Tfs0aPCR/H5KjM8ImWB.',	NULL,	'JY85FSKWQqVTHJeE3HELkAxylh9TXZxOTeM5kCFW4bkC1KY2LS8yyCCdwHUfqGwB3fk9FJ2pPsITR0ch',	'543534534',	'RC36b0c13d',	'254454343',	'fssfsf',	'2000-12-08',	'98890,98885',	NULL,	NULL,	NULL,	'gravatar',	NULL,	1,	NULL,	NULL,	NULL,	'0',	NULL,	'0',	NULL,	'2022-12-02 14:55:56',	'2022-12-06 20:25:17',	NULL,	NULL,	NULL,	NULL,	'54534534534'),
(21,	'substitute',	'rempla',	'thomas',	'thomprud937@gmail.com',	'2022-12-06 09:23:47',	'$2y$10$PPWuumOKE5FPPDSOMdLQ.eju1jtycxnD/lO1c.JITnSaESMURednG',	NULL,	'Bwbo011ynCu0iKr9XUIy6GbLTXnYGkyuDtL2BHJKPxG5tsCt2mdYE2qaawFEX6qlqMdoZMZRRyuobqfM',	'00000000',	'RC3cfc9830',	NULL,	'45 rue sainte',	'1988-12-15',	NULL,	NULL,	NULL,	NULL,	'gravatar',	NULL,	1,	NULL,	NULL,	NULL,	'0',	NULL,	'0',	NULL,	'2022-12-06 09:23:47',	'2022-12-06 09:50:20',	NULL,	NULL,	NULL,	NULL,	NULL),
(5,	'nurse',	'az',	'med amine',	'lib@gmail.com',	'2022-12-02 12:07:36',	'$2y$10$zoSMgqk1oy8UQrW/8K7JoeQ6UWikBPPRIvTRo9DdgB2QpCL9.czzy',	NULL,	'MwkUZpvEfSqFVqFsGSj9BTzNSrcuXFIQYcw2s1f2b7EmYMCTDUDbvecQEuSJbbI5SpwvKx5X02eiaSky',	'123123',	'RCdf9fa2e5',	'234234234',	'zezeze',	'2022-12-01',	'98890,98889',	NULL,	NULL,	NULL,	'gravatar',	NULL,	1,	'Africa/Tunis',	'2022-12-06 11:45:01',	'197.0.26.64',	'0',	NULL,	'0',	NULL,	'2022-12-02 12:07:36',	'2022-12-06 13:20:04',	NULL,	NULL,	NULL,	'#4287f5',	'23423423423'),
(24,	'nurse',	'lepain',	'jack',	'jack@gmail.com',	'2022-12-06 14:41:14',	'$2y$10$xOh8VEuAgAX2uaNlyFYtzuGYGBiOiRnVg5ljXitzQkpN6i8Xai/F.',	NULL,	'cYBBHbgPEudniXaUj1vRZlE4yV4h9FyREM2buS0M308O90RJKGzOckNg8r18rPFf8UZMfee00dxsgVTp',	'12312312',	'RCc48f98e6',	'111111111',	'rue',	'1994-12-17',	'98890,98889',	'je suis jack lepain je suis infirmier libéral',	NULL,	NULL,	'gravatar',	NULL,	1,	NULL,	NULL,	NULL,	'0',	NULL,	'0',	NULL,	'2022-12-06 14:41:14',	'2022-12-06 14:44:56',	NULL,	NULL,	NULL,	NULL,	'11111111111');

DROP TABLE IF EXISTS "vaccines";
DROP SEQUENCE IF EXISTS vaccines_id_seq;
CREATE SEQUENCE vaccines_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."vaccines" (
    "id" bigint DEFAULT nextval('vaccines_id_seq') NOT NULL,
    "trod" character varying(255) NOT NULL,
    "date" date NOT NULL,
    "name" character varying(255) NOT NULL,
    "batch_number" character varying(255) NOT NULL,
    "doses" character varying(255),
    "doses_number" integer,
    "patient_id" bigint NOT NULL,
    "user_id" bigint NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "vaccines_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "vaccines" ("id", "trod", "date", "name", "batch_number", "doses", "doses_number", "patient_id", "user_id", "created_at", "updated_at") VALUES
(1,	'positive',	'2022-12-23',	'Moderna',	'345345',	'initial',	1,	20,	5,	'2022-12-05 21:11:45',	'2022-12-05 21:11:45');

ALTER TABLE ONLY "public"."admin_subscriptions" ADD CONSTRAINT "admin_subscriptions_plan_id_foreign" FOREIGN KEY (plan_id) REFERENCES plans(id) ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE;
ALTER TABLE ONLY "public"."admin_subscriptions" ADD CONSTRAINT "admin_subscriptions_user_id_foreign" FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE;

ALTER TABLE ONLY "public"."announcements" ADD CONSTRAINT "announcements_user_id_foreign" FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE;

ALTER TABLE ONLY "public"."applications" ADD CONSTRAINT "applications_announcement_id_foreign" FOREIGN KEY (announcement_id) REFERENCES announcements(id) ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE;
ALTER TABLE ONLY "public"."applications" ADD CONSTRAINT "applications_user_id_foreign" FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE;

ALTER TABLE ONLY "public"."appointments" ADD CONSTRAINT "appointments_nurse_id_foreign" FOREIGN KEY (nurse_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE;

ALTER TABLE ONLY "public"."covid_treatments" ADD CONSTRAINT "covid_treatments_patient_id_foreign" FOREIGN KEY (patient_id) REFERENCES patients(id) ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE;
ALTER TABLE ONLY "public"."covid_treatments" ADD CONSTRAINT "covid_treatments_user_id_foreign" FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE;

ALTER TABLE ONLY "public"."disponibilites" ADD CONSTRAINT "disponibilites_nurse_id_foreign" FOREIGN KEY (nurse_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE;

ALTER TABLE ONLY "public"."invitations" ADD CONSTRAINT "invitations_invited_id_foreign" FOREIGN KEY (invited_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE;
ALTER TABLE ONLY "public"."invitations" ADD CONSTRAINT "invitations_team_id_foreign" FOREIGN KEY (team_id) REFERENCES teams(id) ON DELETE CASCADE NOT DEFERRABLE;
ALTER TABLE ONLY "public"."invitations" ADD CONSTRAINT "invitations_user_id_foreign" FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE;

ALTER TABLE ONLY "public"."mobile_subscriptions" ADD CONSTRAINT "mobile_subscriptions_plan_id_foreign" FOREIGN KEY (plan_id) REFERENCES plans(id) ON UPDATE CASCADE NOT DEFERRABLE;
ALTER TABLE ONLY "public"."mobile_subscriptions" ADD CONSTRAINT "mobile_subscriptions_user_id_foreign" FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE NOT DEFERRABLE;

ALTER TABLE ONLY "public"."model_has_permissions" ADD CONSTRAINT "model_has_permissions_permission_id_foreign" FOREIGN KEY (permission_id) REFERENCES permissions(id) ON DELETE CASCADE NOT DEFERRABLE;

ALTER TABLE ONLY "public"."model_has_roles" ADD CONSTRAINT "model_has_roles_role_id_foreign" FOREIGN KEY (role_id) REFERENCES roles(id) ON DELETE CASCADE NOT DEFERRABLE;

ALTER TABLE ONLY "public"."passages" ADD CONSTRAINT "passages_patient_id_foreign" FOREIGN KEY (patient_id) REFERENCES patients(id) ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE;
ALTER TABLE ONLY "public"."passages" ADD CONSTRAINT "passages_treatment_id_foreign" FOREIGN KEY (treatment_id) REFERENCES treatments(id) ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE;

ALTER TABLE ONLY "public"."patients" ADD CONSTRAINT "patients_nurse_id_foreign" FOREIGN KEY (nurse_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE;
ALTER TABLE ONLY "public"."patients" ADD CONSTRAINT "patients_user_id_foreign" FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE;

ALTER TABLE ONLY "public"."permissions" ADD CONSTRAINT "permissions_parent_id_foreign" FOREIGN KEY (parent_id) REFERENCES permissions(id) ON DELETE CASCADE NOT DEFERRABLE;

ALTER TABLE ONLY "public"."ratings" ADD CONSTRAINT "ratings_nurse_id_foreign" FOREIGN KEY (nurse_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE;
ALTER TABLE ONLY "public"."ratings" ADD CONSTRAINT "ratings_user_id_foreign" FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE;

ALTER TABLE ONLY "public"."role_has_permissions" ADD CONSTRAINT "role_has_permissions_permission_id_foreign" FOREIGN KEY (permission_id) REFERENCES permissions(id) ON DELETE CASCADE NOT DEFERRABLE;
ALTER TABLE ONLY "public"."role_has_permissions" ADD CONSTRAINT "role_has_permissions_role_id_foreign" FOREIGN KEY (role_id) REFERENCES roles(id) ON DELETE CASCADE NOT DEFERRABLE;

ALTER TABLE ONLY "public"."team_user" ADD CONSTRAINT "team_user_team_id_foreign" FOREIGN KEY (team_id) REFERENCES teams(id) ON DELETE CASCADE NOT DEFERRABLE;
ALTER TABLE ONLY "public"."team_user" ADD CONSTRAINT "team_user_user_id_foreign" FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE;

ALTER TABLE ONLY "public"."teams" ADD CONSTRAINT "teams_admin_id_foreign" FOREIGN KEY (admin_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE;

ALTER TABLE ONLY "public"."trackings" ADD CONSTRAINT "trackings_patient_id_foreign" FOREIGN KEY (patient_id) REFERENCES patients(id) ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE;
ALTER TABLE ONLY "public"."trackings" ADD CONSTRAINT "trackings_user_id_foreign" FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE;

ALTER TABLE ONLY "public"."treatment_requests" ADD CONSTRAINT "treatment_requests_nurse_id_foreign" FOREIGN KEY (nurse_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE;
ALTER TABLE ONLY "public"."treatment_requests" ADD CONSTRAINT "treatment_requests_patient_id_foreign" FOREIGN KEY (patient_id) REFERENCES patients(id) ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE;

ALTER TABLE ONLY "public"."treatments" ADD CONSTRAINT "treatments_nurse_id_foreign" FOREIGN KEY (nurse_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE;
ALTER TABLE ONLY "public"."treatments" ADD CONSTRAINT "treatments_patient_id_foreign" FOREIGN KEY (patient_id) REFERENCES patients(id) ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE;

ALTER TABLE ONLY "public"."users" ADD CONSTRAINT "users_created_by_foreign" FOREIGN KEY (created_by) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE;

ALTER TABLE ONLY "public"."vaccines" ADD CONSTRAINT "vaccines_patient_id_foreign" FOREIGN KEY (patient_id) REFERENCES patients(id) ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE;
ALTER TABLE ONLY "public"."vaccines" ADD CONSTRAINT "vaccines_user_id_foreign" FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE;

-- 2022-12-07 15:51:47.352053+00
