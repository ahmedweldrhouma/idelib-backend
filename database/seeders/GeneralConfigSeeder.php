<?php

namespace Database\Seeders;

use App\Domains\GeneralConfig\Models\GeneralConfig;
use Database\Seeders\Auth\PermissionRoleSeeder;
use Database\Seeders\Auth\UserRoleSeeder;
use Database\Seeders\Auth\UserSeeder;
use Database\Seeders\Traits\DisableForeignKeys;
use Database\Seeders\Traits\TruncateTable;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;
use Spatie\Permission\PermissionRegistrar;

/**
 * Class AuthTableSeeder.
 */
class GeneralConfigSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     */
    public function run()
    {
        $this->disableForeignKeys();

        // Add the master administrator, user id of 1
        GeneralConfig::create([
            'name' => 'delete_account',
            'value' => '1 ',
        ]);
        GeneralConfig::create([
            'name' => 'leave_team',
            'value' => '1 ',
        ]);
        GeneralConfig::create([
            'name' => 'register',
            'value' => '1 ',
        ]);
        GeneralConfig::create([
            'name' => 'new_application',
            'value' => '1 ',
        ]);
        GeneralConfig::create([
            'name' => 'team_invitation',
            'value' => '1 ',
        ]);

        $this->enableForeignKeys();
    }
}
