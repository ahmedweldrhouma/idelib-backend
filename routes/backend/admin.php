<?php

use App\Domains\Auth\Http\Controllers\Backend\User\UserController;
use App\Domains\MobileSubscription\Models\MobileSubscription;
use App\Http\Controllers\Backend\AppointmentController;
use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\DisponibilityController;
use App\Http\Controllers\Backend\GeneralConfigController;
use App\Http\Controllers\Backend\PlanController;
use App\Http\Controllers\Backend\PatientController;
use App\Http\Controllers\Backend\SubscriptionController;
use App\Http\Controllers\Backend\TeamController;
use App\Http\Controllers\Backend\TreatmentController;
use App\Http\Controllers\Backend\CovidTreatmentController;
use App\Http\Controllers\Backend\VaccineController;
use App\Http\Controllers\Backend\CalendarController;
use App\Http\Controllers\Backend\AdminSubscriptionController;
use Tabuna\Breadcrumbs\Trail;

Route::post('/user/search', [UserController::class, 'search'])->name('search');
Route::post('/tour/searchTourSector', [UserController::class, 'searchTourSector'])->name('searchTourSector');
// All route names are prefixed with 'admin.'.
Route::redirect('/', '/admin/dashboard', 301);
Route::get('dashboard', [DashboardController::class, 'index'])
    ->name('dashboard')
    ->breadcrumbs(function (Trail $trail) {
        $trail->push(__('Dashboard'), route('admin.dashboard'));
    });
Route::post('dashboard/events', [DashboardController::class, 'events'])
    ->name('dashboard.events');
Route::group([
    'prefix' => 'patient',
    'as' => 'patient.',
], function () {
    Route::get('/', [PatientController::class, 'index'])
        ->name('index')
        ->breadcrumbs(function (Trail $trail) {
            $trail->parent('admin.dashboard')
                ->push(__('Gestion de la patientèle'), back());
        }
        );
    Route::group([
        // 'middleware' => 'role:'.config('boilerplate.access.role.admin'),

    ], function () {
        Route::get('create', [PatientController::class, 'create'])
            ->name('create')
            ->breadcrumbs(function (Trail $trail) {
                $trail->parent('admin.patient.index')
                    ->push(__('Créer patient'), route('admin.patient.create'));
            }
            );

        Route::post('/', [PatientController::class, 'store'])->name('store');
        Route::group(['prefix' => '/{type}'], function () {
            Route::get('/', [PatientController::class, 'getByType'])
                ->name('indexByType')->breadcrumbs(function (Trail $trail, $type) {
                    $trail->parent('admin.patient.index')
                        ->push(__('Patients ' . $type), route('admin.patient.index'));
                });
        });
        Route::group(['prefix' => '{patient}'], function () {
            Route::get('edit', [PatientController::class, 'edit'])
                ->name('edit')->breadcrumbs(function (Trail $trail, $patient_id) {
                    $trail->parent('admin.patient.index')
                        ->push(__('Modifier patient'), route('admin.patient.edit', $patient_id));
                });

            Route::get('view', [PatientController::class, 'view'])
                ->name('view')->breadcrumbs(function (Trail $trail, $patient) {
                    $trail->parent('admin.patient.index')
                        ->push(__('Patient details'), route('admin.patient.view', $patient));
                });

            Route::patch('/', [PatientController::class, 'update'])->name('update');
            Route::delete('/', [PatientController::class, 'destroy'])->name('destroy');

            Route::post('updateAttachments', [PatientController::class, 'updateAttachments'])
                ->name('updateAttachments')->breadcrumbs(function (Trail $trail, $patient) {
                    $trail->parent('admin.patient.index')
                        ->push(__('Patient details'), route('admin.patient.updateAttachments', $patient));
                });

            Route::get('deleteAttachments/{filename}', [PatientController::class, 'deleteAttachments'])
                ->name('deleteAttachments')->breadcrumbs(function (Trail $trail, $filename) {
                    $trail->parent('admin.patient.index')
                        ->push(__('Patient details'), route('admin.patient.deleteAttachments'));
                });
        }
        );
    }
    );
});

Route::group([
    'prefix' => 'treatments',
    'as' => 'treatments.',
], function () {
    Route::get('/', [TreatmentController::class, 'index'])
        ->name('index')
        ->breadcrumbs(function (Trail $trail) {
            $trail->parent('admin.dashboard')
                ->push(__('Gestion de la patientèle'), route('admin.treatment.index'));
        }
        );
    Route::get('/{type}', [TreatmentController::class, 'getByType'])
        ->name('indexByType')
        ->breadcrumbs(function (Trail $trail, $type) {
            $trail->parent('admin.patient.index')
                ->push(__('Patients ' . $type), route('admin.patient.index'));
        });

    Route::group([
        'prefix' => 'covid',
        'as' => 'covid.'
    ], function () {
        Route::get('/', [TreatmentController::class, 'getAllByNurse'])->name('treatments');
        Route::group([
            'prefix' => '{type}',
        ], function () {
            Route::get('/', [CovidTreatmentController::class, 'getByType'])->name('covid_treatments')->breadcrumbs(function (Trail $trail, $type) {
                $trail->parent('admin.patient.index')
                    ->push(__('Patients ' . $type), route('admin.patient.index'));
            });;
        });
        Route::group([
            'prefix' => 'vaccines',
            'as' => 'vaccines.'
        ], function () {
            Route::get('/all', [VaccineController::class, 'getAllByNurse'])->name('index')->breadcrumbs(function (Trail $trail) {
                $trail->parent('admin.patient.index')
                    ->push(__('Patients vaccines'), route('admin.patient.index'));
            });;
        });

        Route::delete('/', [TreatmentController::class, 'destroy'])->name('destroy');
    });

    Route::get('create', [TreatmentController::class, 'create'])
        ->name('create')
        ->breadcrumbs(function (Trail $trail) {
            $trail->parent('admin.treatments.index')
                ->push(__('Créer treatment'), route('admin.treatment.create'));
        }
        );

    Route::post('/', [TreatmentController::class, 'store'])->name('store');

    Route::group(['prefix' => '{treatment}'], function () {
        Route::get('edit', [TreatmentController::class, 'edit'])
            ->name('edit');

        Route::patch('/', [TreatmentController::class, 'update'])->name('update');
        Route::delete('/', [TreatmentController::class, 'destroy'])->name('destroy');
    }
    );
});

Route::group([
    'prefix' => 'plan',
    'as' => 'plan.',
], function () {
    Route::get('all', [PlanController::class, 'getAllForAutocomplete']);
    Route::get('/', [PlanController::class, 'index'])
        ->name('index')
        ->breadcrumbs(function (Trail $trail) {
            $trail->parent('admin.dashboard')
                ->push(__('Gestion des abonnements'), route('admin.plan.index'));
        }
        );
    Route::group([
        // 'middleware' => 'role:'.config('boilerplate.access.role.admin'),

    ], function () {
        Route::get('create', [PlanController::class, 'create'])
            ->name('create')
            ->breadcrumbs(function (Trail $trail) {
                $trail->parent('admin.plan.index')
                    ->push(__('Créer abonnement'), route('admin.plan.create'));
            }
            );

        Route::post('/', [PlanController::class, 'store'])->name('store');

        Route::group(['prefix' => '{plan}'], function () {
            Route::get('edit', [PlanController::class, 'edit'])
                ->name('edit');

            Route::patch('/', [PlanController::class, 'update'])->name('update');
            Route::delete('/', [PlanController::class, 'destroy'])->name('destroy');
        }
        );
    }
    );
});

Route::group([
    'prefix' => 'admin_subscription',
    'as' => 'admin_subscription.',
], function () {
    Route::get('/', [AdminSubscriptionController::class, 'index'])
        ->name('index')
        ->breadcrumbs(function (Trail $trail) {
            $trail->parent('admin.dashboard')
                ->push(__('Admin Subscription'), route('admin.plan.index'));
        }
        );
    Route::group([
        // 'middleware' => 'role:'.config('boilerplate.access.role.admin'),

    ], function () {
        Route::get('create', [AdminSubscriptionController::class, 'create'])
            ->name('create')
            ->breadcrumbs(function (Trail $trail) {
                $trail->parent('admin.plan.index')
                    ->push(__('Créer Admin Subscription'), route('admin.plan.create'));
            }
            );

        Route::post('/', [AdminSubscriptionController::class, 'store'])->name('store');

        Route::group(['prefix' => '{admin_subscription}'], function () {
            Route::get('edit', [AdminSubscriptionController::class, 'edit'])
                ->name('edit');

            Route::patch('/', [AdminSubscriptionController::class, 'update'])->name('update');
            Route::delete('/', [AdminSubscriptionController::class, 'destroy'])->name('destroy');
        });
    }
    );
});
Route::group([
    'prefix' => 'config',
    'as' => 'config.',
], function () {
Route::get('/', [GeneralConfigController::class, 'index'])
    ->name('index')
    ->breadcrumbs(function (Trail $trail) {
        $trail->parent('admin.dashboard')
            ->push(__('Email'), route('admin.config.index'));
    });
    Route::patch('/', [GeneralConfigController::class, 'update'])->name('edit');
});
Route::group([
    'prefix' => 'subscription',
    'as' => 'subscription.',
], function () {
    Route::get('/', [SubscriptionController::class, 'index'])
        ->name('index')
        ->breadcrumbs(function (Trail $trail) {
            $trail->parent('admin.dashboard')
                ->push(__('Mobile subscription'), route('admin.subscription.index'));
        }
        );
    Route::group(['prefix' => '{id}'], function () {
        Route::get('/edit', [SubscriptionController::class, 'edit'])
            ->name('edit')
            ->breadcrumbs(function (Trail $trail, MobileSubscription $mobileSubscription) {
                $trail->parent('admin.subscription.index')
                    ->push(__('Mobile edit subscription'), route('admin.subscription.edit', $mobileSubscription));
            }
            );
    });


    Route::group(['prefix' => '{subscription}'], function () {
        Route::delete('/', [SubscriptionController::class, 'destroy'])->name('destroy');
    }
    );
});
Route::group([
    'prefix' => 'disponibility',
    'as' => 'disponibility.',
], function () {
    Route::post('/store', [DisponibilityController::class, 'store'])
        ->name('store');
    Route::post('/update', [DisponibilityController::class, 'update'])
        ->name('update');
    Route::post('/destroy', [DisponibilityController::class, 'destroy'])
        ->name('destroy');
});


Route::group([
    'prefix' => 'invoices',
    'as' => 'invoices.',
], function () {
    Route::get('/factures', [UserController::class, 'invoicesGetAll'])
        ->name('getAll');


    Route::get('/unsubscribe', [UserController::class, 'unsubscribe'])
        ->name('unsubscribe');
});
Route::group([
    'prefix' => 'calendar',
    'as' => 'calendar.',
], function () {
    Route::get('/', [CalendarController::class, 'index'])
        ->name('index')
        ->breadcrumbs(function (Trail $trail) {
            $trail->parent('admin.dashboard')
                ->push(__('Calendrier / Planning'), route('admin.calendar.index'), compact($trail));
        }
        );

    Route::get('/infospatient', [CalendarController::class, 'GetPatientDetails'])
        ->name('GetPatientDetails');
    Route::post('/update_treat', [CalendarController::class, 'treatUpdate'])
        ->name('treatUpdate');
    Route::post('/update_vacc', [CalendarController::class, 'vaccUpdate'])
        ->name('vaccUpdate');
    Route::post('/update_cov', [CalendarController::class, 'covUpdate'])
        ->name('covUpdate');
    Route::post('/update_appointment', [CalendarController::class, 'appUpdate'])
        ->name('update_appointment');
    Route::post('/remove_appointment', [CalendarController::class, 'appRemove'])
        ->name('remove_appointment');
    Route::post('/delete-event', [CalendarController::class, 'deleteEvent'])
        ->name('deleteEvent');
    Route::post('/delete-event-vacc', [CalendarController::class, 'deleteVaccEvent'])
        ->name('deleteVaccEvent');
    Route::post('/delete-event-treat', [CalendarController::class, 'deleteTraitEvent'])
        ->name('deleteTraitEvent');
    Route::group(['prefix' => '{type}'], function () {
        Route::get('/', [CalendarController::class, 'getAllByType'])
            ->name('getAllByType')
            ->breadcrumbs(function (Trail $trail) {
                $trail->parent('admin.dashboard')
                    ->push(__('Calendrier / Planning'), route('admin.calendar.getAllByType', 'type'));
            });
        Route::post('/all', [CalendarController::class, 'getData'])
            ->name('getdata');
    });
    Route::get('/{type}', [CalendarController::class, 'getAllByType'])
        ->name('getAllByType')
        ->breadcrumbs(function (Trail $trail) {
            $trail->parent('admin.dashboard')
                ->push(__('Calendrier / Planning'), route('admin.calendar.getAllByType','type'));
        });

    // Route::get('/tour', [CalendarController::class, 'getAllTour'])->name('tour');
    // Route::get('/planning', [CalendarController::class, 'getAllPlanning'])->name('planning');
});


Route::group([
    'prefix' => 'appointment',
    'as' => 'appointment.',
], function () {
    Route::post('/store', [AppointmentController::class, 'store'])
        ->name('store');
    Route::post('/update', [AppointmentController::class, 'update'])
        ->name('update');
    Route::post('/destroy', [AppointmentController::class, 'destroy'])
        ->name('destroy');
});


Route::group([
    'prefix' => 'team',
    'as' => 'team.'
], function () {
    Route::get('/', [TeamController::class, 'index'])->name('index')->breadcrumbs(function (Trail $trail) {
        $trail->parent('admin.dashboard')
            ->push(__('Equipe'), route('admin.team.index'));
    });
    Route::group([
        'prefix' => '{team}'
    ], function () {
        Route::post('/NewOwner', [TeamController::class, 'defineNewOwner'])
            ->name('defineNewOwner');
        Route::delete('/{user}/remove', [TeamController::class, 'detachMember'])
            ->name('removeteamuser');
        Route::post('/storebiller', [UserController::class, 'createBiller'])
            ->name('biller.create');
    });
    Route::delete('/leaveteam', [TeamController::class, 'leaveteam'])->name('leaveteam');
    Route::get('teamuser/create', [TeamController::class, 'createTeamUser'])
        ->name('teamuser.create');
    Route::post('/storeteam', [TeamController::class, 'store'])
        ->name('storeteam');
    Route::post('/storeteamuser', [UserController::class, 'storeteamuser'])
        ->name('storeteamuser');
    Route::get('{user}/makeadmin', [TeamController::class, 'makeadmin'])
        ->name('makeadmin');
    Route::get('teamuser/create', [TeamController::class, 'createTeamUser'])
        ->name('teamuser.create');
});

