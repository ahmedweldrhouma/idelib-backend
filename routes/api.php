<?php

use App\Domains\Appointment\Models\Method\AppointementMethod;
use App\Http\Controllers\Api\AppointmentController;
use App\Http\Controllers\Api\ConversationController;
use App\Http\Controllers\Api\DisponibilityController;
use App\Http\Controllers\Api\MessageController;
use App\Http\Controllers\Api\NotificationController;
use App\Http\Controllers\Api\PlanningController;
use App\Http\Controllers\Api\TreatmentCommentController;
use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Auth\RegisterController;
use App\Http\Controllers\Api\Auth\LoginController;
use App\Http\Controllers\Api\Auth\ProfileController;
use App\Http\Controllers\Api\Auth\ForgotPasswordController;
use App\Http\Controllers\Api\Auth\ResetPasswordController;
use App\Http\Controllers\Api\Auth\UpdatePasswordController;
use App\Http\Controllers\Api\Auth\UserController;
use App\Http\Controllers\Api\PlanController;
use App\Http\Controllers\Api\PatientController;
use App\Http\Controllers\Api\TreatmentController;
use App\Http\Controllers\Api\AnnouncementController;
use App\Http\Controllers\Api\CovidTreatmentController;
use App\Http\Controllers\Api\VaccineController;
use App\Http\Controllers\Api\TeamController;
use App\Http\Controllers\Api\InvitationController;
use App\Http\Controllers\Api\RatingController;
use App\Http\Controllers\Api\ApplicationController;
use App\Http\Controllers\Api\CalendarController;
use App\Http\Controllers\Api\SubscriptionController;
use App\Http\Controllers\Api\TrackingController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group(['namespace' => 'Api', 'as' => 'api.'], function () {
    Route::middleware('verify_rc')->post('subscriptions/event', [SubscriptionController::class, 'subscriptionEvent']);
    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('logout', [ProfileController::class, 'logout']);
        Route::get('me', [ProfileController::class, 'me']);
        Route::delete('me', [ProfileController::class, 'destroy']);
        Route::put('profile/update', [ProfileController::class, 'update']);
        Route::post('profile/upload-avatar', [ProfileController::class, 'uploadAvatar']);

        Route::patch('password/update', [UpdatePasswordController::class, 'update']);
        Route::group([
            'prefix' => 'events',
        ], function () {
            Route::post('/', [\App\Http\Controllers\Api\EventSortController::class, 'store']);
        });
        Route::group([
            'prefix' => 'subscriptions',
        ], function () {
            //Route::get('/', [SubscriptionController::class, 'index']);
            Route::patch('/{plan_id}', [SubscriptionController::class, 'updateSubscription']);
            Route::get('/', [SubscriptionController::class, 'getSubscriptionByUser']);
        });

        Route::get('/patients/{user}/treatments/finished/all', [TreatmentController::class, 'getFinishedTreatments']);
        Route::group([
            'prefix' => 'patients',
        ], function () {
            Route::get('/', [PatientController::class, 'index']);
            Route::post('/', [PatientController::class, 'store']);
            Route::get('covid', [PatientController::class, 'getCovidPatients']);
            Route::group(['prefix' => '{patient}'], function () {
                Route::get('/', [PatientController::class, 'getPatientById']);
                Route::get('/attachments', [PatientController::class, 'getPatientAttachments']);
                Route::patch('/', [PatientController::class, 'update']);
                Route::post('upload-avatar', [PatientController::class, 'uploadAvatar']);
                Route::post('upload-mutual', [PatientController::class, 'uploadMutual']);
                Route::post('upload-attachments', [PatientController::class, 'uploadAttachments']);
                Route::delete('/', [PatientController::class, 'destroy']);
                Route::group([
                    'prefix' => 'treatments',
                ], function () {
                    Route::get('/', [PatientController::class, 'getTreatments']);
                    Route::post('/', [TreatmentController::class, 'store']);
                    Route::patch('/{treatment}', [TreatmentController::class, 'update']);
                    Route::post('/{treatment}/prescription', [TreatmentController::class, 'uploadPrescription']);
                });
                Route::group([
                    'prefix' => 'covid',
                ], function () {
                    Route::group([
                        'prefix' => 'treatments',
                    ], function () {
                        Route::post('/', [TreatmentController::class, 'store']);
                        Route::get('/', [PatientController::class, 'getCovidTreatments']);
                    });
                    Route::group([
                        'prefix' => 'vaccines',
                    ], function () {
                        Route::post('/', [VaccineController::class, 'store']);
                        Route::get('/', [VaccineController::class, 'getAllByPatient']);
                        Route::group([
                            'prefix' => '{vaccine}',
                        ], function () {
                            Route::patch('/', [VaccineController::class, 'update']);
                            Route::delete('/', [VaccineController::class, 'destroy']);
                        });
                    });
                    Route::group([
                        'prefix' => 'tracking',
                    ], function () {
                        Route::post('/', [TrackingController::class, 'store']);
                        Route::get('/', [TrackingController::class, 'getAllByPatient']);
                        Route::group([
                            'prefix' => '{tracking}',
                        ], function () {
                            Route::patch('/', [TrackingController::class, 'update']);
                        });
                    });
                    Route::group([
                        'prefix' => '{type}',
                    ], function () {
                        Route::post('/', [CovidTreatmentController::class, 'store']);
                        Route::post('/{covidTreatment}/upload-prescription', [CovidTreatmentController::class, 'uploadPrescription']);
                        Route::get('/', [CovidTreatmentController::class, 'getAllByPatient']);
                        Route::group([
                            'prefix' => '{covidTreatment}',
                        ], function () {
                            Route::patch('/', [CovidTreatmentController::class, 'update']);
                            Route::delete('/', [CovidTreatmentController::class, 'destroy']);
                        });
                    });
                });
            });
        });


        Route::group([
            'prefix' => 'treatment/{treatment}',
        ], function () {
            Route::group([
                'prefix' => 'comment',
            ], function () {
                Route::post('/', [TreatmentCommentController::class, 'store']);
                Route::get('/', [TreatmentCommentController::class, 'getTreatmentComment']);
                Route::patch('/{comment}', [TreatmentCommentController::class, 'update']);
                Route::delete('/{comment}', [TreatmentCommentController::class, 'destroy']);
            });
        });

        Route::group([
            'prefix' => 'treatments',
        ], function () {
            Route::group([
                'prefix' => '{treatment}',
            ], function () {
                Route::delete('/', [TreatmentController::class, 'destroy']);
                Route::post('renewal', [TreatmentController::class, 'renewal']);
            });
            Route::group([
                'prefix' => 'requests',
            ], function () {
                Route::post('/', [TreatmentController::class, 'storeRequest']);
                Route::get('/', [TreatmentController::class, 'getTreatmentRequest']);
                Route::get('/history', [TreatmentController::class, 'getTreatmentRequestHistory']);
                Route::group([
                    'prefix' => '{request}',
                ], function () {
                    Route::post('/accept', [TreatmentController::class, 'acceptTreatment']);
                    Route::post('/deny', [TreatmentController::class, 'denyTreatment']);
                    Route::get('/', [TreatmentController::class, 'showRequest']);
                    Route::get('/availability', [TreatmentController::class, 'checkAvailability']);
                    Route::post('/availability', [TreatmentController::class, 'updateAvailability']);
                });
            });
        });
        Route::get('maps/positions', [UserController::class, 'getNearNursesPositions']);

        Route::group([
            'prefix' => 'nurses',
        ], function () {
            Route::get('/', [UserController::class, 'getNearNurses']);
            Route::group(['prefix' => '{type}'], function () {
                Route::get('/', [UserController::class, 'getByType']);
            });
            Route::group(['prefix' => '{id}'], function () {
                Route::group(['prefix' => 'ratings'], function () {
                    Route::post('/', [RatingController::class, 'store']);
                    Route::get('/', [RatingController::class, 'show']);
                });
            });
        });

        Route::group([
            'prefix' => 'announcements',
        ], function () {
            Route::get('/', [AnnouncementController::class, 'getUserAnnouncements']);
            Route::get('/offers', [AnnouncementController::class, 'getOffers']);
            Route::post('/', [AnnouncementController::class, 'store']);
            Route::group(['prefix' => '{announcement}'], function () {
                Route::get('/', [AnnouncementController::class, 'getDetails']);
                Route::patch('/', [AnnouncementController::class, 'update']);
                Route::delete('/', [AnnouncementController::class, 'destroy']);
                Route::group([
                    'prefix' => 'applications',
                ], function () {
                    Route::get('/', [ApplicationController::class, 'getApplicationsToAnnouncement']);
                    Route::post('/', [ApplicationController::class, 'store']);
                });
            });
        });

        Route::group([
            'prefix' => 'appointments',
        ], function () {
            Route::get('/', [AppointmentController::class, 'getUserAppointments']);
            Route::post('/', [AppointmentController::class, 'store']);
            Route::group(['prefix' => '{appointment}'], function () {
                Route::get('/', [AppointmentController::class, 'getDetails']);
                Route::patch('/', [AppointmentController::class, 'update']);
                Route::delete('/', [AppointmentController::class, 'destroy']);
            });
        });

        Route::group([
            'prefix' => 'applications',
        ], function () {
            Route::get('/', [ApplicationController::class, 'getApplications']);
            Route::group(['prefix' => '{application}'], function () {
                Route::post('/', [ApplicationController::class, 'accept']);
                Route::delete('/', [ApplicationController::class, 'deny']);
            });
        });

        Route::group([
            'prefix' => 'covid',
        ], function () {
            Route::post('pcr/', [CovidTreatmentController::class, 'storePcr']);
            Route::group(['prefix' => '{announcement}'], function () {
                Route::patch('/', [AnnouncementController::class, 'update']);
                Route::delete('/', [AnnouncementController::class, 'destroy']);
            });
        });

        Route::group([
            'prefix' => 'teams',
        ], function () {
            Route::post('/', [TeamController::class, 'store']);
            Route::get('/owned', [TeamController::class, 'getOwnedTeams']);
            Route::get('/', [TeamController::class, 'getTeams']);
            Route::get('/invitations', [InvitationController::class, 'getPendingInvitations']);

            Route::group(['prefix' => '{team}'], function () {
                Route::group([
                    'prefix' => 'invitations',
                ], function () {
                    Route::post('/', [InvitationController::class, 'invite']);
                });
                Route::delete('/', [TeamController::class, 'leaveTeam']);
                Route::patch('/', [TeamController::class, 'update']);
                Route::group(['prefix' => 'members'], function () {
                    Route::group(['prefix' => '{member}'], function () {
                        Route::patch('/', [TeamController::class, 'updateColor']);
                        Route::delete('/', [TeamController::class, 'detachMember']);
                        Route::patch('/admin', [TeamController::class, 'defineNewAdmin']);
                        Route::patch('/owner', [TeamController::class, 'defineNewOwner']);
                        Route::post('/contract', [TeamController::class, 'createContract']);
                    });
                });
            });
            Route::group([
                'prefix' => 'invitations',
            ], function () {
                Route::group(['prefix' => '{token}'], function () {
                    Route::post('/', [InvitationController::class, 'accept']);
                    Route::delete('/', [InvitationController::class, 'deny']);
                });
            });
        });
        Route::group(['prefix' => 'notifications',
            ], function () {
                Route::get('/', [NotificationController::class, 'getUserNotifications']);
                Route::get('/unread', [NotificationController::class, 'UnreadNotification']);
        });
        Route::group([
            'prefix' => 'calendar',
            'as' => 'calendar.',
        ], function () {
            Route::get('/tour', [CalendarController::class, 'getData']);
            Route::get('/tour/day/{date}', [CalendarController::class, 'getDayData']);
            Route::get('/tour/hours/{date}', [CalendarController::class, 'getHoursData']);
            Route::get('/planning', [PlanningController::class, 'getData']);

            Route::get('/tour/new', [CalendarController::class, 'getNewData']);
            Route::get('/tour/new/day/{date}', [CalendarController::class, 'getNewDayData']);
            Route::get('/tour/hours/{date}', [CalendarController::class, 'getNewHoursData']);
        });
        Route::group(['prefix' => 'disponibilites'], function () {
            Route::get('/', [DisponibilityController::class, 'index']);
            Route::get('/{disponibilite}', [DisponibilityController::class, 'get']);
            Route::post('/', [DisponibilityController::class, 'store']);
            Route::patch('/{disponibilite}', [DisponibilityController::class, 'update']);
            Route::Delete('/{disponibilite}', [DisponibilityController::class, 'destroy']);
        });

        Route::group(['prefix' => 'conversation'], function () {
            Route::post('/', [ConversationController::class, 'store']);
            Route::get('/', [ConversationController::class, 'getConversations']);
            Route::get('/member/{user}', [ConversationController::class, 'getMemberConversations']);
            Route::post('/member/add', [ConversationController::class, 'addMember']);
            Route::group(['prefix' => '{conversation}'], function () {
                Route::get('/', [ConversationController::class, 'getConversation']);
                Route::get('/team/member', [ConversationController::class, 'getTeamMemberConversations']);
                Route::delete('/member/{user}', [ConversationController::class, 'removeMember']);
                Route::patch('/', [ConversationController::class, 'update']);
                Route::delete('/', [ConversationController::class, 'delete']);
                Route::get('/messages',[ConversationController::class, 'messages']);
                Route::post('/message/file',[MessageController::class, 'storeFile']);
                Route::get('/message/pictures',[MessageController::class, 'getMessagePictures']);
            });
            Route::group(['prefix' => '{message}'],function (){
                Route::get('/',[MessageController::class, 'getMessage']);
            });
        });
    });



    Route::group(['middleware' => 'guest'], function () {
        Route::post('login', [LoginController::class, 'login']);
        Route::post('register', [RegisterController::class, 'register']);
        // Password Reset Routes
        Route::post('password/email', [ForgotPasswordController::class, 'sendResetLinkEmailApi']);

        Route::post('password/verify', [ResetPasswordController::class, 'verifyCode']);
        Route::post('password/reset', [ResetPasswordController::class, 'resetApi']);
        Route::get('plans', [PlanController::class, 'getAll']);

        Route::group([
            'prefix' => 'users',
        ], function () {
            Route::group([
                'prefix' => '{user}',
            ], function () {
                Route::patch('/token', [UserController::class, 'saveToken']);
            });
        });
    });

});
