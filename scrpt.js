const WebSocket = require('ws');
let socket = new WebSocket("wss://staging.idelib.com/wss?user_id=44");
socket.onopen = function(e) {
  console.log("[open] Connection established");
};

socket.onmessage = function(event) {
    console.log("[message] Data received from server: ${event.data}");
};

socket.onclose = function(event) {
  if (event.wasClean) {
    console.log(event.code);
    console.log(event.reason);
    //console.log(event);
  } else {
    // e.g. server process killed or network down
    // event.code is usually 1006 in this case
    console.log('[close] Connection died');
  }
};

socket.onerror = function(error) {
  console.log(error);
};
