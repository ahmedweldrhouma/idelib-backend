# FROM php:7.4

# ENV APP_PATH=/usr/src/app

# WORKDIR $APP_PATH
# COPY . $APP_PATH

# RUN apt-get update
# RUN apt-get install -y zip unzip libpq-dev
# RUN docker-php-ext-install pdo_pgsql pgsql gd
# RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
# RUN php composer-setup.php
# RUN php -r "unlink('composer-setup.php');"
# RUN php composer.phar install --ignore-platform-reqs
#   # Upgrade to Node 8
# RUN curl -sL https://deb.nodesource.com/setup_14.x | bash -
  # Install dependencies
# RUN apt-get install git nodejs libcurl4-gnutls-dev libicu-dev libmcrypt-dev libvpx-dev libjpeg-dev libpng-dev libxpm-dev zlib1g-dev libfreetype6-dev libxml2-dev libexpat1-dev libbz2-dev libgmp3-dev libldap2-dev unixodbc-dev libpq-dev libsqlite3-dev libaspell-dev libsnmp-dev libpcre3-dev libtidy-dev -yqq
# RUN npm install && npm run dev
# RUN cp .env.example .env
#    # Run database migrations.
# RUN php artisan migrate:fresh
#   # Run database seed
# RUN php artisan db:seed
# RUN php artisan key:generate
# RUN php artisan config:cache
# RUN php artisan vendor:publish --all

FROM composer:2.4 as build
COPY . /app/
RUN composer
RUN composer install --ignore-platform-reqs
# RUN composer require cboden/ratchet:* --ignore-platform-req=ext-gd


#--prefer-dist --no-dev --optimize-autoloader --no-interaction

FROM php:8.1-apache-buster as production

ENV APP_ENV=production
ENV APP_DEBUG=false

# we need this arg to identiy the true env file be use to prod or staging
ARG ENVFILE
ARG DEFAULT_VHOST
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN  php -r "if (hash_file('sha384', 'composer-setup.php') === '55ce33d7678c5a611085589f1f3ddf8b3c52d662cd01d4ba75c0ee0459970c2200a51f492d557530c71c15d8dba01eae') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
RUN php composer-setup.php
RUN php -r "unlink('composer-setup.php');"
RUN mv composer.phar /usr/local/bin/composer
RUN composer.phar
RUN docker-php-ext-install php-pecl-zip
RUN composer.phar require cboden/ratchet:* --ignore-platform-req=ext-zip
RUN apt-get update
RUN apt install curl -y
RUN curl -sL https://deb.nodesource.com/setup_14.x | bash -
RUN apt-get install git nodejs libcurl4-gnutls-dev libicu-dev libmcrypt-dev \
libvpx-dev libjpeg-dev libpng-dev libxpm-dev zlib1g-dev libfreetype6-dev  \
libxml2-dev libexpat1-dev libbz2-dev libgmp3-dev libldap2-dev unixodbc-dev  \
libpq-dev libsqlite3-dev libaspell-dev libsnmp-dev libpcre3-dev libtidy-dev -yqq

# docker-php-ext-configure opcache --enable-opcache && \
RUN docker-php-ext-install pdo_pgsql pgsql gd
RUN docker-php-ext-configure gd --enable-gd --with-freetype --with-jpeg

COPY opcache.ini /usr/local/etc/php/conf.d/opcache.ini
COPY php.ini /usr/local/etc/php/php.ini
# RUN set -ex \
#   && apk --no-cache add \
#     postgresql-dev

COPY --from=build /app /var/www/html
COPY $DEFAULT_VHOST /etc/apache2/sites-available/000-default.conf

COPY $ENVFILE /var/www/html/.env

# run letencrypt
RUN apt install certbot python3-certbot-apache -y

RUN apt-get clean && rm -rf /var/lib/apt/lists/*

RUN npm install && npm run dev

RUN php artisan config:cache
RUN php artisan cache:clear
RUN php artisan route:cache
#RUN    php artisan db:seed
RUN    yes | php artisan key:generate
RUN    php artisan vendor:publish --all
RUN    chmod 777 -R /var/www/html/storage/
RUN    chown -R www-data:www-data /var/www/
RUN    a2enmod rewrite
RUN php artisan websocket:init
#CMD ["php","artisan", "websocket:init"]
#CMD ["/usr/bin/certbot","--apache"]
