--
-- PostgreSQL database dump
--

-- Dumped from database version 15.1 (Debian 15.1-1.pgdg110+1)
-- Dumped by pg_dump version 15.1 (Debian 15.1-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: activity_log; Type: TABLE; Schema: public; Owner: idelib
--

CREATE TABLE public.activity_log (
    id bigint NOT NULL,
    log_name character varying(255),
    description text NOT NULL,
    subject_id bigint,
    subject_type character varying(255),
    causer_id bigint,
    causer_type character varying(255),
    properties json,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.activity_log OWNER TO idelib;

--
-- Name: activity_log_id_seq; Type: SEQUENCE; Schema: public; Owner: idelib
--

CREATE SEQUENCE public.activity_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.activity_log_id_seq OWNER TO idelib;

--
-- Name: activity_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelib
--

ALTER SEQUENCE public.activity_log_id_seq OWNED BY public.activity_log.id;


--
-- Name: admin_subscriptions; Type: TABLE; Schema: public; Owner: idelib
--

CREATE TABLE public.admin_subscriptions (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    plan_id bigint NOT NULL,
    subscribed_at timestamp(0) without time zone NOT NULL,
    expires_at timestamp(0) without time zone NOT NULL,
    iap boolean DEFAULT false,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.admin_subscriptions OWNER TO idelib;

--
-- Name: admin_subscriptions_id_seq; Type: SEQUENCE; Schema: public; Owner: idelib
--

CREATE SEQUENCE public.admin_subscriptions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_subscriptions_id_seq OWNER TO idelib;

--
-- Name: admin_subscriptions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelib
--

ALTER SEQUENCE public.admin_subscriptions_id_seq OWNED BY public.admin_subscriptions.id;


--
-- Name: announcements; Type: TABLE; Schema: public; Owner: idelib
--

CREATE TABLE public.announcements (
    id bigint NOT NULL,
    start_date date,
    end_date date,
    sector character varying(255),
    has_transport boolean,
    announcement text,
    user_id bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.announcements OWNER TO idelib;

--
-- Name: announcements_id_seq; Type: SEQUENCE; Schema: public; Owner: idelib
--

CREATE SEQUENCE public.announcements_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.announcements_id_seq OWNER TO idelib;

--
-- Name: announcements_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelib
--

ALTER SEQUENCE public.announcements_id_seq OWNED BY public.announcements.id;


--
-- Name: applications; Type: TABLE; Schema: public; Owner: idelib
--

CREATE TABLE public.applications (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    announcement_id bigint NOT NULL,
    accepted boolean,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.applications OWNER TO idelib;

--
-- Name: applications_id_seq; Type: SEQUENCE; Schema: public; Owner: idelib
--

CREATE SEQUENCE public.applications_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.applications_id_seq OWNER TO idelib;

--
-- Name: applications_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelib
--

ALTER SEQUENCE public.applications_id_seq OWNED BY public.applications.id;


--
-- Name: appointments; Type: TABLE; Schema: public; Owner: idelib
--

CREATE TABLE public.appointments (
    id bigint NOT NULL,
    title character varying(255) NOT NULL,
    address character varying(255),
    start_date date NOT NULL,
    end_date date NOT NULL,
    start_time time(0) without time zone,
    end_time time(0) without time zone,
    note text,
    nurse_id bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.appointments OWNER TO idelib;

--
-- Name: appointments_id_seq; Type: SEQUENCE; Schema: public; Owner: idelib
--

CREATE SEQUENCE public.appointments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.appointments_id_seq OWNER TO idelib;

--
-- Name: appointments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelib
--

ALTER SEQUENCE public.appointments_id_seq OWNED BY public.appointments.id;


--
-- Name: covid_treatments; Type: TABLE; Schema: public; Owner: idelib
--

CREATE TABLE public.covid_treatments (
    id bigint NOT NULL,
    type character varying(255) NOT NULL,
    date date NOT NULL,
    result character varying(255),
    prescription character varying(255),
    patient_id bigint NOT NULL,
    user_id bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.covid_treatments OWNER TO idelib;

--
-- Name: covid_treatments_id_seq; Type: SEQUENCE; Schema: public; Owner: idelib
--

CREATE SEQUENCE public.covid_treatments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.covid_treatments_id_seq OWNER TO idelib;

--
-- Name: covid_treatments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelib
--

ALTER SEQUENCE public.covid_treatments_id_seq OWNED BY public.covid_treatments.id;


--
-- Name: disponibilites; Type: TABLE; Schema: public; Owner: idelib
--

CREATE TABLE public.disponibilites (
    id bigint NOT NULL,
    start_date date NOT NULL,
    end_date date NOT NULL,
    start_time time(0) without time zone,
    end_time time(0) without time zone,
    nurse_id bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.disponibilites OWNER TO idelib;

--
-- Name: disponibilites_id_seq; Type: SEQUENCE; Schema: public; Owner: idelib
--

CREATE SEQUENCE public.disponibilites_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.disponibilites_id_seq OWNER TO idelib;

--
-- Name: disponibilites_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelib
--

ALTER SEQUENCE public.disponibilites_id_seq OWNED BY public.disponibilites.id;


--
-- Name: failed_jobs; Type: TABLE; Schema: public; Owner: idelib
--

CREATE TABLE public.failed_jobs (
    id bigint NOT NULL,
    uuid character varying(255) NOT NULL,
    connection text NOT NULL,
    queue text NOT NULL,
    payload text NOT NULL,
    exception text NOT NULL,
    failed_at timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE public.failed_jobs OWNER TO idelib;

--
-- Name: failed_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: idelib
--

CREATE SEQUENCE public.failed_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.failed_jobs_id_seq OWNER TO idelib;

--
-- Name: failed_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelib
--

ALTER SEQUENCE public.failed_jobs_id_seq OWNED BY public.failed_jobs.id;


--
-- Name: general_configs; Type: TABLE; Schema: public; Owner: idelib
--

CREATE TABLE public.general_configs (
    id bigint NOT NULL,
    "MaxNursesByDay" integer DEFAULT 3 NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.general_configs OWNER TO idelib;

--
-- Name: general_configs_id_seq; Type: SEQUENCE; Schema: public; Owner: idelib
--

CREATE SEQUENCE public.general_configs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.general_configs_id_seq OWNER TO idelib;

--
-- Name: general_configs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelib
--

ALTER SEQUENCE public.general_configs_id_seq OWNED BY public.general_configs.id;


--
-- Name: invitations; Type: TABLE; Schema: public; Owner: idelib
--

CREATE TABLE public.invitations (
    id bigint NOT NULL,
    type character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    user_id bigint NOT NULL,
    team_id bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    invited_id bigint,
    CONSTRAINT invitations_type_check CHECK (((type)::text = ANY ((ARRAY['invite'::character varying, 'request'::character varying])::text[])))
);


ALTER TABLE public.invitations OWNER TO idelib;

--
-- Name: invitations_id_seq; Type: SEQUENCE; Schema: public; Owner: idelib
--

CREATE SEQUENCE public.invitations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.invitations_id_seq OWNER TO idelib;

--
-- Name: invitations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelib
--

ALTER SEQUENCE public.invitations_id_seq OWNED BY public.invitations.id;


--
-- Name: migrations; Type: TABLE; Schema: public; Owner: idelib
--

CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE public.migrations OWNER TO idelib;

--
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: idelib
--

CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.migrations_id_seq OWNER TO idelib;

--
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelib
--

ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;


--
-- Name: mobile_subscriptions; Type: TABLE; Schema: public; Owner: idelib
--

CREATE TABLE public.mobile_subscriptions (
    id bigint NOT NULL,
    provider character varying(255) NOT NULL,
    subscription_id character varying(255),
    receipt text NOT NULL,
    user_id bigint NOT NULL,
    plan_id bigint NOT NULL,
    is_active boolean,
    transaction_id character varying(255),
    original_transaction_id character varying(255),
    operating_system character varying(255),
    subscribed_at timestamp(0) without time zone,
    expired_at timestamp(0) without time zone,
    cancelled_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.mobile_subscriptions OWNER TO idelib;

--
-- Name: mobile_subscriptions_id_seq; Type: SEQUENCE; Schema: public; Owner: idelib
--

CREATE SEQUENCE public.mobile_subscriptions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mobile_subscriptions_id_seq OWNER TO idelib;

--
-- Name: mobile_subscriptions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelib
--

ALTER SEQUENCE public.mobile_subscriptions_id_seq OWNED BY public.mobile_subscriptions.id;


--
-- Name: model_has_permissions; Type: TABLE; Schema: public; Owner: idelib
--

CREATE TABLE public.model_has_permissions (
    permission_id bigint NOT NULL,
    model_type character varying(255) NOT NULL,
    model_id bigint NOT NULL
);


ALTER TABLE public.model_has_permissions OWNER TO idelib;

--
-- Name: model_has_roles; Type: TABLE; Schema: public; Owner: idelib
--

CREATE TABLE public.model_has_roles (
    role_id bigint NOT NULL,
    model_type character varying(255) NOT NULL,
    model_id bigint NOT NULL
);


ALTER TABLE public.model_has_roles OWNER TO idelib;

--
-- Name: passages; Type: TABLE; Schema: public; Owner: idelib
--

CREATE TABLE public.passages (
    id bigint NOT NULL,
    period character varying(255),
    time_slot character varying(255),
    ais character varying(255),
    patient_id bigint,
    treatment_id bigint,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.passages OWNER TO idelib;

--
-- Name: passages_id_seq; Type: SEQUENCE; Schema: public; Owner: idelib
--

CREATE SEQUENCE public.passages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.passages_id_seq OWNER TO idelib;

--
-- Name: passages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelib
--

ALTER SEQUENCE public.passages_id_seq OWNED BY public.passages.id;


--
-- Name: password_histories; Type: TABLE; Schema: public; Owner: idelib
--

CREATE TABLE public.password_histories (
    id bigint NOT NULL,
    model_type character varying(255) NOT NULL,
    model_id bigint NOT NULL,
    password character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.password_histories OWNER TO idelib;

--
-- Name: password_histories_id_seq; Type: SEQUENCE; Schema: public; Owner: idelib
--

CREATE SEQUENCE public.password_histories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.password_histories_id_seq OWNER TO idelib;

--
-- Name: password_histories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelib
--

ALTER SEQUENCE public.password_histories_id_seq OWNED BY public.password_histories.id;


--
-- Name: password_resets; Type: TABLE; Schema: public; Owner: idelib
--

CREATE TABLE public.password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);


ALTER TABLE public.password_resets OWNER TO idelib;

--
-- Name: patients; Type: TABLE; Schema: public; Owner: idelib
--

CREATE TABLE public.patients (
    id bigint NOT NULL,
    type character varying(255),
    first_name character varying(255) NOT NULL,
    last_name character varying(255) NOT NULL,
    email character varying(255),
    phone character varying(255),
    address character varying(255),
    birth_date date,
    documents character varying(255),
    social_security_number character varying(255),
    doctor character varying(255),
    doctor_phone character varying(255),
    antecedents json,
    vital_card character varying(255),
    mutual_card character varying(255),
    user_id bigint,
    nurse_id bigint,
    is_replacement timestamp(0) without time zone,
    replaced_at timestamp(0) without time zone,
    avatar_type character varying(255) DEFAULT 'gravatar'::character varying NOT NULL,
    avatar_location character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    zip_code character varying(255)
);


ALTER TABLE public.patients OWNER TO idelib;

--
-- Name: patients_id_seq; Type: SEQUENCE; Schema: public; Owner: idelib
--

CREATE SEQUENCE public.patients_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.patients_id_seq OWNER TO idelib;

--
-- Name: patients_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelib
--

ALTER SEQUENCE public.patients_id_seq OWNED BY public.patients.id;


--
-- Name: permissions; Type: TABLE; Schema: public; Owner: idelib
--

CREATE TABLE public.permissions (
    id bigint NOT NULL,
    type character varying(255) NOT NULL,
    guard_name character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255),
    parent_id bigint,
    sort smallint DEFAULT '1'::smallint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    CONSTRAINT permissions_type_check CHECK (((type)::text = ANY ((ARRAY['super admin'::character varying, 'admin'::character varying, 'patient'::character varying, 'nurse'::character varying, 'substitute'::character varying, 'biller'::character varying])::text[])))
);


ALTER TABLE public.permissions OWNER TO idelib;

--
-- Name: permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: idelib
--

CREATE SEQUENCE public.permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.permissions_id_seq OWNER TO idelib;

--
-- Name: permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelib
--

ALTER SEQUENCE public.permissions_id_seq OWNED BY public.permissions.id;


--
-- Name: personal_access_tokens; Type: TABLE; Schema: public; Owner: idelib
--

CREATE TABLE public.personal_access_tokens (
    id bigint NOT NULL,
    tokenable_type character varying(255) NOT NULL,
    tokenable_id bigint NOT NULL,
    name character varying(255) NOT NULL,
    token character varying(64) NOT NULL,
    abilities text,
    last_used_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.personal_access_tokens OWNER TO idelib;

--
-- Name: personal_access_tokens_id_seq; Type: SEQUENCE; Schema: public; Owner: idelib
--

CREATE SEQUENCE public.personal_access_tokens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.personal_access_tokens_id_seq OWNER TO idelib;

--
-- Name: personal_access_tokens_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelib
--

ALTER SEQUENCE public.personal_access_tokens_id_seq OWNED BY public.personal_access_tokens.id;


--
-- Name: plans; Type: TABLE; Schema: public; Owner: idelib
--

CREATE TABLE public.plans (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    display_price double precision NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    product_name character varying(255),
    interval_count integer,
    apple_id character varying(255),
    google_id character varying(255),
    display_price_google double precision,
    display_price_apple double precision
);


ALTER TABLE public.plans OWNER TO idelib;

--
-- Name: plans_id_seq; Type: SEQUENCE; Schema: public; Owner: idelib
--

CREATE SEQUENCE public.plans_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.plans_id_seq OWNER TO idelib;

--
-- Name: plans_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelib
--

ALTER SEQUENCE public.plans_id_seq OWNED BY public.plans.id;


--
-- Name: ratings; Type: TABLE; Schema: public; Owner: idelib
--

CREATE TABLE public.ratings (
    id bigint NOT NULL,
    rating double precision NOT NULL,
    nurse_id bigint NOT NULL,
    user_id bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.ratings OWNER TO idelib;

--
-- Name: ratings_id_seq; Type: SEQUENCE; Schema: public; Owner: idelib
--

CREATE SEQUENCE public.ratings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ratings_id_seq OWNER TO idelib;

--
-- Name: ratings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelib
--

ALTER SEQUENCE public.ratings_id_seq OWNED BY public.ratings.id;


--
-- Name: role_has_permissions; Type: TABLE; Schema: public; Owner: idelib
--

CREATE TABLE public.role_has_permissions (
    permission_id bigint NOT NULL,
    role_id bigint NOT NULL
);


ALTER TABLE public.role_has_permissions OWNER TO idelib;

--
-- Name: roles; Type: TABLE; Schema: public; Owner: idelib
--

CREATE TABLE public.roles (
    id bigint NOT NULL,
    type character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    guard_name character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    CONSTRAINT roles_type_check CHECK (((type)::text = ANY ((ARRAY['super admin'::character varying, 'admin'::character varying, 'patient'::character varying, 'nurse'::character varying, 'substitute'::character varying, 'biller'::character varying])::text[])))
);


ALTER TABLE public.roles OWNER TO idelib;

--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: idelib
--

CREATE SEQUENCE public.roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.roles_id_seq OWNER TO idelib;

--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelib
--

ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;


--
-- Name: team_user; Type: TABLE; Schema: public; Owner: idelib
--

CREATE TABLE public.team_user (
    id bigint NOT NULL,
    color character varying(255),
    is_admin boolean DEFAULT false,
    user_id bigint NOT NULL,
    team_id bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.team_user OWNER TO idelib;

--
-- Name: team_user_id_seq; Type: SEQUENCE; Schema: public; Owner: idelib
--

CREATE SEQUENCE public.team_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.team_user_id_seq OWNER TO idelib;

--
-- Name: team_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelib
--

ALTER SEQUENCE public.team_user_id_seq OWNED BY public.team_user.id;


--
-- Name: teams; Type: TABLE; Schema: public; Owner: idelib
--

CREATE TABLE public.teams (
    id bigint NOT NULL,
    name character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    admin_id bigint NOT NULL
);


ALTER TABLE public.teams OWNER TO idelib;

--
-- Name: teams_id_seq; Type: SEQUENCE; Schema: public; Owner: idelib
--

CREATE SEQUENCE public.teams_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.teams_id_seq OWNER TO idelib;

--
-- Name: teams_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelib
--

ALTER SEQUENCE public.teams_id_seq OWNED BY public.teams.id;


--
-- Name: trackings; Type: TABLE; Schema: public; Owner: idelib
--

CREATE TABLE public.trackings (
    id bigint NOT NULL,
    prescription_url character varying(255) NOT NULL,
    pickup_date date,
    duration integer,
    duration_unit character varying(255),
    daily_frequency_period character varying(255),
    daily_frequency_time character varying(255),
    absences smallint,
    patient_id bigint NOT NULL,
    user_id bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.trackings OWNER TO idelib;

--
-- Name: trackings_id_seq; Type: SEQUENCE; Schema: public; Owner: idelib
--

CREATE SEQUENCE public.trackings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.trackings_id_seq OWNER TO idelib;

--
-- Name: trackings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelib
--

ALTER SEQUENCE public.trackings_id_seq OWNED BY public.trackings.id;


--
-- Name: treatment_requests; Type: TABLE; Schema: public; Owner: idelib
--

CREATE TABLE public.treatment_requests (
    id bigint NOT NULL,
    patient_id bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    tour_sector character varying(255),
    is_pending boolean,
    nurse_id bigint,
    is_available boolean,
    note text
);


ALTER TABLE public.treatment_requests OWNER TO idelib;

--
-- Name: treatment_requests_id_seq; Type: SEQUENCE; Schema: public; Owner: idelib
--

CREATE SEQUENCE public.treatment_requests_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.treatment_requests_id_seq OWNER TO idelib;

--
-- Name: treatment_requests_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelib
--

ALTER SEQUENCE public.treatment_requests_id_seq OWNED BY public.treatment_requests.id;


--
-- Name: treatments; Type: TABLE; Schema: public; Owner: idelib
--

CREATE TABLE public.treatments (
    id bigint NOT NULL,
    start_at date,
    end_at date,
    period character varying(255),
    daily_frequency character varying(255),
    monthly_frequency character varying(255),
    type character varying(255),
    classic_type character varying(255),
    bsi_type character varying(255),
    treatment character varying(255),
    stoped_at timestamp(0) without time zone,
    absent_from date,
    absent_to date,
    patient_id bigint,
    nurse_id bigint,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    prescription character varying(255)
);


ALTER TABLE public.treatments OWNER TO idelib;

--
-- Name: treatments_id_seq; Type: SEQUENCE; Schema: public; Owner: idelib
--

CREATE SEQUENCE public.treatments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.treatments_id_seq OWNER TO idelib;

--
-- Name: treatments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelib
--

ALTER SEQUENCE public.treatments_id_seq OWNED BY public.treatments.id;


--
-- Name: two_factor_authentications; Type: TABLE; Schema: public; Owner: idelib
--

CREATE TABLE public.two_factor_authentications (
    id bigint NOT NULL,
    authenticatable_type character varying(255) NOT NULL,
    authenticatable_id bigint NOT NULL,
    shared_secret text NOT NULL,
    enabled_at timestamp(0) with time zone,
    label character varying(255) NOT NULL,
    digits smallint DEFAULT '6'::smallint NOT NULL,
    seconds smallint DEFAULT '30'::smallint NOT NULL,
    "window" smallint DEFAULT '0'::smallint NOT NULL,
    algorithm character varying(16) DEFAULT 'sha1'::character varying NOT NULL,
    recovery_codes text,
    recovery_codes_generated_at timestamp(0) with time zone,
    safe_devices json,
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone
);


ALTER TABLE public.two_factor_authentications OWNER TO idelib;

--
-- Name: two_factor_authentications_id_seq; Type: SEQUENCE; Schema: public; Owner: idelib
--

CREATE SEQUENCE public.two_factor_authentications_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.two_factor_authentications_id_seq OWNER TO idelib;

--
-- Name: two_factor_authentications_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelib
--

ALTER SEQUENCE public.two_factor_authentications_id_seq OWNED BY public.two_factor_authentications.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: idelib
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    type character varying(255) DEFAULT 'admin'::character varying NOT NULL,
    first_name character varying(255) NOT NULL,
    last_name character varying(255) NOT NULL,
    email character varying(255),
    email_verified_at timestamp(0) without time zone,
    password character varying(255),
    password_changed_at timestamp(0) without time zone,
    api_token character varying(255),
    phone character varying(255),
    app_user_id character varying(255),
    adeli_number character varying(255),
    address character varying(255),
    birth_date date,
    tour_sector character varying(255),
    presentation character varying(255),
    subscription_type character varying(255),
    operating_system character varying(255),
    avatar_type character varying(255) DEFAULT 'gravatar'::character varying NOT NULL,
    avatar_location character varying(255),
    active smallint DEFAULT '1'::smallint NOT NULL,
    timezone character varying(255),
    last_login_at timestamp(0) without time zone,
    last_login_ip character varying(255),
    to_be_logged_out boolean DEFAULT false NOT NULL,
    confirmation_code character varying(255),
    is_used boolean DEFAULT false,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone,
    created_by bigint,
    fcm_token text,
    color character varying(255),
    rpps_number character varying(255),
    CONSTRAINT users_type_check CHECK (((type)::text = ANY ((ARRAY['super admin'::character varying, 'admin'::character varying, 'patient'::character varying, 'nurse'::character varying, 'substitute'::character varying, 'biller'::character varying])::text[])))
);


ALTER TABLE public.users OWNER TO idelib;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: idelib
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO idelib;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelib
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: vaccines; Type: TABLE; Schema: public; Owner: idelib
--

CREATE TABLE public.vaccines (
    id bigint NOT NULL,
    trod character varying(255) NOT NULL,
    date date NOT NULL,
    name character varying(255) NOT NULL,
    batch_number character varying(255) NOT NULL,
    doses character varying(255),
    doses_number integer,
    patient_id bigint NOT NULL,
    user_id bigint NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.vaccines OWNER TO idelib;

--
-- Name: vaccines_id_seq; Type: SEQUENCE; Schema: public; Owner: idelib
--

CREATE SEQUENCE public.vaccines_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vaccines_id_seq OWNER TO idelib;

--
-- Name: vaccines_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: idelib
--

ALTER SEQUENCE public.vaccines_id_seq OWNED BY public.vaccines.id;


--
-- Name: activity_log id; Type: DEFAULT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.activity_log ALTER COLUMN id SET DEFAULT nextval('public.activity_log_id_seq'::regclass);


--
-- Name: admin_subscriptions id; Type: DEFAULT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.admin_subscriptions ALTER COLUMN id SET DEFAULT nextval('public.admin_subscriptions_id_seq'::regclass);


--
-- Name: announcements id; Type: DEFAULT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.announcements ALTER COLUMN id SET DEFAULT nextval('public.announcements_id_seq'::regclass);


--
-- Name: applications id; Type: DEFAULT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.applications ALTER COLUMN id SET DEFAULT nextval('public.applications_id_seq'::regclass);


--
-- Name: appointments id; Type: DEFAULT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.appointments ALTER COLUMN id SET DEFAULT nextval('public.appointments_id_seq'::regclass);


--
-- Name: covid_treatments id; Type: DEFAULT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.covid_treatments ALTER COLUMN id SET DEFAULT nextval('public.covid_treatments_id_seq'::regclass);


--
-- Name: disponibilites id; Type: DEFAULT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.disponibilites ALTER COLUMN id SET DEFAULT nextval('public.disponibilites_id_seq'::regclass);


--
-- Name: failed_jobs id; Type: DEFAULT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.failed_jobs ALTER COLUMN id SET DEFAULT nextval('public.failed_jobs_id_seq'::regclass);


--
-- Name: general_configs id; Type: DEFAULT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.general_configs ALTER COLUMN id SET DEFAULT nextval('public.general_configs_id_seq'::regclass);


--
-- Name: invitations id; Type: DEFAULT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.invitations ALTER COLUMN id SET DEFAULT nextval('public.invitations_id_seq'::regclass);


--
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);


--
-- Name: mobile_subscriptions id; Type: DEFAULT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.mobile_subscriptions ALTER COLUMN id SET DEFAULT nextval('public.mobile_subscriptions_id_seq'::regclass);


--
-- Name: passages id; Type: DEFAULT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.passages ALTER COLUMN id SET DEFAULT nextval('public.passages_id_seq'::regclass);


--
-- Name: password_histories id; Type: DEFAULT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.password_histories ALTER COLUMN id SET DEFAULT nextval('public.password_histories_id_seq'::regclass);


--
-- Name: patients id; Type: DEFAULT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.patients ALTER COLUMN id SET DEFAULT nextval('public.patients_id_seq'::regclass);


--
-- Name: permissions id; Type: DEFAULT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.permissions ALTER COLUMN id SET DEFAULT nextval('public.permissions_id_seq'::regclass);


--
-- Name: personal_access_tokens id; Type: DEFAULT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.personal_access_tokens ALTER COLUMN id SET DEFAULT nextval('public.personal_access_tokens_id_seq'::regclass);


--
-- Name: plans id; Type: DEFAULT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.plans ALTER COLUMN id SET DEFAULT nextval('public.plans_id_seq'::regclass);


--
-- Name: ratings id; Type: DEFAULT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.ratings ALTER COLUMN id SET DEFAULT nextval('public.ratings_id_seq'::regclass);


--
-- Name: roles id; Type: DEFAULT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);


--
-- Name: team_user id; Type: DEFAULT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.team_user ALTER COLUMN id SET DEFAULT nextval('public.team_user_id_seq'::regclass);


--
-- Name: teams id; Type: DEFAULT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.teams ALTER COLUMN id SET DEFAULT nextval('public.teams_id_seq'::regclass);


--
-- Name: trackings id; Type: DEFAULT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.trackings ALTER COLUMN id SET DEFAULT nextval('public.trackings_id_seq'::regclass);


--
-- Name: treatment_requests id; Type: DEFAULT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.treatment_requests ALTER COLUMN id SET DEFAULT nextval('public.treatment_requests_id_seq'::regclass);


--
-- Name: treatments id; Type: DEFAULT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.treatments ALTER COLUMN id SET DEFAULT nextval('public.treatments_id_seq'::regclass);


--
-- Name: two_factor_authentications id; Type: DEFAULT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.two_factor_authentications ALTER COLUMN id SET DEFAULT nextval('public.two_factor_authentications_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Name: vaccines id; Type: DEFAULT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.vaccines ALTER COLUMN id SET DEFAULT nextval('public.vaccines_id_seq'::regclass);


--
-- Data for Name: activity_log; Type: TABLE DATA; Schema: public; Owner: idelib
--

COPY public.activity_log (id, log_name, description, subject_id, subject_type, causer_id, causer_type, properties, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: admin_subscriptions; Type: TABLE DATA; Schema: public; Owner: idelib
--

COPY public.admin_subscriptions (id, user_id, plan_id, subscribed_at, expires_at, iap, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: announcements; Type: TABLE DATA; Schema: public; Owner: idelib
--

COPY public.announcements (id, start_date, end_date, sector, has_transport, announcement, user_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: applications; Type: TABLE DATA; Schema: public; Owner: idelib
--

COPY public.applications (id, user_id, announcement_id, accepted, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: appointments; Type: TABLE DATA; Schema: public; Owner: idelib
--

COPY public.appointments (id, title, address, start_date, end_date, start_time, end_time, note, nurse_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: covid_treatments; Type: TABLE DATA; Schema: public; Owner: idelib
--

COPY public.covid_treatments (id, type, date, result, prescription, patient_id, user_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: disponibilites; Type: TABLE DATA; Schema: public; Owner: idelib
--

COPY public.disponibilites (id, start_date, end_date, start_time, end_time, nurse_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: failed_jobs; Type: TABLE DATA; Schema: public; Owner: idelib
--

COPY public.failed_jobs (id, uuid, connection, queue, payload, exception, failed_at) FROM stdin;
\.


--
-- Data for Name: general_configs; Type: TABLE DATA; Schema: public; Owner: idelib
--

COPY public.general_configs (id, "MaxNursesByDay", created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: invitations; Type: TABLE DATA; Schema: public; Owner: idelib
--

COPY public.invitations (id, type, email, token, user_id, team_id, created_at, updated_at, invited_id) FROM stdin;
\.


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: idelib
--

COPY public.migrations (id, migration, batch) FROM stdin;
1	2014_10_12_000000_create_users_table	1
2	2014_10_12_100000_create_password_resets_table	1
3	2019_08_19_000000_create_failed_jobs_table	1
4	2019_12_14_000001_create_personal_access_tokens_table	1
5	2020_02_25_034148_create_permission_tables	1
6	2020_05_29_020244_create_password_histories_table	1
7	2020_07_06_215139_create_activity_log_table	1
8	2021_04_05_153840_create_two_factor_authentications_table	1
9	2022_01_03_141858_create_patients_table	1
10	2022_01_05_164706_create_plans_table	1
11	2022_01_06_122542_create_mobile_subscriptions_table	1
12	2022_01_13_152712_create_treatments_table	1
13	2022_01_14_133733_create_passages_table	1
14	2022_01_21_151037_create_annoucements_table	1
15	2022_02_07_140043_create_covid_treatments_table	1
16	2022_02_09_114933_create_vaccines_table	1
17	2022_02_14_132159_add_prescription_column_to_treatments_table	1
18	2022_04_05_134744_create_requests_table	1
19	2022_04_15_152444_add_sector_column_to_treatment_requests_table	1
20	2022_04_25_145843_add_is_pending_column_to_treatment_requests_table	1
21	2022_04_26_151843_add_zip_code_column_to_patients_table	1
22	2022_04_27_135951_add_columns_to_treatment_requests_table	1
23	2022_04_29_114315_drop_columns_from_treatment_requests_table	1
24	2022_05_04_155553_create_teams_table	1
25	2022_05_04_155709_create_invitations_table	1
26	2022_05_23_155929_add_created_by_column_to_users_table	1
27	2022_06_08_190512_create_ratings_table	1
28	2022_06_14_222450_add_invited_id_column_to_invitations_table	1
29	2022_06_17_162805_drop_plans_table	1
30	2022_06_20_155231_create_admin_subscriptions_table	1
31	2022_06_24_095830_create_applications_table	1
32	2022_06_30_133820_add_fcm_token_to_users_table	1
33	2022_07_26_132707_add_rc_id_columns_to_users_table	2
34	2022_07_26_132708_add_new_columns_to_users_table	2
35	2022_08_17_120117_create_appointments_table	2
36	2022_10_18_093415_create_tracking_table	2
37	2022_11_10_190432_create_disponibilites_table	2
38	2022_11_10_215551_create_general_configs_table	2
39	2022_11_17_002156_add_timezone_column_to_users_table	2
40	2022_11_17_002156_upgrade_two_factor_authentications_table	2
\.


--
-- Data for Name: mobile_subscriptions; Type: TABLE DATA; Schema: public; Owner: idelib
--

COPY public.mobile_subscriptions (id, provider, subscription_id, receipt, user_id, plan_id, is_active, transaction_id, original_transaction_id, operating_system, subscribed_at, expired_at, cancelled_at, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: model_has_permissions; Type: TABLE DATA; Schema: public; Owner: idelib
--

COPY public.model_has_permissions (permission_id, model_type, model_id) FROM stdin;
\.


--
-- Data for Name: model_has_roles; Type: TABLE DATA; Schema: public; Owner: idelib
--

COPY public.model_has_roles (role_id, model_type, model_id) FROM stdin;
1	App\\Domains\\Auth\\Models\\User	1
\.


--
-- Data for Name: passages; Type: TABLE DATA; Schema: public; Owner: idelib
--

COPY public.passages (id, period, time_slot, ais, patient_id, treatment_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: password_histories; Type: TABLE DATA; Schema: public; Owner: idelib
--

COPY public.password_histories (id, model_type, model_id, password, created_at, updated_at) FROM stdin;
1	App\\Domains\\Auth\\Models\\User	1	$2y$10$Nt8xDzJA0MT3A6Uwvpm5CeFKzFImMMfeBS3/FGplFDR1Iwb0FHgKi	2022-11-17 00:49:38	2022-11-17 00:49:38
\.


--
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: idelib
--

COPY public.password_resets (email, token, created_at) FROM stdin;
\.


--
-- Data for Name: patients; Type: TABLE DATA; Schema: public; Owner: idelib
--

COPY public.patients (id, type, first_name, last_name, email, phone, address, birth_date, documents, social_security_number, doctor, doctor_phone, antecedents, vital_card, mutual_card, user_id, nurse_id, is_replacement, replaced_at, avatar_type, avatar_location, created_at, updated_at, zip_code) FROM stdin;
\.


--
-- Data for Name: permissions; Type: TABLE DATA; Schema: public; Owner: idelib
--

COPY public.permissions (id, type, guard_name, name, description, parent_id, sort, created_at, updated_at) FROM stdin;
1	super admin	web	admin.access.user	All User Permissions	\N	1	2022-11-17 00:49:38	2022-11-17 00:49:38
2	super admin	web	admin.access.user.list	View Users	1	1	2022-11-17 00:49:38	2022-11-17 00:49:38
3	super admin	web	admin.access.user.deactivate	Deactivate Users	1	2	2022-11-17 00:49:38	2022-11-17 00:49:38
4	super admin	web	admin.access.user.reactivate	Reactivate Users	1	3	2022-11-17 00:49:38	2022-11-17 00:49:38
5	super admin	web	admin.access.user.clear-session	Clear User Sessions	1	4	2022-11-17 00:49:38	2022-11-17 00:49:38
6	super admin	web	admin.access.user.impersonate	Impersonate Users	1	5	2022-11-17 00:49:38	2022-11-17 00:49:38
7	super admin	web	admin.access.user.change-password	Change User Passwords	1	6	2022-11-17 00:49:38	2022-11-17 00:49:38
\.


--
-- Data for Name: personal_access_tokens; Type: TABLE DATA; Schema: public; Owner: idelib
--

COPY public.personal_access_tokens (id, tokenable_type, tokenable_id, name, token, abilities, last_used_at, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: plans; Type: TABLE DATA; Schema: public; Owner: idelib
--

COPY public.plans (id, name, display_price, created_at, updated_at, product_name, interval_count, apple_id, google_id, display_price_google, display_price_apple) FROM stdin;
\.


--
-- Data for Name: ratings; Type: TABLE DATA; Schema: public; Owner: idelib
--

COPY public.ratings (id, rating, nurse_id, user_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: role_has_permissions; Type: TABLE DATA; Schema: public; Owner: idelib
--

COPY public.role_has_permissions (permission_id, role_id) FROM stdin;
\.


--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: idelib
--

COPY public.roles (id, type, name, guard_name, created_at, updated_at) FROM stdin;
1	super admin	Administrator	web	2022-11-17 00:49:38	2022-11-17 00:49:38
\.


--
-- Data for Name: team_user; Type: TABLE DATA; Schema: public; Owner: idelib
--

COPY public.team_user (id, color, is_admin, user_id, team_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: teams; Type: TABLE DATA; Schema: public; Owner: idelib
--

COPY public.teams (id, name, created_at, updated_at, admin_id) FROM stdin;
\.


--
-- Data for Name: trackings; Type: TABLE DATA; Schema: public; Owner: idelib
--

COPY public.trackings (id, prescription_url, pickup_date, duration, duration_unit, daily_frequency_period, daily_frequency_time, absences, patient_id, user_id, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: treatment_requests; Type: TABLE DATA; Schema: public; Owner: idelib
--

COPY public.treatment_requests (id, patient_id, created_at, updated_at, tour_sector, is_pending, nurse_id, is_available, note) FROM stdin;
\.


--
-- Data for Name: treatments; Type: TABLE DATA; Schema: public; Owner: idelib
--

COPY public.treatments (id, start_at, end_at, period, daily_frequency, monthly_frequency, type, classic_type, bsi_type, treatment, stoped_at, absent_from, absent_to, patient_id, nurse_id, created_at, updated_at, prescription) FROM stdin;
\.


--
-- Data for Name: two_factor_authentications; Type: TABLE DATA; Schema: public; Owner: idelib
--

COPY public.two_factor_authentications (id, authenticatable_type, authenticatable_id, shared_secret, enabled_at, label, digits, seconds, "window", algorithm, recovery_codes, recovery_codes_generated_at, safe_devices, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: idelib
--

COPY public.users (id, type, first_name, last_name, email, email_verified_at, password, password_changed_at, api_token, phone, app_user_id, adeli_number, address, birth_date, tour_sector, presentation, subscription_type, operating_system, avatar_type, avatar_location, active, timezone, last_login_at, last_login_ip, to_be_logged_out, confirmation_code, is_used, remember_token, created_at, updated_at, deleted_at, created_by, fcm_token, color, rpps_number) FROM stdin;
1	super admin	Super 	Admin	admin@admin.com	2022-11-17 00:49:38	$2y$10$Nt8xDzJA0MT3A6Uwvpm5CeFKzFImMMfeBS3/FGplFDR1Iwb0FHgKi	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	gravatar	\N	1	\N	\N	\N	f	\N	f	\N	2022-11-17 00:49:38	2022-11-17 00:49:38	\N	\N	\N	\N	\N
\.


--
-- Data for Name: vaccines; Type: TABLE DATA; Schema: public; Owner: idelib
--

COPY public.vaccines (id, trod, date, name, batch_number, doses, doses_number, patient_id, user_id, created_at, updated_at) FROM stdin;
\.


--
-- Name: activity_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelib
--

SELECT pg_catalog.setval('public.activity_log_id_seq', 1, false);


--
-- Name: admin_subscriptions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelib
--

SELECT pg_catalog.setval('public.admin_subscriptions_id_seq', 1, false);


--
-- Name: announcements_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelib
--

SELECT pg_catalog.setval('public.announcements_id_seq', 1, false);


--
-- Name: applications_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelib
--

SELECT pg_catalog.setval('public.applications_id_seq', 1, false);


--
-- Name: appointments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelib
--

SELECT pg_catalog.setval('public.appointments_id_seq', 1, false);


--
-- Name: covid_treatments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelib
--

SELECT pg_catalog.setval('public.covid_treatments_id_seq', 1, false);


--
-- Name: disponibilites_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelib
--

SELECT pg_catalog.setval('public.disponibilites_id_seq', 1, false);


--
-- Name: failed_jobs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelib
--

SELECT pg_catalog.setval('public.failed_jobs_id_seq', 1, false);


--
-- Name: general_configs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelib
--

SELECT pg_catalog.setval('public.general_configs_id_seq', 1, false);


--
-- Name: invitations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelib
--

SELECT pg_catalog.setval('public.invitations_id_seq', 1, false);


--
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelib
--

SELECT pg_catalog.setval('public.migrations_id_seq', 40, true);


--
-- Name: mobile_subscriptions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelib
--

SELECT pg_catalog.setval('public.mobile_subscriptions_id_seq', 1, false);


--
-- Name: passages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelib
--

SELECT pg_catalog.setval('public.passages_id_seq', 1, false);


--
-- Name: password_histories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelib
--

SELECT pg_catalog.setval('public.password_histories_id_seq', 1, true);


--
-- Name: patients_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelib
--

SELECT pg_catalog.setval('public.patients_id_seq', 1, false);


--
-- Name: permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelib
--

SELECT pg_catalog.setval('public.permissions_id_seq', 7, true);


--
-- Name: personal_access_tokens_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelib
--

SELECT pg_catalog.setval('public.personal_access_tokens_id_seq', 1, false);


--
-- Name: plans_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelib
--

SELECT pg_catalog.setval('public.plans_id_seq', 1, false);


--
-- Name: ratings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelib
--

SELECT pg_catalog.setval('public.ratings_id_seq', 1, false);


--
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelib
--

SELECT pg_catalog.setval('public.roles_id_seq', 1, false);


--
-- Name: team_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelib
--

SELECT pg_catalog.setval('public.team_user_id_seq', 1, false);


--
-- Name: teams_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelib
--

SELECT pg_catalog.setval('public.teams_id_seq', 1, false);


--
-- Name: trackings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelib
--

SELECT pg_catalog.setval('public.trackings_id_seq', 1, false);


--
-- Name: treatment_requests_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelib
--

SELECT pg_catalog.setval('public.treatment_requests_id_seq', 1, false);


--
-- Name: treatments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelib
--

SELECT pg_catalog.setval('public.treatments_id_seq', 1, false);


--
-- Name: two_factor_authentications_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelib
--

SELECT pg_catalog.setval('public.two_factor_authentications_id_seq', 1, false);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelib
--

SELECT pg_catalog.setval('public.users_id_seq', 1, true);


--
-- Name: vaccines_id_seq; Type: SEQUENCE SET; Schema: public; Owner: idelib
--

SELECT pg_catalog.setval('public.vaccines_id_seq', 1, false);


--
-- Name: activity_log activity_log_pkey; Type: CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.activity_log
    ADD CONSTRAINT activity_log_pkey PRIMARY KEY (id);


--
-- Name: admin_subscriptions admin_subscriptions_pkey; Type: CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.admin_subscriptions
    ADD CONSTRAINT admin_subscriptions_pkey PRIMARY KEY (id);


--
-- Name: announcements announcements_pkey; Type: CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.announcements
    ADD CONSTRAINT announcements_pkey PRIMARY KEY (id);


--
-- Name: applications applications_pkey; Type: CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_pkey PRIMARY KEY (id);


--
-- Name: appointments appointments_pkey; Type: CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.appointments
    ADD CONSTRAINT appointments_pkey PRIMARY KEY (id);


--
-- Name: covid_treatments covid_treatments_pkey; Type: CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.covid_treatments
    ADD CONSTRAINT covid_treatments_pkey PRIMARY KEY (id);


--
-- Name: disponibilites disponibilites_pkey; Type: CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.disponibilites
    ADD CONSTRAINT disponibilites_pkey PRIMARY KEY (id);


--
-- Name: failed_jobs failed_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.failed_jobs
    ADD CONSTRAINT failed_jobs_pkey PRIMARY KEY (id);


--
-- Name: failed_jobs failed_jobs_uuid_unique; Type: CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.failed_jobs
    ADD CONSTRAINT failed_jobs_uuid_unique UNIQUE (uuid);


--
-- Name: general_configs general_configs_pkey; Type: CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.general_configs
    ADD CONSTRAINT general_configs_pkey PRIMARY KEY (id);


--
-- Name: invitations invitations_email_unique; Type: CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.invitations
    ADD CONSTRAINT invitations_email_unique UNIQUE (email);


--
-- Name: invitations invitations_pkey; Type: CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.invitations
    ADD CONSTRAINT invitations_pkey PRIMARY KEY (id);


--
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- Name: mobile_subscriptions mobile_subscriptions_pkey; Type: CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.mobile_subscriptions
    ADD CONSTRAINT mobile_subscriptions_pkey PRIMARY KEY (id);


--
-- Name: model_has_permissions model_has_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.model_has_permissions
    ADD CONSTRAINT model_has_permissions_pkey PRIMARY KEY (permission_id, model_id, model_type);


--
-- Name: model_has_roles model_has_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.model_has_roles
    ADD CONSTRAINT model_has_roles_pkey PRIMARY KEY (role_id, model_id, model_type);


--
-- Name: passages passages_pkey; Type: CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.passages
    ADD CONSTRAINT passages_pkey PRIMARY KEY (id);


--
-- Name: password_histories password_histories_pkey; Type: CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.password_histories
    ADD CONSTRAINT password_histories_pkey PRIMARY KEY (id);


--
-- Name: patients patients_pkey; Type: CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.patients
    ADD CONSTRAINT patients_pkey PRIMARY KEY (id);


--
-- Name: permissions permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.permissions
    ADD CONSTRAINT permissions_pkey PRIMARY KEY (id);


--
-- Name: personal_access_tokens personal_access_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.personal_access_tokens
    ADD CONSTRAINT personal_access_tokens_pkey PRIMARY KEY (id);


--
-- Name: personal_access_tokens personal_access_tokens_token_unique; Type: CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.personal_access_tokens
    ADD CONSTRAINT personal_access_tokens_token_unique UNIQUE (token);


--
-- Name: plans plans_pkey; Type: CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.plans
    ADD CONSTRAINT plans_pkey PRIMARY KEY (id);


--
-- Name: ratings ratings_pkey; Type: CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.ratings
    ADD CONSTRAINT ratings_pkey PRIMARY KEY (id);


--
-- Name: role_has_permissions role_has_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.role_has_permissions
    ADD CONSTRAINT role_has_permissions_pkey PRIMARY KEY (permission_id, role_id);


--
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: team_user team_user_pkey; Type: CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.team_user
    ADD CONSTRAINT team_user_pkey PRIMARY KEY (id);


--
-- Name: teams teams_pkey; Type: CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.teams
    ADD CONSTRAINT teams_pkey PRIMARY KEY (id);


--
-- Name: trackings trackings_pkey; Type: CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.trackings
    ADD CONSTRAINT trackings_pkey PRIMARY KEY (id);


--
-- Name: treatment_requests treatment_requests_pkey; Type: CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.treatment_requests
    ADD CONSTRAINT treatment_requests_pkey PRIMARY KEY (id);


--
-- Name: treatments treatments_pkey; Type: CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.treatments
    ADD CONSTRAINT treatments_pkey PRIMARY KEY (id);


--
-- Name: two_factor_authentications two_factor_authentications_pkey; Type: CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.two_factor_authentications
    ADD CONSTRAINT two_factor_authentications_pkey PRIMARY KEY (id);


--
-- Name: users users_app_user_id_unique; Type: CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_app_user_id_unique UNIQUE (app_user_id);


--
-- Name: users users_email_unique; Type: CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: vaccines vaccines_pkey; Type: CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.vaccines
    ADD CONSTRAINT vaccines_pkey PRIMARY KEY (id);


--
-- Name: 2fa_auth_type_auth_id_index; Type: INDEX; Schema: public; Owner: idelib
--

CREATE INDEX "2fa_auth_type_auth_id_index" ON public.two_factor_authentications USING btree (authenticatable_type, authenticatable_id);


--
-- Name: activity_log_log_name_index; Type: INDEX; Schema: public; Owner: idelib
--

CREATE INDEX activity_log_log_name_index ON public.activity_log USING btree (log_name);


--
-- Name: causer; Type: INDEX; Schema: public; Owner: idelib
--

CREATE INDEX causer ON public.activity_log USING btree (causer_id, causer_type);


--
-- Name: model_has_permissions_model_id_model_type_index; Type: INDEX; Schema: public; Owner: idelib
--

CREATE INDEX model_has_permissions_model_id_model_type_index ON public.model_has_permissions USING btree (model_id, model_type);


--
-- Name: model_has_roles_model_id_model_type_index; Type: INDEX; Schema: public; Owner: idelib
--

CREATE INDEX model_has_roles_model_id_model_type_index ON public.model_has_roles USING btree (model_id, model_type);


--
-- Name: password_resets_email_index; Type: INDEX; Schema: public; Owner: idelib
--

CREATE INDEX password_resets_email_index ON public.password_resets USING btree (email);


--
-- Name: personal_access_tokens_tokenable_type_tokenable_id_index; Type: INDEX; Schema: public; Owner: idelib
--

CREATE INDEX personal_access_tokens_tokenable_type_tokenable_id_index ON public.personal_access_tokens USING btree (tokenable_type, tokenable_id);


--
-- Name: subject; Type: INDEX; Schema: public; Owner: idelib
--

CREATE INDEX subject ON public.activity_log USING btree (subject_id, subject_type);


--
-- Name: admin_subscriptions admin_subscriptions_plan_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.admin_subscriptions
    ADD CONSTRAINT admin_subscriptions_plan_id_foreign FOREIGN KEY (plan_id) REFERENCES public.plans(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: admin_subscriptions admin_subscriptions_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.admin_subscriptions
    ADD CONSTRAINT admin_subscriptions_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: announcements announcements_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.announcements
    ADD CONSTRAINT announcements_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: applications applications_announcement_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_announcement_id_foreign FOREIGN KEY (announcement_id) REFERENCES public.announcements(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: applications applications_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.applications
    ADD CONSTRAINT applications_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: appointments appointments_nurse_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.appointments
    ADD CONSTRAINT appointments_nurse_id_foreign FOREIGN KEY (nurse_id) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: covid_treatments covid_treatments_patient_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.covid_treatments
    ADD CONSTRAINT covid_treatments_patient_id_foreign FOREIGN KEY (patient_id) REFERENCES public.patients(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: covid_treatments covid_treatments_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.covid_treatments
    ADD CONSTRAINT covid_treatments_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: disponibilites disponibilites_nurse_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.disponibilites
    ADD CONSTRAINT disponibilites_nurse_id_foreign FOREIGN KEY (nurse_id) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: invitations invitations_invited_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.invitations
    ADD CONSTRAINT invitations_invited_id_foreign FOREIGN KEY (invited_id) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: invitations invitations_team_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.invitations
    ADD CONSTRAINT invitations_team_id_foreign FOREIGN KEY (team_id) REFERENCES public.teams(id) ON DELETE CASCADE;


--
-- Name: invitations invitations_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.invitations
    ADD CONSTRAINT invitations_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: mobile_subscriptions mobile_subscriptions_plan_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.mobile_subscriptions
    ADD CONSTRAINT mobile_subscriptions_plan_id_foreign FOREIGN KEY (plan_id) REFERENCES public.plans(id) ON UPDATE CASCADE;


--
-- Name: mobile_subscriptions mobile_subscriptions_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.mobile_subscriptions
    ADD CONSTRAINT mobile_subscriptions_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id) ON UPDATE CASCADE;


--
-- Name: model_has_permissions model_has_permissions_permission_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.model_has_permissions
    ADD CONSTRAINT model_has_permissions_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES public.permissions(id) ON DELETE CASCADE;


--
-- Name: model_has_roles model_has_roles_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.model_has_roles
    ADD CONSTRAINT model_has_roles_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.roles(id) ON DELETE CASCADE;


--
-- Name: passages passages_patient_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.passages
    ADD CONSTRAINT passages_patient_id_foreign FOREIGN KEY (patient_id) REFERENCES public.patients(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: passages passages_treatment_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.passages
    ADD CONSTRAINT passages_treatment_id_foreign FOREIGN KEY (treatment_id) REFERENCES public.treatments(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: patients patients_nurse_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.patients
    ADD CONSTRAINT patients_nurse_id_foreign FOREIGN KEY (nurse_id) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: patients patients_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.patients
    ADD CONSTRAINT patients_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: permissions permissions_parent_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.permissions
    ADD CONSTRAINT permissions_parent_id_foreign FOREIGN KEY (parent_id) REFERENCES public.permissions(id) ON DELETE CASCADE;


--
-- Name: ratings ratings_nurse_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.ratings
    ADD CONSTRAINT ratings_nurse_id_foreign FOREIGN KEY (nurse_id) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: ratings ratings_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.ratings
    ADD CONSTRAINT ratings_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: role_has_permissions role_has_permissions_permission_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.role_has_permissions
    ADD CONSTRAINT role_has_permissions_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES public.permissions(id) ON DELETE CASCADE;


--
-- Name: role_has_permissions role_has_permissions_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.role_has_permissions
    ADD CONSTRAINT role_has_permissions_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.roles(id) ON DELETE CASCADE;


--
-- Name: team_user team_user_team_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.team_user
    ADD CONSTRAINT team_user_team_id_foreign FOREIGN KEY (team_id) REFERENCES public.teams(id) ON DELETE CASCADE;


--
-- Name: team_user team_user_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.team_user
    ADD CONSTRAINT team_user_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: teams teams_admin_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.teams
    ADD CONSTRAINT teams_admin_id_foreign FOREIGN KEY (admin_id) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: trackings trackings_patient_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.trackings
    ADD CONSTRAINT trackings_patient_id_foreign FOREIGN KEY (patient_id) REFERENCES public.patients(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: trackings trackings_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.trackings
    ADD CONSTRAINT trackings_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: treatment_requests treatment_requests_nurse_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.treatment_requests
    ADD CONSTRAINT treatment_requests_nurse_id_foreign FOREIGN KEY (nurse_id) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: treatment_requests treatment_requests_patient_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.treatment_requests
    ADD CONSTRAINT treatment_requests_patient_id_foreign FOREIGN KEY (patient_id) REFERENCES public.patients(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: treatments treatments_nurse_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.treatments
    ADD CONSTRAINT treatments_nurse_id_foreign FOREIGN KEY (nurse_id) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: treatments treatments_patient_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.treatments
    ADD CONSTRAINT treatments_patient_id_foreign FOREIGN KEY (patient_id) REFERENCES public.patients(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: users users_created_by_foreign; Type: FK CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_created_by_foreign FOREIGN KEY (created_by) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: vaccines vaccines_patient_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.vaccines
    ADD CONSTRAINT vaccines_patient_id_foreign FOREIGN KEY (patient_id) REFERENCES public.patients(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: vaccines vaccines_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: idelib
--

ALTER TABLE ONLY public.vaccines
    ADD CONSTRAINT vaccines_user_id_foreign FOREIGN KEY (user_id) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

