/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('../bootstrap');
require('../plugins');

import Vue from 'vue';

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});




function addAutocomplete(idSearchBox, idDropdownMenu, idsearchBoxContainer, idlat, idlng) {
    var timer = null;
    const searchBox = document.getElementById(idSearchBox); //'searchbox');
    const dropdownMenu = document.getElementById(idDropdownMenu);//'dropdown-menu');
    const searchBoxContainer = document.getElementById(idsearchBoxContainer);//'searchbox-container');
    const lat = document.getElementById(idlat);//'searchbox-container');
    const lng = document.getElementById(idlng);//'searchbox-container');


    searchBox.addEventListener("keyup", function (event) {

        clearInterval(timer);
        timer = setTimeout(function () {
            var sr = event.target.value;
            if (!sr) return; //Do nothing for empty value
            searchBoxContainer.classList.add("control", "is-loading");

            const request = new Request('/searchAddress?adresse=' + sr);
            fetch(request)
                .then((response) => response.json())
                .then((data) => {
                    if (searchBox.value) { //src not cleaned, backspace removed
                        dropdownMenu.replaceChildren(searchResult(data,dropdownMenu, searchBoxContainer, searchBox, lng, lat));
                    }
                    searchBoxContainer.classList.remove("is-loading");
                });

        }, 500);

    });

    //keep checking for an empty search box every 5 seconds
    setInterval(function () {
        if (!searchBox.value) { //empty search box
            clearDropdown(dropdownMenu, searchBoxContainer)
        }
    }, 500);

}
function searchResult(result, dropdownMenu, searchBoxContainer, searchBox, lng, lat) {

    const ul = document.createElement('ul')
    ul.classList.add('box', 'mt-1');

    const loc = result;
    loc.forEach((x) => {
        if (!x) return;
        ul.appendChild(createListItem(x, dropdownMenu, searchBoxContainer, searchBox, lng, lat))
    })

    return ul;
}

function createListItem(x, dropdownMenu, searchBoxContainer, searchBox, lng, lat) {

    const li = document.createElement('li')
    li.classList.add('py-1'); //, 'pl-5', 'is-capitalized')
    //li.dataset.lat = x.lat;
    //li.dataset.lon = x.lon;
    //li.dataset.tz = x.tz;
    li.innerText = x.display

    const selectEvent = function (event) {
        event.stopPropagation(); //parent elements will not have the same event
        const li = event.target
        clearDropdown(dropdownMenu, searchBoxContainer);
        searchBox.value = x.display;
        lng.value=x.lng;
        lat.value=x.lat;
    };

    li.addEventListener('click', selectEvent)
    li.addEventListener('touchstart', selectEvent)

    return li
}


addAutocomplete('lib_searchbox', 'lib_dropdown-menu', 'lib_searchbox-container', 'lib_lat', 'lib_lng');
addAutocomplete('remp_searchbox', 'remp_dropdown-menu', 'remp_searchbox-container', 'remp_lat', 'remp_lng');
addAutocomplete('patient_searchbox', 'patient_dropdown-menu', 'patient_searchbox-container', 'patient_lat', 'patient_lng');


function clearDropdown(dropdownMenu, searchBoxContainer) {
    //dropdownMenu.replaceChildren(); https://stackoverflow.com/a/65413839/1097600
    dropdownMenu.innerHTML = '';
    searchBoxContainer.classList.remove("is-loading");
}


