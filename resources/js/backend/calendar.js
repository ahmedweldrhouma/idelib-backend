import {Calendar} from '@fullcalendar/core';
import interactionPlugin from '@fullcalendar/interaction';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import listPlugin from '@fullcalendar/list';
import itLocale from '@fullcalendar/core/locales/fr';
import bootstrap5Plugin from '@fullcalendar/bootstrap5';
import moment from "moment";

var appointmentForm = $('#edit_appointment');
document.addEventListener('DOMContentLoaded', function () {
    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name="_token"]').attr('content')
            }
        });
    });

    if ($('main').hasClass('calendar-page')) {
        loadCalendar();
    }
})

function loadCalendar() {
    const calendarEl = document.getElementById("calendar");
    var type = $(location).attr("href").split('/').pop();
    let url = "/admin/calendar/" + type + "/all";
    var info_data = null;

    //Filtre des users ( planning )
    $('#search_users').click(function (e) {
        calendar.refetchEvents();
    });
    appointmentForm.on('submit', function (e) {
        e.preventDefault();
        update_fn(appointmentForm.find('[name="start_date"]').val() + ' ' + appointmentForm.find('[name="start_time"]').val(), appointmentForm.find('[name="end_date"]').val() + ' ' + appointmentForm.find('[name="end_time"]').val(), info_data, info_data.event.id);
        e.stopPropagation();
    })
    appointmentForm.find('.remove').on('click', function (e) {
        e.preventDefault();
        appointmentForm.closest('.modal').modal('hide');
        Swal.fire({
            title: 'Êtes-vous sûr de bien vouloir supprimer ce rendez-vous ?',
            showCancelButton: true,
            confirmButtonText: 'Confirmer',
            cancelButtonText: 'Annuler',
            icon: 'warning'
        }).then((result) => {
            if (result.isConfirmed) {
                delete_appointment(info_data);
            }
        });
    })
    //Filtre des users ( tournée )
    $('#searchtour_users').click(function (e) {
        calendar.refetchEvents();
    });
    $('#edit-treatment').click(function (e){
        $('#detailsModal').modal('hide');
        $('#editModal').modal('show');
    })
    $('#destroy_dispo').click(function (e) {
        $('#detailsDisponibility').modal('hide');
        Swal.fire({
            /*Delete Disponibility*/
            title: 'Êtes-vous sûr de bien vouloir supprimer cet disponibilité ?',
            showCancelButton: true,
            confirmButtonText: 'Confirmer',
            cancelButtonText: 'Annuler',
            icon: 'warning'

        }).then((result) => {
            if (result.isConfirmed) {
                delete_dispo(info_data);
            }
        });
    })
    $('#update_dispo').click(function (e) {
        e.preventDefault();
        if (info_data != null) {
            let start_time = $('#periode_travail').val().substr(0, 8);
            let end_time = $('#periode_travail').val().substr(9);
            let nurse_id = $('#edit_nurse_id').val();
            update_disponibilite($('#datestart').val(), $('#datefinish').val(), start_time, end_time, info_data, "datetime",nurse_id);
        } else {
            console.log("There is no an event ! ");
        }
    });

    // Delete THE TREATMENT
    $('#treatment_close').click(function (e) {
        $('#detailsModal').modal('hide');
        Swal.fire({
            /*Delete Event*/
            title: 'Êtes-vous sûr de bien vouloir supprimer ce traitement ?',
            showCancelButton: true,
            confirmButtonText: 'Confirmer',
            cancelButtonText: 'Annuler',
            icon: 'warning'

        }).then((result) => {
            if (result.isConfirmed) {
                delete_fn(info_data);
            }
        });
    })

    // Delete Vaccin Or Covid
    $('#vc_close').click(function (e) {
        $('#VCModal').modal('hide');
        Swal.fire({
            /*Delete Event*/
            title: 'Êtes-vous sûr de bien vouloir supprimer ce traitement ?',
            showCancelButton: true,
            confirmButtonText: 'Confirmer',
            cancelButtonText: 'Annuler',
            icon: 'warning'

        }).then((result) => {
            if (result.isConfirmed) {
                delete_fn(info_data);
            }
        });
    })

    //Update treatment
    $('#treatment_update').click(function (e) {
        e.preventDefault();
        console.log(info_data);
        if (info_data != null) {
            update_fn($('#start_time').val(), $('#finish_time').val(), info_data, info_data.event.id);
        }
    });

    //Update Vaccin or covid
    $('#vc_update').click(function (e) {
        e.preventDefault();
        console.log(info_data.title);
        if (info_data != null) {
            update_fn($('#vc_start_time').val(), null, info_data, info_data.event.id);
        }
    });
    let calendar = new Calendar(calendarEl, {
        locale: itLocale,
        plugins: [dayGridPlugin, timeGridPlugin, listPlugin, bootstrap5Plugin, interactionPlugin],
        initialView: 'dayGridMonth',
        headerToolbar: {
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,listWeek'
        },
        views: {
            dayGridMonth: {
                dayMaxEventRows: 5
            }
        },
        eventLimit: true,
        eventStartEditable: false,
        eventDurationEditable: true,
        displayEventTime: true,
        slotMinTime: "04:00",
        slotMaxTime: "23:00",
        events: {
            url: url,
            method: 'POST',
            extraParams: function () {
                let filters = {};
                if (type == "planning") {
                    if ($('#filtre_user').val() !== "all") {
                        filters.user_id = $('#filtre_user').val();
                    }
                    ;
                }
                if (type == "tour") {
                    if ($('#filtretour_user').val() !== "all") {
                        filters.user_id = $('#filtretour_user').val();
                    }
                    if ($('#filtre_trait').val() !== "all") {
                        filters.filtre_trait = $('#filtre_trait').val();
                    }
                }
                filters._token = $('meta[name="csrf-token"]').attr('content');
                return filters;
            },
        },
        loading: function (isLoading) {
            if (isLoading) {
                $('#calendar').addClass('spinner');
                $('#calendar .fc-view-harness').hide();
                // $('.fc-view').addClass('opacity-0');
                // $('#loading').show();
            } else {
                $('#calendar').removeClass('spinner');
                $('#calendar .fc-view-harness').show();
                // $('.fc-view').removeClass('opacity-0');
                // $('#loading').hide();
            }
        },

        eventContent: function (info) {
            if (type == 'planning') {
                let arrayOfDomNodes = []
                // title event
                let titleEvent = document.createElement('span')
                if (info.event.title) {
                    titleEvent.innerHTML = info.event.title + '<br>' + info.event.extendedProps.start_time + ' - ' + info.event.extendedProps.end_time
                    titleEvent.classList = "fc-event-title fc-sticky"
                }
                // image event
                let imgEventWrap = document.createElement('span')
                if (info.event.extendedProps.image_url) {
                    let imgEvent = '<img src="' + info.event.extendedProps.image_url + '" height="35" width="35" style="border-radius: 50%">' + ' '
                    imgEventWrap.classList = "fc-event-img "
                    imgEventWrap.innerHTML = imgEvent;
                }
                arrayOfDomNodes = [imgEventWrap, titleEvent]

                return {domNodes: arrayOfDomNodes}
            } else {

                let ColorEvent = document.createElement('div')
                ColorEvent.innerHTML = '<div id="cercle" color="info.event.backgroundColor"> </div>'
               // ColorEvent.innerHTML = '<div style=" border: 1px solid background : ' + info.event.backgroundColor + ';">  </div>'
                ColorEvent.classList = "fc-event-color"

                let arrayOfDomNodes = []
                // Name & Picture of Patient
                let titleEvent = document.createElement('span')
                if (info.event.title != "Appointment") {
                    titleEvent.innerHTML = '<img src="' + info.event.extendedProps.patient_img + '" height="25" width="25" style="border-radius: 50% ;border: 2px solid ' + info.event.backgroundColor + ';">'+(info.event.extendedProps.absent?' <span class="absent" >A</span>' : ' ' ) + ' ' + info.event.extendedProps.patient_name
                    titleEvent.classList = "fc-event-title fc-sticky"
                } else {
                    titleEvent.innerHTML = info.event.extendedProps.title
                    titleEvent.classList = "fc-event-title fc-sticky"
                }
                // image event

                arrayOfDomNodes = [ColorEvent, titleEvent]

                return {domNodes: arrayOfDomNodes}

            }
        },

        eventClick: function (info, jsEvent, view, calEvent) {

            if (type == "tour") {
                //On traite les traitements dans la tournée
                let datestart = '';
                let dateend = '';
                // Fixer format de date selon le type ( soin , vaccin , covid .. )
                $('.patient_name').text(info.event.extendedProps.patient_name);
                $('.patient_type').text(info.event.extendedProps.patient_type);
                $('.patient_img').attr('src', info.event.extendedProps.patient_img);
                $('.patient_mail').text(info.event.extendedProps.patient_mail).attr('href', 'mailto:' + info.event.extendedProps.patient_mail);
                $('.patient_phone').text(info.event.extendedProps.patient_phone);
                if (info.event.title.startsWith('Soin')) {
                    datestart = new Date(info.event.extendedProps.original_start).toISOString().substring(0, 10);
                    dateend = info.event.end !== null ? new Date(info.event.extendedProps.original_end).toISOString().substring(0, 10) : datestart;
                    $('#start_time').val(datestart);
                    $('#finish_time').val(dateend);
                    info_data = info;
                    $('#trait_title').text(info.event.title);
                    $('#nurse_name').text(info.event.extendedProps.nurse_name);
                    $('#creation_date').text(info.event.extendedProps.creation_date);
                    $('#start_date').text(info.event.extendedProps.start_date);
                    $('#end_date').text(info.event.extendedProps.end_date);
                    $('#period').text(info.event.extendedProps.period);
                    $('#show-patient').attr('href',info.event.extendedProps.show_link);
                    $('#edit-patient').attr('href',info.event.extendedProps.edit_link);
                    $('#heb_frequency').html("");
                    info.event.extendedProps.heb_frequency.forEach(function (item){
                        $('#heb_frequency').append(`<span class="badge badge-info mr-2 text-capitalize">${item}</span>`);
                    });
                    $('#passages').html("");
                    info.event.extendedProps.passages.forEach(function (item,index){
                        $('#passages').append(`<div class="bg-flickr m-2 p-2 text-capitalize"><strong>Passage ${index+1}:</strong> ${item}</div>`);
                    });
                    $('#detailsModal').modal();
                } else if (info.event.title === "Appointment") {
                    appointmentForm.find('[name="start_date"]').val(info.event.extendedProps.original_start);
                    appointmentForm.find('[name="end_date"]').val(info.event.extendedProps.original_end);
                    appointmentForm.find('[name="start_time"]').val(info.event.extendedProps.original_start_time);
                    appointmentForm.find('[name="end_time"]').val(info.event.extendedProps.original_end_time);
                    info_data = info;
                    appointmentForm.closest('.modal').modal('show');
                } else {
                    info_data = info;
                    datestart = info.event.start.toISOString().substring(0, 10);
                    $('#vc_start_time').val(datestart);
                    //Récuperer les infos du patient
                    $('#vc_title').val(info.event.title);
                    $('#vc_patient_name').val(info.event.extendedProps.patient_first_name);
                    $('#vc_patient_lname').val(info.event.extendedProps.patient_last_name);
                    $('#VCModal').modal();
                }
            } else {
                //On traite les disponibilites dans le planning
                let datestart = new Date(info.event.start).toISOString().substring(0, 10);
                let datefinish = new Date(info.event.end).toISOString().substring(0, 10);
                var start_time = info.event.start.toLocaleTimeString();
                var end_time = info.event.end !== null ? info.event.end.toLocaleTimeString() : start_time;
                let image = info.event.extendedProps.image;
                $('#periode_travail').val(start_time + '-' + end_time);
                $('#datestart').val(datestart);
                $('#datefinish').val(datefinish);
                $('#detailsDisponibility').modal();
                info_data = info;
            }
        },
        editable: true,
        droppable: true,
        eventResize: function (info) {
            if (type == "planning") {
                Swal.fire({
                    title: 'Êtes-vous sûr de mettre à jour votre disponibilité ?',
                    showCancelButton: true,
                    confirmButtonText: 'Confirmer',
                    cancelButtonText: 'Annuler',
                    icon: 'warning'

                }).then(
                    (result) => {
                        if (result.isConfirmed) {
                            var hours_end = info.event.end.getHours();
                            var hours_start = info.event.start.getHours();
                            var new_start_time = hours_start + ":00" + ":00";
                            var new_end_time = hours_end + ":00" + ":00";
                            update_disponibilite(Date.parse(info.event.start), Date.parse(info.event.end), new_start_time, new_end_time, info, "time");

                        } else {
                            info.revert();
                        }
                    })

            } else if (type == "tour") {
                Swal.fire({
                    title: 'Êtes-vous sûr de mettre à jour ce ' + (info.event.title === "Appointment" ? "Rendez-vous ?" : "Traitement ?"),
                    showCancelButton: true,
                    confirmButtonText: 'Confirmer',
                    cancelButtonText: 'Annuler',
                    icon: 'warning'

                }).then(
                    (result) => {
                        if (result.isConfirmed) {
                            update_fn(info.event.extendedProps.original_start + ' ' + moment(Date.parse(info.event.start)).format('HH:mm'), info.event.extendedProps.original_end + ' ' + moment(Date.parse(info.event.end)).format('HH:mm'), info, info.event.id);
                        } else {
                            info.revert();
                        }
                    })
            }
        },
        eventDrop: function (info) {
            if (type == "tour") {
                Swal.fire({
                    title: 'Êtes-vous sûr de mettre à jour votre traitement ?',
                    showCancelButton: true,
                    confirmButtonText: 'Confirmer',
                    cancelButtonText: 'Annuler',
                    icon: 'warning'

                }).then(
                    async (result) => {
                        if (result.isConfirmed) {
                            if (info.event.title.startsWith('Soin')) {
                                console.log(info.event.start);
                                console.log(info.event.end);
                                update_fn(Date.parse(info.event.start), Date.parse(info.event.end), info, info.event.id);
                            } else {
                                update_fn(Date.parse(info.event.start), info.event.extendedProps.end_time, info, info.event.id);
                            }

                        } else {
                            info.revert();
                        }
                    })
            }
            if (type == "planning") {
                Swal.fire({
                    title: 'Êtes-vous sûr de mettre à jour votre disponibilité ?',
                    showCancelButton: true,
                    confirmButtonText: 'Confirmer',
                    cancelButtonText: 'Annuler',
                    icon: 'warning'

                }).then(
                     (result) => {
                        if (result.isConfirmed) {

                            update_disponibilite(Date.parse(info.event.start), Date.parse(info.event.end), info.event.extendedProps.start_time, info.event.extendedProps.end_time, info, "date");

                        } else {
                            info.revert();
                        }
                    })

            }
        }
    })

    function update_disponibilite(startdate, enddate, starttime, endtime, info, time_or_date,nurse_id=null) {
        if (time_or_date == "date") {
            var data = {
                "_token": $('meta[name="csrf-token"]').attr('content'),
                id: info.event.id,
                start_date: moment(startdate).format('YYYY-MM-DD'),
                end_date: moment(enddate).format('YYYY-MM-DD'),
                nurse_id: nurse_id,
            };
        } else if (time_or_date == "time") {
            var data = {
                "_token": $('meta[name="csrf-token"]').attr('content'),
                id: info.event.id,
                start_time: starttime,
                end_time: endtime,
                nurse_id: nurse_id,
            };
        } else if (time_or_date = "datetime") {
            var data = {
                "_token": $('meta[name="csrf-token"]').attr('content'),
                id: info.event.id,
                start_date: moment(startdate).format('YYYY-MM-DD'),
                end_date: moment(enddate).format('YYYY-MM-DD'),
                start_time: starttime,
                end_time: endtime,
                nurse_id: nurse_id,
            };
        }
        $.ajax({
            url: "/admin/disponibility/update",
            method: 'post',
            data:data,
            success: function (data) {
                Swal.fire({
                    icon: 'success',
                    title: info.event.title + " a était changé avec succés"
                });
                calendar.refetchEvents();
                $('#detailsDisponibility').modal('hide');
            },
            error: function (e) {
                Swal.fire({
                    icon: 'error',
                    title: e.responseJSON.message
                });
                info.revert();

            }
        });
    }

    function update_fn(start_time, finish_time, info, id) {
        let url = '';
        var titre = info.event.title;
        if (titre.startsWith('Soin')) {
            url = "update_treat";
        } else if (titre == 'Vaccin') {
            url = "update_vacc";
        } else if (titre == 'Appointment') {
            url = "update_appointment";
        } else if (titre == 'Visite à domicile' || titre == ('Test antigénique') || titre == ('Test PCR')) {
            url = "update_cov";
        }
        var data = {
            "_token": $('meta[name="csrf-token"]').attr('content'),
            id: id,
            start_at: moment(start_time).format('YYYY-MM-DD HH:mm')
        };
        data.end_at = finish_time != null ? moment(finish_time).format('YYYY-MM-DD HH:mm') : data.start_at
        $.ajax({
            url: url,
            method: 'post',
            data,
            success: function (data) {
                Swal.fire({
                    icon: 'success',
                    title: info.event.title + " a était changer avec succés"
                });
                calendar.refetchEvents();
                $('#detailsModal').modal('hide');
                $('#VCModal').modal('hide');
            },
            error: function () {
                Swal.fire({
                    icon: 'error',
                    title: "Un erreur est survenu lors de mise a jour de " + info.event.title
                });
                info.revert();
            }
        });

    }

    function delete_fn(info) {
        let url = '';
        var titre = info.event.title;
        if (titre.startsWith('Soin')) {
            url = "delete-event-treat";
        } else if (titre == ('Vaccin')) {
            url = "delete-event-vacc";
        } else if (titre == ('Test antigénique') || titre == ('Visite à domicile') || titre == ('Test PCR')) {
            url = "delete-event";
        }

        var token = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: url,
            method: 'post',
            data: "&id=" + info.event.id + '&_token=' + token,

            success: function (response) {
                calendar.refetchEvents();
            }
        });


    }

    function delete_dispo(info) {
        var token = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: "/admin/disponibility/destroy",
            method: 'POST',
            data: "&id=" + info.event.id + '&_token=' + token,

            success: function (response) {
                info.event.remove();
            }
        });
    }

    function delete_appointment(info) {
        var token = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: "/admin/calendar/remove_appointment",
            method: 'POST',
            data: "&id=" + info.event.id + '&_token=' + token,
            success: function (response) {
                calendar.refetchEvents();
            }
        });
    }

    calendar.render();
}

