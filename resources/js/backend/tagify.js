import Tagify from '@yaireo/tagify'

// The DOM element you wish to replace with Tagify
var input = document.querySelector('.mails');

// initialize Tagify on the above input node reference
new Tagify(input)