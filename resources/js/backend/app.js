import 'alpinejs'

window.$ = window.jQuery = require('jquery');
window.Swal = require('sweetalert2');
window.Tagify = require('@yaireo/tagify');
//window.Select2 = require('select2');
// CoreUI
require('@coreui/coreui');

// Boilerplate
require('../plugins');
require('./calendar');
require('./admin_subscription');
require('./tagify');
require('select2');
require('bootstrap');
