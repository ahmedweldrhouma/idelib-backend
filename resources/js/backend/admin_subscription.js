import 'jquery-ui';
import 'jquery-ui/ui/widgets/autocomplete.js';

document.addEventListener('DOMContentLoaded', function () {
    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name="_token"]').attr('content')
            }
        });
    });

    if ($('main').hasClass('create-admin-sub-page')) {
        
        $( "#user_name" ).on('focus', function(){
            search($( "#user_name" ), '/admin/auth/user/all', 'user')
        })

    }
})

function search(el, url, type) {
    console.log('here ', el);
    el.on("input", function(e) { 
        if(e.originalEvent.inputType == undefined || e.originalEvent.inputType == 'deleteContentBackward'){
            if($('#user-id').length > 0){
                $('#user-id').remove();
            }
        }
    });

    var users = [];
    $.ajax(
        url,
    ).done(function (res) {
        users = res;
    });
    
    el.autocomplete({
        source: function(request, response) {
            var data = users
            
            var datamap = data.map(function(i) {
              return {
                label: i.label,
                value: i.value,
                id: i.id
              }
            });
            
            var key = request.term;
            
            datamap = datamap.filter(function(i) {
              return i.label.toLowerCase().indexOf(key.toLowerCase()) >= 0;
            });
            console.log('datamap => ', datamap);
            response(datamap);
          },
        select: function( event, ui ) 
        {
            // console.log('ui => ', ui);
            if(type == "user"){
                if($('user-id').length == 0){
                    $('<input>').attr({
                        type: 'hidden',
                        id: 'user-id',
                        name: 'user_id',
                        value: ui.item.id
                    }).appendTo('#user-col')
                }else{
                    $('user-id').val(ui.item.id);
                }
            }

        },
        minLength: 0,
    });
}