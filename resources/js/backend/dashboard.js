window.$ = window.jQuery = require('jquery');
window.Highcharts = require('highcharts');

let chart = document.getElementById('patients-pie');
let timeline = document.getElementById('timeline');
if (chart) {
    Highcharts.chart('patients-pie', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        credits: false,
        title: {
            text: 'Vos patients'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.number}%</b>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                }
            }
        },
        series: [{
            name: 'Patient',
            colorByPoint: true,
            data: Object.values(stats)
        }]
    });
}
if (timeline) {
    function showSpinner() {
        document.getElementById("loading-spinner").style.display = "block";
    }
    function hideSpinner() {
        document.getElementById("loading-spinner").style.display = "none";
    }
    var menu = document.getElementById('week-menu');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(document).ready(function (){
        menu.querySelector('.nav-link.active').dispatchEvent(new Event('click'));
    })
    menu.querySelectorAll('.nav-link').forEach(item => {
        item.addEventListener('click', function (e) {
            e.preventDefault();
            menu.querySelector('.nav-link.active').classList.remove('active');
            item.classList.add('active');
            timeline.innerHTML = "";
            showSpinner();
            getEvents(item.dataset.date);
        })
    })
    function getEvents(date) {
        $.ajax({
            url: 'dashboard/events',
            method: 'POST',
            dataType: 'JSON',
            data: {
                'date': date,
            },
            success: function (data) {
                timeline.innerHTML = "";
                if (data.length === 0) {
                    timeline.innerHTML = "<div style='text-align: center;height: 40vh;display: flex;justify-content: center;align-items: center;font-size: 20px;font-weight: 600;'>Pas des evénements pour aujourd'hui</div>";
                   return;
                }
                $('.events_nbr').text(data.length);
                data.forEach(item => {
                    if (item.type === "treatment"){
                        if (item.absent){
                            timeline.innerHTML += `<span id="status-badge" class="badge" style="display:   !important ">Absent</span>`
                        }
                        timeline.innerHTML += `<div class="d-flex align-items-center mb-6">
                                <span data-kt-element="bullet" class="bullet bullet-vertical d-flex align-items-center min-h-70px mh-100 me-4 bg-warning"></span>
                                <div class="flex-grow-1 me-5" id="myDiv">
                                    <div class="text-gray-800 fw-semibold fs-2">${item.start}

                                    </div>
                                    <div class="text-gray-700 fw-semibold fs-6">${item.title}
                                    </div>
                                    <div class="text-gray-400 fw-semibold fs-7">Pour le patient :
                                        <a href="${item.patient_url}" class="text-primary opacity-75-hover fw-semibold">${item.patient_name}</a>

                                    </div>
                                </div>
                            </div
                            ><hr style="border-color: lightgrey">`;
                        }else if(item.type === "appointment"){
                        timeline.innerHTML += `<div class="d-flex align-items-center mb-6">
                                <span data-kt-element="bullet" class="bullet bullet-vertical d-flex align-items-center min-h-70px mh-100 me-4 bg-primary"></span>
                                <div class="flex-grow-1 me-5">
                                    <div class="text-gray-800 fw-semibold fs-2">${item.start} - ${item.end}</div>
                                    <div class="text-gray-700 fw-semibold fs-6">${item.title}
                                    </div>
                                    <div class="text-gray-400 fw-semibold fs-7"><strong>Note : </strong>${item.note}</div>
                                </div>
                            </div><hr style="border-color: lightgrey">`;
                    }
                })
            },
            error: function (data) {
                timeline.innerHTML = "";
            },
            complete: function(data) {
                // Hide the spinner element after the request completes
                hideSpinner()
            },
        })
    }
}

