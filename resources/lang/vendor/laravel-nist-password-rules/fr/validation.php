<?php

return [
    // The :attribute can not contain the word \':word\'.
    'can-not-contain-word'              => 'Le :attribute ne peut pas contenir le mot \':word\'.',

    // The :attribute can not be similar to the word \':word\'.
    'can-not-be-similar-to-word'        => 'Le :attribute ne peut pas être similaire au mot \':word\'.',

    // The :attribute was found in a third party data breach, and can not be used.
    'found-in-data-breach'              => 'Le :attribute a été trouvé dans une violation de données d\'un tiers, et ne peut pas être utilisé.',

    // The :attribute can not be a dictionary word.
    'can-not-be-dictionary-word'        => 'Le :attribute ne peut pas être un mot du dictionnaire.',

    // The :attribute can not be repetitive characters.
    'can-not-be-repetitive-characters'  => 'Le :attribute ne peut pas être composé uniquement de caractères répétitifs.',

    // The :attribute can not be sequential characters.
    'can-not-be-sequential-characters'  => 'Le :attribute ne peut pas être composé uniquement de caractères séquentiels.',
];


