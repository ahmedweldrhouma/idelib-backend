@extends('frontend.layouts.app')

@section('content')
    <div id="landing-container">
        <div class="page-header">
            <div class="container">
                <div class="title-box">
                </div>
            </div>
            <div class="shape-bottom">
                <img src="/images/shapes/price-shape.svg" alt="shape" class="bottom-shape img-fluid">
            </div>
        </div>
        <div class="container-row2 row justify-content-center mx-0">
            <div class="col-md-1  px-md-1 ">
                <div class="card h-100 shadow text-center">
                        <img src="/img/logo-idelib.png" class="margincenter" width="125"/>
                </div>
            </div>
        </div>
        <div class="container-row2 row justify-content-center mx-0">
            <div class="col-md-3 pt-lg-4  text-center" >
                <div class="card h-100 shadow">
                    <div class="card-body">
                        <h2 class="thx">Merci pour votre abonnement !
                            <!-- Solution de gestion de patients, d'organisation et de simplification de l'activité libéral infirmier. -->
                        </h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-row2 row justify-content-center mx-0">

            <div class="col-md-5 pt-lg-4 px-md-3 ">
                <div class="card h-100 shadow">
                    <div class="card-body">
                        <div class="login-form">
                            <div class="login-page">
                                <div class="col-md-12">
                                    <div class="container-fluid">
                                        <!-- <a href="index.html" class="res-logo"><img src="/logo/logo-idelib.png" alt="Logo"/></a> -->
                                        <div class="login-form">

                                            <div class="login-form-head">
                                                <a href="/"><img src="/logo/logo-idelib.png" alt=""
                                                                 class="logo-landing2"></a>

                                            </div>
                                            <div class="col-md-12">
                                                <div class="container-fluid">
                                                    <!-- <a href="index.html" class="res-logo"><img src="/logo/logo-idelib.png" alt="Logo"/></a> -->
                                                    <div class="login-form">

                                                        <div class="login-form-head borderB">


                                                            <p class="title-wording text-center">Téléchargez
                                                                l'application idelib </p>
                                                        </div>
                                                        <div class="download">
                                                            <a href="https://apps.apple.com/fr/app/idelib/id1610761079"
                                                               target="_blank"><img src="/images/pictos/apple-store.png"
                                                                                    alt=""></a>
                                                            <a href="https://play.google.com/store/apps/details?id=com.idelibapp.app"
                                                               target="_blank"><img
                                                                    src="/images/pictos/google-store.png"
                                                                    alt=""></a>
                                                        </div>

                                                    </div>

                                                    <div class="footer">
                                                        <div class="container">
                                                            <div class="socials">

                                                                <ul class="list-inline mb-0 share-buttons text-center">
                                                                    <li class="list-inline-item">
                                                                        <a class="btn btn-sm" target="_blank"
                                                                           href="https://www.facebook.com/profile.php?id=100083275480272">
                                                                            <span
                                                                                class="fab fa-facebook-f icoconf"></span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="list-inline-item">
                                                                        <a class="btn btn-sm" target="_blank"
                                                                           href="https://www.instagram.com/idelib_infirmiers_/">
                                                                            <span
                                                                                class="fab fa-instagram icoconf"></span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="list-inline-item">
                                                                        <a class="btn btn-sm" target="_blank"
                                                                           href="https://www.linkedin.com/company/85373157/">
                                                                            <span
                                                                                class="fab fa-linkedin icoconf"></span>
                                                                        </a>
                                                                    </li>
                                                                    <li class="list-inline-item">
                                                                        <a class="btn btn-sm" target="_blank"
                                                                           href="https://www.youtube.com/channel/UC90sDyhZDWjQ5sxJyMX2IeQ/featured">
                                                                            <span class="fab fa-youtube icoconf"></span>
                                                                        </a>
                                                                    </li>
                                                                </ul>

                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>



                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-row2 row justify-content-center mx-0">
            <div class="col-md-2 pt-lg-4 px-md-3 text-center ">
                <div class="card h-100 shadow">
                    <div class="card-body btnCard">
                        <a href="/" class="btn other-btn2">Retour à
                            l'accueil</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
