@extends('backend.layouts.app')
<!-- lzm nraj3ha frontend.layouts... -->
@section('title', __('My Account'))

@section('content')
    <div class="row gutters-sm" id="profile-card">
        <div class="col-md-12 mb-3 ">
            <div class="card">
                <div class="card-body pb-0">

                    <div class="d-flex pb-3 align-items-center">
                        <img src="{{ $logged_in_user->avatar }}" alt="Admin" width="100" class="imgg">

                        <div class="d-flex flex-column mt-2">
                            <h5 class="mb-0 ml-3"><strong id="patient_name"> {{ $logged_in_user->name }} </strong> <span
                                    class="badge badge-primary ml-2"
                                    id="patient_type"> {{ $logged_in_user->type }} </span>
                            </h5>
                            <br>
                            <div class="d-flex flex-wrap fw-semibold fs-8 mb-4 pe-2 ml-3">
                                <div class="d-flex align-items-center gap-2">
                                <span class="svg-icon svg-icon-2">
                                    <svg width="22" height="22" viewBox="0 0 24 24" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path opacity="0.2"
                                              d="M21 19H3C2.4 19 2 18.6 2 18V6C2 5.4 2.4 5 3 5H21C21.6 5 22 5.4 22 6V18C22 18.6 21.6 19 21 19Z"
                                              fill="currentColor"></path>
                                        <path
                                            d="M21 5H2.99999C2.69999 5 2.49999 5.10005 2.29999 5.30005L11.2 13.3C11.7 13.7 12.4 13.7 12.8 13.3L21.7 5.30005C21.5 5.10005 21.3 5 21 5Z"
                                            fill="currentColor"></path>
                                    </svg>
                                </span>
                                    <a href="mailto" class="text-muted text-hover-primary ml-1" id="patient_mail"> {{
                                    $logged_in_user->email }} </a>
                                </div>
                                <br>
                                <div class="d-flex align-items-center gap-2 ml-3">
                                <span class="svg-icon svg-icon-2">
                                    <svg width="22" height="22" viewBox="0 0 24 24" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M5 20H19V21C19 21.6 18.6 22 18 22H6C5.4 22 5 21.6 5 21V20ZM19 3C19 2.4 18.6 2 18 2H6C5.4 2 5 2.4 5 3V4H19V3Z"
                                            fill="currentColor"></path>
                                        <path opacity="0.2" d="M19 4H5V20H19V4Z" fill="currentColor"></path>
                                    </svg>
                                </span>
                                    <a href="javascript:" class="text-muted text-hover-primary ml-1" id="patient_phone"> {{
                                    $logged_in_user->phone }}</a>
                                </div>
                            </div>
                            @if ($logged_in_user->isAdmin() || $logged_in_user->isNurse())
                                <div class="d-flex flex-wrap">
                                    <div class="d-flex flex-wrap flex-stack">
                                        <div class="d-flex flex-column flex-grow-1 pe-8">
                                            <div class="d-flex flex-wrap">
                                                <div class="border-dashed rounded min-w-115px py-3 px-4 me-6 mb-3">
                                                    <div class="d-flex align-items-center justify-content-center">

                                                        <div class="fs-2 fw-bold counted" data-kt-countup="true"
                                                             data-kt-countup-value="4500" data-kt-countup-prefix="$"
                                                             data-kt-initialized="1">
                                                            {{ $patients_number}}</div>
                                                    </div>
                                                    <div class="fw-semibold fs-6 text-gray-400 text-center">Patients</div>
                                                    <div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-wrap flex-stack">
                                        <div class="d-flex flex-column flex-grow-1 pe-8">
                                            <div class="d-flex flex-wrap">
                                                <div class="border-dashed rounded min-w-115px py-3 px-4 me-6 mb-3">
                                                    <div class="d-flex align-items-center justify-content-center">
                                                        <div class="fs-2 fw-bold counted" data-kt-countup="true"
                                                             data-kt-countup-value="4500" data-kt-countup-prefix="$"
                                                             data-kt-initialized="1">
                                                            {{$dispo_numbers}}
                                                        </div>
                                                    </div>
                                                    <div class="fw-semibold fs-6 text-gray-400 text-center">Disponibilités</div>
                                                    <div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>

                        <div>
                        </div>

                    </div>
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <x-utils.link :text="__('My Profile')" class="nav-link active ml-0 pb-2"
                                          id="my-profile-tab" data-toggle="pill" href="#my-profile" role="tab"
                                          aria-controls="my-profile" aria-selected="true"/>

                            <x-utils.link :text="__('Edit Information')" class="nav-link pb-2"
                                          id="information-tab" data-toggle="pill" href="#information" role="tab"
                                          aria-controls="information" aria-selected="false"/>

                            @if (! $logged_in_user->isSocial())
                                <x-utils.link :text="__('Password')" class="nav-link pb-2" id="password-tab"
                                              data-toggle="pill" href="#password" role="tab" aria-controls="password"
                                              aria-selected="false"/>
                            @endif

                            <x-utils.link :text="__('Two Factor Authentication')" class="nav-link pb-2"
                                          id="two-factor-authentication-tab" data-toggle="pill"
                                          href="#two-factor-authentication" role="tab"
                                          aria-controls="two-factor-authentication" aria-selected="false"/>
                        </div>
                    </nav>
                </div>
            </div>
            <div class="card">
                <div class="row justify-content-center">
                    <div class="col-md-12">


                        <div class="tab-content" id="my-profile-tabsContent">
                            <div class="tab-pane fade pt-3 show active" id="my-profile" role="tabpanel"
                                 aria-labelledby="my-profile-tab">
                                <x-frontend.card>
                                    <x-slot name="header">
                                        @lang('My Account')
                                    </x-slot>
                                    <x-slot name="body">
                                        @include('frontend.user.account.tabs.profile')
                                    </x-slot>
                                </x-frontend.card>
                            </div><!--tab-profile-->

                            <div class="tab-pane fade p-5" id="information" role="tabpanel"
                                 aria-labelledby="information-tab">
                                @include('frontend.user.account.tabs.information')
                            </div><!--tab-information-->

                            @if (! $logged_in_user->isSocial())
                                <div class="tab-pane fade p-5" id="password" role="tabpanel"
                                     aria-labelledby="password-tab">
                                    @include('frontend.user.account.tabs.password')
                                </div><!--tab-password-->
                            @endif

                            <div class="tab-pane fade p-5" id="two-factor-authentication" role="tabpanel"
                                 aria-labelledby="two-factor-authentication-tab">
                                @include('frontend.user.account.tabs.two-factor-authentication')
                            </div><!--tab-information-->
                        </div><!--tab-content-->
                    </div><!--col-md-10-->
                </div>
            </div>
@endsection
