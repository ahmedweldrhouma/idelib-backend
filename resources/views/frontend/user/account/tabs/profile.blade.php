<div>
    <div class="row mb-7">
        <!--begin::Label-->
        <label class="col-lg-3 fw-semibold text-muted">@lang('Name')

        </label>

        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-6">
            <span class="fw-bold fs-6 text-gray-800">{{ $logged_in_user->name }}</span>
        </div>
        <!--end::Col-->
    </div>
    <div class="row mb-7">
        <!--begin::Label-->
        <label class="col-lg-3 fw-semibold text-muted">@lang('E-mail Address')</label>
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-6 fv-row">
            <span class="fw-semibold text-gray-800 fs-6">{{ $logged_in_user->email }}</span>
            <span class="badge badge-success">Verified</span>
        </div>
        <!--end::Col-->
    </div>

    <div class="row mb-7">
        <!--begin::Label-->
        <label class="col-lg-3 fw-semibold text-muted">@lang('Type')</label>
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-6 fv-row">
            <span class="fw-semibold text-gray-800 fs-6">@include('backend.auth.user.includes.type', ['user' => $logged_in_user])</span>
        </div>
        <!--end::Col-->
    </div>
    @if (!$logged_in_user->isSuperAdmin())
    <div class="row mb-7">
        <!--begin::Label-->
        <label class="col-lg-3 fw-semibold text-muted">@lang('Identifiant abonnement') </label>
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-6 fv-row">
            <span class="fw-semibold text-gray-800 fs-6">{{ $logged_in_user->app_user_id }}</span>
        </div>
        <!--end::Col-->
    </div>
    @endif
    
    @if ($logged_in_user->isSocial())
    <div class="row mb-7">
        <!--begin::Label-->
        <label class="col-lg-3 fw-semibold text-muted">@lang('Social Provider') </label>
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-6 fv-row">
            <span class="fw-semibold text-gray-800 fs-6">{{ ucfirst($logged_in_user->provider) }}</span>
        </div>
        <!--end::Col-->
    </div>
    @endif
    <div class="row mb-7">
        <!--begin::Label-->
        <label class="col-lg-3 fw-semibold text-muted">@lang('Timezone')</label>
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-6 fv-row">
            <span class="fw-semibold text-gray-800 fs-6">{{ $logged_in_user->timezone ? str_replace('_', ' ', $logged_in_user->timezone) : __('N/A') }}</span>
        </div>
        <!--end::Col-->
    </div>
    <div class="row mb-7">
        <!--begin::Label-->
        <label class="col-lg-3 fw-semibold text-muted">@lang('Account Created') </label>
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-6 fv-row">
            <span class="fw-semibold text-gray-800 fs-6">@displayDate($logged_in_user->created_at) ({{ $logged_in_user->created_at->diffForHumans() }})</span>
        </div>
        <!--end::Col-->
    </div>
    <div class="row mb-7">
        <!--begin::Label-->
        <label class="col-lg-3 fw-semibold text-muted">@lang('Last Updated') </label>
        <!--end::Label-->
        <!--begin::Col-->
        <div class="col-lg-6 fv-row">
            <span class="fw-semibold text-gray-800 fs-6">@displayDate($logged_in_user->updated_at) ({{ $logged_in_user->updated_at->diffForHumans() }})</span>
        </div>
        <!--end::Col-->
    </div>
    
</div>