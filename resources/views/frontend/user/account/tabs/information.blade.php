<x-forms.patch :action="route('frontend.user.profile.update')" enctype="multipart/form-data" id="avatar-form">
    <div class="form-group row">
        <label for="avatar" class="col-md-3 col-form-label text-md-right">@lang('Avatar')</label>

        <div class="col-md-9">
            <div class="avatar-upload" >
                <div class="avatar-edit">
                    <input type='file' name="avatar_location" id="imageUpload" accept=".png, .jpg, .jpeg"/>
                    <label for="imageUpload">
                        <i class="cil-pencil icon"></i>
                    </label>
                </div>
                <div class="avatar-preview" >
                    <div id="imagePreview" style="background-image: url({{ $logged_in_user->getAvatar() }});">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label for="first_name" class="col-md-3 col-form-label text-md-right">@lang('Nom')</label>

        <div class="col-md-9">
            <input type="text" name="first_name" class="form-control" placeholder="{{ __('first_name') }}"
                   value="{{ old('first_name') ?? $logged_in_user->first_name }}" required autofocus
                   autocomplete="first_name"/>
        </div>
    </div><!--form-group-->
    <div class="form-group row">
        <label for="last_name" class="col-md-3 col-form-label text-md-right">@lang('Prénom')</label>

        <div class="col-md-9">
            <input type="text" name="last_name" class="form-control" placeholder="{{ __('last_name') }}"
                   value="{{ old('last_name') ?? $logged_in_user->last_name }}" required autofocus
                   autocomplete="last_name"/>
        </div>
    </div><!--form-group-->
    @if ($logged_in_user->canChangeEmail())
        <div class="form-group row">
            <label for="email" class="col-md-3 col-form-label text-md-right">@lang('E-mail Address')</label>

            <div class="col-md-9">
                <x-utils.alert type="info" class="mb-3" :dismissable="false">
                    <i class="fas fa-info-circle"></i> @lang('If you change your e-mail you will be logged out until you confirm your new e-mail address.')
                </x-utils.alert>

                <input type="email" name="email" id="email" class="form-control"
                       placeholder="{{ __('E-mail Address') }}" value="{{ old('email') ?? $logged_in_user->email }}"
                       required autocomplete="email"/>
            </div>
        </div><!--form-group-->
    @endif
    <div class="form-group row">
        <label for="phone" class="col-md-3 col-form-label text-md-right">@lang('Télephone')</label>

        <div class="col-md-9">
            <input type="text" name="phone" class="form-control" placeholder="{{ __('Télephone') }}"
                   value="{{ old('phone') ?? $logged_in_user->phone }}" required autocomplete="phone"/>
        </div>
    </div><!--form-group-->
    <div class="form-group row">
        <label for="birth_date" class="col-md-3 col-form-label text-md-right">@lang('Date de naissance')</label>

        <div class="col-md-9">
            <input type="date" name="birth_date" class="form-control" placeholder="{{ __('Birth Date') }}"
                   value="{{ old('birth_date')  ?? \Carbon\Carbon::parse($logged_in_user->birth_date)->format('Y-m-d')  }}"
                   autocomplete="birth_date"/>
        </div>
    </div><!--form-group-->
    <div class="form-group row">
        <label for="address" class="col-md-3 col-form-label text-md-right">@lang('Adresse')</label>

        <div class="col-md-9">
            <input type="text" name="address" class="form-control" placeholder="{{ __('Address') }}"
                   value="{{ old('address') ?? $logged_in_user->address }}" required autocomplete="address"/>
        </div>
    </div><!--form-group-->
    <div class="form-group row">
        <label for="address" class="col-md-3 col-form-label text-md-right">@lang('Numéro Adeli')</label>

        <div class="col-md-9">
            <input type="text" name="adeli_number" class="form-control" placeholder="{{ __('Numéro Adeli') }}"
                   value="{{ old('adeli_number') ?? $logged_in_user->adeli_number }}" required
                   autocomplete="adeli_number"/>
        </div>
    </div><!--form-group-->
    <div class="form-group row">
        <label for="address" class="col-md-3 col-form-label text-md-right">@lang('Numéro RPPS')</label>

        <div class="col-md-9">
            <input type="text" name="rpps_number" class="form-control" placeholder="{{ __('Numéro RPPS') }}"
                   value="{{ old('rpps_number') ?? $logged_in_user->rpps_number }}" required
                   autocomplete="rpps_number"/>
        </div>
    </div><!--form-group-->
    <div class="form-group row mb-0">
        <div class="col-md-12 text-right">
            <button class="btn btn-sm btn-primary float-right" type="submit">@lang('Update')</button>
        </div>
    </div><!--form-group-->
</x-forms.patch>
@push('after-scripts')
    <script>
        function displayAvatar() {
            const input = document.getElementById('imageUpload');
            // Check if a file is selected
            if (input.files && input.files[0]) {
                const reader = new FileReader();
                reader.onload = function (e) {
                    const avatarImage = document.createElement('img');
                    avatarImage.src = e.target.result;

                    avatarImage.style.width = '100px';
                    avatarImage.style.height = '100px';
                    avatarImage.style.objectFit = 'cover';
                    avatarImage.style.borderRadius = '50%';

                    const previousAvatar = document.getElementById('avatar-preview');
                    if (previousAvatar) {
                        previousAvatar.remove();
                    }
                    avatarImage.id = 'avatar-preview';
                    const container = document.getElementById('imagePreview');
                    container.appendChild(avatarImage);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        const input = document.getElementById('imageUpload');
        input.addEventListener('change', displayAvatar);
    </script>
@endpush
