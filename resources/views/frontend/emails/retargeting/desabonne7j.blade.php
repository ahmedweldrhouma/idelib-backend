<!doctype html>
<html lang="en">
<head>
    <title>{{ config('MAIL_FROM_ADDRESS') }}</title>
    <style>@import url("https://fonts.googleapis.com/css?family=Varela+Round");

        * {
            margin: 0;
            padding: 0;
            font-family: Varela Round, "Segoe UI", "Arial", "san serif";
        }

        img {
            display: inline-block;
        }

        .container {
            max-width: 500px;
            margin: 20px auto;
            border-radius: 4px;
            border: 1px solid rgba(0, 0, 0, 0.1);
            min-height: 100px;
            position: relative;
        }

        .container::before {
            content: "";
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 3px;
            background: linear-gradient(to right, #0267c1, #d65108);
        }

        .logo {
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .logo a {
            display: block;
            width: 30px;
            height: 30px;
        }

        .logo img {
            width: 50%;
            margin: auto;
        }

        .logo .c-name {
            display: inline-block;
            font-weight: 600;
        }

        .thumbs img {
            width: 100%;
        }

        .illustration {
            width: 100%;
            text-align: center;
            box-shadow: 0 10px 20px -5px rgba(0, 0, 0, 0.05);
            border-radius: 0 0 50% 50%/1%;
            text-align: center;
        }

        .illustration img {
            width: 70%;
        }

        .separator {
            display: block;
            height: 3px;
            width: 70%;
            margin: 10px auto;
            background-color: rgba(0, 0, 0, 0.05);
            border-radius: 10px;
            position: relative;
            overflow: hidden;
        }

        .separator::before, .separator::after {
            content: "";
            position: absolute;
            left: 0;
            top: 0;
            height: 100%;
            width: 33%;
            border-radius: inherit;
            opacity: 0.05;
        }

        .separator::before {
            left: 0;
            background: #efa00b;
        }

        .separator::after {
            left: initial;
            right: 0;
            background: #d65108;
        }

        .hgroup {
            text-align: center;
            padding: 50px 30px 30px;
        }

        .name {
            display: block;
            color: #0267c1;
            font-weight: 600;
            font-size: 1.1rem;
        }

        .hgroup h1 {
            font-size: 20px;
            font-weight: 600;
            color: #333;
        }

        .hgroup h2 {
            font-size: 19px;
        }

        .hgroup p {
            font-size: 15px;
            color: slategrey;
            margin-top: 15px;
            text-align: justify;
            line-height: 25px;
        }

        .items {
            padding: 30px;
            display: flex;
        }

        .item {
            margin-bottom: 10px;
            text-align: center;
            width: 100%;
        }

        .item .icon {
            margin-bottom: 10px;
        }

        .item .title {
            margin-bottom: 5px;
            font-size: 16px;
            font-weight: 600;
        }

        .item .subtitle {
            font-size: 13px;
            color: slategrey;
            padding: 1rem;
        }

        .button-wrap {
            text-align: center;
            padding: 2rem;
        }

        button.explore {
            padding: 15px 25px;
            font: inherit;
            background: linear-gradient(to right, #0267c1, #0280ef);
            border-radius: 50px;
            border: 0;
            color: #fff;
            margin: auto;
            display: inline-block;
            transition: all 0.2s ease-in-out;
            cursor: pointer;
            box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);
        }

        button.explore:hover {
            transform: translateY(-5px);
            box-shadow: 0 15px 10px -7px rgba(0, 0, 0, 0.1);
        }

        footer {
            font-size: 12px;
            color: slategrey;
            text-align: center;
            padding: 30px;
        }

        .rad {
            margin: 0 !important;
            text-align: center !important;
            font-size: 18px !important;
        }

        .raised {
            font-size: 16px;
            color: #777;
            display: block;
            color: steelblue;
        }
    </style>
</head>
<body>
<div class="container">
    <a class="logo" href="https://idelib.com">
        <img src="https://idelib.com/logo/logo-idelib.png" border="0">
    </a>
    <div class="illustration">
        <div class="hgroup">
            <span class="name">Bonjour,</span>

            <p>Votre inscription a bien été validée par nos équipes.</p>

            <p>Vos 7 jours d’essai viennent de prendre fin et vous hésitez encore à vous abonner ?
                Demandez dès maintenant un rendez-vous téléphonique avec notre service abonnés et
                recevez une explication détaillée de toutes nos fonctionnalités ainsi que toutes les réponses
                à vos interrogations.</p>

            <p>
                Comment déduire mon abonnement des frais professionnels ? Comment saisir ma tournée ?
                Comment éditer un contrat de remplacement ?
            </p>

            <p>
                Transmettez-nous dès maintenant vos disponibilités à l’adresse <a href="mailto:contact@idelib.com">contact@idelib.com</a>
                et notre
                équipe reviendra vers vous au plus vite !
            </p>

            <p>
                Si vous avez laissé passer votre période d'essai sans profiter des avantages de l'app et que
                vous êtes en plein doute, ne vous inquiétez pas, nous vous proposons une remise de 10%
                sur votre abonnement grâce au code <strong>ESSAI10</strong>.
            </p>

            <p>
                À très vite sur IDELIB,<br/>
                L’app des infirmiers.<br/>
                www.idelib.com
            </p>

        </div>
    </div>

</div>
</body>
</html>
