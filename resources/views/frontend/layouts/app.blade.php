<!doctype html>
<html lang="{{ htmlLang() }}" @langrtl dir="rtl" @endlangrtl>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ appName() }} | @yield('title')</title>
    <meta name="description" content="@yield('meta_description', appName())">
    <link rel="icon" type="image/png" href="{{ url('/img/favicon-idelib.png') }}">
    @yield('meta')

    @stack('before-styles')
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="{{ mix('css/frontend.css') }}" rel="stylesheet">
    <livewire:styles />
    @stack('after-styles')
</head>
<body>
    @include('includes.partials.read-only')
    @include('includes.partials.logged-in-as')
    {{--  @include('includes.partials.announcements')  --}}
    <header class="header-area sticky" id="header-area">
    <nav class="navbar navbar-expand-md fixed-top">
        <div class="container mw-100">
            <div class="site-logo"><a class="navbar-brand" href="https://idelib.com">
                    <img src="{{ url('/img/logo-idelib200x100.png') }}" class="img-fluid" alt="Img"></a>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon">
                    <i class="c-icon c-icon-lg cil-menu"></i>
                </span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav">
                    <li class="nav-item"><a href="https://idelib.com/#header-area">Accueil</a></li>
                    <li class="nav-item"><a href="https://idelib.com/#solution" data-scroll-nav="3">Solution</a></li>
                    <li class="nav-item"><a href="https://idelib.com/#innovations" data-scroll-nav="7">Innovations</a></li>
                    <li class="nav-item"><a href="https://idelib.com/#pricing" data-scroll-nav="8">Tarifs</a></li>
                    <li class="nav-item"><a href="https://idelib.com/#appScreenshots" data-scroll-nav="9">App</a></li>
                    <li class="nav-item"><a href="https://idelib.com/#faq" data-scroll-nav="10">FAQ</a></li>
                    <li class="nav-item"><a href="https://idelib.com/#blog" data-scroll-nav="11">Actus</a></li>
                    <li class="nav-item"><a href="https://idelib.com/#contact" data-scroll-nav="12">Contact</a></li>
                </ul>
                <div class="inscription-connexion">
                    <ul class="start m-0 p-0">
                        <li class="nav-item">
                            <a href="{{ route('frontend.auth.login') }}" class="header-connexion">Connectez-vous</a>
                        </li>
                    </ul>
                    <ul class="start m-0 p-0">
                        <li class="nav-item">
                            <a href="{{ route('frontend.auth.register') }}" class="header-inscription">Inscrivez-vous</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    </header>
    <div id="app">
        {{--@include('frontend.includes.nav')
        @include('includes.partials.messages')--}}

        <main>
            @yield('content')
        </main>
    </div><!--app-->

    @stack('before-scripts')
    <script src="{{ mix('js/manifest.js') }}"></script>
    <script src="{{ mix('js/vendor.js') }}"></script>
    <script src="{{ mix('js/frontend.js') }}"></script>
    <livewire:scripts />
    @stack('after-scripts')
</body>
</html>
