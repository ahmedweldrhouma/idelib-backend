@extends('frontend.layouts.app')

@section('title', 'Inscrivez-vous sur IDELIB et oubliez la charge mentale, la phobie administrative liées à votre activité d’IDEL tout en trouvant des remplaçants/remplacements et patients !')
@section('meta_description', 'Inscrivez-vous sur IDELIB et oubliez la charge mentale, la phobie administrative liées à votre activité d’IDEL tout en trouvant des remplaçants/remplacements et patients !')

@push('after-scripts')
    <script type="text/javascript"
            src="//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <script src="https://unpkg.com/imask"></script>
    <script src="https://js.stripe.com/v3/"></script>

    <script>

    </script>
    <script src="js/typeahead.js"></script>
    <script src="js/bootstrap-tagsinput.js"></script>
    <script>

        (function ($) {


            function checkValidityPromocode() {


                var prefix = $(this).attr('prefix');
                var el = $('.' + prefix + 'codepromo');
                var promo = el.val();

                var offre_selected = $("input[name='" + prefix + "payment']:checked").val();
                var offre_type = $("input[name='" + prefix + "payment']:checked").data('typeabo');

                if (promo.length <= 3) {
                    return false;
                } else {
                    $('.' + prefix + 'msg_promotional_code_ko').hide();
                    $('.' + prefix + 'msg_promotional_code_ok').hide();

                    $.ajax({
                        url: "{{ route('frontend.auth.validatePromocode', [], false) }}",
                        data: {code: promo, offre: offre_selected},
                        method: "POST",

                    }).done(function (result) {
                        if (result.success == true) {
                            //Il y a un promocode
                            $('.' + prefix + 'promotional_code').val(result.id);
                            $('.' + prefix + 'msg_promotional_code_ok').show();
                            $('.' + prefix + 'msg_promotional_code_ko').hide();

                            if (offre_type == 'men_')
                                $('.' + prefix + offre_type + 'price').html('<strike>'+result.old_price+'€ / mois</strike><br/>'+result.new_price + '€ / mois');
                            else {
                                $('.' + prefix + offre_type + 'price').html('<strike>'+result.old_price+'€ / an</strike><br/>'+result.new_price + '€ / an');
                                $('.' + prefix + offre_type + 'sub_price').html('Soit ' + result.new_price_men + '€ / mois <strike>'+result.old_price_men+'€ / mois</strike><br/>');
                            }

                            el.addClass('green');
                        } else {
                            //il n'y en a pas
                            el.removeClass('green');
                            $('.' + prefix + 'promotional_code').val();
                            $('.' + prefix + 'msg_promotional_code_ko').show();
                            $('.' + prefix + 'msg_promotional_code_ok').hide();

                            if (offre_type == 'men_')
                                $('.' + prefix + offre_type + 'price').html(result.new_price + '€ / mois');
                            else {
                                $('.' + prefix + offre_type + 'price').html(result.new_price + '€ / an');
                                $('.' + prefix + offre_type + 'sub_price').html('Soit ' + result.new_price_men + '€ / mois ');
                            }

                        }



                    });
                }
            }


            $('.promotional_code').keyup(function () {
                checkValidityPromocode();
            });

            function mountCardStrip(idToMount, idSubmit) {
                const stripe = Stripe('{{ env('STRIPE_KEY') }}');
                const elements = stripe.elements();
                const cardElement = elements.create('card');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });


                cardElement.unmount();

                //cardElement.mount('#card-element', {
                cardElement.mount('#' + idToMount, {
                    classes: {
                        base: 'form-input form-bg'
                    }
                });

                //const cardButton = document.getElementById('submit-button');
                const cardButton = document.getElementById(idSubmit);

                cardButton.addEventListener('click', async (e) => {
                    e.preventDefault();
                    const {paymentMethod, error} = await stripe.createPaymentMethod('card', cardElement);


                    if (error) {
                        // Display "error.message" to the user...
                        document.getElementById('error_message').style.display = true
                        return false;
                    } else {
                        document.getElementById('payment_method').value = paymentMethod.id;
                        document.getElementById('msform').submit();
                    }

                });


            }

            var countries = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                prefetch: {
                    url: 'cp.json',
                    filter: function (list) {
                        return $.map(list, function (name) {
                            return {name: name};
                        });
                    }
                }
            });
            countries.initialize();

            $('#tags-input').tagsinput({
                typeaheadjs: {
                    name: 'countries',
                    displayKey: 'name',
                    valueKey: 'name',
                    source: countries.ttAdapter()
                }
            });


            IMask(
                document.getElementById('date-mask'),
                {
                    mask: Date,
                    mask: '00/00/0000',
                    min: new Date(1800, 0, 1),
                    max: new Date(2100, 0, 1),
                    lazy: true
                }
            )
            IMask(
                document.getElementById('date-mask2'),
                {
                    mask: Date,
                    mask: '00/00/0000',
                    min: new Date(1800, 0, 1),
                    max: new Date(2100, 0, 1),
                    lazy: true
                }
            )


//jQuery time
            var current_fs, next_fs, previous_fs; //fieldsets
            var left, opacity, scale; //fieldset properties which we will animate
            var animating; //flag to prevent quick multi-click glitches

            $('.buttonFormInsc').click(function () {

                $('.typeInsc').val($(this).val());

                if ($(this).hasClass('inf_liberal')) {

                    mountCardStrip('card-element', 'submit-button');
                    $('.inf_liberal_block').show();
                    $('.inf_remplacant_block').each(function () {
                        $(this).hide();
                    });
                    $('.patient_block').each(function () {
                        $(this).hide();
                    });

                    $('.btnfirststep').each(function () {
                        $(this).removeClass('firstTopRemp');
                        $(this).removeClass('step3NinfLib');
                    });

                    $('.btnsndststep').each(function () {
                        $(this).removeClass('step5PinfLib');
                    });
                    $('.btnsndststep2').each(function () {
                        $(this).removeClass('step5NinfLib');
                    });

                    $('.btnsndststep').each(function () {
                        $(this).removeClass('step6PinfLib');
                    });
                    $('.btnsndststep2').each(function () {
                        $(this).removeClass('step6NinfLib');
                        $(this).val('Suivant');
                    });


                }
                if ($(this).hasClass('inf_remplacant')) {

                    mountCardStrip('card-element2', 'submit-button2');

                    $('.inf_remplacant_block').show();
                    $('.inf_liberal_block').hide();
                    $('.patient_block').hide();


                    $('.btnfirststep').each(function () {
                        $(this).addClass('firstTopRemp');
                        $(this).addClass('step3NinfLib');
                    });
                    $('.btnsndststep').each(function () {
                        $(this).addClass('step6PinfLib');
                    });
                    $('.btnsndststep2').each(function () {
                        $(this).addClass('step6NinfLib');
                        $(this).addClass('finalStep');
                        $(this).val('Choisir');
                    });
                }
                if ($(this).hasClass('patient')) {
                    $('.patient_block').show();
                    $('.inf_remplacant_block').hide();
                    $('.inf_liberal_block').hide();

                    $('.btnfirststep').each(function () {
                        $(this).removeClass('firstTopRemp');
                    });

                }
            });

            $('.priceradio').click(function () {
                $('.blocPrice').hide();
                $('.' + $(this).attr('id') + 'BlockPrice').show();
                checkValidityPromocode();

            });

            var prefixCheck = '';
            $(".next").click(function () {

                if (animating) return false;
                animating = true;

                current_fs = $(this).parent();
                next_fs = $(this).parent().next();

                var el = $(this);
                typeInsc = el.val();


                if ($(this).attr('name') == 'inf_lib') {
                    if (typeInsc == 'Infirmièr(e) libéral(e)') {
                        prefix = 'lib_';
                    }


                    if (typeInsc == 'Infirmièr(e) remplaçant(e)') {
                        prefix = 'remp_';
                    }
                    if (typeInsc == 'Patient(e)') {
                        prefix = 'patient_';
                    }
                }


                //vérification des champs obligatoires
                var validate = true;

                $(current_fs.find('input').each(function () {

                    if ($(this).attr('name') != 'inf_lib') {

                        if ($(this).data('mandatory') && $(this).attr('name').indexOf(prefix) === 0) {
                            //  console.log($(this).val());
                            if ($(this).data('mandatory') && $(this).val() === '') {
                                console.log($(this).data('error'));
                                if ($(this).data('error')) {
                                    $("input[name='" + $(this).data('error') + "']").addClass('formError');
                                } else {
                                    $(this).addClass('formError');
                                }

                                validate = false;
                                animating = false;
                            } else {
                                if ($(this).data('error')) {
                                    $("input[name='" + $(this).data('error') + "']").removeClass('formError');

                                } else {
                                    $(this).removeClass('formError');

                                }
                            }

                            if ($(this).data('phone')) {

                                var phone_pattern = /([0-9]{10})|(\([0-9]{3}\)\s+[0-9]{3}\-[0-9]{4})/;

                                if (phone_pattern.test($(this).val()) === false) {
                                    $(this).addClass('formError');
                                    validate = false;
                                    animating = false;
                                } else {
                                    $(this).removeClass('formError');
                                }
                            }

                            if ($(this).data('mail')) {
                                var validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
                                if (validRegex.test($(this).val()) === false) {

                                    $(this).addClass('formError');
                                    validate = false;
                                    animating = false;
                                } else {
                                    $(this).removeClass('formError');
                                }
                            }
                            if ($(this).data('mailconf')) {
                                if ($(this).val() != $("input[name='" + $(this).data('namemailconf') + "']").val()) {
                                    $(this).addClass('formError');
                                    validate = false;
                                    animating = false;
                                } else {
                                    $(this).removeClass('formError');
                                }
                            }
                            if ($(this).data('mdpconf')) {
                                if ($(this).val() != $("input[name='" + $(this).data('namemdpconf') + "']").val()) {
                                    $(this).addClass('formError');
                                    validate = false;
                                    animating = false;
                                } else {
                                    $(this).removeClass('formError');
                                }
                            }


                            /*
                        if ($(this).data('mdp') && $(this).val() === ''){
                            $(this).addClass('formError');
                            validate = false;
                            animating = false;
                        }
                        else{
                            $(this).removeClass('formError');
                        }

                        if ($(this).data('mail') && $(this).val() === ''){
                            $(this).addClass('formError');
                            validate = false;
                            animating = false;
                        }
                        else{
                            $(this).removeClass('formError');
                        }*/

                            // return false;

                        }
                    }
                }));

                //activate next step on progressbar using the index of next_fs
                //  $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

                if (validate) {
                    //show the next fieldset
                    next_fs.show();
                    //hide the current fieldset with style
                    current_fs.animate({opacity: 0}, {
                        step: function (now, mx) {
                            //as the opacity of current_fs reduces to 0 - stored in "now"
                            //1. scale current_fs down to 80%
                            scale = 1 - (1 - now) * 0.2;
                            //2. bring next_fs from the right(50%)
                            left = (now * 50) + "%";
                            //3. increase opacity of next_fs to 1 as it moves in
                            opacity = 1 - now;
                            current_fs.css({
                                'transform': 'scale(' + scale + ')',
                                'position': 'absolute'
                            });
                            next_fs.css({'left': left, 'opacity': opacity});
                        },
                        duration: 800,
                        complete: function () {
                            current_fs.hide();
                            animating = false;

                            if (el.hasClass('finalStep')){

                                //On affiche le recap de l'abonnement choisi

                                //On récupére l'offre choisi
                                var offreselected = $("input[name='" + prefix + "payment']:checked").next('label').html();
                                var offre_type = $("input[name='" + prefix + "payment']:checked").data('typeabo');


                                if (offre_type == 'men_')
                                    var recap = $('.' + prefix + offre_type + 'price').html();
                                else {
                                    var recap =  $('.' + prefix + offre_type + 'price').html();
                                    recap += '<br/><span class="priceSubTitle">'+$('.' + prefix + offre_type + 'sub_price').html()+'</span>';
                                }

                                $('#'+prefix+'recap').html('<h3>Récapitulatif de votre abonnement</h3><label class="labelrecap">'+offreselected+'</label> <br/> <span class="price priceRecap">'+recap+'</span>');

                            }

                            if (el.hasClass('step3NinfLib')) {
                                $('#msform').css('width', '200%');
                                $('#msform').css('margin-left', '-53%');

                                $('.h-100').addClass('removeBg');
                                $('.formInsc').addClass('removeBg');
                            } else {
                                $('#msform').css('width', '100%');
                                $('#msform').css('margin-left', 'auto');
                                $('.h-100').removeClass('removeBg');
                                $('.formInsc').removeClass('removeBg');

                            }
                        },
                        //this comes from the custom easing plugin
                        easing: 'easeInOutBack'
                    });
                }
            });

            $(".previous").click(function () {
                if (animating) return false;
                animating = true;

                current_fs = $(this).parent();
                previous_fs = $(this).parent().prev();

                /* if ($(this).hasClass('step3NinfLib')){
                     $('#msform').css('width', '100%');
                 }else{
                     $('#msform').css('width', '800px');
                 }*/

                var el = $(this);
                $('#msform').css('width', '100%');
                $('#msform').css('margin-left', 'auto');
                $('.h-100').removeClass('removeBg');
                $('.formInsc').removeClass('removeBg');
                //de-activate current step on progressbar
                // $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

                if (el.hasClass('widthfull')) {

                    $('#msform').css('width', '200%');
                    $('#msform').css('margin-left', '-53%');
                    $('.h-100').addClass('removeBg');
                    $('.formInsc').addClass('removeBg');

                }
                //show the previous fieldset
                previous_fs.show();
                //hide the current fieldset with style
                current_fs.animate({opacity: 0}, {
                    step: function (now, mx) {
                        //as the opacity of current_fs reduces to 0 - stored in "now"
                        //1. scale previous_fs from 80% to 100%
                        scale = 0.8 + (1 - now) * 0.2;
                        //2. take current_fs to the right(50%) - from 0%
                        left = ((1 - now) * 50) + "%";
                        //3. increase opacity of previous_fs to 1 as it moves in
                        opacity = 1 - now;
                        current_fs.css({'left': left});
                        previous_fs.css({'transform': 'scale(' + scale + ')', 'opacity': opacity});
                    },
                    duration: 800,
                    complete: function () {
                        current_fs.hide();
                        animating = false;
                        previous_fs.css({'position': 'relative'});


                    },
                    //this comes from the custom easing plugin
                    easing: 'easeInOutBack'
                });
            });


        })
        (jQuery);


    </script>

@endpush

@section('content')

    <div class="page-header">
        <div class="container">
            <div class="title-box">
            </div>
        </div>
        <div class="shape-bottom">
            <img src="/images/shapes/price-shape.svg" alt="shape" class="bottom-shape img-fluid">
        </div>
    </div>

    <div class="container-row-g row justify-content-center mx-0">
        <div class="col-md-8 pt-lg-4 px-md-3">
            <div class="card h-100">
                <div class="card-body">
                    <div class="login-form px-md-5">
                        <div class="login-page">
                            <div class="col-md-12">
                                <div class="container-fluid">
                                    <!-- <a href="index.html" class="res-logo"><img src="/logo/logo-idelib.png" alt="Logo"/></a> -->
                                    <div class="login-form">

                                        <div class="login-form-head">
<!--                                            <a href="/" class="logRegis"><img src="/img/logo-idelib200x100.png"
                                                                              width="100"
                                                                              class="logo-landing2"></a>-->
                                            <h2 class="inscriptionH2">Inscription
                                                <!-- Solution de gestion de patients, d'organisation et de simplification de l'activité libéral infirmier. -->
                                            </h2>
                                        </div>


                                        <div class="">
                                            <!-- multistep form -->

                                            @if ($errors->any())
                                                <div class="alert alert-danger">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                            <form id="msform" method="post"
                                                  action="{{ route('frontend.auth.registerTreat', [], false) }}">
                                                @csrf

                                                <!-- fieldsets -->
                                                <fieldset>

                                                    <input type="button" name="inf_lib"
                                                           class="buttonFormInsc next inf_liberal"
                                                           value="Infirmièr(e) libéral(e)"/>
                                                    <input type="button" name="inf_lib"
                                                           class="buttonFormInsc next inf_remplacant"
                                                           value="Infirmièr(e) remplaçant(e)"/>
                                                    <input type="button" name="inf_lib"
                                                           class="buttonFormInsc next patient"
                                                           value="Patient(e)"/>
                                                    <input type="hidden" name="typeInsc" class="typeInsc" value=""/>
                                                </fieldset>
                                                <fieldset class="formInsc">


                                                    <div class="inf_liberal_block">
                                                        <h3 class="fs-subtitle">Inscription infirmièr(e) libéral(e)</h3>

                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <input type="text"
                                                                       @if ($errors->has('lib_prenom')) class="formError"
                                                                       @endif  placeholder="Prénom *" name="lib_prenom"
                                                                       value="{{old('lib_prenom')}}"
                                                                       data-mandatory="true"/>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <input type="text"
                                                                       @if ($errors->has('lib_nom')) class="formError"
                                                                       @endif placeholder="Nom *" name="lib_nom"
                                                                       value="{{old('lib_nom')}}"
                                                                       data-mandatory="true"/>
                                                            </div>

                                                        </div>


                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <input type="text"
                                                                       @if ($errors->has('lib_mail')) class="formError"
                                                                       @endif  placeholder="Mail *" name="lib_mail"
                                                                       value="{{old('lib_mail')}}"
                                                                       data-mandatory="true"
                                                                       data-mail="true"
                                                                />
                                                            </div>


                                                            <div class="col-md-6">
                                                                <input type="text" placeholder="Confirmation de mail *"
                                                                       name="lib_conf_mail"
                                                                       @if ($errors->has('lib_conf_mail')) class="formError"
                                                                       @endif
                                                                       value="{{ old('lib_conf_mail') }}"
                                                                       data-mandatory="true"
                                                                       data-mailConf="true"
                                                                       data-nameMailConf="lib_mail"
                                                                />
                                                            </div>
                                                        </div>

                                                        <div class="row">

                                                            <div class="col-md-6">
                                                                <input type="password"
                                                                       @if ($errors->has('lib_mdp')) class="formError"
                                                                       @endif placeholder="Mot de passe *"
                                                                       name="lib_mdp" value=""
                                                                       data-mandatory="true"/>

                                                            </div>

                                                            <div class="col-md-6">
                                                                <input type="password"
                                                                       @if ($errors->has('lib_mdp_confirmation')) class="formError"
                                                                       @endif  placeholder="Confirmation mot de passe *"
                                                                       name="lib_mdp_confirmation"
                                                                       value="" data-mandatory="true"
                                                                       data-mdpConf="true"
                                                                       data-nameMdpConf="lib_mdp"
                                                                />

                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <input
                                                                    @if ($errors->has('lib_telephone')) class="formError"
                                                                    @endif type="text" placeholder="Téléphone *"
                                                                    name="lib_telephone"
                                                                    value="{{old('lib_telephone')}}"
                                                                    data-mandatory="true"
                                                                    data-phone="true"
                                                                />

                                                            </div>


                                                        </div>

                                                    </div>

                                                    <div class="inf_remplacant_block">
                                                        <h3 class="fs-subtitle">Insription infirmièr(e)
                                                            remplaçant(e)</h3>

                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <input type="text"
                                                                       @if ($errors->has('remp_prenom')) class="formError"
                                                                       @endif  placeholder="Prénom *" name="remp_prenom"
                                                                       value="{{old('remp_prenom')}}"
                                                                       data-mandatory="true"
                                                                />
                                                            </div>

                                                            <div class="col-md-6">
                                                                <input type="text"
                                                                       @if ($errors->has('remp_nom')) class="formError"
                                                                       @endif placeholder="Nom *" name="remp_nom"
                                                                       value="{{old('remp_nom')}}"

                                                                       data-mandatory="true"
                                                                />
                                                            </div>




                                                        </div>

                                                        <div class="row">



                                                            <div class="col-md-6">
                                                                <input
                                                                    @if ($errors->has('remp_telephone')) class="formError"
                                                                    @endif type="text" placeholder="Téléphone *"
                                                                    name="remp_telephone"
                                                                    value="{{old('remp_telephone')}}"
                                                                    data-mandatory="true"
                                                                    data-phone="true"
                                                                />

                                                            </div>

                                                            <div class="col-md-6" id="remp_searchbox-container">
                                                                <input type="text"
                                                                       @if ($errors->has('remp_adresse')) class="formError"
                                                                       @endif  placeholder="Adresse *"
                                                                       name="remp_adresse"
                                                                       id="remp_searchbox"
                                                                       value="{{old('remp_adresse')}}"
                                                                       data-mandatory="true"

                                                                       data_prefix="remp_"

                                                                />
                                                                <input type="hidden" name="remp_lat" id="remp_lat" value="{{old('remp_lat')}}"
                                                                       data-mandatory="true" data-error="remp_adresse"/>
                                                                <input type="hidden" name="remp_lng" id="remp_lng" value="{{old('remp_lng')}}"
                                                                       data-mandatory="true" data-error="remp_adresse"/>
                                                                <div class="control drop" id="remp_dropdown-menu"></div>

                                                            </div>

                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <input type="text"
                                                                       @if ($errors->has('remp_mail')) class="formError"
                                                                       @endif  placeholder="Mail *" name="remp_mail"
                                                                       value="{{old('remp_mail')}}"
                                                                       data-mail="true"
                                                                       data-mandatory="true"
                                                                />
                                                            </div>
                                                            <div class="col-md-6">
                                                                <input type="text" placeholder="Confirmation de mail *"
                                                                       name="remp_conf_mail"
                                                                       @if ($errors->has('remp_conf_mail')) class="formError"
                                                                       @endif
                                                                       value="{{ old('remp_conf_mail') }}"
                                                                       data-mandatory="true"
                                                                       data-mailConf="true"
                                                                       data-nameMailConf="remp_mail"
                                                                />
                                                            </div>




                                                        </div>
                                                        <div class="row">

                                                            <div class="col-md-6">
                                                                <input type="password"
                                                                       @if ($errors->has('remp_mdp')) class="formError"
                                                                       @endif placeholder="Mot de passe *"
                                                                       name="remp_mdp" value=""
                                                                       data-mandatory="true"
                                                                />

                                                            </div>

                                                            <div class="col-md-6">
                                                                <input type="password"
                                                                       @if ($errors->has('remp_mdp_confirmation')) class="formError"
                                                                       @endif  placeholder="Confirmation mot de passe *"
                                                                       name="remp_mdp_confirmation"
                                                                       data-mdpconf="true"
                                                                       data-nameMdpConf="remp_mdp"

                                                                       data-mandatory="true"

                                                                       value=""/>

                                                            </div>

                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <input
                                                                    @if ($errors->has('remp_adeli')) class="formError"
                                                                    @endif type="text" placeholder="N° Adeli"
                                                                    name="remp_adeli"
                                                                    value="{{old('remp_adeli')}}"
                                                                />

                                                            </div>
                                                        </div>


                                                    </div>
                                                    <div class="patient_block">
                                                        <h3 class="fs-subtitle">Patient(e)</h3>

                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <input type="text"
                                                                       @if ($errors->has('patient_prenom')) class="formError"
                                                                       @endif  placeholder="Prénom *"
                                                                       name="patient_prenom"
                                                                       value="{{old('patient_prenom')}}"
                                                                       data-mandatory="true"
                                                                />
                                                            </div>

                                                            <div class="col-md-6">
                                                                <input type="text"
                                                                       @if ($errors->has('patient_nom')) class="formError"
                                                                       @endif  placeholder="Nom *"
                                                                       name="patient_nom"
                                                                       value="{{old('patient_nom')}}"
                                                                       data-mandatory="true"
                                                                />
                                                            </div>



                                                        </div>
                                                        <div class="row">

                                                            <div class="col-md-6">
                                                                <input type="text"
                                                                       @if ($errors->has('patient_telephone')) class="formError"
                                                                       @endif  placeholder="Téléphone *"
                                                                       name="patient_telephone"
                                                                       value="{{old('patient_telephone')}}"
                                                                       data-phone="true"
                                                                       data-mandatory="true"
                                                                />
                                                            </div>


                                                            <div class="col-md-6" id="patient_searchbox-container">
                                                                <input type="text"
                                                                       @if ($errors->has('patient_adresse')) class="formError"
                                                                       @endif  placeholder="Adresse *"
                                                                       name="patient_adresse"
                                                                       id="patient_searchbox"
                                                                       value="{{old('patient_adresse')}}"
                                                                       data-mandatory="true"

                                                                       data-prefix="patient_"

                                                                />
                                                                <input type="hidden" name="patient_lat" id="patient_lat" value="{{old('patient_lat')}}"
                                                                       data-mandatory="true"
                                                                       data-error="patient_adresse"/>
                                                                <input type="hidden" name="patient_lng" id="patient_lng" value="{{old('patient_lng')}}"
                                                                       data-mandatory="true"
                                                                       data-error="patient_adresse"/>
                                                                <div class="control drop"
                                                                     id="patient_dropdown-menu"></div>

                                                            </div>


                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <input type="text"
                                                                       @if ($errors->has('patient_mail')) class="formError"
                                                                       @endif  placeholder="Mail *"
                                                                       name="patient_mail"
                                                                       value="{{old('patient_mail')}}"
                                                                       data-mandatory="true"
                                                                       data-mail="true"
                                                                />
                                                            </div>
                                                            <div class="col-md-6">
                                                                <input type="text"
                                                                       @if ($errors->has('patient_conf_mail')) class="formError"
                                                                       @endif  placeholder="Confirmation mail *"
                                                                       name="patient_conf_mail"
                                                                       value="{{old('patient_conf_mail')}}"
                                                                       data-mailConf="true"
                                                                       data-nameMailConf="patient_mail"
                                                                />
                                                            </div>






                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <input type="password"
                                                                       @if ($errors->has('patient_mdp')) class="formError"
                                                                       @endif  placeholder="Mot de passe *"
                                                                       name="patient_mdp"
                                                                       value=""
                                                                       data-mandatory="true"
                                                                />
                                                            </div>

                                                            <div class="col-md-6">
                                                                <input type="password"
                                                                       @if ($errors->has('patient_mdp_confirmation')) class="formError"
                                                                       @endif  placeholder="Confirmation mot de passe *"
                                                                       name="patient_mdp_confirmation"
                                                                       data-mdpConf="true"
                                                                       data-nameMdpConf="patient_mdp"
                                                                       value=""
                                                                />
                                                            </div>


                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">

                                                                <input type="text"
                                                                       @if ($errors->has('patient_birthday')) class="formError"
                                                                       @endif  placeholder="Date de naissance *"
                                                                       name="patient_birthday"
                                                                       value="{{old('patient_birthday')}}"
                                                                       data-mandatory="true"
                                                                       id="date-mask2"
                                                                />
                                                            </div>


                                                        </div>

                                                    </div>


                                                    <input type="button" name="previous"
                                                           class="previous action-button prevB btnfirststep inf_liberal_block"
                                                           value="Précédent"/>
                                                    <input type="button" name="next"
                                                           class="next action-button nextB btnfirststep inf_liberal_block"
                                                           value="Suivant"/>


                                                    <input type="button" name="previous"
                                                           class="previous action-button prevB btnfirststep inf_remplacant_block"
                                                           value="Précédent"/>
                                                    <input type="button" name="next"
                                                           class="next action-button nextB btnfirststep inf_remplacant_block"
                                                           value="Suivant"/>


                                                    <input type="button" name="previous"
                                                           class="previous action-button prevB btnfirststep prevBPatient patient_block"
                                                           value="Précédent"/>
                                                    <input type="submit" name="next"
                                                           class="next action-button nextB btnfirststep nextBPatient patient_block "
                                                           value="Confirmer"/>

                                                </fieldset>


                                                <fieldset class="formInsc">

                                                    <div class="inf_liberal_block">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <h3 class="fs-subtitle">Inscription infirmièr(e)
                                                                    libéral(e)</h3>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <input type="text"
                                                                       placeholder="Date de naissance jj/mm/AAAA *"
                                                                       id="date-mask"
                                                                       name="lib_birthday"
                                                                       @if ($errors->has('lib_birthday')) class="formError"
                                                                       @endif
                                                                       value="{{old('lib_birthday')}}"
                                                                       data-mandatory="true"
                                                                />
                                                            </div>
                                                            <div class="col-md-6">
                                                                <input type="text" placeholder="Numéro Adeli"
                                                                       name="lib_adeli"
                                                                       value="{{ old('lib_adeli') }}"/>

                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <input type="text" placeholder="Secteurs de tournée"
                                                                       name="lib_secteur"
                                                                       id="tags-input"
                                                                       value="{{ old('lib_secteur') }}"/>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <input type="text" placeholder="Numéro RPPS"
                                                                       name="lib_rpps"
                                                                       value="{{ old('lib_rpps') }}"/>

                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-6" id="lib_searchbox-container">
                                                                <input type="text" placeholder="Adresse*"
                                                                       name="lib_adresse_inf_lib"
                                                                       class="autocompleteAdresse"
                                                                       id="lib_searchbox"
                                                                       value="{{ old('lib_adresse_inf_lib') }}"
                                                                       data-mandatory="true"
                                                                       data_prefix="lib_"
                                                                />
                                                                <input type="hidden" name="lib_lat" id="lib_lat"
                                                                       data-mandatory="true"
                                                                       data-error="lib_adresse_inf_lib"/>
                                                                <input type="hidden" name="lib_lng" id="lib_lng"
                                                                       data-mandatory="true"
                                                                       data-error="lib_adresse_inf_lib"/>
                                                                <div class="control drop" id="lib_dropdown-menu"></div>


                                                            </div>

                                                        </div>

                                                    </div>

                                                    <div class="inf_remplacant_block">

                                                        <div class="row">
                                                            <div class="col-md-3 blocLeftPrice">
                                                                <h3 class="titlerose">7 Jours<br/>d'essai gratuit</h3>
                                                                <div class="radio-toolbar">
                                                                    <input type="radio" name="remp_payment"
                                                                           id="radioAnnuel2"
                                                                           @if ( old('remp_payment') =='remp_14999_1y_7j_stripe' || empty( old('remp_payment') )) checked="checked"
                                                                           @endif class="priceradio"
                                                                           data-prefix="remp_"
                                                                           data-typeAbo="an_"
                                                                           value="remp_14999_1y_7j_stripe"/> <label
                                                                        for="radioAnnuel2">Annuel</label><br/>
                                                                    <input type="radio" name="remp_payment"
                                                                           id="radioMensuel2"
                                                                           class="priceradio"
                                                                           data-prefix="remp_"
                                                                           data-typeAbo="men_"
                                                                           value="remp_1499_1m_7j_stripe"
                                                                           @if ( old('remp_payment') =='remp_1499_1m_7j_stripe'  ))
                                                                           checked="checked" @endif/>
                                                                    <label
                                                                        for="radioMensuel2">Mensuel</label>

                                                                    <div class="radioAnnuel2BlockPrice blocPrice">
                                                                        <span
                                                                            class="price remp_an_price">149,99 € / an</span><br/>
                                                                        <span
                                                                            class="priceSubTitle remp_an_sub_price">Soit 12,49€ / mois</span>
                                                                        <br/>

                                                                    </div>

                                                                    <div class="radioMensuel2BlockPrice blocPrice">
                                                                        <span class="price remp_men_price">14,99 € / mois</span><br/>

                                                                    </div>

                                                                    <input class="promotional_code remp_codepromo"
                                                                           name="remp_codepromo"
                                                                           type="text" placeholder="Code Promo"
                                                                           data-prefix="remp_"/>
                                                                    <input class="remp_promotional_code"
                                                                           name="remp_promotional_code" type="hidden"
                                                                           value=""/>
                                                                    <span class="remp_msg_promotional_code_ok">Code promo valide. Il sera appliqué au paiement</span>
                                                                    <span class="remp_msg_promotional_code_ko">Code promo non valide.</span>

                                                                </div>
                                                            </div>

                                                            <div class="col-md-8 offset-1 blocRighttPrice">
                                                                <div class="row bloc_title">
                                                                    <div class="col-md-5 align-left"><img
                                                                            src="/logo/logo-idelib.png" alt=""
                                                                            class="logo-landing3"></div>
                                                                    <div class="col-md-7"><h3 class="titlerose2">
                                                                            Infirmiers remplaçants</h3>
                                                                    </div>
                                                                </div>


                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <ul class="pointPositif">
                                                                            <li>Gérez votre profil</li>
                                                                            <li>Postez vos annonces</li>
                                                                            <li>Recherchez des offres de remplacements
                                                                                par secteur
                                                                            </li>
                                                                            <li>Consultez & postulez sur les offres de
                                                                                remplacements
                                                                            </li>
                                                                            <li>Échangez avec vos potentiels titulaires
                                                                                par messagerie privée
                                                                            </li>
                                                                        </ul>


                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <ul class="pointPositif">
                                                                            <li>Intégrez une équipe
                                                                            </li>
                                                                            <li>Accédez au planning et à l'agenda de la
                                                                                tournée
                                                                            </li>
                                                                            <li>Faites vos relèves grâce au système de
                                                                                messagerie instantanée et la prise de
                                                                                notes vocales
                                                                            </li>
                                                                            <li>Gérez un agenda partagé de vos tournées
                                                                            </li>


                                                                        </ul>
                                                                    </div>
                                                                </div>

                                                            </div>

                                                        </div>


                                                    </div>
                                                    <input type="button" name="previous"
                                                           class="previous action-button prevB step2PinfLib btnsndststep "
                                                           value="Précédent"/>
                                                    <input type="button" name="next"
                                                           class="next action-button nextB step2NinfLib btnsndststep2"
                                                           value="Suivant"/>


                                                </fieldset>

                                                <fieldset class="formInsc">

                                                    <div class="inf_liberal_block">
                                                        <h3 class="title_insc">Présentez-vous</h3>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <textarea class="textarea_inscr"
                                                                          placeholder="Prenez le temps de vous présenter en quelques mots et mettez en confiance vos futurs patients"
                                                                          name="lib_prez_inf_liberal"
                                                                          rows="7">{{ old('lib_prez_inf_liberal') }}</textarea>
                                                            </div>

                                                        </div>


                                                    </div>

                                                    <div class="inf_remplaçant_block">

                                                        <div id="remp_recap" class="recap"></div>
                                                        <div id="error_message2"></div>
                                                        <input id="payment_method2" name="payment_method2"
                                                               type="hidden"/>


                                                        <div id="card-element2"></div>


                                                    </div>

                                                    <input type="button" name="previous"
                                                           class="previous action-button widthfull inf_remplacant_block"
                                                           value="Précédent"/>
                                                    <input type="submit" name="submitForm" id="submit-button2"
                                                           class=" action-button  step5NinfLib inf_remplacant_block"
                                                           value="Confirmer"/>


                                                    <input type="button" name="previous"
                                                           class="previous action-button prevB step3PinfLib inf_liberal_block"
                                                           value="Précédent"/>
                                                    <input type="button" name="next"
                                                           class="next action-button nextB step3NinfLib inf_liberal_block"
                                                           value="Suivant"/>

                                                </fieldset>


                                                <fieldset class="formInsc">

                                                    <div class="inf_liberal_block">

                                                        <div class="row">
                                                            <div class="col-md-3 blocLeftPrice">
                                                                <h3 class="titlerose">7 Jours<br/>d'essai gratuit</h3>
                                                                <div class="radio-toolbar">
                                                                    <input type="radio" name="lib_payment"
                                                                           id="radioAnnuel"
                                                                           @if ( old('lib_payment') =='lib_29999_1y_7j_stripe' || empty( old('lib_payment') )) checked="checked"
                                                                           @endif class="priceradio"
                                                                           data-prefix="lib_"
                                                                           data-typeAbo="an_"
                                                                           value="lib_29999_1y_7j_stripe"/> <label
                                                                        for="radioAnnuel">Annuel</label><br/>
                                                                    <input type="radio" name="lib_payment"
                                                                           id="radioMensuel"
                                                                           class="priceradio"
                                                                           data-prefix="lib_"
                                                                           data-typeAbo="men_"
                                                                           value="lib_2999_1m_7j_stripe"
                                                                           @if ( old('lib_payment') =='lib_2999_1m_7j_stripe'  ))
                                                                           checked="checked" @endif/>
                                                                    <label
                                                                        for="radioMensuel">Mensuel</label>

                                                                    <div class="radioAnnuelBlockPrice blocPrice">
                                                                        <span
                                                                            class="price lib_an_price">299,99 € / an</span><br/>
                                                                        <span
                                                                            class="priceSubTitle lib_an_sub_price">Soit 24,99€ / mois</span>
                                                                        <br/>

                                                                    </div>

                                                                    <div class="radioMensuelBlockPrice blocPrice">
                                                                        <span
                                                                            class="price lib_men_price">29,99 € / mois</span><br/>

                                                                    </div>

                                                                    <input class="promotional_code lib_codepromo"
                                                                           name="lib_codepromo"
                                                                           type="text" placeholder="Code Promo"
                                                                           data-prefix="lib_"/>
                                                                    <input class="lib_promotional_code"
                                                                           name="lib_promotional_code" type="hidden"
                                                                           value=""/>
                                                                    <span class="lib_msg_promotional_code_ok">Code promo valide. Il sera appliqué au paiement</span>
                                                                    <span class="lib_msg_promotional_code_ko">Code promo non valide.</span>

                                                                </div>
                                                            </div>

                                                            <div class="col-md-8 offset-1 blocRighttPrice">
                                                                <div class="row bloc_title">
                                                                    <div class="col-md-5 align-left"><img
                                                                            src="/logo/logo-idelib.png" alt=""
                                                                            class="logo-landing3"></div>
                                                                    <div class="col-md-7"><h3 class="titlerose2">
                                                                            Infirmiers libéraux &
                                                                            remplaçants réguliers</h3>
                                                                    </div>
                                                                </div>


                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <ul class="pointPositif">
                                                                            <li>Gérez votre profil</li>
                                                                            <li>Postez vos annonces de remplacement</li>
                                                                            <li>Gagnez du temps</li>
                                                                            <li>Paramétrez un planning commun</li>
                                                                            <li>Gérez un agenda partagé de vos
                                                                                tournées
                                                                            </li>
                                                                            <li>Stockez et sécurisez vos données</li>
                                                                        </ul>


                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <ul class="pointPositif">
                                                                            <li>Optimisez la gestion de votre
                                                                                patientèle
                                                                            </li>
                                                                            <li>Paramètrez toutes les informations pour
                                                                                la facturation
                                                                            </li>
                                                                            <li>Développez votre patientèle</li>
                                                                            <li>Contactez les potentiels remplaçants par
                                                                                messagerie privée
                                                                            </li>
                                                                            <li>Faites vos relèves grâce au système de
                                                                                mesagerie et la prise
                                                                                de notes vocales
                                                                            </li>
                                                                            <li>Évaluez vos remplaçants au terme de
                                                                                votre collaboration
                                                                            </li>

                                                                        </ul>
                                                                    </div>
                                                                </div>

                                                            </div>

                                                        </div>


                                                    </div>

                                                    <input type="button" name="previous"
                                                           class="previous action-button prevB step5PinfLib"
                                                           value="Précédent"/>
                                                    <input type="button" name="next"
                                                           class="next action-button nextB step5NinfLib finalStep"
                                                           value="Choisir"/>

                                                </fieldset>

                                                <fieldset class="formInsc">

                                                    <div class="inf_liberal_block"></div>
                                                    <div id="error_message"></div>
                                                    <div id="lib_recap" class="recap"></div>
                                                    <input id="payment_method" name="payment_method" type="hidden"/>
                                                    <input name="lib_promotional_code" type="hidden" value=""/>


                                                    <div id="card-element"></div>

                                                    <input type="button" name="previous"
                                                           class="previous action-button widthfull "
                                                           value="Précédent"/>
                                                    <input type="submit" name="submitForm" id="submit-button"
                                                           class=" action-button  step5NinfLib"
                                                           value="Confirmer"/>
                                                </fieldset>

                                            </form>
                                        </div>

                                    </div>


                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    </div>

@endsection
