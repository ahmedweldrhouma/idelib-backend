@extends('frontend.layouts.app')

@section('title', __('Register'))

@push('after-scripts')
    <script type="text/javascript"
            src="//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <script src="https://unpkg.com/imask"></script>
    <script src="https://js.stripe.com/v3/"></script>

    <script>

    </script>
    <script src="js/typeahead.js"></script>
    <script src="js/bootstrap-tagsinput.js"></script>
    <script>
        (function ($) {


            function checkValidityPromocode() {


                @if (Auth::user()->isType(\App\Domains\Auth\Models\User::TYPE_SUBSTITUTE_NURSE))
                var prefix = 'remp_';

                @else
                var prefix = 'lib_';

                @endif

                var el = $('.' + prefix + 'codepromo');
                var promo = el.val();

                var offre_selected = $("input[name='" + prefix + "payment']:checked").val();
                var offre_type = $("input[name='" + prefix + "payment']:checked").data('typeabo');

                if (promo.length <= 3) {
                    return false;
                } else {
                    $('.' + prefix + 'msg_promotional_code_ko').hide();
                    $('.' + prefix + 'msg_promotional_code_ok').hide();

                    $.ajax({
                        url: "{{ route('frontend.auth.validatePromocode', [], false) }}",
                        data: {code: promo, offre: offre_selected},
                        method: "POST",

                    }).done(function (result) {
                        if (result.success == true) {
                            //Il y a un promocode
                            $('.' + prefix + 'promotional_code').val(result.id);
                            $('.' + prefix + 'msg_promotional_code_ok').show();
                            $('.' + prefix + 'msg_promotional_code_ko').hide();

                            if (offre_type == 'men_')
                                $('.' + prefix + offre_type + 'price').html('<strike>' + result.old_price + '€ / mois</strike><br/>' + result.new_price + '€ / mois');
                            else {
                                $('.' + prefix + offre_type + 'price').html('<strike>' + result.old_price + '€ / an</strike><br/>' + result.new_price + '€ / an');
                                $('.' + prefix + offre_type + 'sub_price').html('Soit ' + result.new_price_men + '€ / mois <strike>' + result.old_price_men + '€ / mois</strike><br/>');
                            }

                            el.addClass('green');
                        } else {
                            //il n'y en a pas
                            el.removeClass('green');
                            $('.' + prefix + 'promotional_code').val();
                            $('.' + prefix + 'msg_promotional_code_ko').show();
                            $('.' + prefix + 'msg_promotional_code_ok').hide();

                            if (offre_type == 'men_')
                                $('.' + prefix + offre_type + 'price').html(result.new_price + '€ / mois');
                            else {
                                $('.' + prefix + offre_type + 'price').html(result.new_price + '€ / an');
                                $('.' + prefix + offre_type + 'sub_price').html('Soit ' + result.new_price_men + '€ / mois ');
                            }

                        }


                    });
                }
            }


            $('.promotional_code').keyup(function () {

                checkValidityPromocode();
            });


            $('.priceradio').click(function () {
                $('.blocPrice').hide();
                $('.' + $(this).attr('id') + 'BlockPrice').show();
                checkValidityPromocode();

            });

            @if (Auth::user()->isType(\App\Domains\Auth\Models\User::TYPE_SUBSTITUTE_NURSE))
            mountCardStrip('card-element2', 'submit-button2');

            @else
            mountCardStrip('card-element', 'submit-button');

            @endif


            function mountCardStrip(idToMount, idSubmit) {
                const stripe = Stripe('{{ env('STRIPE_KEY') }}');
                const elements = stripe.elements();
                const cardElement = elements.create('card');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });


                cardElement.unmount();

                //cardElement.mount('#card-element', {
                cardElement.mount('#' + idToMount, {
                    classes: {
                        base: 'form-input form-bg'
                    }
                });

                //const cardButton = document.getElementById('submit-button');
                const cardButton = document.getElementById(idSubmit);

                cardButton.addEventListener('click', async (e) => {
                    e.preventDefault();
                    const {paymentMethod, error} = await stripe.createPaymentMethod('card', cardElement);


                    if (error) {
                        // Display "error.message" to the user...
                        document.getElementById('error_message').style.display = true
                        return false;
                    } else {
                        document.getElementById(idToMount).value = paymentMethod.id;
                        document.getElementById('msform').submit();
                    }

                });


            }
        })
        (jQuery);

    </script>

@endpush

@section('content')

    <div class="page-header">
        <div class="container">
            <div class="title-box">
            </div>
        </div>
        <div class="shape-bottom">
            <img src="/images/shapes/price-shape.svg" alt="shape" class="bottom-shape img-fluid">
        </div>
    </div>

    <div class="container-row-g row justify-content-center mx-0">
        <div class="col-md-8 pt-lg-4 px-md-3">
            <div class="card h-100">
                <div class="card-body">
                    <div class="login-form px-md-5">
                        <div class="login-page">
                            <div class="col-md-12">
                                <div class="container-fluid">
                                    <!-- <a href="index.html" class="res-logo"><img src="/logo/logo-idelib.png" alt="Logo"/></a> -->
                                    <div class="login-form">

                                        <div class="login-form-head">
                                            <!--                                            <a href="/" class="logRegis"><img src="/img/logo-idelib200x100.png"
                                                                                                                          width="100"
                                                                                                                          class="logo-landing2"></a>-->
                                            <h2 class="inscriptionH2">Inscription
                                                <!-- Solution de gestion de patients, d'organisation et de simplification de l'activité libéral infirmier. -->
                                            </h2>
                                        </div>


                                        <div class="">
                                            <!-- multistep form -->

                                            @if ($errors->any())
                                                <div class="alert alert-danger">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                            <form id="msform" method="post"
                                                  action="{{ route('frontend.auth.registerSubscription', [], false) }}">
                                                @csrf

                                                <!-- fieldsets -->

                                                @if (Auth::user()->isType(\App\Domains\Auth\Models\User::TYPE_SUBSTITUTE_NURSE))
                                                    <fieldset class="formInsc">


                                                        <div class="inf_remplacant_block">

                                                            <div class="row">
                                                                <div class="col-md-3 blocLeftPrice">
                                                                    <h3 class="titlerose">7 Jours<br/>d'essai gratuit
                                                                    </h3>
                                                                    <div class="radio-toolbar">
                                                                        <input type="radio" name="remp_payment"
                                                                               id="radioAnnuel2"
                                                                               @if ( old('remp_payment') =='remp_14999_1y_7j_stripe' || empty( old('remp_payment') )) checked="checked"
                                                                               @endif class="priceradio"
                                                                               data-prefix="remp_"
                                                                               data-typeAbo="an_"
                                                                               value="remp_14999_1y_7j_stripe"/> <label
                                                                            for="radioAnnuel2">Annuel</label><br/>
                                                                        <input type="radio" name="remp_payment"
                                                                               id="radioMensuel2"
                                                                               class="priceradio"
                                                                               data-prefix="remp_"
                                                                               data-typeAbo="men_"
                                                                               value="remp_1499_1m_7j_stripe"
                                                                               @if ( old('remp_payment') =='remp_1499_1m_7j_stripe'  ))
                                                                               checked="checked" @endif/>
                                                                        <label
                                                                            for="radioMensuel2">Mensuel</label>

                                                                        <div class="radioAnnuel2BlockPrice blocPrice">
                                                                        <span
                                                                            class="price remp_an_price">149,99 € / an</span><br/>
                                                                            <span
                                                                                class="priceSubTitle remp_an_sub_price">Soit 12,49€ / mois</span>
                                                                            <br/>

                                                                        </div>

                                                                        <div class="radioMensuel2BlockPrice blocPrice">
                                                                            <span class="price remp_men_price">14,99 € / mois</span><br/>

                                                                        </div>

                                                                        <input class="promotional_code remp_codepromo"
                                                                               name="remp_codepromo"
                                                                               type="text" placeholder="Code Promo"
                                                                               data-prefix="remp_"/>
                                                                        <input class="remp_promotional_code"
                                                                               name="remp_promotional_code"
                                                                               type="hidden"
                                                                               value=""/>

                                                                        <input type="hidden" name="havetosubscribe"
                                                                               value="remp"/>


                                                                        <span class="remp_msg_promotional_code_ok">Code promo valide. Il sera appliqué au paiement</span>
                                                                        <span class="remp_msg_promotional_code_ko">Code promo non valide.</span>

                                                                    </div>
                                                                </div>

                                                                <div class="col-md-8 offset-1 blocRighttPrice">
                                                                    <div class="row bloc_title">
                                                                        <div class="col-md-5 align-left"><img
                                                                                src="/logo/logo-idelib.png" alt=""
                                                                                class="logo-landing3"></div>
                                                                        <div class="col-md-7"><h3 class="titlerose2">
                                                                                Infirmiers remplaçants</h3>
                                                                        </div>
                                                                    </div>


                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <ul class="pointPositif">
                                                                                <li>Gérez votre profil</li>
                                                                                <li>Postez vos annonces</li>
                                                                                <li>Recherchez des offres de
                                                                                    remplacements
                                                                                    par secteur
                                                                                </li>
                                                                                <li>Consultez & postulez sur les offres
                                                                                    de
                                                                                    remplacements
                                                                                </li>
                                                                                <li>Échangez avec vos potentiels
                                                                                    titulaires
                                                                                    par messagerie privée
                                                                                </li>
                                                                            </ul>


                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <ul class="pointPositif">
                                                                                <li>Intégrez une équipe
                                                                                </li>
                                                                                <li>Accédez au planning et à l'agenda de
                                                                                    la
                                                                                    tournée
                                                                                </li>
                                                                                <li>Faites vos relèves grâce au système
                                                                                    de
                                                                                    messagerie instantanée et la prise
                                                                                    de
                                                                                    notes vocales
                                                                                </li>
                                                                                <li>Gérez un agenda partagé de vos
                                                                                    tournées
                                                                                </li>


                                                                            </ul>
                                                                        </div>
                                                                        <div class="col-md-12">

                                                                            <br/><br/>
                                                                            <input id="payment_method2"
                                                                                   name="payment_method2"
                                                                                   type="hidden"/>


                                                                            <div id="card-element2"></div>
                                                                            <input type="submit" name="submitForm"
                                                                                   id="submit-button2"
                                                                                   class=" action-button  step5NinfLib inf_remplacant_block"
                                                                                   value="Confirmer"/>

                                                                        </div>
                                                                    </div>

                                                                </div>


                                                            </div>


                                                        </div>


                                                    </fieldset>
                                                @endif

                                                @if (Auth::user()->isType(\App\Domains\Auth\Models\User::TYPE_NURSE) )

                                                    <fieldset class="formInsc">

                                                        <div class="inf_liberal_block">

                                                            <div class="row">
                                                                <div class="col-md-3 blocLeftPrice">
                                                                    <h3 class="titlerose">7 Jours<br/>d'essai gratuit
                                                                    </h3>
                                                                    <div class="radio-toolbar">
                                                                        <input type="radio" name="lib_payment"
                                                                               id="radioAnnuel"
                                                                               @if ( old('lib_payment') =='lib_29999_1y_7j_stripe' || empty( old('lib_payment') )) checked="checked"
                                                                               @endif class="priceradio"
                                                                               data-prefix="lib_"
                                                                               data-typeAbo="an_"
                                                                               value="lib_29999_1y_7j_stripe"/> <label
                                                                            for="radioAnnuel">Annuel</label><br/>
                                                                        <input type="radio" name="lib_payment"
                                                                               id="radioMensuel"
                                                                               class="priceradio"
                                                                               data-prefix="lib_"
                                                                               data-typeAbo="men_"
                                                                               value="lib_2999_1m_7j_stripe"
                                                                               @if ( old('lib_payment') =='lib_2999_1m_7j_stripe'  ))
                                                                               checked="checked" @endif/>



                                                                        <label
                                                                            for="radioMensuel">Mensuel</label>

                                                                        <div class="radioAnnuelBlockPrice blocPrice">
                                                                        <span
                                                                            class="price lib_an_price">299,99 € / an</span><br/>
                                                                            <span
                                                                                class="priceSubTitle lib_an_sub_price">Soit 24,99€ / mois</span>
                                                                            <br/>

                                                                        </div>

                                                                        <div class="radioMensuelBlockPrice blocPrice">
                                                                        <span
                                                                            class="price lib_men_price">29,99 € / mois</span><br/>

                                                                        </div>

                                                                        <input class="promotional_code lib_codepromo"
                                                                               name="lib_codepromo"
                                                                               type="text" placeholder="Code Promo"
                                                                               data-prefix="lib_"/>
                                                                        <input class="lib_promotional_code"
                                                                               name="lib_promotional_code" type="hidden"
                                                                               value=""/>
                                                                        <input type="hidden" name="havetosubscribe"
                                                                               value="inf"/>
                                                                        <span class="lib_msg_promotional_code_ok">Code promo valide. Il sera appliqué au paiement</span>
                                                                        <span class="lib_msg_promotional_code_ko">Code promo non valide.</span>

                                                                    </div>
                                                                </div>

                                                                <div class="col-md-8 offset-1 blocRighttPrice">
                                                                    <div class="row bloc_title">
                                                                        <div class="col-md-5 align-left"><img
                                                                                src="/logo/logo-idelib.png" alt=""
                                                                                class="logo-landing3"></div>
                                                                        <div class="col-md-7"><h3 class="titlerose2">
                                                                                Infirmiers libéraux &
                                                                                remplaçants réguliers</h3>
                                                                        </div>
                                                                    </div>


                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <ul class="pointPositif">
                                                                                <li>Gérez votre profil</li>
                                                                                <li>Postez vos annonces de
                                                                                    remplacement
                                                                                </li>
                                                                                <li>Gagnez du temps</li>
                                                                                <li>Paramétrez un planning commun</li>
                                                                                <li>Gérez un agenda partagé de vos
                                                                                    tournées
                                                                                </li>
                                                                                <li>Stockez et sécurisez vos données
                                                                                </li>
                                                                            </ul>


                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <ul class="pointPositif">
                                                                                <li>Optimisez la gestion de votre
                                                                                    patientèle
                                                                                </li>
                                                                                <li>Paramètrez toutes les informations
                                                                                    pour
                                                                                    la facturation
                                                                                </li>
                                                                                <li>Développez votre patientèle</li>
                                                                                <li>Contactez les potentiels remplaçants
                                                                                    par
                                                                                    messagerie privée
                                                                                </li>
                                                                                <li>Faites vos relèves grâce au système
                                                                                    de
                                                                                    mesagerie et la prise
                                                                                    de notes vocales
                                                                                </li>
                                                                                <li>Évaluez vos remplaçants au terme de
                                                                                    votre collaboration
                                                                                </li>

                                                                            </ul>
                                                                        </div>

                                                                        <div class="col-md-12">

                                                                            <br/><br/>
                                                                            <input id="payment_method"
                                                                                   name="payment_method" type="hidden"/>


                                                                            <div id="card-element"></div>


                                                                            <input type="submit" name="submitForm"
                                                                                   id="submit-button"
                                                                                   class=" action-button  step5NinfLib"
                                                                                   value="Confirmer"/>

                                                                        </div>
                                                                    </div>

                                                                </div>

                                                            </div>


                                                        </div>


                                                    </fieldset>
                                                @endif
                                            </form>
                                        </div>

                                    </div>


                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    </div>

@endsection
