@extends('frontend.layouts.app')

@section('title', __('Login'))

@section('content')
    <div class="container-row row justify-content-center mx-0">
        <div class="col-lg-5 col-md-8 col-lg-6 pt-5 px-md-5">
            <div class="card h-100">
                <div class="card-body">
                    <div class="login-form px-md-5">
                        <div class="text-center mb-4">
                            <img class="logo mb-3 d-none d-sm-block" src="{{ url('/img/logo-idelib200x100.png') }}"
                                 alt="logo">
                            <h1 class="text-dark bolder mb-1">Connexion</h1>
                            <div class="sub-title">Accéder à votre espace
                            </div>
                        </div>
                        <x-forms.post action="login">
                            @if ($errors->has('email'))
                                <div class="alert alert-danger">
                                    {{ $errors->first('email') }}
                                </div>
                            @endif
                            @if(session()->get('flash_danger'))
                                <x-utils.alert type="danger" class="header-message">
                                    {{ session()->get('flash_danger') }}
                                </x-utils.alert>
                            @endif
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="input-icon">
                                        <span class="icon far fa-envelope"></span>
                                    </div>
                                    <input type="email" name="email" id="email" class="form-control"
                                           placeholder="{{ __('E-mail Address') }}" value="{{ old('email') }}"
                                           maxlength="255" required autofocus autocomplete="email"/>
                                </div>
                            </div><!--form-group-->

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="input-icon">
                                        <span class="icon fas fa-lock"></span>
                                    </div>
                                    <input type="password" name="password" id="password" class="form-control"
                                           placeholder="{{ __('Password') }}" maxlength="100" required
                                           autocomplete="current-password"/>
                                </div>
                            </div><!--form-group-->

                            <div class="col-md-12 text-center">
                                <x-utils.link :href="route('frontend.auth.password.request')" class="btn btn-link"
                                              :text="__('Forgot Your Password?')"/>
                            </div><!--form-group-->

                            @if(config('boilerplate.access.captcha.login'))
                                <div class="row">
                                    <div class="col">
                                        @captcha
                                        <input type="hidden" name="captcha_status" value="true"/>
                                    </div><!--col-->
                                </div><!--row-->
                            @endif

                            <div class="form-group row text-center">
                                <div class="col-md-12">
                                    <button class="btn theme-btn w-100" type="submit">@lang('Login')</button>
                                </div>
                            </div><!--form-group-->

                            <div class="text-center">
                                @include('frontend.auth.includes.social')
                            </div>
                            <div class="form-group row text-center">
                                <p>Vous n'avez pas encore de compte ?
                                    <x-utils.link
                                        :text="__('Register')"
                                        :href=" route('frontend.auth.register') "
                                    />
                                </p>
                            </div>
                        </x-forms.post>
                        <div class="text-center">
                            <a href="{{ route('frontend.index') }}" class="btn other-btn">Retour à l'accueil</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
