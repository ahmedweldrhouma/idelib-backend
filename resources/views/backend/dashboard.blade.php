@extends('backend.layouts.app')

@section('title', __('Dashboard'))

@push('after-scripts')
    @if ($logged_in_user->isSuperAdmin())
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/data.js"></script>
        <script src="https://code.highcharts.com/modules/drilldown.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/modules/export-data.js"></script>
        <script src="https://code.highcharts.com/modules/accessibility.js"></script>

        <script>
            Highcharts.setOptions({
                lang: {
                    downloadCSV: "Télécharger csv",
                    downloadJPEG: "Télécharger Image format jpeg",
                    viewFullscreen: "Voir en plein écran",
                    printChart: "Imprimer",
                    downloadPNG: "Télécharger Image format PNG",
                    downloadPDF: "Télécharger Image format PDF",
                    downloadSVG: "Télécharger Image format SVG",
                }
            })
            Highcharts.chart('container4', {
                chart: {
                    type: 'pie',
                    height :  260
                },
                title: {
                    text: '',
                    align: ''
                },
                colors:['#fa319a', '#00e2ff'],


                accessibility: {
                    announceNewData: {
                        enabled: true
                    },
                    point: {
                        valueSuffix: ''
                    }
                },

                plotOptions: {
                    series: {
                        color:'#00e2ff',
                        borderRadius: 5,
                        dataLabels: {
                            enabled: true,
                            format: '<span class="{point.name}_label">{point.name}: {point.y:.2f}%</span>',
                            useHTLM: true,
                            className: 'point_label'
                        },

                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b><br/>'
                },

                series: [
                    {
                        name: 'Utilisateurs',
                        colorByPoint: true,
                        data: [

                            {
                                name: 'Infirmiers',
                                y: {{ $statsAdmin['nbNurseAbonnePercent'] }},
                                drilldown: 'Infirmiers'
                            },
                            {
                                name: 'Remplaçants',
                                y: {{ $statsAdmin['nbSubstituteAbonnePercent'] }},
                                drilldown: 'Remplaçants',
                                classNamePoint:'infirmiers_label'
                            },

                        ]
                    }
                ],

            });


            Highcharts.chart('container3', {
                chart: {
                    type: 'pie',
                    height :  260
                },
                title: {
                    text: '',
                    align: ''
                },
                colors:['#fa319a', '#00e2ff'],


                accessibility: {
                    announceNewData: {
                        enabled: true
                    },
                    point: {
                        valueSuffix: ''
                    }
                },

                plotOptions: {
                    series: {
                        color:'#00e2ff',
                        borderRadius: 5,
                        dataLabels: {
                            enabled: true,
                            format: '<span class="{point.name}_label">{point.name}: {point.y:.2f}%</span>',
                            useHTLM: true,
                            className: 'point_label'
                        },

                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b><br/>'
                },

                series: [
                    {
                        name: 'Utilisateurs',
                        colorByPoint: true,
                        data: [
                            {
                                name: 'Mensuels',
                                y: {{ $statsAdmin['nbSubstituteMensuelPercent'] }},
                                drilldown: 'Mensuels',
                                classNamePoint:'infirmiers_label'
                            },
                            {
                                name: 'Annuels',
                                y: {{ $statsAdmin['nbSubstituteAnnuelPercent'] }},
                                drilldown: 'Annuels'
                            },

                        ]
                    }
                ],

            });



            Highcharts.chart('container2', {
                chart: {
                    type: 'pie',
                    height :  260
                },
                title: {
                    text: '',
                    align: ''
                },
                colors:['#fa319a', '#00e2ff'],


                accessibility: {
                    announceNewData: {
                        enabled: true
                    },
                    point: {
                        valueSuffix: ''
                    }
                },

                plotOptions: {
                    series: {
                        color:'#00e2ff',
                        borderRadius: 5,
                        dataLabels: {
                            enabled: true,
                            format: '<span class="{point.name}_label">{point.name}: {point.y:.2f}%</span>',
                            useHTLM: true,
                            className: 'point_label'
                        },

                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b><br/>'
                },

                series: [
                    {
                        name: 'Utilisateurs',
                        colorByPoint: true,
                        data: [
                            {
                                name: 'Mensuels',
                                y: {{ $statsAdmin['nbNurseMensuelPercent'] }},
                                drilldown: 'Mensuels',
                                classNamePoint:'infirmiers_label'
                            },
                            {
                                name: 'Annuels',
                                y: {{ $statsAdmin['nbNurseAnnuelPercent'] }},
                                drilldown: 'Annuels'
                            },

                        ]
                    }
                ],

            });


            Highcharts.chart('container', {
                chart: {
                    type: 'pie',
                    height :  260
                },
                title: {
                    text: '',
                    align: ''
                },
                colors:['#fa319a', '#00e2ff', '#9c9b9b'],


                accessibility: {
                    announceNewData: {
                        enabled: true
                    },
                    point: {
                        valueSuffix: ''
                    }
                },

                plotOptions: {
                    series: {
                        color:'#00e2ff',
                        borderRadius: 5,
                        dataLabels: {
                            enabled: true,
                            format: '<span class="{point.name}_label">{point.name}: {point.y}</span>',
                            useHTLM: true,
                            className: 'point_label'
                        },

                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                },

                series: [
                    {
                        name: 'Utilisateurs',
                        colorByPoint: true,
                        data: [
                            {
                                name: 'Infirmiers',
                                y: {{ $statsAdmin['nbNurse'] }},
                                drilldown: 'Infirmiers',
                                classNamePoint:'infirmiers_label'
                            },
                            {
                                name: 'Remplaçants',
                                y: {{ $statsAdmin['nbSubstitute'] }},
                                drilldown: 'Remplaçants'
                            },
                            {
                                name: 'Patients',
                                y: {{ $statsAdmin['nbPatient'] }},
                                drilldown: 'Patients'
                            },

                        ]
                    }
                ],

            });

            Highcharts.chart('container5', {
                chart: {
                    type: 'pie',
                    height :  260
                },
                title: {
                    text: '',
                    align: ''
                },
                colors:['#fa319a', '#00e2ff'],


                accessibility: {
                    announceNewData: {
                        enabled: true
                    },
                    point: {
                        valueSuffix: ''
                    }
                },

                plotOptions: {
                    series: {
                        color:'#00e2ff',
                        borderRadius: 5,
                        dataLabels: {
                            enabled: true,
                            format: '<span class="{point.name}_label">{point.name}: {point.y:.2f}%</span>',
                            useHTLM: true,
                            className: 'point_label'
                        },

                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b><br/>'
                },

                series: [
                    {
                        name: 'Utilisateurs Totla',
                        colorByPoint: true,
                        data: [

                            {
                                name: 'Infirmiers',
                                y: {{ $statsAdmin['nbNurseAbonnePercentAll'] }},
                                drilldown: 'Infirmiers'
                            },
                            {
                                name: 'Remplaçants',
                                y: {{ $statsAdmin['nbSubstituteAbonnePercentAll'] }},
                                drilldown: 'Remplaçants',
                                classNamePoint:'infirmiers_label'
                            },

                        ]
                    }
                ],

            });

            Highcharts.chart('container6', {
                chart: {
                    type: 'pie',
                    height :  260
                },
                title: {
                    text: '',
                    align: ''
                },
                colors:['#fa319a', '#00e2ff'],


                accessibility: {
                    announceNewData: {
                        enabled: true
                    },
                    point: {
                        valueSuffix: ''
                    }
                },

                plotOptions: {
                    series: {
                        color:'#00e2ff',
                        borderRadius: 5,
                        dataLabels: {
                            enabled: true,
                            format: '<span class="{point.name}_label">{point.name}: {point.y:.2f}%</span>',
                            useHTLM: true,
                            className: 'point_label'
                        },

                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b><br/>'
                },

                series: [
                    {
                        name: 'Utilisateurs total',
                        colorByPoint: true,
                        data: [
                            {
                                name: 'Mensuels',
                                y: {{ $statsAdmin['nbSubstituteMensuelPercentAll'] }},
                                drilldown: 'Mensuels',
                                classNamePoint:'infirmiers_label'
                            },
                            {
                                name: 'Annuels',
                                y: {{ $statsAdmin['nbSubstituteAnnuelPercentAll'] }},
                                drilldown: 'Annuels'
                            },

                        ]
                    }
                ],

            });


            document.getElementById('container').style. maxHeight = '450px';


        </script>
    @endif

@endpush
@section('content')
    @if ($logged_in_user->isAdmin() || $logged_in_user->isNurse() || $logged_in_user->isSubstituteNurse())
        <div class="row">
            <div class="col-md-6">
                <div class="row m-0">
                    <div class="col-md-4 px-1">
                        <div class="card mb-3">
                            <div class="card-header">
                                <div
                                    class="card-title d-flex justify-content-center align-content-end m-0 py-2">
                                    <span
                                        class="fs-2hx fw-bold text-dark me-2 lh-1 ls-n2">{{ $stats['covid']['y']??0 }}</span>
                                    <span class="text-gray-400 pt-1 fw-semibold fs-6">{{ __('Covid') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 px-1">
                        <div class="card mb-3">
                            <div class="card-header">
                                <div
                                    class="card-title d-flex justify-content-center align-content-end m-0 py-2">
                            <span class="fs-2hx fw-bold text-dark me-2 lh-1 ls-n2">{{ $stats['punctual']['y']??0
                                }}</span>
                                    <span
                                        class="text-gray-400 pt-1 fw-semibold fs-6">{{ __('punctual') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class=" col-md-4 px-1">
                        <div class="card mb-3">
                            <div class="card-header">
                                <div
                                    class="card-title d-flex justify-content-center align-content-end m-0 py-2">
                            <span class="fs-2hx fw-bold text-dark me-2 lh-1 ls-n2">{{ $stats['chronic']['y']??0
                                }}</span>
                                    <span
                                        class="text-gray-400 pt-1 fw-semibold fs-6">{{ __('chronics') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 p-0">
                        <x-backend.card>
                            @if ($stats->isEmpty())
                                <x-slot name="body">
                                    <div
                                        class="card-title d-flex justify-content-center align-content-end m-0 py-2">
                                                <span
                                                    class="fs-2 fw-bold text-dark me-2 lh-1 ls-n2">Pas de patients</span>
                                    </div>
                                    <div class="d-flex justify-content-center align-content-end m-0 py-2">

                                        <img class="logo mb-3" src="{{ url('/img/Medicine-bro.png') }}"
                                             width="40%"
                                             height="40%">
                                    </div>
                                    <div class="d-flex justify-content-center align-content-end m-0 py-2">
                                        <div class="dropdown show">
                                            <a class="btn btn-primary btn-sm dropdown-toggle" href="#"
                                               role="button"
                                               id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                                               aria-expanded="false">
                                                Ajouter
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                <x-utils.link
                                                    :href="route('admin.patient.create', ['type' => 'chronic'])"
                                                    class="dropdown-item"
                                                    :text="__('Patient chronique')"
                                                />
                                                <x-utils.link
                                                    :href="route('admin.patient.create', ['type' => 'punctual'])"
                                                    class="dropdown-item"
                                                    :text="__('Patient ponctuel')"
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </x-slot>
                            @else
                                <x-slot name="body">
                                    <div id="patients-pie"></div>
                                </x-slot>
                            @endif
                        </x-backend.card>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card h-md-100">
                    <div class="card-header border-0  align-items-center justify-content-between d-flex">
                        <div class="card-title mb-1">
                            <h4 class="card-label fw-bold">Quoi de neuf aujourd'hui?</h4>
                            <h6 class="text-muted mt-1 fw-semibold fs-7"><strong class="events_nbr">0</strong>
                                evénements
                                vous attendent</h6>
                        </div>
                        <div class="card-toolbar">
                            <a href="#" class="btn btn-sm btn-primary" data-toggle="modal"
                               data-target="#addAppointment">Ajouter
                                un rendez-vous</a>
                        </div>
                    </div>
                    <div class="card-body pt-7 px-0">
                        <ul class="nav nav-stretch nav-pills nav-pills-custom nav-pills-active-custom d-flex justify-content-between mb-4 px-4"
                            role="tablist" id="week-menu">
                            @php $monday = \Carbon\Carbon::now()->tz("Europe/Paris")->subDays(5) @endphp
                            @for($i =0 ; $i<10;$i++)
                                @php if($i!=0)$monday=$monday->copy()->addDay() @endphp
                                <li class="nav-item p-0 ms-0" role="presentation">
                                    <a class="nav-link btn d-flex flex-column flex-center rounded-pill min-w-45px py-4 px-3 btn-active-danger @if($monday->isToday()) active @endif"
                                       data-bs-toggle="tab" href="#kt_timeline_widget_3_tab_content_1"
                                       tabindex="-1"
                                       role="tab"
                                       data-date="{{ $monday->format('Y-m-d') }}">
                                <span class="fs-7 fw-semibold" style="text-transform: capitalize">{{
                                    substr(__(strtolower($monday->format('l'))),0,2) }}</span>
                                        <span class="fs-6 fw-bold">{{ $monday->format('d') }}</span>
                                    </a>
                                </li>
                            @endfor
                        </ul>
                        <div class="loading" id="loading-spinner" style="display:none">
                            <img class="loading-spinner" src=" {{ asset('img/loading-spinner.gif') }} "
                                 alt="Loading...">
                        </div>
                        <div class="mb-2 px-3" id="timeline">

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="addAppointment" tabindex="-1" role="dialog" aria-labelledby="addAppointment"
             aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="ModalLabel">Préciser les détails du rendez-vous :</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form name="add_appointment" id="add_appointment" method="POST"
                              action="{{ route('admin.appointment.store') }}">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="title" class="col-form-label"><h6> Titre : </h6></label>
                                    <input type="text" class="form-control" name="title" id="title">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="address" class="col-form-label"><h6> Addresse : </h6></label>
                                    <input type="text" class="form-control" name="address" id="address">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="start_date" class="col-form-label"><h6> Date de début : </h6>
                                    </label>
                                    <input type="date" class="form-control" name="start_date" id="start_date">
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="end_date" class="col-form-label"><h6> Date de fin : </h6>
                                    </label>
                                    <input type="date" class="form-control" name="end_date" id="end_date">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="start_time" class="col-form-label"><h6> Heure de début : </h6>
                                    </label>
                                    <input type="time" class="form-control" name="start_time" id="start_time">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="end_time" class="col-form-label"><h6> Heure de fin : </h6>
                                    </label>
                                    <input type="time" class="form-control" name="end_time" id="end_time">
                                </div>
                            </div>
                            <span id="error"></span>
                            <div class="form-group">
                                <label for="note" class="col-form-label"><h6> Notes : </h6></label>
                                <input type="text" class="form-control" name="note" id="note">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">
                                    Annuler
                                </button>
                                <button type="submit" class="btn btn-sm  btn-flickr">Ajouter</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        @push('before-scripts')
            <script>
                let stats = @json($stats);
            </script>
            <script src="{{ mix('js/dashboard.js') }}"></script>
        @endpush
    @endif
    @if ($logged_in_user->isSuperAdmin())
        <div class="row">
            <div class="col-md-8">
                <x-backend.card>
                    <x-slot name="body">
                        <div class="card-title d-flex justify-content-center align-content-end m-0 py-2">
                            <span class="fs-2 fw-bold  me-2 lh-1 ls-n2 titStats">Utilisateurs inscrits</span>
                        </div>
                        <div class="d-flex justify-content-center align-content-end m-0 py-2">

                            <div class="container ">
                                <div class="row bloc_user_register">
                                    <div class="col-md-5 offset-3 bloc ">
                                        {{ $statsAdmin['totalRegister'] }}
                                    </div>
                                    <div class="col-md-4 label bloc">
                                        Utilisateurs
                                    </div>
                                </div>

                                <div class="row text-center registerDetail registerDetailHeight">
                                    <div class="col-md-4">
                                        Patients<br/>
                                        <span>{{ $statsAdmin['nbPatient'] }}</span>
                                    </div>
                                    <div class="col-md-4 lib">
                                        Inf.libéraux<br/>
                                        <span>{{ $statsAdmin['nbNurse'] }}</span>
                                    </div>
                                    <div class="col-md-4 remp">
                                        Remplaçants<br/>
                                        <span>{{ $statsAdmin['nbSubstitute'] }}</span>
                                    </div>

                                </div>


                            </div>
                        </div>

                    </x-slot>
                </x-backend.card>
            </div>

            <div class="col-md-4 text-center">
                <x-backend.card>
                    <x-slot name="body">
                        <div class="card-title d-flex justify-content-center align-content-end m-0 py-2">
                            <span class="fs-2 fw-bold titStats me-2 lh-1 ls-n2">Utilisateurs</span>
                        </div>

                        <figure class="highcharts-figure">
                            <div id="container"></div>
                            <p class="highcharts-description">

                            </p>
                        </figure>
                        <br/><br/>
                        <p class="legend rose "><span>Infirmiers libéraux</span><span>{{ $statsAdmin['nbNurse'] }}</span></p>
                        <p class="legend bleu "><span>Remplaçants </span><span>{{ $statsAdmin['nbSubstitute'] }} </span></p>
                        <p class="legend gris "><span>Patients </span><span>{{ $statsAdmin['nbPatient'] }} </span></p>
                    </x-slot>
                </x-backend.card>
            </div>
        </div>


        <div class="row">
            <div class="col-md-8">
                <x-backend.card>
                    <x-slot name="body">
                        <div class="card-title d-flex justify-content-center align-content-end m-0 py-2">
                            <span class="fs-2 fw-bold titStats me-2 lh-1 ls-n2">Abonnements Actif</span>
                        </div>
                        <div class="d-flex justify-content-center align-content-end m-0 py-2">
                            <div class="container ">


                                <div class="row text-center registerDetail">
                                    <div class="col-md-6">
                                        Inf.libéraux<br/>
                                        <figure class="highcharts-figure">
                                            <div id="container2"></div>
                                            <p class="highcharts-description">

                                            </p>
                                        </figure>
                                        <br/><br/>
                                        <p class="legend rose "><span class="rose">Mensuels </span><span>{{ $statsAdmin['nbNurseMensuel'] }} </span></p>
                                        <p class="legend bleu "><span class="bleu">Annuels </span><span>{{ $statsAdmin['nbNurseAnnuel'] }} </span></p>

                                    </div>
                                    <div class="col-md-6 remp">
                                        Remplaçants<br/>
                                        <figure class="highcharts-figure">
                                            <div id="container3"></div>
                                            <p class="highcharts-description">

                                            </p>
                                        </figure>

                                        <br/><br/>
                                        <p class="legend rose "><span>Mensuels </span><span>{{ $statsAdmin['nbSubstituteMensuel'] }}</span> </p>
                                        <p class="legend bleu "><span>Annuels </span><span>{{ $statsAdmin['nbSubstituteAnnuel'] }} </span></p>
                                    </div>


                                </div>


                            </div>

                        </div>

                    </x-slot>
                </x-backend.card>
            </div>

            <div class="col-md-4 text-center">
                <x-backend.card>
                    <x-slot name="body">
                        <div class="card-title d-flex justify-content-center align-content-end m-0 py-2">
                            <span class="fs-2 fw-bold titStats me-2 lh-1 ls-n2">Abonnés Actifs</span>
                        </div>
                        <figure class="highcharts-figure">
                            <div id="container4"></div>
                            <p class="highcharts-description">

                            </p>
                        </figure>
                        <br/><br/>
                        <p class="legend rose "><span>Infirmiers libéraux </span><span>{{ $statsAdmin['nbNurseAbonne'] }} </span></p>
                        <p class="legend bleu "><span>Remplaçants</span><span>{{ $statsAdmin['nbSubstituteAbonne'] }} </span></p>
                    </x-slot>
                </x-backend.card>
            </div>
        </div>




        <div class="row">
            <div class="col-md-8">
                <x-backend.card>
                    <x-slot name="body">
                        <div class="card-title d-flex justify-content-center align-content-end m-0 py-2">
                            <span class="fs-2 fw-bold titStats me-2 lh-1 ls-n2">Abonnements Total (actifs / inactifs)</span>
                        </div>
                        <div class="d-flex justify-content-center align-content-end m-0 py-2">
                            <div class="container ">


                                <div class="row text-center registerDetail">
                                    <div class="col-md-6">
                                        Inf.libéraux<br/>
                                        <figure class="highcharts-figure">
                                            <div id="container5"></div>
                                            <p class="highcharts-description">

                                            </p>
                                        </figure>
                                        <br/><br/>
                                        <p class="legend rose "><span class="rose">Mensuels </span><span>{{ $statsAdmin['nbNurseMensuelAll'] }} </span></p>
                                        <p class="legend bleu "><span class="bleu">Annuels </span><span>{{ $statsAdmin['nbNurseAnnuelAll'] }} </span></p>

                                    </div>
                                    <div class="col-md-6 remp">
                                        Remplaçants<br/>
                                        <figure class="highcharts-figure">
                                            <div id="container6"></div>
                                            <p class="highcharts-description">

                                            </p>
                                        </figure>

                                        <br/><br/>
                                        <p class="legend rose "><span>Mensuels </span><span>{{ $statsAdmin['nbSubstituteMensuelAll'] }}</span> </p>
                                        <p class="legend bleu "><span>Annuels </span><span>{{ $statsAdmin['nbSubstituteAnnuelAll'] }} </span></p>
                                    </div>


                                </div>


                            </div>

                        </div>

                    </x-slot>
                </x-backend.card>
            </div>

            <div class="col-md-4 text-center">
                <x-backend.card>
                    <x-slot name="body">
                        <div class="card-title d-flex justify-content-center align-content-end m-0 py-2">
                            <span class="fs-2 fw-bold titStats me-2 lh-1 ls-n2">Abonnés Actifs</span>
                        </div>
                        <figure class="highcharts-figure">
                            <div id="container4"></div>
                            <p class="highcharts-description">

                            </p>
                        </figure>
                        <br/><br/>
                        <p class="legend rose "><span>Infirmiers libéraux </span><span>{{ $statsAdmin['nbNurseAbonne'] }} </span></p>
                        <p class="legend bleu "><span>Remplaçants</span><span>{{ $statsAdmin['nbSubstituteAbonne'] }} </span></p>
                    </x-slot>
                </x-backend.card>
            </div>
        </div>


    @endif
@endsection

