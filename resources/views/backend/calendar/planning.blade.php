@extends('backend.layouts.app')
@section('title', __('Calendrier / Planning'))
@section('page_class', 'calendar-page')

@section('content')
    <x-backend.card>
        <x-slot name="body">
            @if(session()->has('message'))
                <div class="alert alert-warning"> {{ session()->get('message')  }}</div>
            @endif

            <div class="row d-flex justify-content-between align-items-center">
                <h4 class="col-md-4">Filtres</h4>
                <div class="col-md-4 d-flex align-items-center row m-0">
                        <label class="col-4 col-form-label">Utilisateur:</label>
                        <select class="col-4 form-control select @error('filtre_user') is-invalid @enderror"
                                name="filtre_user"
                                id="filtre_user">
                            <option selected value="all">Tous</option>
                            @foreach($users as $user)
                                <option value="{{$user->id}}"> {{$user->first_name}}</option>
                            @endforeach
                        </select>
                    <div class="col-3 text-right">
                        <button class="btn btn-primary btn-sm" type="submit" id="search_users">Filtrer</button>
                    </div>
                    @error('filtre_user')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
            </div>
        </x-slot>
    </x-backend.card>
    <x-backend.card>
        <x-slot name="header">
            @lang('Calendar / Planning')
            @if(!auth()->user()->isBiller())
                <div>
                    <button type="button" class="btn btn-flickr btn-sm" data-toggle="modal" data-target="#Modal1">
                        Ajouter un évènement au planning
                    </button>
                </div>
            @endif
        </x-slot>
        <x-slot name="body">
            <div class="modal fade" id="Modal1" tabindex="-1" role="dialog" aria-labelledby="Modal1" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="ModalLabel">Préciser les détails de période du travail :</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form name="add_appointment" id="add_appointment" method="POST" action="{{ route('admin.disponibility.store') }}">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label for="start_date" class="col-form-label"> <h4> Date de début : </h4> </label>
                                    <input type="date" class="form-control" name="start_date" id="start_date">
                                </div>

                                <div class="form-group">
                                    <label for="end_date" class="col-form-label"> <h4> Date de fin : </h4> </label>
                                    <input type="date" class="form-control" name="end_date" id="end_date">
                                </div>
                                <span id="error"></span>
                            @if (Auth::user()->team->count() > 0)
                                    @if(Auth::user()->team->first()->admin_id == Auth::id())
                                        <div class="form-group" >
                                            <label for="nurse_id"> <h4> sélectionnez un membre: </h4> </label>
                                            <select id="nurse_id" class="form-control" name="nurse_id" data-role="select-dropdown" data-profile="minimal">
                                                    @foreach(Auth::user()->team->first()->members as $member)
                                                <option value="{{ $member->id }}" > {{ $member->name }}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                       @endif
                                @else
                                            <div class="form-group" style="display: none" >
                                                <label for="nurse_id"> <h4> sélectionnez un membre: </h4> </label>
                                            </div>

                                @endif
                                <div class="form-group">
                                    <label for="demo_overview_minimal"> <h4> Période de travail : </h4> </label>
                                    <select id="periode" class="form-control" name="periode" data-role="select-dropdown" data-profile="minimal">
                                        <option value="matin"> Demi journée (matin)</option>
                                        <option value="apresmidi"> Demi journée (Après-midi)</option>
                                        <option value="jour"> Une journée entière</option>
                                        <option value="semaine"> Une semaine entière</option>
                                    </select>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                                    <button type="submit" class="btn btn-flickr" >Ajouter</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>

            <div class="modal fade" id="detailsDisponibility" tabindex="-1" role="dialog" aria-labelledby="Modal2" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="ModalLabel">Modifier la période de travail :</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form name="edit_dispo" id="edit_dispo" method="POST" action="">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label for="datestart" class="col-form-label"> <h4> Date de début : </h4> </label>
                                    <input type="date" class="form-control" name="datestart" id="datestart">
                                </div>
                                <div class="form-group">
                                    <label for="datefinish" class="col-form-label"> <h4> Date de fin : </h4> </label>
                                    <input type="date" class="form-control" name="datefinish" id="datefinish">
                                </div>
                                @if (Auth::user()->team->count() > 0)
                                    @if(Auth::user()->team->first()->admin_id == Auth::id())
                                    <div class="form-group">
                                        <label for="nurse_id"> <h4> sélectionnez un membre: </h4> </label>
                                            <select id="edit_nurse_id" class="form-control" name="edit_nurse_id" data-role="select-dropdown" data-profile="minimal">
                                                @foreach(Auth::user()->team->first()->members as $member)
                                                    <option value="{{ $member->id }}"
                                                    > {{ $member->name }}</option>
                                                @endforeach
                                            </select>
                                    </div>
                                    @endif
                                @endif
                                <div class="form-group">
                                    <label for="demo_overview_minimal"> <h4> Période de travail : </h4> </label>
                                    <select id="periode_travail" class="form-control" name="periode_travail" data-role="select-dropdown" data-profile="minimal">
                                        <option value="08:00:00-12:00:00"> Demi journée (matin)</option>
                                        <option value="13:00:00-18:00:00"> Demi journée (Après-midi)</option>
                                        <option value="08:00:00-18:00:00"> Une journée entière</option>
                                        <option value="semaine"> Une semaine entière</option>
                                    </select>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" id="destroy_dispo" data-dismiss="modal">Supprimer</button>
                                    <input type="button" class="btn btn-primary" id="update_dispo" value="Modifier">
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>

            <div id="calendar"></div>
            {{--  <div id="loading" style="display:none;">
                <img id="loading-image" src="https://upload.wikimedia.org/wikipedia/commons/c/c7/Loading_2.gif?20170503175831" alt="Loading..." />
            </div>  --}}

            <script>
                let myForm = document.getElementById('add_appointment');
                myForm.addEventListener('submit' , function(e) {
                    let datestart = document.getElementById('start_date').value;
                    let dateend = document.getElementById('end_date').value;
                    if (dateend < datestart)
                    {
                        let myError = document.getElementById('error');
                        myError.innerHTML = "La date de fin est invalide.";
                        myError.style.color = "red";
                        e.preventDefault();
                    }

                });

                </script>
        </x-slot>
    </x-backend.card>
@endsection
