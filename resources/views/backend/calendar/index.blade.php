@extends('backend.layouts.app')

@section('title', __('Calendrier / Tournée'))

@section('page_class', 'calendar-page')
@push('after-styles')
    <style>
        .passage input[type="radio"]:checked + label {
            border: 2px solid #E53EA1;
        }

        .passage .pill {
            padding: 10px 4px;
            border-radius: 40px;
            text-align: center;
            width: 100%;
            border: 1px solid;
        }

        .passage .col-4, .passage .col-6 {
            padding: 1px;
        }
    </style>
@endpush
@section('content')
    <x-backend.card class="mb-5">
        <x-slot name="body">
            <div class="row  align-items-center">
                <h4 class="col-md-2 m-0">Filtres</h4>
                    <div class="col-md-4 d-flex align-items-center row m-0">
                        <label class="col-4 col-form-label">Utilisateur:</label>
                        <select class="col-6 form-control select @error('filtretour_user') is-invalid @enderror"
                                name="filtretour_user"
                                id="filtretour_user">
                            <option selected value="all">Tous</option>
                            @foreach($users as $user)
                                <option value="{{$user->id}}"> {{$user->first_name}}</option>
                            @endforeach
                        </select>

                    </div>
                    <div class="col-md-4 d-flex align-items-center row m-0">
                        <label class="col-4 col-form-label">Traitement:</label>
                        <select class="col-6 form-control select @error('filtre_trait') is-invalid @enderror"
                                name="filtre_trait"
                                id="filtre_trait">
                            <option selected value="all">Tous</option>
                            <option value="covid"> Covid</option>
                            <option value="vaccin"> Vaccin</option>
                            <option value="soin"> Soin Classique où ponctuelle</option>
                            <option value="appointment"> Rendez-vous</option>
                        </select>

                    </div>
                <div class="row m-0 justify-content-between align-items-center ">
                    <button class="btn btn-primary btn-sm" type="submit" id="searchtour_users">Filtrer</button>
                </div>
                @error('filtre_user')
                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                @enderror
            </div>


        </x-slot>
    </x-backend.card>
    <x-backend.card>
        <x-slot name="header">
            @lang('Calendar / Tour')
        </x-slot>

        <x-slot name="body">
            <div class="d-flex">
                <p class="mr-3"><span class="dot dot-treatment mr-2"></span>Soin ponctuel/chroniques </p>
                <p class="mr-3"><span class="dot dot-covid-treatment mr-2"></span>Covid tests/visites/suivi </p>
                <p class="mr-3"><span class="dot dot-rdv mr-2"></span>Rendez-vous </p>
                <p><span class="dot dot-vaccine mr-2"></span>Vaccins </p>
            </div>
            <div id="calendar"></div>
            {{-- <div id="loading" style="display:none;">
                <img id="loading-image"
                    src="https://upload.wikimedia.org/wikipedia/commons/c/c7/Loading_2.gif?20170503175831"
                    alt="Loading..." />
            </div> --}}

            <div class="modal fade" id="detailsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-xl" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Les détails du traitement :</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div
                                    class="col-md-4 pb-3 align-items-center border-right d-flex flex-column">
                                    <h4 class="text-center fw-bold mb-3">Patient Info</h4>
                                    <img src="" alt="image" class="patient_img mb-3">
                                    <div class="d-flex flex-column ml-3">
                                        <h5>
                                            <strong class="patient_name"></strong>
                                            <span class="badge badge-primary patient_type"></span>
                                        </h5>
                                        <div class="d-flex align-items-center gap-2 mb-2">
                                        <span class="svg-icon svg-icon-2 mr-2">
                                            <svg width="20" height="20" viewBox="0 0 24 24" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path opacity="0.3"
                                                      d="M21 19H3C2.4 19 2 18.6 2 18V6C2 5.4 2.4 5 3 5H21C21.6 5 22 5.4 22 6V18C22 18.6 21.6 19 21 19Z"
                                                      fill="currentColor"></path>
                                                <path
                                                    d="M21 5H2.99999C2.69999 5 2.49999 5.10005 2.29999 5.30005L11.2 13.3C11.7 13.7 12.4 13.7 12.8 13.3L21.7 5.30005C21.5 5.10005 21.3 5 21 5Z"
                                                    fill="currentColor"></path>
                                            </svg>
                                        </span>
                                            <a href="mailto" class="text-muted text-hover-primary patient_mail"></a>
                                        </div>
                                        <div class="d-flex align-items-center gap-2">
                                        <span class="svg-icon svg-icon-2 mr-2">
                                            <svg width="20" height="20" viewBox="0 0 24 24" fill="none"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M5 20H19V21C19 21.6 18.6 22 18 22H6C5.4 22 5 21.6 5 21V20ZM19 3C19 2.4 18.6 2 18 2H6C5.4 2 5 2.4 5 3V4H19V3Z"
                                                    fill="currentColor"></path>
                                                <path opacity="0.3" d="M19 4H5V20H19V4Z" fill="currentColor"></path>
                                            </svg>
                                        </span>
                                            <a href="javascript:"
                                               class="text-muted text-hover-primary patient_phone"></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <h4 class="text-center fw-bold mb-3">Traitement Info</h4>
                                    <div class="row mb-3">
                                        <div class="col-md-4">
                                            <strong>Traitement :</strong>
                                        </div>
                                        <div class="col-md-6" id="trait_title">

                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-md-4">
                                            <strong>Ajouté(e) par :</strong>
                                        </div>
                                        <div class="col-md-6" id="nurse_name">

                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-md-4">
                                            <strong>Commencer le :</strong>
                                            <div id="start_date"></div>
                                        </div>
                                        <div class="col-md-4">
                                            <strong>Fini le :</strong>
                                            <div id="end_date"></div>
                                        </div>
                                        <div class="col-md-4">
                                            <strong>Durée:</strong>
                                            <div id="period"></div>
                                        </div>
                                    </div>

                                    <div class="mb-3">
                                        <strong>Fréquence Hebdomadaire :</strong>
                                        <div id="heb_frequency">

                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <strong>Passages :</strong>
                                        <div id="passages">

                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <div class="col-md-4">
                                            <strong>Créer le :</strong>
                                        </div>
                                        <div class="col-md-6" id="creation_date">

                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="mt-4">
                                <div class="row">
                                    <div class="col-md-4 border-right">
                                        <div class="row w-100">
                                            <div class="col-md-6">
                                                <a href="" class="btn btn-flickr w-100" id="show-patient">Fiche
                                                    patient</a>
                                            </div>
                                            <div class="col-md-6">
                                                <a href="" class="btn btn-light w-100" id="edit-patient">Modifier</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="row w-100 m-auto mt-4">
                                            <div class="col-md-6">
                                                <a href="javascript:void(0)" class="btn btn-light w-100"
                                                   id="edit-treatment">Modifier la période</a>
                                            </div>
                                            <div class="col-md-6">
                                                <a href="javascript:void(0)" class="btn btn-danger w-100"
                                                   id="treatment_close">Supprimer le Traitement</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Modifier le traitement</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="start_time">Date de début</label>
                                <input type="date" class="form-control" name="start_time" id="start_time">
                            </div>
                            <div class="form-group">
                                <label for="finish_time">Date de fin</label>
                                <input type="date" class="form-control" name="finish_time" id="finish_time">
                            </div>
                            <div class="footer">
                                <input type="submit" class="btn btn-secondary btn-sm" id="treatment_update"
                                       value="Sauvegarder">
                                <button type="button" class="btn btn-danger btn-sm" data-bs-dismiss="modal"> Annuler
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="VCModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Les détails du traitement </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="d-flex pb-3 align-items-center">
                                <img src="" alt="image" class="rounded-circle patient_img">
                                <div class="d-flex flex-column ml-3">
                                    <h5 class="mb-0"><strong class="patient_name"></strong> <span
                                            class="badge badge-primary patient_type"></span></h5>
                                    <div class="d-flex align-items-center gap-2">
                                    <span class="svg-icon svg-icon-2">
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                             xmlns="http://www.w3.org/2000/svg">
                                            <path opacity="0.3"
                                                  d="M21 19H3C2.4 19 2 18.6 2 18V6C2 5.4 2.4 5 3 5H21C21.6 5 22 5.4 22 6V18C22 18.6 21.6 19 21 19Z"
                                                  fill="currentColor"></path>
                                            <path
                                                d="M21 5H2.99999C2.69999 5 2.49999 5.10005 2.29999 5.30005L11.2 13.3C11.7 13.7 12.4 13.7 12.8 13.3L21.7 5.30005C21.5 5.10005 21.3 5 21 5Z"
                                                fill="currentColor"></path>
                                        </svg>
                                    </span>
                                        <a href="mailto" class="text-muted text-hover-primary patient_mail"></a>
                                    </div>
                                    <div class="d-flex align-items-center gap-2">
                                    <span class="svg-icon svg-icon-2">
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                             xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M5 20H19V21C19 21.6 18.6 22 18 22H6C5.4 22 5 21.6 5 21V20ZM19 3C19 2.4 18.6 2 18 2H6C5.4 2 5 2.4 5 3V4H19V3Z"
                                                fill="currentColor"></path>
                                            <path opacity="0.3" d="M19 4H5V20H19V4Z" fill="currentColor"></path>
                                        </svg>
                                    </span>
                                        <a href="javascript:" class="text-muted text-hover-primary patient_phone"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Traitement</label>
                                <input type="text" class="form-control" name="vc_title" id="vc_title">
                            </div>
                            <div class="form-group">
                                <label>Date</label>
                                <input type="date" class="form-control" name="vc_start_time" id="vc_start_time">
                            </div>
                        </div>

                        <div class="modal-footer">
                            <input type="button" class="btn btn-secondary btn-sm" id="vc_update" value="Update">
                            <button type="button" class="btn btn-danger btn-sm" id="vc_close"> Supprimer</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="detailsAppointment" tabindex="-1" role="dialog" aria-labelledby="Modal2"
                 aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="ModalLabel">Modifier le rendez-vous :</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                        </div>
                        <div class="modal-body">
                            <form name="edit_appointment" id="edit_appointment" method="POST" action="">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label for="appdatestart" class="col-form-label">
                                        <h4> Date de début : </h4>
                                    </label>
                                    <input type="date" class="form-control" name="start_date" id="appdatestart">
                                </div>
                                <div class="form-group">
                                    <label for="appdatefinish" class="col-form-label">
                                        <h4> Date de fin : </h4>
                                    </label>
                                    <input type="date" class="form-control" name="end_date" id="appdatefinish">
                                </div>
                                <div class="form-group">
                                    <label for="apptimestart" class="col-form-label">
                                        <h4> Heure de début : </h4>
                                    </label>
                                    <input type="time" class="form-control" name="start_time" id="apptimestart">
                                </div>
                                <div class="form-group">
                                    <label for="apptimeend" class="col-form-label">
                                        <h4> Heure de fin : </h4>
                                    </label>
                                    <input type="time" class="form-control" name="end_time" id="apptimeend">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary remove" data-dismiss="modal">
                                        Supprimer
                                    </button>
                                    <input type="submit" class="btn btn-primary" value="Modifier">
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </x-slot>
    </x-backend.card>
@endsection
