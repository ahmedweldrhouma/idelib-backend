@inject('model', '\App\Domains\Treatment\Models\Treatment')

@extends('backend.layouts.app')

@section('title', __('Créer un patient'))

@section('content')
    <x-forms.post :action="route('admin.patient.store')" :enctype="__('multipart/form-data')">
        <x-backend.card>
            <x-slot name="header">
                @lang('Créer un patient')
            </x-slot>

            <x-slot name="headerActions">
                <x-utils.link class="card-header-action" :href="URL::previous()" :text="__('Cancel')" />
            </x-slot>

            <x-slot name="body">
                <div>
                    <input type="hidden" name="user_type" value="patient">
                    <div class="form-group row">
                        <label for="avatar" class="col-md-2 col-form-label">@lang('Avatar')</label>

                        <div class="col-md-10">
                            <div class="avatar-upload">
                                <div class="avatar-edit">
                                    <input type='file' name="avatar_location" id="imageUpload" accept=".png, .jpg, .jpeg" />
                                    {{-- <input type="hidden" id="avatar_type" name="avatar_type" value="storage"> --}}
                                    <label for="imageUpload">
                                        <i class="cil-pencil icon"></i>
                                    </label>
                                </div>
                                <div class="avatar-preview">
                                    <div id="imagePreview" style="background-image: url({{ asset('img/default-avatar.png') }});">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="type" class="col-md-2 col-form-label">
                            <span class="alert-required" aria-hidden="true">*</span>@lang('Type')</label>

                        <div class="col-md-10">
                            <select name="type" class="form-control" required >
                                <option value="" selected disabled>@lang('Type')</option>
                                <option value="{{ $model::TYPE_CHRONIC }}">@lang('Chronique')</option>
                                <option value="{{ $model::TYPE_PUNCTUAL }}">@lang('Penctuel')</option>
                                <option value="{{ $model::TYPE_COVID }}">@lang('Covid')</option>
                            </select>
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="first_name" class="col-md-2 col-form-label">
                            <span class="alert-required" aria-hidden="true">*</span>@lang('Prénom')</label>

                        <div class="col-md-10">
                            <input type="text" name="first_name" class="form-control" placeholder="{{ __('Prénom') }}" value="{{ old('first_name') }}" maxlength="100" required />
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="last_name" class="col-md-2 col-form-label">
                            <span class="alert-required" aria-hidden="true">*</span>@lang('Nom')</label>

                        <div class="col-md-10">
                            <input type="text" name="last_name" class="form-control" placeholder="{{ __('Nom') }}" value="{{ old('last_name') }}" maxlength="100" required />
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="email" class="col-md-2 col-form-label">
                            <span class="alert-required" aria-hidden="true">*</span>@lang('E-mail Address')</label>

                        <div class="col-md-10">
                            <input type="email" name="email" class="form-control" placeholder="{{ __('E-mail Address') }}" value="{{ old('email') }}" maxlength="255" required />
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="password" class="col-md-2 col-form-label">
                            <span class="alert-required" aria-hidden="true">*</span>@lang('Password')</label>

                        <div class="col-md-10">
                            <input type="password" name="password" id="password" class="form-control" placeholder="{{ __('Password') }}" maxlength="100" required autocomplete="new-password" />
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="password_confirmation" class="col-md-2 col-form-label">
                            <span class="alert-required" aria-hidden="true">*</span>@lang('Password Confirmation')</label>

                        <div class="col-md-10">
                            <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="{{ __('Password Confirmation') }}" maxlength="100" required autocomplete="new-password" />
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="social_security_number" class="col-md-2 col-form-label">@lang('N° de sécurité sociale')</label>

                        <div class="col-md-10">
                            <input type="text" name="social_security_number" id="social_security_number" class="form-control" placeholder="{{ __('N° de sécurité sociale') }}" maxlength="100"  />
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="phone" class="col-md-2 col-form-label">@lang('Numéro de téléphone')</label>

                        <div class="col-md-10">
                            <input type="phone" name="phone" id="phone" class="form-control" placeholder="{{ __('Numéro de téléphone') }}" maxlength="100"  />
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="birth_date" class="col-md-2 col-form-label">@lang('Date de naissance')</label>

                        <div class="col-md-10">
                            <input type="date" name="birth_date" id="birth_date" class="form-control" placeholder="{{ __('Date de naissance') }}" maxlength="100"  />
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="address" class="col-md-2 col-form-label">@lang('Adresse')</label>

                        <div class="col-md-10">
                            <input type="text" name="address" id="address" class="form-control" placeholder="{{ __('Adresse') }}" maxlength="100"  />
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="doctor" class="col-md-2 col-form-label">@lang('Médecin traitant')</label>

                        <div class="col-md-10">
                            <input type="text" name="doctor" id="doctor" class="form-control" placeholder="{{ __('Médecin traitant') }}" maxlength="100"  />
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="doctor_phone" class="col-md-2 col-form-label">@lang('Tel médecin traitant')</label>

                        <div class="col-md-10">
                            <input type="text" name="doctor_phone" id="doctor_phone" class="form-control" placeholder="{{ __('Tel médecin traitant') }}" maxlength="100"  />
                        </div>
                    </div><!--form-group-->

                </div>
            </x-slot>

            <x-slot name="footer">
                <button class="btn btn-sm btn-primary float-right" type="submit">@lang('Créer abonnement')</button>
            </x-slot>
        </x-backend.card>
    </x-forms.post>
@endsection
