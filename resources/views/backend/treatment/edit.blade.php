@inject('model', '\App\Domains\Plan\Models\Plan')

@extends('backend.layouts.app')

@section('title', __('Modifier abonnement'))

@section('content')
    <x-forms.patch :action="route('admin.plan.update', $plan)" >
        <x-backend.card>
            <x-slot name="header">
                @lang('Modifier abonnement')
            </x-slot>

            <x-slot name="headerActions">
                <x-utils.link class="card-header-action btn btn-light btn-sm" :href="URL::previous()">
                    <x-slot name="slot">
                        {{ __('Cancel') }}
                    </x-slot>
                </x-utils.link>
            </x-slot>

            <x-slot name="body">
                <div>
                    <div class="form-group row">
                        <label for="name" class="col-md-2 col-form-label">@lang('Name')</label>

                        <div class="col-md-10">
                            <input type="text" name="name" class="form-control" placeholder="{{ __('Name') }}" value="{{ old('name') ?? $plan->name }}" maxlength="100" required />
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="price" class="col-md-2 col-form-label">@lang('Prix')</label>

                        <div class="col-md-10">
                            <input type="text" name="price" class="form-control" placeholder="{{ __('Prix') }}" value="{{ old('price') ?? $plan->price }}" maxlength="100" required />
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="stripe_plan_id" class="col-md-2 col-form-label">@lang('Stripe id')</label>

                        <div class="col-md-10">
                            <input type="text" name="stripe_plan_id" class="form-control" placeholder="{{ __('Stripe id') }}" value="{{ old('stripe_plan_id') ?? $plan->stripe_plan_id }}" maxlength="100" />
                        </div>
                    </div>
                </div>
            </x-slot>

            <x-slot name="footer">
                <button class="btn btn-sm btn-primary float-right" type="submit">@lang('Mettre à jour l\'abonnement')</button>
            </x-slot>
        </x-backend.card>
    </x-forms.patch>
@endsection
