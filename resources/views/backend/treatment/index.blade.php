@extends('backend.layouts.app')

@section('title', __('Gestion de la patientèle'))

@section('content')
    <x-backend.card>
        <x-slot name="header">
            @lang('Gestion de la patientèle') @lang($type)
        </x-slot>

        @if ($logged_in_user->isAdmin())
            <x-slot name="headerActions">
                <x-utils.link
                    icon="c-icon cil-plus"
                    class="btn btn-primary btn-sm"
                    :href="route('admin.patient.create', ['type' => $type])"
                >
                    <x-slot name="slot">
                        {{__('Créer un patient')}}
                    </x-slot>
                </x-utils.link>
            </x-slot>
        @endif

        <x-slot name="body">
            @if ($type == 'covid')
                <livewire:backend.treatments-table treatmentType="covid"/>
            @elseif($type == 'punctual')
                <livewire:backend.treatments-table treatmentType="punctual"/>
            @elseif($type == 'chronic')
                <livewire:backend.treatments-table treatmentType="chronic"/>
            @endif
        </x-slot>
    </x-backend.card>
@endsection
