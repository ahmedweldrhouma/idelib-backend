@if ($logged_in_user->isBiller())
    <div class="dropdown d-inline-block position-static">
        <a class="btn btn-sm btn-secondary dropdown-toggle" href="#" role="button" data-toggle="dropdown"
           data-boundary="window" aria-haspopup="true" aria-expanded="false">
            {{ ('Actions') }} </a>

        <div class="dropdown-menu">
            <x-utils.link class="dropdown-item" :href="route('admin.patient.view', $patient)">
                {{ __('View') }}
            </x-utils.link>

        </div>
    </div>
@elseif($logged_in_user->isAdmin() || $logged_in_user->isNurse() || $logged_in_user->isSubstituteNurse())
    <div class="dropdown d-inline-block position-static">
        <a class="btn btn-sm btn-secondary dropdown-toggle" href="#" role="button" data-toggle="dropdown"
           data-boundary="window" aria-haspopup="true" aria-expanded="false">
            {{ ('Actions') }} </a>

        <div class="dropdown-menu">
                <x-utils.link class="dropdown-item" :href="route('admin.patient.view', $patient)">
                    {{ __('View') }}
                </x-utils.link>

            <x-utils.link class="dropdown-item" :href="route('admin.patient.edit', $patient)">
                {{ __('Edit') }}
            </x-utils.link>
            <x-utils.form-button
                :action="route('admin.patient.destroy', $patient)" method="delete" name="confirm-item"
                button-class="dropdown-item">
                @lang('Delete')
            </x-utils.form-button>
        </div>
    </div>
@endif
