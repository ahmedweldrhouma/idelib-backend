<table>
    <tbody>
    <tr>
        <td colspan="3" rowspan="2">
            <img src="img/logo-idelib200x100.png" width="100" style="margin-left: 50px;left: 50px"/>
        </td>
        <td colspan="18">
            <strong>Exporter le {{ date('d-m-Y') }} à {{ date('H:i') }}
                par {{ \Illuminate\Support\Facades\Auth::user()->getFullName() }}</strong>
        </td>
    </tr>
    <tr>
        <td colspan="18">
            <p><a href="https://idelib.com">@lang('Visit Our Website') : www.idelib.com</a></p>
        </td>
    </tr>
    <tr>
        <td bgcolor="#E53EA1" size="5">Id Patient</td>
        <td bgcolor="#E53EA1"> Nom</td>
        <td bgcolor="#E53EA1"> Prénom</td>
        <td bgcolor="#E53EA1"> Date de naissance</td>
        <td bgcolor="#E53EA1"> Carte mutuelle</td>
        <td bgcolor="#E53EA1"> Carte vitale</td>
        <td bgcolor="#E53EA1"> Antécédents</td>
        <td bgcolor="#E53EA1"> Date de prise en charge</td>
        <td bgcolor="#E53EA1"> Type de soins</td>
        <td bgcolor="#E53EA1"> Fréquence journalière</td>
        <td bgcolor="#E53EA1"> Passage 1</td>
        <td bgcolor="#E53EA1"> Passage 2</td>
        <td bgcolor="#E53EA1"> Passage 3</td>
        <td bgcolor="#E53EA1"> Fréquence hebdomadaire</td>
        <td bgcolor="#E53EA1"> Absence(s)</td>
        <td bgcolor="#E53EA1"> Décès ou arrêt de traitement</td>
        <td bgcolor="#E53EA1"> Soin realisé</td>
        <td bgcolor="#E53EA1"> Date de début</td>
        <td bgcolor="#E53EA1"> Date de fin</td>
        <td bgcolor="#E53EA1"> Durée</td>
        <td bgcolor="#E53EA1"> Piéces jointes</td>
    </tr>
    @foreach($patients as $patient)
        @if($patient->treatments->isNotEmpty())
            @foreach($patient->treatments as $treatment)
                <tr>
                    @if($loop->first)
                        <td rowspan="{{ $patient->treatments->count() }}"> {{$patient->id}} </td>
                        <td rowspan="{{ $patient->treatments->count() }}"> {{$patient->first_name}} </td>
                        <td rowspan="{{ $patient->treatments->count() }}"> {{$patient->last_name}} </td>
                        <td rowspan="{{ $patient->treatments->count() }}"> {{$patient->birth_date->format('d-m-Y')}} </td>
                        <td rowspan="{{ $patient->treatments->count() }}"> @if($patient->mutual_card)
                                <a href="{{ Storage::disk('s3')->url($patient->mutual_card) }}">Voir carte</a>
                            @endif</td>
                        <td rowspan="{{ $patient->treatments->count() }}"> {{$patient->vital_card}} </td>
                        <td rowspan="{{ $patient->treatments->count() }}">  {{$patient->getAntecedentsForHumans()}} </td>
                    @endif
                    <td> {{$treatment->start_at->format('d-m-Y')}}</td>
                    <td> {{ __($treatment->type) .' '.__($treatment->bsi_interval) }}</td>
                    <td>{{$treatment->daily_frequency}}</td>
                    <td>{{isset($treatment->passages[0])?$treatment->passages[0]->getForPatient():""}}</td>
                    <td>{{isset($treatment->passages[1])?$treatment->passages[1]->getForPatient():""}}</td>
                    <td>{{isset($treatment->passages[2])?$treatment->passages[2]->getForPatient():""}}</td>
                    <td>{{$treatment->monthly_frequency}}</td>
                  <td>
                      @foreach($patient->absences as $absence)
                          @if($absence->absent_from->between(\Illuminate\Support\Carbon::parse($treatment->start_at), \Illuminate\Support\Carbon::parse($treatment->end_at)))
                              @if(!$loop->first)
                                  <br>
                              @endif
                              De {{$absence->absent_from->format('H:i d-m-Y')}} à {{$absence->absent_to->format('H:i d-m-Y')}}
                          @endif
                      @endforeach
                    </td>
                    <td> {{$treatment->stoped_at}} </td>

                    @if($treatment->type.' '.$treatment->bsi_interval != \App\Domains\Treatment\Models\Treatment::TYPE_BSI. ' '.'>'.'85')
                        <td>{{__($treatment->care_performed)}}</td>
                    @else
                        <td>{{__($treatment->bsi_type)}}</td>
                    @endif

                    <td bgcolor="#00FFFF"> {{$treatment->start_at->format('H:i d-m-Y')}} </td>
                    <td bgcolor="#00FFFF"> {{$treatment->end_at->format('H:i d-m-Y')}} </td>
                    <td>
                            <?php
                            $interval = explode(' ', $treatment->period);
                            ?>
                        {{ $interval[0].' '.__($interval[1]) }}
                    </td>
                    <td>
                        @if($treatment->prescription != null)
                            <a href="{{ Storage::disk('s3')->url($treatment->prescription) }}">Voir Ordonnance</a>
                        @endif
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td> {{$patient->id}} </td>
                <td> {{$patient->first_name}} </td>
                <td> {{$patient->last_name}} </td>
                <td> {{$patient->birth_date->format('d-m-Y')}}</td>
                <td> @if($patient->mutual_card)
                        <a href="{{ Storage::disk('s3')->url($patient->mutual_card) }}">Voir carte</a>
                    @endif</td>
                <td> {{$patient->vital_card}}</td>
                <td>  {{($patient->getAntecedentsForHumans())}}
                </td>
                <td>{{__("Pas de traitement")}}</td>
            </tr>
        @endif
    @endforeach
    </tbody>
</table>
