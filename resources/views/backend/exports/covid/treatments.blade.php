<table>

    <thead>
    <tr>
        <th>#</th>
        <th>Prénom</th>
        <th>Nom</th>
        <th>Date de naissance</th>
        <th>Antécédents</th>
        <th>Suivit patient</th>
        <th>Durée</th>
        <th>Passage Matin</th>
        <th>Passage Midi</th>
        <th>Passage soir</th>
        <th>Fréquence hebdomadaire</th>
        <th>Absence</th>
        <th>Décès ou arrêt de traitement</th>
        <th>Soin réalisé</th>
    </tr>
    </thead>
    <tbody>
    @foreach($treatments as $treatment)
        <tr>
            <td>{{ $treatment->id }}</td>
            <td>{{ $treatment->patient->first_name }}</td>
            <td>{{ $treatment->patient->last_name }}</td>
            <td>{{ date("d/m/Y",strtotime($treatment->patient->birth_date)) }}</td>
            <td>{!! $treatment->patient->formedAntecedents !!}</td>
            <td>{{ date("d/m/Y",strtotime($treatment->start_at)) }}</td>
            <td>{{ $treatment->period }}</td>
            <td>@if($morning_passages) {{ $morning_passages->time_slot }} @endif</td>
            <td>@if($midday_passages) {{ 'Oui' }} @else {{ 'Non' }} @endif</td>
            <td>@if($night_passages) {{ $night_passages->time_slot }} @endif</td>
            <td>{{ $treatment->monthly_frequency }}</td>
            <td>{{ $treatment->absent_from }}</td>
            <td>{{ $treatment->stoped_at }}</td>
            <td>{{ $treatment->treatment }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
