<table>

    <thead>
    <tr>
        <th>#</th>
        <th>Prénom</th>
        <th>Nom</th>
        <th>Date de naissance</th>
        <th>Antécédents</th>
        <th>Date de prise en charge</th>
        <th>Date vaccin</th>
        <th>dose</th>
    </tr>
    </thead>
    <tbody>
    @foreach($treatments as $treatment)
        <tr>
            <td>{{ $treatment->id }}</td>
            <td>{{ $treatment->patient->first_name }}</td>
            <td>{{ $treatment->patient->last_name }}</td>
            <td>{{ date("d/m/Y",strtotime($treatment->patient->birth_date)) }}</td>
            <td>{!! $treatment->patient->formedAntecedents !!}</td>
            <td>{{ date("d/m/Y",strtotime($treatment->created_at)) }}</td>
            <td>{{ date("d/m/Y",strtotime($treatment->date)) }}</td>
            <td>{{ $treatment->doses_number }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
