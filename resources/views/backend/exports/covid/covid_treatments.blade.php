<table>

    <thead>
    <tr>
        <td colspan="3" rowspan="2">
            <img src="img/logo-idelib200x100.png" width="100" style="margin-left: 50px;left: 50px"/>
        </td>
        <td colspan="18">
            <strong>Exporter le {{ date('d-m-Y') }} à {{ date('H:i') }}
                par {{ \Illuminate\Support\Facades\Auth::user()->getFullName() }}</strong>
        </td>
    </tr>
    <tr>
        <td colspan="18">
            <p><a href="https://idelib.com">@lang('Visit Our Website') : www.idelib.com</a></p>
        </td>
    </tr>
    <tr>
        <td bgcolor="#E53EA1">Id Patient</td>
        <td bgcolor="#E53EA1"> Nom</td>
        <td bgcolor="#E53EA1"> Prénom</td>
        <td bgcolor="#E53EA1"> Date de naissance</td>
        <td bgcolor="#E53EA1">N° de sécurité sociale</td>
        <td bgcolor="#E53EA1">Date test PCR</td>
        <td bgcolor="#E53EA1">Date test Antigénique</td>
        <td bgcolor="#E53EA1">Date vaccin</td>
        <td bgcolor="#E53EA1">Vaccin</td>
        <td bgcolor="#E53EA1">N° lot</td>
        <td bgcolor="#E53EA1">Dose</td>
        <td bgcolor="#E53EA1">Date visite à domicile</td>
        <td bgcolor="#E53EA1">Suivie patient</td>
        <td bgcolor="#E53EA1">Durée</td>
        <td bgcolor="#E53EA1"> Pièces-jointes</td>
    </tr>
    </thead>
    <tbody>
    @foreach($patients as $patient)
        @php
            $treatments = $patient->covid_treatments->groupBy('type');
            $max = max($patient->vaccins->count(),$patient->trackings->count(),$treatments->map->count()->max());
            $max = $max==0?1:$max;
        @endphp
        @for($i = 0; $i<$max ;$i++)
            <tr>
                @if($i == 0)
                    <td rowspan="{{ max($max,1) }}"> {{$patient->id}} </td>
                    <td rowspan="{{ max($max,1) }}"> {{$patient->first_name}} </td>
                    <td rowspan="{{ max($max,1) }}"> {{$patient->last_name}} </td>
                    <td rowspan="{{ max($max,1) }}"> {{$patient->birth_date->format('d-m-Y')}} </td>
                    <td rowspan="{{ max($max,1) }}"> {{$patient->vital_card}} </td>
                @endif
                <td>{{$treatments['pcr'][$i]['date']??''}}</td>
                <td>{{$treatments['antigen'][$i]['date']??''}}</td>
                <td>{{$patient->vaccins[$i]['date']??''}}</td>
                <td>{{$patient->vaccins[$i]['name']??''}}</td>
                <td>{{$patient->vaccins[$i]['batch_number']??''}}</td>
                <td>{{$patient->vaccins[$i]['dose']??''}}</td>
                <td>{{$treatments['visite'][$i]['date']??''}}</td>
                <td>{{$patient->trackings[$i]['pickup_date']??''}}</td>
                <td>
                    @if(isset($patient->trackings[$i]))
                        {{ $patient->trackings[$i]->duration?$patient->trackings[$i]->duration.' '.__($patient->trackings[$i]->duration_unit):"" }}
                    @endif
                </td>
                <td>
                    @if(isset($patient->trackings[$i]) && $patient->trackings[$i]->prescription_url != null)
                        <a href="{{ Storage::disk('s3')->url($patient->trackings[$i]->prescription_url) }}">Voir
                            fichier</a>
                    @endif
                </td>
            </tr>
        @endfor
    @endforeach
    </tbody>
</table>
