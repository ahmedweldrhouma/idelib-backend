@inject('model', '\App\Domains\AdminSubscription\Models\AdminSubscription')

@extends('backend.layouts.app')

@section('title', __('Créer abonnement'))

@section('page_class', 'create-admin-sub-page')

@section('content')
    <x-forms.post :action="route('admin.admin_subscription.store')">
        <x-backend.card>
            <x-slot name="header">
                @lang('Créer abonnement')
            </x-slot>

            <x-slot name="headerActions">
                <x-utils.link class="card-header-action btn btn-light btn-sm" :href="URL::previous()">
                    <x-slot name="slot">
                        {{ __('Cancel') }}
                    </x-slot>
                </x-utils.link>
            </x-slot>

            <x-slot name="body">
                <div>
                    <div class="form-group row">
                        <label for="user_name" class="col-md-2 col-form-label">
                            @lang('Nom et Prénom')<span class="alert-required" aria-hidden="true">*</span></label>

                        <div class="col-md-10">
                            <div class="row">
                                <div class="col-md-8">
                                    <select class="user_id form-control" id="user_id" name="user_id"></select>
                                </div>
                                <div class="col-md-3">
                                    @lang('Or')
                                    <button class="btn btn-primary btn-sm" type="button" data-toggle="modal" data-target="#Modal1" >@lang('Create User') </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="plan_id" class="col-md-2 col-form-label">
                            @lang('Abonnement')<span class="alert-required" aria-hidden="true">*</span></label>
                        <div class="col-md-10">
                            <select name="plan_id" id="plan_id" class="form-control" required>
                                <option disabled selected>Choisissez un abonnement</option>
                                @foreach ($plans as $plan)
                                    <option value="{{ $plan->id }}">{{ $plan->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="subscribed_at" class="col-md-2 col-form-label">
                         @lang('Souscrit le')<span class="alert-required" aria-hidden="true">*</span></label>

                        <div class="col-md-10">
                            <input type="date" name="subscribed_at" class="form-control" placeholder="{{ __('Souscrit le') }}" value="{{ old('subscribed_at') }}" maxlength="100" required />
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="expires_at" class="col-md-2 col-form-label">
                          @lang('Expire le')<span class="alert-required" aria-hidden="true">*</span></label>

                        <div class="col-md-10">
                            <input type="date" name="expires_at" class="form-control" placeholder="{{ __('Expire le') }}" value="{{ old('expires_at') }}" maxlength="100" required />
                        </div>
                    </div>

                </div>
            </x-slot>

            <x-slot name="footer">
                <button class="btn btn-sm btn-primary float-right" type="submit">@lang('Créer abonnement')</button>
            </x-slot>
        </x-backend.card>
    </x-forms.post>
    <div class="modal fade " id="Modal1" tabindex="-1" role="dialog" aria-labelledby="Modal1"
         aria-hidden="true">
        <div class="modal-dialog modal-xl" style="max-width: 500!important;" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="ModalLabel">@lang('Create User')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form name="add_user" id="add_user" method="POST"
                          action="{{ route('admin.auth.user.store')}}" enctype='multipart/form-data'>
                        {{csrf_field()}}
                        <div class="container mt-3">
                            <input type="hidden" name="user_type" value="patient">
                            <div class="row jumbotron box8">
                                <div class="col-sm-12 mx-t3 mb-4 " style="margin-top: -4rem; ">
                                    <div class="avatar-upload">
                                        <div class="avatar-edit">
                                            <input type='file' name="avatar_location" id="imageUpload" accept=".png, .jpg, .jpeg" />
                                            {{-- <input type="hidden" id="avatar_type" name="avatar_type" value="storage"> --}}
                                            <label for="imageUpload">
                                                <i class="cil-pencil icon"></i>
                                            </label>
                                        </div>
                                        <div class="avatar-preview">
                                            <div id="imagePreview" style="background-image: url({{ asset('img/default-avatar.png') }});">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="type" value="nurse">

                                <div class="col-sm-6 form-group">
                                    <label for="first_name">
                                        @lang('Prénom')<span class="alert-required"
                                                             aria-hidden="true">*</span></label>
                                    <input type="text" name="first_name" class="form-control"
                                           placeholder="{{ __('Prénom') }}"
                                           value="{{ old('first_name') }}" maxlength="100" required/>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label for="last_name">
                                        @lang('Nom')<span class="alert-required" aria-hidden="true">*</span></label>
                                        <input type="text" name="last_name" class="form-control"
                                               placeholder="{{ __('Nom') }}" value="{{ old('last_name') }}"
                                               maxlength="100" required/>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label for="email">
                                        @lang('E-mail Address')<span class="alert-required"
                                                                     aria-hidden="true">*</span></label>
                                        <input type="email" name="email" class="form-control"
                                               placeholder="{{ __('E-mail Address') }}"
                                               value="{{ old('email') }}" maxlength="255" required/>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label for="birth_date">@lang('Date de naissance')</label>
                                    <input type="date" name="birth_date" id="birth_date"
                                           class="form-control" value="{{ old('birth_date') }}"
                                           placeholder="{{ __('Date de naissance') }}" maxlength="100"/>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label for="password" >
                                        @lang('Password') <span class="alert-required"
                                                                aria-hidden="true">*</span></label>
                                        <input type="password" name="password" id="password"
                                               class="form-control" placeholder="{{ __('Password') }}"
                                               maxlength="100" required autocomplete="new-password"/>

                                </div>
                                <div class="col-sm-6 form-group">
                                    <label for="password_confirmation">
                                        @lang('Password Confirmation')<span class="alert-required"
                                                                            aria-hidden="true">*</span></label>
                                        <input type="password" name="password_confirmation"
                                               id="password_confirmation" class="form-control"
                                               placeholder="{{ __('Password Confirmation') }}"
                                               maxlength="100" required autocomplete="confirm-password"/>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label for="phone">@lang('Numéro de téléphone')</label>
                                        <input type="phone" name="phone" id="phone" class="form-control"
                                               value="{{ old('phone') }}"
                                               placeholder="{{ __('Numéro de téléphone') }}"
                                               maxlength="100"/>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label for="adeli_number">@lang('Numéro Adéli')</label>
                                        <input type="text" name="adeli_number" id="adeli_number"
                                               class="form-control" value="{{ old('adeli_number') }}"
                                               placeholder="{{ __('Numéro Adéli') }}" maxlength="100"/>
                                </div>
                                 <div class="col-sm-6 form-group">
                                     <label for="tour_sector">@lang('Secteur de tournée')</label>
                                     <select class="tour_sector js-example-basic-multiple" id="tour_sector" name="tour_sector[]"  style="width: 100%"></select>
                                </div>
                                <div class="col-sm-6 form-group" >
                                    <label for="address">@lang('Adresse')</label>
                                    <input type="text" name="address" id="address" class="form-control"
                                           value="{{ old('address') }}"
                                           placeholder="{{ __('Adresse') }}" maxlength="100"/>
                                </div>

                            </div>

                            <button class="btn btn-sm btn-primary float-right " style="margin-top: -40px"
                                    type="submit" >@lang('Create User')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('after-scripts')
    <script type="text/javascript">
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $(document).ready(function () {
            $('.user_id').select2({
                placeholder: 'Selectionnez un utilisateur ',
                minimumInputLength: 3,
                ajax: {
                    url: "/admin/user/search",
                    type: 'post',
                    dataType: 'json',
                    delay: 250,
                    data: function (data) {
                        return {
                            _token: CSRF_TOKEN,
                            search: data.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.first_name + ' ' + item.last_name,
                                    id: item.id
                                }
                            })
                        }
                    },
                }
            });
        });
        $(document).ready(function () {
            $('.tour_sector').select2({
                placeholder: 'Selectionnez Secteur de tournée ',
                multiple: 'multiple',
                minimumInputLength: 3,
                ajax: {
                    url: "/admin/tour/searchTourSector ",
                    type: 'post',
                    dataType: 'json',
                    delay: 250,
                    data: function (data) {
                        return {
                            _token: CSRF_TOKEN,
                            search: data.term
                        };
                    },
                    processResults: function(data) {
                        var results = [];
                        $.each(data, function(index, item) {
                            results.push({
                                text: item,
                                id: item
                            });
                        });
                        return { results: results };
                    },
                }
            });
        });
    </script>
@endpush
