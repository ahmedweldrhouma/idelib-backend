@inject('model', '\App\Domains\AdminSubscription\Models\AdminSubscription')

@extends('backend.layouts.app')

@section('title', __('Modifier abonnement'))

@section('content')
    <x-forms.patch :action="route('admin.admin_subscription.update', $admin_subscription)" >
        <x-backend.card>
            <x-slot name="header">
                @lang('Modifier abonnement')
            </x-slot>

            <x-slot name="headerActions">
                <x-utils.link class="card-header-action btn btn-light btn-sm" :href="route('admin.admin_subscription.index')">
                    <x-slot name="slot">
                        {{ __('Cancel') }}
                    </x-slot>
                </x-utils.link>
            </x-slot>

            <x-slot name="body">
                <div>
                    <div class="form-group row">
                        <label for="plan_id" class="col-md-2 col-form-label">@lang('Abonnement')</label>

                        <div class="col-md-10">
                            <select name="plan_id" id="plan_id" class="form-control" required>
                                {{-- <option disabled selected>Choisissez un abonnement</option> --}}
                                @foreach ($plans as $plan)
                                    <option value="{{ $plan->id }}" @if($plan->id == $admin_subscription->plan_id) selected @endif>{{ $plan->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="subscribed_at" class="col-md-2 col-form-label">@lang('Souscrit le')</label>

                        <div class="col-md-10">
                            <input type="date" name="subscribed_at" class="form-control" placeholder="{{ __('Souscrit le') }}" value="{{ old('subscribed_at') ?? date('Y-m-d',strtotime($admin_subscription->subscribed_at)) }}" maxlength="100" required />
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="expires_at" class="col-md-2 col-form-label">@lang('Expire le')</label>

                        <div class="col-md-10">
                            <input type="date" name="expires_at" class="form-control" placeholder="{{ __('Expire le') }}" value="{{ old('expires_at') ?? date('Y-m-d',strtotime($admin_subscription->expires_at)) }}" maxlength="100" required />
                        </div>
                    </div>

                </div>
            </x-slot>

            <x-slot name="footer">
                <button class="btn btn-sm btn-primary float-right" type="submit">@lang('Mettre à jour l\'abonnement')</button>
            </x-slot>
        </x-backend.card>
    </x-forms.patch>
@endsection
