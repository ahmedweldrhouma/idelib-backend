<x-livewire-tables::bs4.table.cell>
    {{ $row->user->first_name }} {{ $row->user->last_name }}
</x-livewire-tables::bs4.table.cell>

<x-livewire-tables::bs4.table.cell>
    {{ $row->plan->name }}
</x-livewire-tables::bs4.table.cell>

<x-livewire-tables::bs4.table.cell>
    {{ $row->user->getTypeFormatted() }}
</x-livewire-tables::bs4.table.cell>

<x-livewire-tables::bs4.table.cell>
    {{ date("d-m-Y", strtotime($row->subscribed_at)) }}
</x-livewire-tables::bs4.table.cell>

<x-livewire-tables::bs4.table.cell>
    {{ date("d-m-Y", strtotime($row->expires_at)) }}
</x-livewire-tables::bs4.table.cell>

<x-livewire-tables::bs4.table.cell>
    @include('backend.adminSubscription.includes.actions', ['admin_subscription' => $row])
</x-livewire-tables::bs4.table.cell>
