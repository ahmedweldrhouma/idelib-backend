@if ($logged_in_user->isSuperAdmin())


<div class="dropdown">
        <a class="btn btn-sm btn-secondary dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {{ ('Actions') }} </a>

        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
        <x-utils.link class="dropdown-item" :href="route('admin.admin_subscription.edit', $admin_subscription)">
                {{ __('Edit') }}
            </x-utils.link>

            <x-utils.form-button
                :action="route('admin.admin_subscription.destroy',$admin_subscription)" method="delete"
                button-class="dropdown-item">
                @lang('Delete')
            </x-utils.form-button>
        </div>
    </div>

@endif
