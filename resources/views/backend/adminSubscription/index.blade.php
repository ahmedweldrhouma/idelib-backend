@extends('backend.layouts.app')

@section('title', __('Admin Subscription'))

@section('content')
    <x-backend.card>
        <x-slot name="header">
            @lang('Admin Subscription')
        </x-slot>

        @if ($logged_in_user->hasAllAccess())
            <x-slot name="headerActions">
                <x-utils.link
                    icon="c-icon cil-plus"
                    class="card-header-action btn btn-primary btn-sm"
                    :href="route('admin.admin_subscription.create')"
                    >
                Créer un abonnement
                                </x-utils.link>
            </x-slot>
        @endif

        <x-slot name="body">
            <livewire:backend.admin-subscriptions-table />
        </x-slot>
    </x-backend.card>
@endsection
