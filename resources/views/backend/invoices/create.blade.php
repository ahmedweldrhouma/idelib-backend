@inject('model', '\App\Domains\Patient\Models\Patient')

@extends('backend.layouts.app')

@section('title', __('Créer un patient') . ' ' . __($type))
<style>
    .card-footer{
        margin-top: -80px;
    }
</style>
@section('content')
<x-forms.post :action="route('admin.patient.store')" :enctype="__('multipart/form-data')">
    <x-backend.card>
        <x-slot name="header">
            @lang('Créer un patient') @lang($type)
        </x-slot>

        <x-slot name="headerActions">
            <x-utils.link class="card-header-action btn btn-light btn-sm" :href="URL::previous()">
                <x-slot name="slot">
                    {{ __('Cancel') }}
                </x-slot>
            </x-utils.link>
        </x-slot>
        <x-slot name="body">
        <div class="container mt-3">
            <input type="hidden" name="user_type" value="patient">
                <div class="row jumbotron box8">
                    <div class="col-sm-12 mx-t3 mb-4 " style="margin-top: -4rem; ">
                        <div class="avatar-upload">
                            <div class="avatar-edit">
                                <input type='file' name="avatar_location" id="imageUpload" accept=".png, .jpg, .jpeg" />
                                {{-- <input type="hidden" id="avatar_type" name="avatar_type" value="storage"> --}}
                                <label for="imageUpload">
                                    <i class="cil-pencil icon"></i>
                                </label>
                            </div>
                            <div class="avatar-preview">
                                <div id="imagePreview"
                                     style="background-image: url({{ asset('img/default-avatar.png') }});">
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name='type' value="{{ $type }}">
                    <div class="col-sm-6 form-group">
                        <label for="first_name">@lang('Prénom')<span class="alert-required" aria-hidden="true">*</span></label>
                        <input type="text" name="first_name" class="form-control" placeholder="{{ __('Prénom') }}"
                               value="{{ old('first_name') }}" maxlength="100" required />
                    </div>
                    <div class="col-sm-6 form-group">
                        <label for="last_name">@lang('Nom')<span class="alert-required" aria-hidden="true">*</span></label>
                        <input type="text" name="last_name" class="form-control" placeholder="{{ __('Nom') }}"
                               value="{{ old('last_name') }}" maxlength="100" required />
                    </div>
                    <div class="col-sm-6 form-group">
                        <label for="email" >@lang('E-mail Address')</label>
                        <input type="email" name="email" class="form-control" placeholder="{{ __('E-mail Address') }}"
                               value="{{ old('email') }}" maxlength="255" />
                    </div>
                    <div class="col-sm-6 form-group">
                        <label for="social_security_number" style="white-space : nowrap;">
                            @lang('N° de sécurité sociale')<span class="alert-required" aria-hidden="true">*</span></label>
                        <input type="text" name="social_security_number" id="social_security_number"
                               class="form-control" placeholder="{{ __('N° de sécurité sociale') }}" maxlength="100" />
                    </div>
                    <div class="col-sm-6 form-group">
                        <label for="phone">@lang('Numéro de téléphone')</label>
                        <input type="phone" name="phone" id="phone" class="form-control"
                               placeholder="{{ __('Numéro de téléphone') }}" maxlength="100" />
                    </div>
                    <div class="col-sm-4 form-group">
                        <label for="birth_date">@lang('Date de naissance')</label>
                        <input type="date" name="birth_date" id="birth_date" class="form-control"
                               placeholder="{{ __('Date de naissance') }}" maxlength="100" />
                    </div>
                    <div class="col-sm-2 form-group">
                        <label for="address" >@lang('Adresse')</label>
                        <input type="text" name="address" id="address" class="form-control"
                               placeholder="{{ __('Adresse') }}" maxlength="100" />
                    </div>
                    <div class="col-sm-6 form-group">
                        <label for="doctor" >@lang('Médecin traitant')</label>
                        <input type="text" name="doctor" id="doctor" class="form-control"
                               placeholder="{{ __('Médecin traitant') }}" maxlength="100" />
                    </div>
                    <div class="col-sm-6 form-group">
                        <label for="doctor_phone">@lang('Tel médecin traitant')</label>
                        <input type="text" name="doctor_phone" id="doctor_phone" class="form-control"
                               placeholder="{{ __('Tel médecin traitant') }}" maxlength="100" />
                    </div>
                    <div class="col-sm-6 form-group">
                        <label for="vital_card">@lang('Carte vital')</label>
                        <input type="number" name="vital_card" id="vital_card" class="form-control"
                               placeholder="{{ __('Carte vitale') }}" maxlength="100" />
                    </div>
                    <div class="col-sm-2 form-group">
                        <label for="mutual_card">@lang('Carte mutuelle')</label>
                        <input type="file" accept=".gif , .pdf , .png, .jpg, .jpeg " name="mutual_card"
                               id="mutual_card" />
                    </div>
                    <div class="col-sm-2 ml-xxl-5 form-group" >
                        <label> @lang('Documents') </label>
                        <input type="file" name="documents[]" id="documents"
                               accept=".doc , .pdf , .docx , .png, .jpg, .jpeg " multiple />
                    </div>
                </div>
        </div>
        </x-slot>
        <x-slot name="footer">
            <button class="btn btn-sm btn-primary float-right" type="submit">@lang('Créer patient')</button>
        </x-slot>
    </x-backend.card>
</x-forms.post>
@endsection
@push('after-scripts')
    <script>
        function displayAvatar() {
            const input = document.getElementById('imageUpload');
            // Check if a file is selected
            if (input.files && input.files[0]) {
                const reader = new FileReader();
                reader.onload = function (e) {
                    const avatarImage = document.createElement('img');
                    avatarImage.src = e.target.result;

                    avatarImage.style.width = '100px';
                    avatarImage.style.height = '100px';
                    avatarImage.style.objectFit = 'cover';
                    avatarImage.style.borderRadius = '50%';

                    const previousAvatar = document.getElementById('avatar-preview');
                    if (previousAvatar) {
                        previousAvatar.remove();
                    }
                    avatarImage.id = 'avatar-preview';
                    const container = document.getElementById('imagePreview');
                    container.appendChild(avatarImage);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        const input = document.getElementById('imageUpload');
        input.addEventListener('change', displayAvatar);
    </script>
@endpush
