@extends('backend.layouts.app')

@section('title', __('Gestion de la patientèle'))

@section('content')
    <x-backend.card><!--
        <x-slot name="header">
            @lang('Factures')
        </x-slot>-->


        <x-slot name="body">

            @if (isset($tabNextInvoice['date']))
                <i>Prochaine facture sera prélevée le
                    <strong>{{$tabNextInvoice['date']}}</strong>, d'un montant de
                    <strong>{{$tabNextInvoice['amount']}} &euro;</strong>
                </i>
            @endif
            @if(!empty($invoices))
                <table class="table table-striped">
                    <thead>
                    <tr>

                        <th class="">
                            Montant
                        </th>
                        <th>
                            Émise le
                        </th>

                        <th class="">
                            Télécharger
                        </th>

                    </tr>
                    </thead>

                    <tbody wire:sortable="">

                    @foreach ($invoices['invoices'] as $invoice)
                        <tr>
                            <td colspan="">
                                {{ $invoice->amount_paid / 100  }} &euro;
                            </td>

                            <td colspan="">
                                {{ \Carbon\Carbon::parse($invoice->effective_at)->format('Y-m-d H:i:s') }}
                            </td>

                            <td>
                                <a href="{{ $invoice->invoice_pdf}}" target="_blank">Télécharger</a>
                            </td>
                            <td></td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>
            @endif

            @if (session('message'))
                <br/><br/>
                <span class="alert alert-success">{{ session('message') }}</span><br/><br/>
            @endif
            @if ($oneAbo)
                @if ($messageSoonExpired != null)
                    <br/><br/>
                    {{$messageSoonExpired}}
                @else
                    <button class="btn btn-default" data-href="{{ route('admin.invoices.unsubscribe') }}"
                            data-toggle="modal" data-target="#confirm-delete">
                        Se désabonner
                    </button>
                @endif
            @endif

            <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Confirmation désabonnement</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                        </div>

                        <div class="modal-body">
                            <p>Voulez-vous vraiment vous désabonner de Idelib ?</p>
                            <p class="debug-url"></p>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                            <a class="btn btn-danger btn-ok">Supprimer</a>
                        </div>
                    </div>
                </div>
            </div>


        </x-slot>
    </x-backend.card>
@endsection

@push('after-scripts')
    <script>
        $('#confirm-delete').on('show.bs.modal', function (e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));

            $('.debug-url').html('Delete URL: <strong>' + $(this).find('.btn-ok').attr('href') + '</strong>');
        });
    </script>
@endpush
