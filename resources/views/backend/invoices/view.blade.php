@inject('model', '\App\Domains\Plan\Models\Plan')

@extends('backend.layouts.app')

@section('title', __('Details patient'))

@section('content')
    <!-- Breadcrumb -->

    <div class="row gutters-sm" id="patient-card">
        <div class="col-md-4 mb-3 ">
            <div class="card">
                <div class="card-body p-5">
                    <div class="d-flex flex-column align-items-center text-center">
                        <img src="{{ $patient->getPicture() }}" alt="Admin"
                             class="rounded-circle" width="100">
                        <div class="mt-3">
                        <span class="h5 d-block" href="javascript:">
                            <strong> {{ $patient->getName() }} </strong></span>
                            <span class="badge badge-info">{{ __($patient->type) }}</span>
                            {{--
                            <p class="text-muted font-size-sm">{{ $patient->phone }}</p>
                            --}}
                        </div>
                    </div>
                    <div class="container" style="padding-top: 10px">
                        @if($patient->died_at)
                            <div class="alert alert-danger " role="alert">
                                Décédé le : {{ \Carbon\Carbon::parse($patient->died_at)->format('H:i m-d-Y') }}
                            </div>
                        @endif
                    </div>

                    <!-- Accordion -->
                    <div class="acc-container">
                        <div class="acc">
                            <div class="acc-head" style="border-bottom: 1px solid darkgrey">
                                <p>Détails</p>
                            </div>
                            <div class="acc-content" >
                                <div class="mt-3"><strong>Id du patient</strong></div>
                                <div class="text-muted">{{ $patient->id }}</div>
                                @if(Auth::user()->isBiller())
                                    <div class="mt-3"><strong>Email</strong></div>
                                    <div class="text-muted">
                                        <a href="#" class="text-muted text-hover-primary">
                                            {{   Str::mask( Str::before($patient->email, '@'),'*',0) . '@' . Str::after($patient->email, '@')  }}
                                        </a>
                                    </div>
                                    <div class="mt-3"><strong>Date de naissance </strong></div>
                                    <div
                                        class="text-muted">{{ Str::mask($patient->birth_date->format('d-m-Y'), '*',-4)   ?? __('No Information Available')}}</div>
                                    <div class="mt-3"><strong>Adresse</strong></div>
                                    <div class="text-muted">
                                        {{Str::mask($patient->address, '*',4) ?? __('No Information Available')}}
                                    </div>
                                    <div class="mt-3"><strong>Numéro de sécurité sociale</strong></div>
                                    <div
                                        class="text-muted">{{Str::mask( $patient->social_security_number, '*',4) ?? __('No Information Available')}}</div>
                                    <div class="mt-3"><strong>Carte vitale</strong></div>
                                    <div
                                        class="text-muted">{{ Str::mask( $patient->vital_card , '*',4) ?? __('No Information Available')}}</div>
                                    <div class="mt-3"><strong>Carte mutuelle</strong></div>
                                    <div class="text-muted">
                                        @if ($patient->getMutualCard() !=null && !auth()->user()->isBiller())
                                            <img src="{{ $patient->getMutualCard() }}" class="w-100">
                                        @else
                                            @lang('No Information Available')
                                        @endif
                                    </div>
                                    <div class="mt-3"><strong>Médecin traitant </strong></div>
                                    <div
                                        class="text-muted">{{  $patient->doctor  ?? __('No Information Available')}}</div>
                                    <div class="mt-3"><strong>Antécédents</strong></div>
                                    <div class="text-muted">{!! $patient->formedAnecedants()!!}</div>
                                    <div class="mt-3"><strong>Date de création</strong></div>
                                    <div
                                        class="text-muted">{{ $patient->created_at->format('H:i d-m-Y') ?? __('No Information Available')}}</div>
                                @else
                                    <div class="mt-3"><strong>Email</strong></div>
                                    <div class="text-muted">
                                        <a href="#" class="text-muted text-hover-primary">{{ $patient->email }}</a>
                                    </div>
                                    <div class="mt-3"><strong>Date de naissance </strong></div>
                                    <div
                                        class="text-muted">{{ $patient->birth_date->format('d-m-Y') ?? __('No Information Available')}}</div>
                                    <div class="mt-3"><strong>Adresse</strong></div>
                                    <div
                                        class="text-muted">{{ $patient->address ?? __('No Information Available')}}</div>
                                    <div class="mt-3"><strong>Numéro de sécurité sociale</strong></div>
                                    <div
                                        class="text-muted">{{ $patient->social_security_number ?? __('No Information Available')}}</div>
                                    <div class="mt-3"><strong>Carte vitale</strong></div>
                                    <div
                                        class="text-muted">{{ $patient->vital_card ?? __('No Information Available')}}</div>
                                    <div class="mt-3"><strong>Carte mutuelle</strong></div>
                                    <div class="text-muted">
                                        @if ($patient->getMutualCard() !=null)
                                            <img src="{{ $patient->getMutualCard() }}" class="w-100">
                                        @else
                                            @lang('No Information Available')
                                        @endif
                                    </div>
                                    <div class="mt-3"><strong>Numéro de sécurité sociale</strong></div>
                                    <div
                                        class="text-muted">{{ $patient->social_security_number ?? __('No Information Available')}}</div>
                                    <div class="mt-3"><strong>Carte vitale</strong></div>
                                    <div
                                        class="text-muted">{{ $patient->vital_card ?? __('No Information Available')}}</div>
                                    <div class="mt-3"><strong>Médecin traitant </strong></div>
                                    <div
                                        class="text-muted">{{ $patient->doctor ?? __('No Information Available')}}</div>
                                    <div class="mt-3"><strong>Antécédents</strong></div>
                                    <div class="text-muted">{!! $patient->formedAnecedants()!!}</div>
                                    <div class="mt-3"><strong>Date de création</strong></div>
                                    <div
                                        class="text-muted">{{ $patient->created_at->format('H:i d-m-Y') ?? __('No Information Available')}}</div>
                                @endif

                            </div>
                        </div>

                        <div class="acc">
                            <div class="acc-head">
                                <p>Absence(s)</p>
                            </div>
                            <div class="acc-content">
                                @foreach($patient->absences as $absent)
                                    <li>
                                        De {{$absent->absent_from->format('H:i d-m-Y')}} a {{$absent->absent_to->format('H:i d-m-Y')}}
                                    </li>
                                @endforeach
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="d-flex justify-content-between">
                <ul class="nav nav-custom nav-tabs nav-line-tabs nav-line-tabs-2x border-0 fs-4 fw-semibold mb-4"
                    role="tablist">
                    <li class="nav-item">
                        <a class="nav-link text-active-primary pb-2 ml-0 active" data-toggle="tab" href="#tabletreats"
                           aria-selected="true" role="tab">@lang('Treatments')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-active-primary pb-2" data-toggle="tab" href="#tablecovids"
                           data-kt-initialized="1" aria-selected="false" tabindex="-1" role="tab">Covid</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-active-primary pb-2" data-toggle="tab" href="#tablevaccins"
                           aria-selected="false" tabindex="-1" role="tab">Vaccins</a>
                    </li>
                    @if(!auth()->user()->isBiller())
                    <li class="nav-item">
                        <a class="nav-link text-active-primary pb-2" data-toggle="tab" href="#tabledocuments"
                           aria-selected="false" tabindex="-1" role="tab">Documents</a>
                    </li>
                    @endif
                </ul>
                @if(!Auth::user()->isBiller())
                    <div>
                        <a href="{{ route('admin.patient.edit',$patient->id) }}"
                           class="btn btn-secondary btn-sm">@lang('Edit')</a>
                        <x-utils.delete-button :href="route('admin.patient.destroy', $patient)"/>

                    </div>
                @endif
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="panel-body">
                        <div class="tab-content">
                            <div class="tab-pane fade active show p-3" id="tabletreats">
                                <livewire:backend.patient-treatments-table :patient="$patient"/>
                            </div>
                            <div class="tab-pane fade p-3" id="tablecovids">
                                <livewire:backend.patient-covid-table :patient="$patient"/>
                            </div>
                            <div class="tab-pane fade p-3" id="tablevaccins">
                                <livewire:backend.patient-vaccins-table :patient="$patient"/>
                            </div>
                            <div class="tab-pane fade p-3" id="tabledocuments">
                                <div class="row">
                                    <div class="col-md-3 col-12">
                                        <div class="card h-100" style="background: aliceblue;border: 1px dashed dodgerblue;">
                                            <div class="card-body d-flex justify-content-center text-center flex-column p-8">
                                                <form name="add_documents" id="add_documents" method="POST"
                                                      enctype="multipart/form-data"
                                                      action="{{ route('admin.patient.updateAttachments',$patient) }}">
                                                    {{csrf_field()}}
                                                    <span class="text-gray-800 text-hover-primary d-flex flex-column">
                                                        <label for="documents" style="cursor: pointer">
                                                            <div class="symbol symbol-60px mb-3">
                                                                <img src="{{ asset('img/svg/upload.svg') }}"
                                                                     class="theme-light-show" alt="">
                                                            </div>
                                                            <span class="text-hover-primary fw-bold mb-2 h6"><strong>Nouveau fichier</strong></span>
                                                            <small class="text-muted">Cliquer pour Télecharger</small>
                                                        </label>
                                                        <input type="file" name="documents[]" id="documents"
                                                               accept=".doc , .pdf , .docx , .png, .jpg, .jpeg "
                                                               multiple style="display: none" required/>
                                                        <div class="fs-5 fw-bold mb-2"><button
                                                                class="btn btn-sm btn-secondary">Sauvegarder</button></div>
                                                    </span>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    @foreach($patient->getAttachments() as $attachment)
                                        <div class="col-md-3 col-12">
                                            <div class="card h-100">
                                                <div
                                                    class="card-body d-flex justify-content-center text-center flex-column p-8">
                                                    <a href="{{ route('admin.patient.deleteAttachments',['filename' => basename($attachment),'patient'=>$patient]) }}"
                                                       class="c-icon mr-6 cil-x-circle"
                                                       style="font-size:22px;color:red">
                                                    </a>
                                                    <a href="{{ $attachment }}"
                                                       class="text-gray-800 text-hover-primary d-flex flex-column">

                                                        <div class="symbol symbol-60px mb-5">
                                                            <img src="{{ asset('img/svg/'.explode('.',basename($attachment))[1]).'.svg' }}"
                                                                 class="theme-light-show w-50" alt="">
                                                        </div>
                                                        <div class="fs-5 fw-bold mb-2">{{ basename($attachment) }}</div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('after-scripts')
    <script>

        $(document).ready(function() {
            $('.acc-container .acc:nth-child(1) .acc-head').addClass('active');
            $('.acc-container .acc:nth-child(1) .acc-content').slideDown();
            $('.acc-head').on('click', function() {
                if($(this).hasClass('active')) {
                    $(this).siblings('.acc-content').slideUp();
                    $(this).removeClass('active');
                }
                else {
                    $('.acc-content').slideUp();
                    $('.acc-head').removeClass('active');
                    $(this).siblings('.acc-content').slideToggle();
                    $(this).toggleClass('active');
                }
            });
        });

    </script>
@endpush
