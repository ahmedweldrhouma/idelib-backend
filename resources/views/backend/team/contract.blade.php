<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Contrat de remplacement ENTRE UN INFIRMIER LIBERAL ET UN INFIRMIER TITULAIRE D’UNE AUTORISATION DE REMPLACEMENT</title>
</head>
<header style="text-align: center;">
    <h2>Contrat de remplacement ENTRE UN INFIRMIER LIBERAL ET UN INFIRMIER TITULAIRE D’UNE AUTORISATION DE REMPLACEMENT</h2>
</header>
<body style="text-align: justify;">
    <section>
        <p>
            Entre <strong>M./Mme</strong> {{ $data['first_name_liberal'] }} {{ $data['last_name_liberal'] }}, Infirmier(e) Diplomé(e) d’Etat,</br>
            n° ordinal {{ $data['ordinal_number_liberal'] }},</br>
            n° adeli {{ $data['adeli_number_liberal'] }},</br>
            titulaire d’un cabinet sis {{ $data['cabinet_sis_liberal'] }}, d’une part,</br>
        </p>
        <p>Et</p>
        <p>
            <strong>M./Mme</strong> {{ $data['first_name_substitute'] }} {{ $data['last_name_substitute'] }}, Infirmier(e) Diplomé(e) d’Etat,</br>
            n° ordinal {{ $data['ordinal_number_substitute'] }},</br>
            n° adeli {{ $data['adeli_number_substitute'] }},</br>
            et possédant le statut de remplaçant(e) accordé par autorisation du conseil de l’Ordre sous le numéro………….., en date du ……………….,<br>
            et autorisé(e) par la CPAM de ………… </br>
            domicilié(e) à………………………., d’autre part</br>
        </p>
    </section>
    <section>
        <h3>PREAMBULE *</h3>
        <p>
            L’infirmier(ère) remplacé(e), Diplômé(e) d’Etat, devant suspendre personnellement, provisoirement et ponctuellement son exercice professionnel pour le motif suivant : …………………………..…… ,fait temporairement appel à l’infirmier(ère) remplaçant(e), en qualité d’Infirmièr(e) Diplômé(e) d’Etat, remplaçant(e), inscrite à l’Ordre, afin d’assurer la continuité des soins délivrés à ses patients.
        </p>
        <p>
            L’infirmier(ère) remplaçant(e), exercera ce remplacement à titre libéral sans aliéner son indépendance professionnelle.
        </p>
        <p>
            L’infirmier(ère) remplacé(e) et l’infirmier(ère) remplaçant(e), déclarent ne faire l’objet d’aucune sanction disciplinaire interdisant d’exercer la profession ni d’aucune mesure de déconventionnement.
        </p>
        <p>
            L’infirmier(ère) remplaçant(e) déclare solennellement ne pas remplacer plus de deux infirmiers ou infirmières concomitamment, y compris dans une association d’infirmiers ou d’infirmières ou dans un cabinet de groupe.
        </p>
        <p>
            [<u><i>Le cas échéant</i></u>] : L’infirmier(ère) remplacé(e) déclare avoir informé du remplacement l’ensemble des associés de la Société d’Exercice Libéral <u>ou</u> de la Société Civile Professionnelle <u>ou</u> l’ensemble de ses partenaires dans le cadre d’un exercice en commun <u>ou</u> son cocontractant dans le cadre d’un contrat de collaboration (<u><i>rayer la mention inutile</i></u>) .
            A cet effet, notamment, M./Mme ... a communiqué à l’ensemble de ses associés une copie du présent contrat de remplacement.
        </p>
        <p>Vu le Code de la Santé Publique, notamment ses articles R.4312-83 à R.4312-87;</p>
        <p>Vu la convention nationale des infirmiers conclue le 22 juin 2007, ainsi que ses avenants ; </p>
        <p>Il a été convenu ce qui suit :</p>
    </section>
    <section>
        <h3>Article ler – Objet</h3>
        <p>
            M./Mme {{ $data['first_name_substitute'] }} {{ $data['last_name_substitute'] }} exercera, pendant la durée prévue à l’article 2 du présent contrat, la profession d’infirmier en lieu et place de M./Mme {{ $data['first_name_liberal'] }} {{ $data['last_name_liberal'] }}, indisponible temporairement.
        </p>
        <p>
            Les patients devront être informés dès que possible de la présence d’un(e) infirmier(e) remplaçant(e), notamment lors de visites à domicile ou de rendez-vous au cabinet.
        </p>
    </section>
    <section>
        <h3>Article 2 – Durée *</h3>
        <p>
            Le présent contrat est conclu :
        </p>
        <ul>
            <li>Du {{ date("d-m-Y", strtotime($data['date_start'])) }} au {{ date("d-m-Y", strtotime($data['date_end'])) }} et selon un planning annexé au présent contrat et déterminé dans un délai raisonnable</li>
        </ul>
        <p><u>OU</u></p>
        <ul>
            <li>Pour les jours suivants : …………….</li>
        </ul>
        <p>Il pourra être prolongé dans les conditions prévues à l’article 9 du présent contrat si l’indisponibilité du remplacé le justifie. </p>
    </section>
    <section>
        <h3>Article 3 – Lieu d’exercice professionnel</h3>
        <p>
            L’infirmier(ère) remplacé(e) met à disposition de l’infirmier(ère) remplaçant(e) son cabinet comprenant : ………………………………….……………………… (exemple : un local professionnel, des installations, des appareils et du matériel à usage unique, son secrétariat…), sis {{ $data['cabinet_sis_liberal'] }}., 
            sans qu’aucun lien contractuel de location, de sous location ou d’occupation emportant indemnité ne soit créé entre les deux parties nonobstant les dispositions de l’article 5 du présent contrat.
        </p>
        <p>
            L’infirmier(ère) remplaçant(e) en fera un usage exclusivement professionnel et s’interdira toute modification des lieux et/ou de leur destination.
        </p>
        <p>
            Notamment, l’infirmier(ère) remplaçant(e) devra veiller à l’entretien et à la maintenance du local professionnel, des installations et du matériel mis à disposition par l’infirmier(ère) remplacé(e) pendant toute la durée du remplacement.
        </p>
    </section>
    <section>
        <h3>Article 4 – Obligation des parties *</h3>
        <p><u>4.1. Obligations du (de la) remplaçant(e) </u></p>
        <p>L’infirmier(ère) remplaçant(e):</p>
        <ul>
            <li>
                Agit en toute circonstance dans l’intérêt des patients qui lui sont confiés par l’infirmier(ère) remplacé(e). Il/elle leur délivre des soins consciencieux, attentifs et conformes aux données acquises de la science, dans le respect des règles applicables à la profession d’infirmier, notamment du code de déontologie.
            </li>
            <li>
                consacrer à cette activité tout le temps nécessaire selon les modalités habituelles de fonctionnement du cabinet.
            </li>
            <li>
                Entretient avec les autres infirmièr(e)s avec qui il/elle est en relation durant le contrat de remplacement des rapports de bonne confraternité.
            </li>
            <li>
                S’engage à respecter les dispositions légales, réglementaires, conventionnelles et déontologiques applicables à la profession d’infirmier et, le cas échéant, le règlement intérieur du cabinet de l’infirmier(ère) remplacé(e) qui lui est temporairement mis à sa disposition.
            </li>
            <li>
                Apporte la preuve qu’il/elle a contracté une police d’assurance responsabilité civile professionnelle avant le début de son activité. Son attestation de responsabilité civile professionnelle est annexée au présent contrat de remplacement.
            </li>
            <li>
                Sera seul(e) responsable vis-à-vis des patients et des tiers des conséquences de son activité professionnelle dans le cadre du remplacement temporaire.
            </li>
            <li>
                S’assure en tout état de cause que les cotations sont conformes à la NGAP en particulier lorsque c’est l’infirmier remplacé qui procède à la facturation. 
            </li>
        </ul>

        <p><u>4.2. Obligations du (de la) remplacé(e)</u></p>
        <p>L’infirmier(ère) remplacé(e) :</p>
        <ul>
            <li>
                s’interdit pendant la durée du présent contrat toute activité professionnelle d’infirmier à l’exception toutefois du suivi d’une formation professionnelle et sous réserve des articles R.4312-7 (assistance aux personnes blessées ou en péril) et R.4312-8 (collaboration à un dispositif de secours en situation d’urgence) du Code de la santé publique.
            </li>
            <li>
                s’engage à mettre à la disposition de l’infirmier(ère) remplaçant(e) des locaux et du matériel professionnel en état et en nombre suffisant afin qu’il/elle soit en mesure de remplir au mieux la mission qui lui est confiée.
            </li>
            <li>
                s’engage par ailleurs à mettre à disposition de l’infirmier(ère) remplaçant(e)l’ensemble des informations nécessaires au bon déroulement et à la continuité des soins.
            </li>
            <li>
                s’engage à porter à la connaissance de l’infirmier(ère) remplaçant(e) les dispositions de la convention nationale des infirmiers et à l’informer des droits et obligations qui s’imposent à lui/elle dans ce cadre.
            </li>
            <li>
                s’engage à informer les organismes d’assurance maladie en leur indiquant le nom du(de la) remplaçant(e), la durée et les dates de son remplacement ainsi que le numéro et la date de délivrance de l’autorisation par le Conseil de l’ordre.
            </li>
            <li>
                Fourni au remplaçant les documents permettant de vérifier la concordance entre la cotation des actes facturés et la rémunération due.
            </li>
        </ul>
    </section>
    <section>
        <h3>Article 5 – Honoraires *</h3>
        <p>
            L’infirmier(ère) remplaçant(e) utilisera la carte de professionnel de santé (CPS) remplaçant délivrée par l’ASIP santé à l’occasion de son activité de soins et pendant la durée du présent contrat <u>OU</u> conformément aux règles fixées par les caisses d’assurance maladie les feuilles de soins et imprimés pré-identifiés au nom de l’infirmier(ère) remplacé(e). 
        </p>
        <p>
            En cas d’usage de feuilles de soins, il/elle devra y faire mention de son identification personnelle.
        </p>
        <ul>En cas de paiement direct par l’assuré à l’infirmier(ère) à remplaçant(e):
            <li>L’infirmier(ère) remplaçant(e) percevra lui/elle-même pour le compte de l’infirmier(ère) remplacé(e) l’ensemble des honoraires correspondant aux actes effectués sur les patients à qui il/elle aura donné ses soins.</li>
            <li>Un bordereau récapitulatif sera tenu à cet effet par l’infirmier(ère) remplaçant(e). Ces recettes seront remises au plus tard à l’infirmier(ère) remplacé(e) le ………………………</li>
        </ul>
        <p>
            Dans ce cas, l’infirmier(ère) remplaçant(e) devra justifier auprès de l’infirmier(ère) remplacé(e) l’ensemble brut des honoraires et rémunérations perçus par lui/elle pour le compte de l’infirmier(ère) remplacé(e) pendant son activité de remplacement par un relevé des actes effectués ou des rémunérations perçues, quels qu’en soient le montant et la forme (y compris les recettes devant être encaissées a posteriori).
        </p>
        <p>
            Sur le total des honoraires perçus pendant le remplacement au titre des soins que l’infirmier(ère) remplaçant(e) a effectivement accomplis à l’exception des indemnités kilométriques, l’infirmier(ère) remplacé(e) en reversera   …..%   à l’infirmier(ère) remplaçant(e) et ce, dans un délai de … mois qui suit la fin du remplacement.
            En cas de tiers payant, l’infirmier(ère) remplacé(e) continue de recevoir directement des caisses d’assurance maladie les honoraires remboursés pour les actes effectués par l’infirmier(ère) remplaçant(e).
        </p>
        <p>
            Sur le total des honoraires tiers payant au titre des actes que l’infirmier(ère) remplaçant(e) a effectivement effectués, l’infirmier(ère) remplacé(e) en reversera  … % à l’infirmier(ère) remplaçant(e), et ce, dans un délai de … mois suivant la fin du remplacement.
        </p>
    </section>
    <section>
        <h3>Article 6 – Obligations fiscales et sociales </h3>
        <p>
            Chaque partie contractante procédera à ses déclarations fiscales et sociales de manière indépendante et supportera personnellement, chacune en ce qui la concerne, la totalité de ses charges fiscales et sociales afférentes audit remplacement.
        </p>
    </section>
    <section>
        <h3>Article 7 – Non concurrence * </h3>
        <p>
            Conformément à l’article R.4312-87 du code de la santé publique, l’infirmier qui remplace un de ses collègues pendant une période supérieure à trois mois, consécutifs ou non, ne doit pas, pendant une période de deux ans, s’installer dans un cabinet où il puisse entrer en concurrence directe avec le confrère remplacé et, éventuellement, avec les infirmiers exerçant en association ou en société avec celui-ci, à moins qu’il n’y ait entre les intéressés un accord, lequel doit être notifié au conseil départemental de l’ordre. 
        </p>
        <p>
            Cette zone est fixée d’un commun accord à un rayon de ……. kms autour du lieu d’exercice <u>OU</u> couvre les communes de …….......
        </p>
    </section>
    <section>
        <h3>Article 8 – Résolution des différends découlant du présent contrat</h3>
        <p>
            En cas de difficultés soulevées par l’exécution, l’interprétation ou la résiliation du présent contrat, les parties s’engagent, préalablement à toute action contentieuse ou disciplinaire, à soumettre leur différend à un arbitre librement choisi par les parties qui peut être le Conseil départemental de l’Ordre des infirmiers. Celui-ci s’efforcera de concilier les parties et d’amener à une solution amiable dans un délai maximum de 2 mois à compter de sa saisine.
        </p>
    </section>
    <section>
        <h3>Article 9 – Renouvellement</h3>
        <p>
            Le présent contrat est conclu pour la durée fixée à l’article 2. En cas de prolongement temporaire de l’indisponibilité de l’infirmier(ère) remplacé(e), le contrat pourra être prolongé pour une durée équivalente qui devra faire l’objet d’un avenant signé par les parties au plus tard au jour du terme du présent contrat.
        </p>
    </section>
    <section>
        <h3>Article 10 – Incessibilité</h3>
        <p>
            Compte tenu du fort caractère intuitu personae attaché au présent contrat de remplacement, celui-ci n’est pas cessible.
        </p>
    </section>
    <section>
        <h3>Article 11 – Résiliation anticipée </h3>
        <p><u>Article 11. 1 : résiliation d’un commun accord</u></p>
        <p>
            Le présent contrat pourra être résilié d’un commun accord entre les parties co-contractantes moyennant le respect d’un préavis de …………….. jours. Un document cosigné par les parties en prend acte.
        </p>
        <p><u>Article 11.2 : résiliation unilatérale</u></p>
        <p>
            Au cas où, pendant la durée du présent contrat, l’une des parties ne respecterait pas l’une de ses obligations contractuelles et déontologiques, l’autre partie pourra à tout moment adresser à la partie défaillante une notification écrite par lettre recommandée avec accusé de réception, avec un préavis de …...… jours avant la date où la résiliation doit prendre effet, en spécifiant la nature du manquement et la manière selon laquelle il y a lieu d’y remédier. Si la partie qui reçoit la notification prend les mesures nécessaires spécifiées dans ladite notification et selon les modalités qui sont fixées, la résiliation ne prend pas effet.</br>
            A défaut, la résiliation prendra effet au terme du préavis fixé au paragraphe ci-dessus.
        </p>
        <p><u>Article 11.3 : résiliation de plein droit</u></p>
        <p>
            Le prononcé d’une sanction disciplinaire tenant dans une interdiction d’exercice égale ou supérieure à trois mois à l’encontre de l’infirmier (ère) remplaçant (e) et/ou de l’infirmier(ère) remplacé(e), entraîne la résiliation de plein droit du présent contrat, sans qu’il soit nécessaire de respecter un quelconque préavis.</br>
            De même, le présent contrat est résilié de plein droit dès lors que l’indisponibilité temporaire de  l’infirmier(ère) remplacé(e), devient définitive.<br>
            En cas d’échec de la conciliation, les litiges soulevés soit par l’exécution, l’interprétation ou la résiliation du présent contrat peuvent être soumis à la juridiction compétente.
        </p>
    </section>
    <section>
        <h3>
            Article 12 –  Transmission à l’Ordre *
        </h3>
        <p>
            Conformément aux dispositions de l’article L.4113-9 du code de la santé publique, ce contrat est communiqué par chacune des parties au Conseil départemental de l’Ordre des infirmiers du Tableau auquel elles sont inscrites dans un délai d’un mois à compter de sa signature.
            Les parties affirment sur l’honneur n’avoir passé aucune contre-lettre ou avenant relatif au présent contrat qui ne soit soumis au Conseil départemental de l’Ordre des infirmiers compétent.
        </p>
    </section>
    <section>
        <h3>Article 13 – Fin du remplacement*</h3>
        <p>
            Au terme du présent contrat, l’infirmier (ère) remplaçant (e) ayant achevé sa mission et assuré la continuité des soins délivrés aux patients de l’infirmier(ère) remplacé(e), il/elle cesse l’ensemble de ses activités de remplacement auprès des patients de ce dernier ,et lui transmet l’ensemble des informations nécessaires à la mise en œuvre de la continuité des soins.
        </p>
    </section>
    <p>
        Fait en trois exemplaires (<i>dont un pour le Conseil départemental de l’Ordre des infirmiers</i>)
        Le {{ date("d-m-Y") }}, à …………….
    </p>
    <p><span style="text-align: left;">Monsieur/Madame …………………. </span> <span style="text-align: right;">Monsieur/Madame ………………….</span></p>
</body>
</html>