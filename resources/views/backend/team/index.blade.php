@inject('model', '\App\Domains\Auth\Models\User')
@extends('backend.layouts.app')

@section('title', __('Active Users'))
@push('before-styles')
    <link href="{{ ('/css/_tagify.css') }}" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

@endpush
@section('content')

    @if(!Auth::user()->ownedTeams->first() && !Auth::user()->team->first())
        <x-backend.card>
            <x-slot name="body">
                <div class="card-body">
                    <div class="row mt-6 justify-content-center">
                        <div class="align-items-center">
                            <h4 class="text-center"><strong> Vous n'avez pas encore aucune équipe . </strong></h4> <br>
                            <h5> Veuillez créer une nouvelle équipe pour pouvoir ajouter des membres et un facturier
                                . </h5>
                        </div>
                    </div>
                    <div class="row mt-6 justify-content-center">
                        <div>
                            <button type="button"
                                    class="card-header-action btn btn-primary btn-sm mt-6"
                                    data-toggle="modal"
                                    data-target="#AddTeam">@lang ("Creér une équipe")
                            </button>
                        </div>
                    </div>
                    <div class="row mt-6 justify-content-center">
                        <div class="col-md-14">
                            <a href="#">
                                <img src="{{ asset("img/Team work-amico.png") }}" height="320" width="320">
                            </a>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="AddTeam" tabindex="-1" role="dialog" aria-labelledby="AddTeam"
                     aria-hidden="true">
                    <div class="modal-dialog" style="max-width: 500px!important;" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="ModalLabel">Créer une équipe :</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form name="add_biller" id="add_biller" method="POST"
                                      action="{{ route('admin.team.storeteam') }}">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <label for="name" class="col-md-6 col-form-label">@lang('Nom : ')</label>
                                        <br>
                                        <div class="col-md-6">
                                            <input type="name" name="name" class="form-control"
                                                   placeholder="{{ __('Nom de léquipe') }}" value="{{ old('name') }}"
                                                   maxlength="255" required/>
                                        </div>
                                    </div><!--form-group-->
                                    <button class="btn btn-sm btn-primary float-right"
                                            type="submit">@lang('Créer une équipe')</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </x-slot>
        </x-backend.card>
    @else
        <x-backend.card>
            <x-slot name="header">
                <span>@lang("Equipe : ") <strong>{{ Auth::user()->team->first()->name }}</strong></span>
                @if($logged_in_user->isOwnerOfTeam($logged_in_user->team->first()->id))
                    @if( $logged_in_user->team->first()->admins->count() > 1)
                        <div class="modal fade" id="Modal5" tabindex="-1" role="dialog" aria-labelledby="Modal5"
                             aria-hidden="true">
                            <div class="modal-dialog" style="max-width: 500!important;" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="ModalLabel">Selectionnez un admin pour
                                            l'équipe {{ Auth::user()->team->first()->name}} </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form name="add_biller" id="add_biller" method="POST"
                                              action="{{ route('admin.team.defineNewOwner',[$logged_in_user->team()->first()->id ] )}}">
                                            {{csrf_field()}}
                                            <div class="form-group">
                                                <label for="user"><h4> Membres : </h4></label>
                                                <select id="user_id" class="form-control" name="user_id"
                                                        data-role="select-dropdown" data-profile="minimal">
                                                    @foreach(Auth::user()->team->first()->admins->except($logged_in_user->id) as $member)
                                                        <option value="{{ $member->id }}"> {{ $member->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <button class="btn btn-sm btn-primary float-right"
                                                    type="submit">@lang('Add')</button>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>
                    @endif
                @endif
            </x-slot>
            <x-slot name="headerActions">
                @if ($logged_in_user->hasAllAccess())
                    <x-utils.link
                        icon="c-icon cil-plus"
                        class="card-header-action btn btn-primary btn-sm"
                        :href="route('admin.auth.user.create')"
                    >
                        {{ __('Create User') }}
                    </x-utils.link>
                @endif
                <button
                    type="button"
                    class="btn btn-danger btn-sm"
                    id="leaveTeam"
                ><i class="fa fa-window-close mr-2"
                    ></i>@lang('Quitter')</button>
            </x-slot>

            <!-- $logged_in_user->team()->first()->users -->
            @if($logged_in_user->isOwnerOfTeam($logged_in_user->team->first()->id) && $logged_in_user->team->first()->pivot->is_admin == true )
                <x-slot name="headerActions">
                    <div class="dropdown show">
                        <a class="btn btn-primary btn-sm dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-plus mr-2"></i> Ajouter
                        </a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            <button type="button"
                                    class="dropdown-item"
                                    data-toggle="modal"
                                    data-target="#Modal1">Facturier
                            </button>
                            <button type="button"
                                    class="dropdown-item"
                                    data-toggle="modal"
                                    data-target="#addMembers">Membres
                            </button>
                        </div>
                        <button
                            type="button"
                            class="btn btn-danger btn-sm"
                            data-toggle="modal" data-target="#Modal5"
                        ><i class="fa fa-window-close mr-2 "></i>@lang('Quitter')</button>
                    </div>
                </x-slot>
            @endif
            <x-slot name="body">
                @if(request()->route()->getName() == 'admin.auth.user.index')
                    <livewire:backend.users-table/>
                @elseif(!$logged_in_user->team->first()->is_admin)
                    <livewire:backend.team-table/>
                @endif
                <div class="modal fade" id="Modal1" tabindex="-1" role="dialog" aria-labelledby="Modal1"
                     aria-hidden="true">
                    <div class="modal-dialog" style="max-width: 500px!important;" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="ModalLabel">@lang('Create biller')</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form name="add_biller" id="add_biller" method="POST"
                                      action="{{ route('admin.team.biller.create',$logged_in_user->team->first()->id ) }}">
                                    {{csrf_field()}}
                                    <div class="form-group row">
                                        <label for="first_name" class="col-md-6 control-label">
                                            @lang('Prénom')<span class="alert-required"
                                                                 aria-hidden="true">*</span></label>
                                        <div class="col-sm-6"><input type="text" name="first_name" class="form-control"
                                                                     placeholder="{{ __('Prénom') }}"
                                                                     value="{{ old('first_name') }}" maxlength="100"
                                                                     required/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="last_name" class="col-md-6 control-label">
                                            @lang('Nom')<span class="alert-required" aria-hidden="true">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="text" name="last_name" class="form-control"
                                                   placeholder="{{ __('Nom') }}"
                                                   value="{{ old('last_name') }}" maxlength="100" required/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="email" class="col-md-6 control-label ">
                                            @lang('E-mail Address')<span class="alert-required"
                                                                         aria-hidden="true">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="email" name="email" class="form-control"
                                                   placeholder="{{ __('E-mail Address') }}"
                                                   value="{{ old('email') }}" maxlength="255"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="password" class="col-md-6 control-label">
                                            @lang('Password') <span class="alert-required"
                                                                    aria-hidden="true">*</span></label>
                                        <div class="col-sm-6">
                                            <input type="password" name="password" id="password" class="form-control"
                                                   placeholder="{{ __('Password') }}" maxlength="100" required
                                                   autocomplete="new-password"/>
                                        </div>
                                    </div>
                                    <button class="btn btn-sm btn-primary float-right"
                                            type="submit">@lang('Add')</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="addMembers" tabindex="-1" role="dialog" aria-labelledby="addMembers"
                     aria-hidden="true">
                    <div class="modal-dialog" style="max-width: 500!important;" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="ModalLabel">Ajouter un où plusieurs membres à l'équipe
                                    :</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form name="add_team_user" id="add_team_user" method="POST"
                                      action="{{ route('admin.team.storeteamuser') }}">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <label for="email"
                                               class="col-md-auto col-form-label">@lang('E-mail Address :')</label>
                                        <br>
                                        <div name="tagif" id="tagify">
                                            <input name='mails'
                                                   class='mails'
                                                   placeholder='ajouter un email'
                                                   value=''>
                                        </div>
                                    </div>
                                    <button class="btn btn-sm btn-primary float-right"
                                            type="submit">@lang('Invité')</button>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </x-slot>
        </x-backend.card>
    @endif
    @push('before-scripts')
        <script src="{{ ('/resources/js/backend/tagify.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tagify/4.3.0/tagify.min.js" crossorigin="anonymous"
                referrerpolicy="no-referrer"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tagify/4.3.0/tagify.min.css"
              crossorigin="anonymous" referrerpolicy="no-referrer"/>
    @endpush
@endsection

@push('after-scripts')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
                    $('#imagePreview').hide();
                    $('#imagePreview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imageUpload").change(function () {
            readURL(this);
            $('<input>', {
                type: 'hidden',
                id: 'avatar_type',
                name: 'avatar_type',
                value: 'storage'
            }).appendTo('#imageUpload');
        });

        // The DOM element you wish to replace with Tagify
        var inputtag = document.querySelector('input[name=basic]');

        // initialize Tagify on the above input node reference
        new Tagify(inputtag)
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#leaveTeam').click(function (){
            Swal.fire({
                title: 'Êtes-vous sûr de quitter cette équipe ?',
                showCancelButton: true,
                confirmButtonText: 'Confirmer',
                cancelButtonText: 'Annuler',
                icon: 'warning'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url:"/admin/team/leaveteam",
                        method:"delete",

                        success:function (result){
                            window.location.reload();
                        },
                        error:function(){
                            Swal.fire({
                                title: 'Un erreur est survenue !',
                                icon: 'error'
                            })
                        }
                    })
                }
            });
        })
    </script>
@endpush
