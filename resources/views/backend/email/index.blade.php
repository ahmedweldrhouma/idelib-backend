@extends('backend.layouts.app')

@section('title', __('Gestion des emails'))

@section('content')
        <div class="row d-flex justify-content-center mt-100">
            <div class="col-md-4 mb-3">
                <div class="main-card  card h-100">
                    <div class="card-header">
                        <i class="fa fa-envelope mr-2 mb-1"> </i>
                        Welcome
                        <label class="switch float-right ">
                            <input type="checkbox" class="switch-status" data-name="{{$generalConfig->where('name','register')->first()->name}}"
                               {{ old('value', $generalConfig->where('name','register')->first()->value ) ? 'checked' : '' }}>
                            <span class="slider"></span>
                        </label>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane show active" id="tab-eg3-0" role="tabpanel">
                                @include('frontend.emails.welcome',['user' => \Illuminate\Support\Facades\Auth::user()])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <div class="main-card  card  h-100">
                    <div class="card-header">
                        <i class="fa fa-envelope mr-2 mb-1"> </i>
                        Quitter une équipe
                        <label class="switch float-right ">
                            <input type="checkbox" class="switch-status" data-name="{{$generalConfig->where('name','leave_team')->first()->name}}"
                                {{ old('value', $generalConfig->where('name','leave_team')->first()->value ) ? 'checked' : '' }}>
                            <span class="slider"></span>
                        </label>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane show active" id="tab-eg3-0" role="tabpanel">
                                @include('frontend.emails.leaveteam',['user' => \Illuminate\Support\Facades\Auth::user()])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <div class="main-card  card  h-100">
                    <div class="card-header">
                        <i class="fa fa-envelope mr-2 mb-1"> </i>
                        Compte supprimer
                        <label class="switch float-right ">
                            <input type="checkbox" class="switch-status" data-name="{{$generalConfig->where('name','delete_account')->first()->name}}"
                                {{ old('value', $generalConfig->where('name','delete_account')->first()->value ) ? 'checked' : '' }}>
                            <span class="slider"></span>
                        </label>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane show active" id="tab-eg3-0" role="tabpanel">
                                @include('frontend.emails.deleteaccount',['user' => \Illuminate\Support\Facades\Auth::user()])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <div class="main-card  card  h-100">
                    <div class="card-header">
                        <i class="fa fa-envelope mr-2 mb-1"> </i>
                        Invitation a équipe
                        <label class="switch float-right ">
                            <input type="checkbox" class="switch-status" data-name="{{$generalConfig->where('name','team_invitation')->first()->name}}"
                                {{ old('value', $generalConfig->where('name','team_invitation')->first()->value ) ? 'checked' : ' ' }}>
                            <span class="slider"></span>
                        </label>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane show active" id="tab-eg3-0" role="tabpanel">
                                @include('frontend.emails.teamInvitation',['user' => \Illuminate\Support\Facades\Auth::user()])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-3">
                <div class="main-card  card  h-100">
                    <div class="card-header">
                        <i class="fa fa-envelope mr-2 mb-1"> </i>
                       Nouveau Application
                        <label class="switch float-right ">
                            <input type="checkbox" class="switch-status" data-name="{{$generalConfig->where('name','new_application')->first()->name}}"
                                {{ old('value', $generalConfig->where('name','new_application')->first()->value ) ? 'checked' : ' ' }}>
                            <span class="slider"></span>
                        </label>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane show active" id="tab-eg3-0" role="tabpanel">
                                @include('frontend.emails.newapplication',['user' => \Illuminate\Support\Facades\Auth::user()])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection
@push('after-scripts')
<script>
    $(function() {
        $('.switch-status').on('change', function() {
            let checked = $(this).is(":checked") ? 1:0;
            $.ajax({
                url: ' {{route('admin.config.edit')}} ',
                type: 'PATCH',
                data: { name:$(this).data("name"), value:checked,_token:$('[name="csrf-token"]').attr('content') },
                success:function (data){
                    if(data.success){
                        Swal.fire({
                            icon: "success",
                            title: `Email ${ checked ? "activé":"désactivé"} avec succées !`
                        });
                    }
                },
                error:function (){
                    Swal.fire({
                        icon: "error",
                        title: `Erreur est survenu lors de ${ checked ? "l'activation":"desactivation"} de l'email!`
                    });
                }
            });
        });
    });

</script>
@endpush

