<footer class="c-footer">
    <div>
        <strong>
            @lang('Copyright') &copy; {{ date('Y') .' '.__(appName())}}
        </strong>

        @lang('All Rights Reserved')
    </div>

</footer>
