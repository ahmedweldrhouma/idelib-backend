<div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show" id="sidebar">
    <div class="c-sidebar-brand d-lg-down-none">
        <img src="{{ url('/img/logo-idelib-seul.png') }}" alt="" width="80">
    </div><!--c-sidebar-brand-->

    <ul class="c-sidebar-nav justify-content-center">
        <li class="c-sidebar-nav-item">
            <x-utils.link
                class="c-sidebar-nav-link"
                :href="route('admin.dashboard')"
                :active="activeClass(Route::is('admin.dashboard'), 'c-active')"
                icon="c-sidebar-nav-icon cil-speedometer"
                :text="__('Dashboard')"/>
        </li>

        @if (
            $logged_in_user->hasAllAccess() ||
            (
                $logged_in_user->can('admin.access.user.list') ||
                $logged_in_user->can('admin.access.user.deactivate') ||
                $logged_in_user->can('admin.access.user.reactivate') ||
                $logged_in_user->can('admin.access.user.clear-session') ||
                $logged_in_user->can('admin.access.user.impersonate') ||
                $logged_in_user->can('admin.access.user.change-password')
            )
        )
            <li class="c-sidebar-nav-dropdown {{ activeClass(Route::is('admin.auth.user.*') || Route::is('admin.auth.role.*'), 'c-open c-show') }}">

            @if (
                $logged_in_user->hasAllAccess() ||
                (
                    $logged_in_user->can('admin.access.user.list') ||
                    $logged_in_user->can('admin.access.user.deactivate') ||
                    $logged_in_user->can('admin.access.user.reactivate') ||
                    $logged_in_user->can('admin.access.user.clear-session') ||
                    $logged_in_user->can('admin.access.user.impersonate') ||
                    $logged_in_user->can('admin.access.user.change-password')
                )
            )
                <li class="c-sidebar-nav-item c-sidebar-nav-dropdown dropright">
                    <x-utils.link
                        data-toggle="dropdown"
                        icon="c-sidebar-nav-icon cil-user"
                        href="#"
                        class="c-sidebar-nav-dropdown-toggle"
                        :text="__('Users')"
                    />
                    <div class="dropdown-menu dropdown-menu-right p-4">
                        <h6 class="mb-3"><strong>{{__('Users')}}</strong></h6>
                        <x-utils.link
                            :href="route('admin.auth.user.index')"
                            class="dropdown-item"
                            :text="__('Active Users')"
                            :active="activeClass(Route::is('admin.auth.user.index'), 'c-active')"
                        />
                        <x-utils.link
                            :href="route('admin.auth.user.deactivated')"
                            class="dropdown-item"
                            :text="__('Deactivated Users')"
                            :active="activeClass(Route::is('admin.auth.user.deactivated'), 'c-active')"
                        />
                        <x-utils.link
                            :href="route('admin.auth.user.deleted')"
                            class="c-sidebar-nav-link"
                            class="dropdown-item"
                            :text="__('Deleted Users')"
                            :active="activeClass(Route::is('admin.auth.user.deleted'), 'c-active')"
                        />
                    </div>
                </li>
            @endif
            @if ($logged_in_user->hasAllAccess())
                <li class="c-sidebar-nav-item">
                    <x-utils.link
                        icon="c-sidebar-nav-icon cil-money"
                        :href="route('admin.plan.index')"
                        class="c-sidebar-nav-link"
                        :text="__('Subscriptions')"
                        :active="activeClass(Route::is('admin.plan.*'), 'c-active')"/>
                </li>
                <li class="c-sidebar-nav-item">
                    <x-utils.link
                        icon="c-sidebar-nav-icon cil-gift"
                        :href="route('admin.admin_subscription.index')"
                        class="c-sidebar-nav-link"
                        :text="__('Admin Subscriptions')"
                        :active="activeClass(Route::is('admin.admin_subscription.*'), 'c-active')"/>
                </li>
                <li class="c-sidebar-nav-item">
                    <x-utils.link
                        icon="c-sidebar-nav-icon cil-mobile"
                        :href="route('admin.subscription.index')"
                        class="c-sidebar-nav-link"
                        :text="__('Mobile subscription')"
                        :active="activeClass(Route::is('admin.subscription.*'), 'c-active')"/>
                </li>
                <li class="c-sidebar-nav-item">
                    <x-utils.link
                        icon="c-sidebar-nav-icon fa fa-envelope"
                        :href="route('admin.config.index')"
                        class="c-sidebar-nav-link"
                        :text="__('E-mail Configuration')"
                        />
                </li>
                <!--
                    <li class="c-sidebar-nav-item">
                        <x-utils.link
                            icon="c-sidebar-nav-icon cil-people"
                            :href="route('admin.patient.index')"
                            class="c-sidebar-nav-link"
                            :text="__('Gestion de la patientèle')"
                            :active="activeClass(Route::is('admin.patient.*'), 'c-active')" />
                    </li>-->
                @endif
                </li>
            @endif

            @if ($logged_in_user->isAdmin() || $logged_in_user->isNurse() || $logged_in_user->isSubstituteNurse() || $logged_in_user->isBiller())
                {{-- <li class="c-sidebar-nav-item">
                    <x-utils.link
                        icon="c-sidebar-nav-icon cil-people"
                        :href="route('admin.treatment.index')"
                        class="c-sidebar-nav-link"
                        :text="__('Gestion de la patientèle')"
                        :active="activeClass(Route::is('admin.treatment.*'), 'c-active')" />
                </li> --}}

                @php
                    $type = isset(Route::current()->parameters['type']) ? Route::current()->parameters['type'] : '';
                @endphp
                <li class="c-sidebar-nav-item c-sidebar-nav-dropdown dropright">
                    <x-utils.link
                        data-toggle="dropdown"
                        href="#"
                        class="c-sidebar-nav-link"
                        icon="c-sidebar-nav-icon cil-people"
                        :active="activeClass(Route::is('admin.patient.*') && $type!='covid', 'c-active')"
                        :text="__('Patients')"/>

                    <div class="dropdown-menu dropdown-menu-right p-4">
                        <h6 class="mb-3"><strong>{{__('Patients')}}</strong></h6>
                        <x-utils.link
                            :href="route('admin.patient.indexByType', ['type' => 'chronic'])"
                            class="dropdown-item"
                            :text="__('Patients chroniques')"
                            :active="activeClass($type == 'chronic', 'c-active')"/>
                        <x-utils.link
                            :href="route('admin.patient.indexByType', ['type' => 'punctual'])"
                            class="dropdown-item"
                            :text="__('Patients ponctuels')"
                            :active="activeClass($type == 'punctual', 'c-active')"/>
                    </div>
                </li>
                <li class="c-sidebar-nav-item">
                    <x-utils.link
                        :href="route('admin.patient.indexByType',['type' => 'covid'])"
                        icon="c-sidebar-nav-icon cil-people"
                        class="c-sidebar-nav-link"
                        :active="activeClass(Route::is('admin.patient.*') && $type=='covid', 'c-active')"
                        :text="__('Patients covid')"/>

<!--                    <div class="dropdown-menu dropdown-menu-right p-4">
                        <h6 class="mb-3"><strong>{{__('Patients covid')}}</strong></h6>
                        <x-utils.link
                            :href="route('admin.treatments.covid.covid_treatments', 'pcr')"
                            class="dropdown-item"
                            :text="__('Tests PCR')"
                            :active="activeClass($type == 'pcr', 'c-active')"/>
                        <x-utils.link
                            :href="route('admin.treatments.covid.covid_treatments', 'antigen')"
                            class="dropdown-item"
                            :text="__('Tests antigéniques')"
                            :active="activeClass($type == 'antigen', 'c-active')"/>

                        <x-utils.link
                            :href="route('admin.treatments.covid.covid_treatments','visite')"
                            class="dropdown-item"
                            :text="__('Visites à domicile')"
                            :active="activeClass($type == 'visit', 'c-active')"/>

                        <x-utils.link
                            :href="route('admin.treatments.covid.vaccines.index')"
                            class="dropdown-item"
                            :text="__('Vaccins')"
                            :active="activeClass(Route::is('admin.treatments.covid.vaccines.index'), 'c-active')"/>

                        <x-utils.link
                            :href="route('admin.treatments.covid.treatments')"
                            class="dropdown-item"
                            :text="__('Suivi patient Covid')"
                            :active="activeClass($type == 'covid', 'c-active')"/>
                    </div>-->
                </li>
                <li class="c-sidebar-nav-item">
                    <x-utils.link
                        icon="c-sidebar-nav-icon cil-user"
                        :href="route('admin.team.index')"
                        class="c-sidebar-nav-link"
                        :text="__('Equipes')"
                        :active="activeClass(Route::is('admin.team.index'), 'c-active')"/>
                </li>
                <li class="c-sidebar-nav-item c-sidebar-nav-dropdown dropright">
                    <x-utils.link
                        href="#"
                        data-toggle="dropdown"
                        icon="c-sidebar-nav-icon cil-calendar"
                        :active="activeClass(Route::is('admin.calendar.*'), 'c-active')"
                        class="c-sidebar-nav-dropdown-toggle"
                        :text="__('Calendrier')"/>
                    <div class="dropdown-menu dropdown-menu-right p-4">
                        <h6 class="mb-3"><strong>{{__('Calendrier')}}</strong></h6>
                        <x-utils.link
                            :href="route('admin.calendar.getAllByType', 'tour')"
                            class="dropdown-item"
                            :text="__('La tournée')"
                            :active="activeClass(request()->type == 'tour', 'c-active')"/>
                        <x-utils.link
                            :href="route('admin.calendar.getAllByType', 'planning')"
                            class="dropdown-item"
                            :text="__('Le Planning')"
                            :active="activeClass(request()->type == 'planning', 'c-active')"/>
                    </div>
                </li>
                <li class="c-sidebar-nav-item  ">


                    <x-utils.link
                        :href="route('admin.invoices.getAll')"

                        icon="c-sidebar-nav-icon cil-file"
                        :active="activeClass(Route::is('admin.invoices.*'), 'c-active')"
                        class="c-sidebar-nav-link"
                        :text="__('Factures')"/>
<!--                    <div class="dropdown-menu dropdown-menu-right p-4">
                        <h6 class="mb-3"><strong>{{__('Calendrier')}}</strong></h6>
                        <x-utils.link
                            :href="route('admin.calendar.getAllByType', 'tour')"
                            class="dropdown-item"
                            :text="__('La tournée')"
                            :active="activeClass(request()->type == 'tour', 'c-active')"/>
                        <x-utils.link
                            :href="route('admin.calendar.getAllByType', 'planning')"
                            class="dropdown-item"
                            :text="__('Le Planning')"
                            :active="activeClass(request()->type == 'planning', 'c-active')"/>
                    </div>-->
                </li>

            @endif
    </ul>
    <!--
        <button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent" data-class="c-sidebar-minimized"></button>
    -->
</div><!--sidebar-->
