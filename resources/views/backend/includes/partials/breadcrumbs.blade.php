@if (Breadcrumbs::has())
    <div class="d-md-flex align-items-center">
        <h5 class="m-0 mb-sm-0 mr-sm-2 mb-2"><strong>{{ Breadcrumbs::current()[count(Breadcrumbs::current())-1]->title() }}</strong></h5>
        <ol class="breadcrumb border-0 m-0 p-0">
            @if(count(Breadcrumbs::current()) > 1)
                @foreach (Breadcrumbs::current() as $crumb)
                    @if ($crumb->url() && !$loop->last)
                        <li class="breadcrumb-item">
                            <x-utils.link :href="$crumb->url()" >
                                <x-slot name="slot">
                                    {{ $crumb->title() }}
                                </x-slot>
                            </x-utils.link>
                        </li>
                    @else
                        <li class="breadcrumb-item active">
                            {{ $crumb->title() }}
                        </li>
                    @endif
                @endforeach
            @endif

        </ol>
    </div>
@endif
