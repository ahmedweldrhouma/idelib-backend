<header class="c-header c-header-light c-header-fixed">
    <button class="c-header-toggler c-class-toggler d-lg-none mfe-auto" type="button" data-target="#sidebar" data-class="c-sidebar-show">
        <i class="c-icon c-icon-lg cil-menu"></i>
    </button>

    <a class="c-header-brand d-lg-none" href="#">

        <img src="{{ url('/img/logo-idelib.png') }}" alt="" width="118">
        {{--  <svg width="118" height="46" alt="CoreUI Logo">
            <use xlink:href="{{ asset('img/brand/coreui.svg#full') }}"></use>
        </svg>  --}}
    </a>

    <button class="c-header-toggler c-class-toggler mfs-3 d-md-down-none" type="button" data-target="#sidebar" data-class="c-sidebar-lg-show" responsive="true">
        <i class="c-icon c-icon-lg cil-menu"></i>
    </button>

    <ul class="c-header-nav title d-md-down-none">
        @if(config('boilerplate.locale.status') && count(config('boilerplate.locale.languages')) > 1)
            <li class="c-header-nav-item dropdown">
                <x-utils.link
                    :text="__(getLocaleName(app()->getLocale()))"
                    class="c-header-nav-link dropdown-toggle"
                    id="navbarDropdownLanguageLink"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false" />

                @include('includes.partials.lang')
            </li>
        @endif
    </ul>

    <ul class="c-header-nav ml-auto mr-4">
        <li class="c-header-nav-item dropdown">
            <x-utils.link class="c-header-nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <x-slot name="slot">
                    <span>{{ ucwords(strtolower($logged_in_user->getFullName())) }}</span>

                    <img class="ml-2 u-avatar" style="" src="{{ $logged_in_user->avatar }}" alt="{{ $logged_in_user->email ?? '' }}">

                </x-slot>
            </x-utils.link>

            <div class="dropdown-menu dropdown-menu-right pt-0">

                 <x-utils.link
                    class="dropdown-item"
                    icon="c-icon mr-2 cil-user"
                    :href="route('frontend.user.account')">
                    <x-slot name="text">
                        @lang('Account')

                    </x-slot>
                </x-utils.link>
                <x-utils.link
                    class="dropdown-item"
                    icon="c-icon mr-2 cil-account-logout"
                    onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    <x-slot name="text">
                        @lang('Logout')
                        <x-forms.post :action="route('frontend.auth.logout')" id="logout-form" class="d-none" />
                    </x-slot>
                </x-utils.link>
            </div>
        </li>
    </ul>
</header>
<div class="c-subheader justify-content-between p-3">
    @include('backend.includes.partials.breadcrumbs')
    {{--        <div class="c-subheader-nav mfe-2">
                @yield('breadcrumb-links')
            </div>--}}
</div>
