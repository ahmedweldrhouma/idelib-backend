@extends('backend.layouts.app')

@section('title', __('Gestion de la patientèle'))

@section('content')
    <x-backend.card>
        <x-slot name="header">
            @lang('Gestion de la patientèle covids') / Vaccins
        </x-slot>

        @if ($logged_in_user->isAdmin())
        <x-slot name="headerActions">
            <x-utils.link
                icon="c-icon cil-plus"
                class="btn btn-primary btn-sm"
                :href="route('admin.patient.create')"
            >
                <x-slot name="slot">
                    {{__('Créer un patient')}}
                </x-slot>
            </x-utils.link>
        </x-slot>
        @endif
        

        <x-slot name="body">
            <livewire:backend.vaccines-table/>
        </x-slot>
    </x-backend.card>
@endsection
