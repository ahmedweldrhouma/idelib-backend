@if ($logged_in_user->isAdmin() || $logged_in_user->isNurse() || $logged_in_user->isSubstituteNurse())
    {{-- <x-utils.edit-button :href="route('admin.patient.edit', $patient)" /> --}}
    <x-utils.delete-button :href="route('admin.treatments.destroy', $covidTreatment)" />
@endif
