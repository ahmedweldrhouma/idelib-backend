@if ($logged_in_user->isSuperAdmin())
    <x-utils.edit-button :href="route('admin.subscription.edit', $subscription)" />
    <x-utils.delete-button :href="route('admin.subscription.destroy', $subscription)" />

@endif
