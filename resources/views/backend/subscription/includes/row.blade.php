<x-livewire-tables::bs4.table.cell>
    {{ $row->id }}
</x-livewire-tables::bs4.table.cell>

<x-livewire-tables::bs4.table.cell>
    {{ $row->user()->withTrashed()->first()->getFullName() }}
</x-livewire-tables::bs4.table.cell>

<x-livewire-tables::bs4.table.cell>
    {{ $row->operating_system }}
</x-livewire-tables::bs4.table.cell>

<x-livewire-tables::bs4.table.cell>
    @if(!empty($row->original_transaction_id))
        {{ $row->original_transaction_id }}
    @else
        {{ $row->stripe_id }}
    @endif
</x-livewire-tables::bs4.table.cell>

<x-livewire-tables::bs4.table.cell>
    {{ $row->transaction_id }}
</x-livewire-tables::bs4.table.cell>

<x-livewire-tables::bs4.table.cell>
    {{ $row->plan->name }}
</x-livewire-tables::bs4.table.cell>


 <x-livewire-tables::bs4.table.cell>
    @include('backend.subscription.includes.actions', ['subscription' => $row])
</x-livewire-tables::bs4.table.cell>
