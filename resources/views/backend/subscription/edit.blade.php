@inject('model', '\App\Domains\MobileSubscription\Models\MobileSubscription')

@extends('backend.layouts.app')

@section('title', __('Update Mobile Subscription'))
<style>
    .card-footer {
        margin-top: -80px;
    }
</style>
@section('content')
    <x-backend.card>
        <x-slot name="header">
            @lang('See Subscription')
        </x-slot>

        <x-slot name="headerActions">
            <x-utils.link class="card-header-action btn btn-light btn-sm" :href="route('admin.auth.user.index')">
                <x-slot name="slot">
                    {{ __('Cancel') }}
                </x-slot>
            </x-utils.link>
        </x-slot>

        <x-slot name="body">
            <div class="container mt-3">
                <div class="row jumbotron box8">
                    <div class="col-sm-12 mx-t3 mb-4 " style="margin-top: -4rem; ">
                        Liste des factures disponibles
                    </div>
                    @if (!$user->isMasterAdmin())
                        <div class="col-sm-6 form-group">
                            <label for="name">@lang('Type')</label>
                            <select name="type" class="form-control" required
                                    x-on:change="userType = $event.target.value">
                                @if ($user->type === $model::TYPE_BILLER)
                                    <option
                                        value="{{ $model::TYPE_BILLER }}" {{ $user->type === $model::TYPE_BILLER ? 'selected' : '' }}>@lang('Biller')</option>
                                @else
                                    <option
                                        value="{{ $model::TYPE_ADMIN }}" {{ $user->type === $model::TYPE_ADMIN ? 'selected' : '' }}>@lang('admin')</option>
                                    <option
                                        value="{{ $model::TYPE_SUPER_ADMIN }}" {{ $user->type === $model::TYPE_SUPER_ADMIN ? 'selected' : '' }}>@lang('Administrator')</option>
                                    <option
                                        value="{{ $model::TYPE_PATIENT }}" {{ $user->type === $model::TYPE_PATIENT ? 'selected' : '' }}>@lang('Patient')</option>
                                    <option
                                        value="{{ $model::TYPE_NURSE }}" {{ $user->type === $model::TYPE_NURSE ? 'selected' : '' }}>@lang('Nurse')</option>
                                    <option
                                        value="{{ $model::TYPE_SUBSTITUTE_NURSE }}" {{ $user->type === $model::TYPE_SUBSTITUTE_NURSE ? 'selected' : '' }}>@lang('Substitute nurse')</option>
                                    <option
                                        value="{{ $model::TYPE_BILLER }}" {{ $user->type === $model::TYPE_BILLER ? 'selected' : '' }}>@lang('Biller')</option>
                                @endif
                            </select>
                        </div><!--form-group-->
                    @endif


                </div>
            </div>
            {{-- @if (!$user->isMasterAdmin())
                 @include('backend.auth.includes.roles')

                 @if (!config('boilerplate.access.user.only_roles'))
                     @include('backend.auth.includes.permissions')
                 @endif
             @endif --}}
        </x-slot>
        <x-slot name="footer">
            <button class="btn btn-sm btn-primary float-right" type="submit">@lang('Update User')</button>
        </x-slot>
    </x-backend.card>
@endsection
@push('after-scripts')

@endpush
