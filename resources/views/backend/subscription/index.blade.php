@extends('backend.layouts.app')

@section('title', __('Mobile subscription'))

@section('content')
    <x-backend.card>
        <x-slot name="header">
            @lang('Mobile subscription')
        </x-slot>
        
        <x-slot name="body">
            <livewire:backend.subscriptions-table />
        </x-slot>
    </x-backend.card>
@endsection
