@inject('model', '\App\Domains\Plan\Models\Plan')

@extends('backend.layouts.app')

@section('title', __('Modifier abonnement'))

@section('content')
    <x-forms.patch :action="route('admin.plan.update', $plan)" >
        <x-backend.card>
            <x-slot name="header">
                @lang('Modifier abonnement')
            </x-slot>

            <x-slot name="headerActions">
                <x-utils.link class="card-header-action btn btn-light btn-sm" :href="route('admin.plan.index')">
                    <x-slot name="slot">
                        {{ __('Cancel') }}
                    </x-slot>
                </x-utils.link>
            </x-slot>

            <x-slot name="body">
                <div>
                    <div class="form-group row">
                        <label for="name" class="col-md-2 col-form-label">@lang('Name')</label>

                        <div class="col-md-10">
                            <input type="text" name="name" class="form-control" placeholder="{{ __('Name') }}" value="{{ old('name') ?? $plan->name }}" maxlength="100" required />
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="display_price" class="col-md-2 col-form-label">@lang('Prix')</label>

                        <div class="col-md-10">
                            <input type="number" name="display_price" step="any" class="form-control" placeholder="{{ __('Prix') }}" value="{{ old('display_price') ?? $plan->display_price }}" maxlength="100" required />
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="product_name" class="col-md-2 col-form-label">@lang('Product name')</label>

                        <div class="col-md-10">
                            <input type="text" name="product_name" class="form-control" placeholder="{{ __('Product name') }}" value="{{ old('product_name') ?? $plan->product_name }}" maxlength="100" />
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="apple_id" class="col-md-2 col-form-label">@lang('Apple ID')</label>

                        <div class="col-md-10">
                            <input type="text" name="apple_id" class="form-control" placeholder="{{ __('Apple ID') }}" value="{{ old('apple_id') ?? $plan->apple_id }}" maxlength="100" required />
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="google_id" class="col-md-2 col-form-label">@lang('Google ID')</label>

                        <div class="col-md-10">
                            <input type="text" name="google_id" class="form-control" placeholder="{{ __('Google ID') }}" value="{{ old('google_id') ?? $plan->google_id }}" maxlength="100" required />
                        </div>
                    </div>

                </div>
            </x-slot>

            <x-slot name="footer">
                <button class="btn btn-sm btn-primary float-right" type="submit">@lang('Mettre à jour l\'abonnement')</button>
            </x-slot>
        </x-backend.card>
    </x-forms.patch>
@endsection
