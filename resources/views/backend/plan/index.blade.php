@extends('backend.layouts.app')

@section('title', __('Gestion des abonnements'))

@section('content')
<!-- @if ($logged_in_user->hasAllAccess())
            <x-slot name="headerActions">
                <x-utils.link
                    icon="c-icon cil-plus"
                    class="card-header-action btn btn-primary btn-sm"
                    :href="route('admin.plan.create')"
                >
                Créer un abonnement
                                </x-utils.link>

            </x-slot>
        @endif
        -->
    <x-backend.card>
        <x-slot name="header">
            @lang('Gestion des abonnements')
        </x-slot>
       
        <x-slot name="body">
            <livewire:backend.plans-table />
        </x-slot>
    </x-backend.card>
@endsection
