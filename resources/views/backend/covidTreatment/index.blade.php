@extends('backend.layouts.app')

@section('title', __('Gestion de la patientèle'))

@section('content')
    <x-backend.card>
        <x-slot name="header">
            @lang('Gestion de la patientèle covid') / @if($type === 'visite') Visites à domicile @else Tests @lang($type) @endif
        </x-slot>


            <x-slot name="headerActions">
                <x-utils.link
                    icon="c-icon cil-plus"
                    class="btn btn-primary btn-sm"
                    :href="route('admin.patient.create', ['type' => $type])"
                >
                    <x-slot name="slot">
                        {{__('Créer un patient')}}
                    </x-slot>
                </x-utils.link>
            </x-slot>


        <x-slot name="body">
            <livewire:backend.covid-treatments-table treatmentType="{{ $type }}"/>
        </x-slot>
    </x-backend.card>
@endsection
