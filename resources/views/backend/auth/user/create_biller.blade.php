@inject('model', '\App\Domains\Auth\Models\User')

@extends('backend.layouts.app')

@section('title', __('Create User'))

@section('content')
    <x-forms.post :action="route('admin.auth.user.store')" :enctype="__('multipart/form-data')">
        <x-backend.card>
            <x-slot name="header">
                @lang('Create User')
            </x-slot>

            <x-slot name="headerActions">
                <x-utils.link class="card-header-action btn btn-light btn-sm" :href="route('admin.auth.user.index')">
                    <x-slot name="slot">
                        {{ __('Cancel') }}
                    </x-slot>
                </x-utils.link>
            </x-slot>
            @php
                //$type = isset($_GET['type']) ? $_GET['type'] : '';
                //dd($type);
            @endphp
            <x-slot name="body">
                <div x-data="{userType : '{{ $model::TYPE_ADMIN }}'}">
                    <div class="form-group row">
                        <label for="name" class="col-md-2 col-form-label">@lang('Type')</label>

                        <div class="col-md-10">
                            <select name="type" class="form-control" required x-on:change="userType = $event.target.value">
                                <option value="{{ $model::TYPE_BILLER }}" selected>@lang('Facturier')</option>
                            </select>
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="avatar" class="col-md-2 col-form-label">@lang('Avatar')</label>

                        <div class="col-md-10">
                            <div class="avatar-upload">
                                <div class="avatar-edit">
                                    <input type='file' name="avatar_location" id="imageUpload" accept=".png, .jpg, .jpeg" />
                                    {{-- <input type="hidden" id="avatar_type" name="avatar_type" value="storage"> --}}
                                    <label for="imageUpload">
                                        <i class="cil-pencil icon"></i>
                                    </label>
                                </div>
                                <div class="avatar-preview">
                                    <div id="imagePreview" style="background-image: url({{ asset('img/default-avatar.png') }});">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="first_name" class="col-md-2 col-form-label">
                            <span class="alert-required" aria-hidden="true">*</span>@lang('Prénom')</label>

                        <div class="col-md-10">
                            <input type="text" name="first_name" class="form-control" placeholder="{{ __('Prénom') }}" value="{{ old('first_name') }}" maxlength="100" required />
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="last_name" class="col-md-2 col-form-label">
                            <span class="alert-required" aria-hidden="true">*</span>@lang('Nom')</label>

                        <div class="col-md-10">
                            <input type="text" name="last_name" class="form-control" placeholder="{{ __('Nom') }}" value="{{ old('last_name') }}" maxlength="100" required />
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="email" class="col-md-2 col-form-label">
                            <span class="alert-required" aria-hidden="true">*</span>@lang('E-mail Address')</label>

                        <div class="col-md-10">
                            <input type="email" name="email" class="form-control" placeholder="{{ __('E-mail Address') }}" value="{{ old('email') }}" maxlength="255" required />
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="password" class="col-md-2 col-form-label">
                            <span class="alert-required" aria-hidden="true">*</span>@lang('Password')</label>

                        <div class="col-md-10">
                            <input type="password" name="password" id="password" class="form-control" placeholder="{{ __('Password') }}" maxlength="100" required autocomplete="new-password" />
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="password_confirmation" class="col-md-2 col-form-label">
                            <span class="alert-required" aria-hidden="true">*</span>@lang('Password Confirmation')</label>

                        <div class="col-md-10">
                            <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="{{ __('Password Confirmation') }}" maxlength="100" required autocomplete="new-password" />
                        </div>
                    </div><!--form-group-->

                    <div class="form-group row d-none">
                        <label for="active" class="col-md-2 col-form-label">@lang('Active')</label>

                        <div class="col-md-10">
                            <div class="form-check">
                                <input name="active" id="active" class="form-check-input" type="checkbox" value="1" {{ old('active', true) ? 'checked' : '' }} />
                            </div><!--form-check-->
                        </div>
                    </div><!--form-group-->
                </div>
            </x-slot>

            <x-slot name="footer">
                <button class="btn btn-sm btn-primary float-right" type="submit">@lang('Create User')</button>
            </x-slot>
        </x-backend.card>
    </x-forms.post>
@endsection

@push('after-scripts')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#imagePreview').css('background-image', 'url('+e.target.result +')');
                    $('#imagePreview').hide();
                    $('#imagePreview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#imageUpload").change(function() {
            readURL(this);
            $('<input>', {
                type: 'hidden',
                id: 'avatar_type',
                name: 'avatar_type',
                value: 'storage'
            }).appendTo('#imageUpload');
        });
    </script>
@endpush
