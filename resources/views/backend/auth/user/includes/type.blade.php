@if ($user->isSuperAdmin())
    Super Admin
@elseif ($user->isAdmin())
    Admin
@elseif ($user->isNurse())
    Infirmier
@elseif ($user->isSubstituteNurse())
    Remplaçant
@elseif ($user->isPatient())
    Patient
@elseif ($user->isBiller())
    Facturier
@else
    @lang('N/A')
@endif
