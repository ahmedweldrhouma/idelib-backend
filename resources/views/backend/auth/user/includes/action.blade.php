@if( $user->team->first() && Auth::user()->team->first()->pivot->is_admin == true)
    @if($user->id != Auth::id() && $user->team->first()->pivot->is_admin == false)
    <x-utils.delete-button
        :href="route('admin.team.removeteamuser', [$user->team()->first()->id ,$user->id])"
        :text="__('Remove')"/>
    @endif
    @if($user->team->first()->pivot->is_admin == false && !$user->isBiller())
    <x-utils.form-button :action="route('admin.team.makeadmin', $user)"
                         method="get"
                         button-class="btn btn-info btn-sm"
                         name="confirm-item">
        @lang('Rendre admin')
    </x-utils.form-button>
    @endif
@endif
