@if ($user->adminSubscription || $user->activeSubscription)
    <span class="badge badge-success" data-toggle="tooltip" >@lang('Yes')</span>
@else
    <span class="badge badge-danger">@lang('No')</span>
@endif
