<x-livewire-tables::bs4.table.cell>
    @include('backend.auth.user.includes.type', ['user' => $row])
</x-livewire-tables::bs4.table.cell>


<x-livewire-tables::bs4.table.cell>
    {{ $row->name }}
</x-livewire-tables::bs4.table.cell>

<x-livewire-tables::bs4.table.cell>
    @if($row->team->contains('admin_id',$row->id))
       {{ __('Owner') }}
    @elseif($row->teams->contains('pivot.is_admin',true))
        {{'Admin'}}
    @else
        {{__('Member')}}
    @endif
</x-livewire-tables::bs4.table.cell>

<x-livewire-tables::bs4.table.cell>
    <a href="mailto:{{ $row->email }}">{{ $row->email }}</a>
</x-livewire-tables::bs4.table.cell>

<x-livewire-tables::bs4.table.cell>
    {{ date("d-m-Y",strtotime($row->created_at)) }}
</x-livewire-tables::bs4.table.cell>

<x-livewire-tables::bs4.table.cell>
@include('backend.auth.user.includes.action', ['user' => $row])
</x-livewire-tables::bs4.table.cell>
