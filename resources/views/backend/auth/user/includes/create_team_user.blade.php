@inject('model', '\App\Domains\Auth\Models\User')

@extends('backend.layouts.app')

@section('title', __('Create A Member'))

@section('content')
    <x-forms.post :action="route('admin.auth.user.storeteamuser')" :enctype="__('multipart/form-data')">
        <x-backend.card>
            <x-slot name="header">
                @lang('Ajouter un membre')
            </x-slot>

            <x-slot name="headerActions">
                <x-utils.link class="card-header-action btn btn-light btn-sm" :href="route('admin.auth.user.index')">
                    <x-slot name="slot">
                        {{ __('Cancel') }}
                    </x-slot>
                </x-utils.link>
            </x-slot>
            <x-slot name="body">
                    <div class="form-group row">
                        <label for="name" class="col-md-2 col-form-label">@lang('Choisir un utilisateur')</label>

                        <select class="col-6 form-control select @error('users') is-invalid @enderror"
                                name="users"
                                id="users">
                            <option selected value="all">Choisir un utilisateur</option>
                            @foreach($users as $user)
                                <option value="{{$user->id}}"> {{$user->first_name}}</option>
                            @endforeach
                        </select>
                    </div><!--form-group-->
            </x-slot>

            <x-slot name="footer">
                <button class="btn btn-sm btn-primary float-right" type="submit">@lang('Ajouter le membre')</button>
            </x-slot>
        </x-backend.card>
    </x-forms.post>
@endsection
