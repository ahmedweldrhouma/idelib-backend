<x-livewire-tables::bs4.table.cell>
    {{ $row->id }}
</x-livewire-tables::bs4.table.cell>
<x-livewire-tables::bs4.table.cell>
    {{ $row->rc_id }}
</x-livewire-tables::bs4.table.cell>
<x-livewire-tables::bs4.table.cell>
    <a href="mailto:{{ $row->email }}">{{ $row->email }}</a>
</x-livewire-tables::bs4.table.cell>

<x-livewire-tables::bs4.table.cell>
    {{ $row->name }}
</x-livewire-tables::bs4.table.cell>

<x-livewire-tables::bs4.table.cell>
    @include('backend.auth.user.includes.type', ['user' => $row])
</x-livewire-tables::bs4.table.cell>

<x-livewire-tables::bs4.table.cell>
    @include('backend.auth.user.includes.subscription', ['user' => $row])
</x-livewire-tables::bs4.table.cell>

<x-livewire-tables::bs4.table.cell>
     @include('backend.auth.user.includes.active', ['user' => $row])
</x-livewire-tables::bs4.table.cell>

<x-livewire-tables::bs4.table.cell>
    {{ $row->phone }}
</x-livewire-tables::bs4.table.cell>

<x-livewire-tables::bs4.table.cell>
    {{ date("d-m-Y",strtotime($row->created_at)) }}
</x-livewire-tables::bs4.table.cell>

<x-livewire-tables::bs4.table.cell>
    {{-- //TODO: change it with Souscription via iap yes/no --}}
    --
</x-livewire-tables::bs4.table.cell>

<x-livewire-tables::bs4.table.cell>
    {{-- //TODO: Admin subscription yes/no --}}
    @if($row->adminSubscription)
        <span class="badge badge-success" data-toggle="tooltip" >@lang('Yes')</span>
    @else
        <span class="badge badge-danger">@lang('No')</span>
    @endif

</x-livewire-tables::bs4.table.cell>

<x-livewire-tables::bs4.table.cell>
    {{-- //TODO: subscription date --}}
    @if ($row->activeSubscription)
        {{$row->activeSubscription->subscribed_at->format('d-m-Y')}}
    @elseif($row->adminSubscription)
        {{$row->adminSubscription->subscribed_at->format('d-m-Y')}}
    @endif

</x-livewire-tables::bs4.table.cell>

<x-livewire-tables::bs4.table.cell>
    {{-- //TODO: subscription expire date --}}
    @if ($row->activeSubscription)
        {{$row->activeSubscription->expired_at?$row->activeSubscription->expired_at->format('d-m-Y'):""}}
    @elseif($row->adminSubscription)
        {{$row->adminSubscription->expires_at?$row->adminSubscription->expires_at->format('d-m-Y'):""}}
    @endif
</x-livewire-tables::bs4.table.cell>

<x-livewire-tables::bs4.table.cell>
    {{-- //TODO: Team name --}}
    @if($row->team->first())
        {{$row->team->first()->name}}
    @else
        <span class="badge badge-danger">@lang('No')</span>
    @endif

</x-livewire-tables::bs4.table.cell>

<x-livewire-tables::bs4.table.cell>
    {{-- //TODO: Team members Ids --}}
    @if($row->nurse)
        {{$row->nurse->getFullName()}}
    @else
    --
    @endif
</x-livewire-tables::bs4.table.cell>

{{-- <x-livewire-tables::bs4.table.cell class="sticky-col"> --}}
<x-livewire-tables::bs4.table.cell class="">
    @include('backend.auth.user.includes.actions', ['user' => $row])
</x-livewire-tables::bs4.table.cell>
