@extends('backend.layouts.app')

@section('title', __('Deleted Users'))

@section('content')
    <x-backend.card>
        <x-slot name="header">
            @lang('Deleted Users')
        </x-slot>

        <x-slot name="body">
            <livewire:backend.users-table status="deleted" />
        </x-slot>
    </x-backend.card>
@endsection
