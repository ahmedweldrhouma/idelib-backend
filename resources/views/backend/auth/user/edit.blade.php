@inject('model', '\App\Domains\Auth\Models\User')

@extends('backend.layouts.app')

@section('title', __('Update User'))
<style>
    .card-footer{
        margin-top: -80px;
    }
</style>
@section('content')
    <x-forms.patch :action="route('admin.auth.user.update', $user)" :enctype="__('multipart/form-data')">
        <x-backend.card>
            <x-slot name="header">
                @lang('Update User')
            </x-slot>

            <x-slot name="headerActions">
                @if ($user->type === $model::TYPE_BILLER)
                    <x-utils.link class="card-header-action btn btn-light btn-sm" :href="route('admin.auth.user.billers')">
                        <x-slot name="slot">
                            {{ __('Cancel') }}
                        </x-slot>
                    </x-utils.link>
                @else
                <x-utils.link class="card-header-action btn btn-light btn-sm" :href="route('admin.auth.user.index')">
                    <x-slot name="slot">
                        {{ __('Cancel') }}
                    </x-slot>
                </x-utils.link>
                @endif
            </x-slot>

            <x-slot name="body">
                <div class="container mt-3">
                    <input type="hidden" name="user_type" value="patient">
                    <div class="row jumbotron box8">
                        <div class="col-sm-12 mx-t3 mb-4 " style="margin-top: -4rem; ">
                            <div class="avatar-upload">
                                <div class="avatar-edit">
                                    <input type='file' name="avatar_location" id="imageUpload" accept=".png, .jpg, .jpeg" />
                                    {{-- <input type="hidden" id="avatar_type" name="avatar_type" value="storage"> --}}
                                    <label for="imageUpload">
                                        <i class="cil-pencil icon"></i>
                                    </label>
                                </div>
                                <div class="avatar-preview">
                                    <div id="imagePreview" style="background-image: url({{ $user->picture }});">
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if (!$user->isMasterAdmin())
                            <div class="col-sm-6 form-group">
                                <label for="name">@lang('Type')</label>
                                    <select name="type" class="form-control" required x-on:change="userType = $event.target.value">
                                        @if ($user->type === $model::TYPE_BILLER)
                                            <option value="{{ $model::TYPE_BILLER }}" {{ $user->type === $model::TYPE_BILLER ? 'selected' : '' }}>@lang('Biller')</option>
                                        @else
                                            <option value="{{ $model::TYPE_ADMIN }}" {{ $user->type === $model::TYPE_ADMIN ? 'selected' : '' }}>@lang('admin')</option>
                                            <option value="{{ $model::TYPE_SUPER_ADMIN }}" {{ $user->type === $model::TYPE_SUPER_ADMIN ? 'selected' : '' }}>@lang('Administrator')</option>
                                            <option value="{{ $model::TYPE_PATIENT }}" {{ $user->type === $model::TYPE_PATIENT ? 'selected' : '' }}>@lang('Patient')</option>
                                            <option value="{{ $model::TYPE_NURSE }}" {{ $user->type === $model::TYPE_NURSE ? 'selected' : '' }}>@lang('Nurse')</option>
                                            <option value="{{ $model::TYPE_SUBSTITUTE_NURSE }}" {{ $user->type === $model::TYPE_SUBSTITUTE_NURSE ? 'selected' : '' }}>@lang('Substitute nurse')</option>
                                            <option value="{{ $model::TYPE_BILLER }}" {{ $user->type === $model::TYPE_BILLER ? 'selected' : '' }}>@lang('Biller')</option>
                                        @endif
                                    </select>
                            </div><!--form-group-->
                        @endif

                        <div class="col-sm-6 form-group">
                            <label for="first_name" >@lang('Prénom')</label>
                            <input type="text" name="first_name" class="form-control" placeholder="{{ __('Prénom') }}" value="{{ old('first_name') ?? $user->first_name }}" maxlength="100" required />
                        </div>
                        <div class="col-sm-6 form-group">
                            <label for="last_name">@lang('Nom')</label>
                            <input type="text" name="last_name" class="form-control" placeholder="{{ __('Nom') }}" value="{{ old('last_name') ?? $user->last_name }}" maxlength="100" required />
                        </div>
                        <div class="col-sm-6 form-group">
                            <label for="email">@lang('E-mail Address')</label>
                            <input type="email" name="email" id="email" @if($user->type === $model::TYPE_BILLER) readonly @endif class="form-control" placeholder="{{ __('E-mail Address') }}" value="{{ old('email') ?? $user->email }}" maxlength="255"  />
                        </div>
                        <div class="col-sm-6 form-group" x-show="userType != '{{ $model::TYPE_BILLER }}'">
                                <label for="phone">@lang('Numéro de téléphone')</label>
                                <input type="phone" name="phone" id="phone" class="form-control" placeholder="{{ __('Numéro de téléphone') }}" value="{{ old('phone') ?? $user->phone }}" maxlength="100"  />
                        </div>
                        <div class="col-sm-3 form-group" x-show="userType != '{{ $model::TYPE_BILLER }}'">
                            <label for="birth_date">@lang('Date de naissance')</label>
                            <input type="date" name="birth_date" id="birth_date" class="form-control" placeholder="{{ __('Date de naissance') }}" value="{{ old('birth_date') ?? $user->birth_date->format('Y-m-d') }}" maxlength="100"  />
                        </div>
                        <div class="col-sm-3 form-group"  x-show="userType === '{{ $model::TYPE_NURSE }}' || userType === '{{ $model::TYPE_SUBSTITUTE_NURSE }}'">
                            <label for="adeli_number">@lang('Numéro Adéli')</label>
                            <input type="text" name="adeli_number" id="adeli_number" class="form-control" placeholder="{{ __('Numéro Adéli') }}" value="{{ old('adeli_number') ?? $user->adeli_number }}" maxlength="100"  />
                        </div>
                        <div class="col-sm-6 form-group" x-show="userType != '{{ $model::TYPE_BILLER }}'">
                            <label for="address">@lang('Adresse')</label>
                            <input type="text" name="address" id="address" class="form-control" value="{{ old('address') ?? $user->address }}" placeholder="{{ __('Adresse') }}" maxlength="100"  />
                        </div>
                        <div class="col-sm-6 form-group" x-show="userType === '{{ $model::TYPE_NURSE }}' || userType === '{{ $model::TYPE_SUBSTITUTE_NURSE }}'" >
                            <label for="tour_sector">@lang('Secteur de tournée')</label>
                            <select class="tour_sector js-example-basic-multiple" id="tour_sector" name="tour_sector[]" multiple style="width: 100%">
                                @foreach(explode(",",$user->tour_sector) as $sector)
                                    <option selected>{{ $sector }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-6 form-group" style="display: none">
                            <label for="active">@lang('Active')</label>
                            <input name="active" id="active" class="form-check-input" type="checkbox" value="1" {{ old('active', true) ? 'checked' : '' }}/>
                        </div>
                    </div>
                </div>
                {{-- @if (!$user->isMasterAdmin())
                     @include('backend.auth.includes.roles')

                     @if (!config('boilerplate.access.user.only_roles'))
                         @include('backend.auth.includes.permissions')
                     @endif
                 @endif --}}
            </x-slot>
            <x-slot name="footer">
                <button class="btn btn-sm btn-primary float-right" type="submit">@lang('Update User')</button>
            </x-slot>
        </x-backend.card>
    </x-forms.patch>
@endsection
@push('after-scripts')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#imagePreview').css('background-image', 'url('+e.target.result +')');
                    $('#imagePreview').hide();
                    $('#imagePreview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#imageUpload").change(function() {
            readURL(this);
            $('<input>', {
                type: 'hidden',
                id: 'avatar_type',
                name: 'avatar_type',
                value: 'storage'
            }).appendTo('#imageUpload');
        });
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $('.tour_sector').select2({
            placeholder: 'Selectionnez Secteur de tournée ',
            multiple: 'multiple',
            minimumInputLength: 3,
            ajax: {
                url: "/admin/tour/searchTourSector ",
                type: 'post',
                dataType: 'json',
                delay: 250,
                data: function (data) {
                    return {
                        _token: CSRF_TOKEN,
                        search: data.term
                    };
                },
                processResults: function(data) {
                    var results = [];
                    $.each(data, function(index, item) {
                        results.push({
                            text: item,
                            id: item
                        });
                    });
                    return { results: results };
                },
            }
        });
    </script>
@endpush
