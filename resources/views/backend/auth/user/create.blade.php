@inject('model', '\App\Domains\Auth\Models\User')

@extends('backend.layouts.app')

@section('title', __('Create User'))
<style>
    .card-footer{
        margin-top: -80px;
    }
</style>
@section('content')
    <x-forms.post :action="route('admin.auth.user.store')" :enctype="__('multipart/form-data')">
        <x-backend.card>
            <x-slot name="header">
                @lang('Create User')
            </x-slot>

            <x-slot name="headerActions">
                <x-utils.link class="card-header-action btn btn-light btn-sm" :href="route('admin.auth.user.index')">
                    <x-slot name="slot">
                        {{ __('Cancel') }}
                    </x-slot>
                </x-utils.link>
            </x-slot>
            <x-slot name="body">
                <div class="container mt-3">
                    <input type="hidden" name="user_type" value="patient">
                    <div class="row jumbotron box8">
                        <div class="col-sm-12 mx-t3 mb-4 " style="margin-top: -4rem; ">
                            <div class="avatar-upload">
                                <div class="avatar-edit">
                                    <input type='file' name="avatar_location" id="imageUpload" accept=".png, .jpg, .jpeg" />
                                    {{-- <input type="hidden" id="avatar_type" name="avatar_type" value="storage"> --}}
                                    <label for="imageUpload">
                                        <i class="cil-pencil icon"></i>
                                    </label>
                                </div>
                                <div class="avatar-preview">
                                    <div id="imagePreview" style="background-image: url({{ asset('img/default-avatar.png') }});">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 form-group">
                            <label for="name">@lang('Type')<span class="alert-required" aria-hidden="true">*</span></label>
                            <select name="type" class="form-control" required x-on:change="userType = $event.target.value">
                                <option value="{{ $model::TYPE_SUPER_ADMIN }}">@lang('Super Admin')</option>
                                <option value="{{ $model::TYPE_ADMIN }}">@lang('Admin')</option>
                                <option value="{{ $model::TYPE_PATIENT }}">@lang('Patient')</option>
                                <option value="{{ $model::TYPE_NURSE }}">@lang('Infirmier libérale')</option>
                                <option value="{{ $model::TYPE_SUBSTITUTE_NURSE }}">@lang('Infirmier remplaçant')</option>
                                <option value="{{ $model::TYPE_BILLER }}">@lang('Facturier')</option>
                            </select>
                        </div>
                        <div class="col-sm-6 form-group">
                            <label for="first_name" >@lang('Prénom')<span class="alert-required" aria-hidden="true">*</span></label>
                            <input type="text" name="first_name" class="form-control" placeholder="{{ __('Prénom') }}" value="{{ old('first_name') }}" maxlength="100" required />
                        </div>
                        <div class="col-sm-6 form-group">
                            <label for="last_name">@lang('Nom')<span class="alert-required" aria-hidden="true">*</span></label>
                            <input type="text" name="last_name" class="form-control" placeholder="{{ __('Nom') }}" value="{{ old('last_name') }}" maxlength="100" required />
                        </div>
                        <div class="col-sm-6 form-group">
                            <label for="email">@lang('E-mail Address')<span class="alert-required" aria-hidden="true">*</span></label>
                            <input type="email" name="email" class="form-control" placeholder="{{ __('E-mail Address') }}" value="{{ old('email') }}" maxlength="255" required />
                        </div>
                        <div class="col-sm-6 form-group">
                            <label for="password">@lang('Password') <span class="alert-required" aria-hidden="true">*</span></label>
                            <input type="password" name="password" id="password" class="form-control" placeholder="{{ __('Password') }}" maxlength="100" required autocomplete="new-password" />
                        </div>
                        <div class="col-sm-6 form-group">
                            <label for="password_confirmation">@lang('Password Confirmation')<span class="alert-required" aria-hidden="true">*</span></label>
                            <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="{{ __('Password Confirmation') }}" maxlength="100" required autocomplete="new-password" />
                        </div>
                        <div class="col-sm-6 form-group">
                            <label for="phone">@lang('Numéro de téléphone')<span class="alert-required" aria-hidden="true">*</span></label>
                            <input type="phone" name="phone" id="phone" class="form-control" value="{{ old('phone') }}" placeholder="{{ __('Numéro de téléphone') }}" maxlength="100" required />
                        </div>
                        <div class="col-sm-3 form-group">
                            <label for="address">@lang('Adresse') <span class="alert-required" aria-hidden="true">*</span></label>
                            <input type="text" name="address" id="address" class="form-control" value="{{ old('address') }}" placeholder="{{ __('Adresse') }}" maxlength="100"  required/>
                        </div>
                        <div class="col-sm-3 form-group">
                            <div x-show="userType === '{{ $model::TYPE_NURSE }}' || userType === '{{ $model::TYPE_SUBSTITUTE_NURSE }}'" >
                                <label for="birth_date">@lang('Date de naissance')</label>
                                <input type="date" name="birth_date" id="birth_date" class="form-control" value="{{ old('birth_date') }}" placeholder="{{ __('Date de naissance') }}" maxlength="100"  />
                            </div>
                        </div>
                        <div class="col-sm-6 form-group">
                            <div x-show="userType === '{{ $model::TYPE_NURSE }}' || userType === '{{ $model::TYPE_SUBSTITUTE_NURSE }}'">
                                <label for="tour_sector">@lang('Secteur de tournée')</label>
                                <select class="tour_sector js-example-basic-multiple" id="tour_sector"
                                        name="tour_sector[]" style="width: 100%"></select>
                            </div>
                        </div>
                        <div class="col-sm-6 form-group">
                            <label for="adeli_number">@lang('Numéro Adéli')</label>
                            <input type="text" name="adeli_number" id="adeli_number" class="form-control" value="{{ old('adeli_number') }}" placeholder="{{ __('Numéro Adéli') }}" maxlength="100"  />
                        </div>
                        <div class="col-sm-6 form-group" style="display: none !important;">
                                <label for="active">@lang('Active')</label>
                                <input name="active" id="active" class="form-check-input" type="checkbox" value="1" {{ old('active', true) ? 'checked' : '' }} />

                        </div>
                    </div>
                </div>
                    {{-- <div x-data="{ emailVerified : false }">
                        <div class="form-group row">
                            <label for="email_verified" class="col-md-2 col-form-label">@lang('E-mail Verified')</label>

                            <div class="col-md-10">
                                <div class="form-check">
                                    <input
                                        type="checkbox"
                                        name="email_verified"
                                        id="email_verified"
                                        value="1"
                                        class="form-check-input"
                                        x-on:click="emailVerified = !emailVerified"
                                        {{ old('email_verified') ? 'checked' : '' }} />
                                </div><!--form-check-->
                            </div>
                        </div><!--form-group-->

                        <div x-show="!emailVerified">
                            <div class="form-group row">
                                <label for="send_confirmation_email" class="col-md-2 col-form-label">@lang('Send Confirmation E-mail')</label>

                                <div class="col-md-10">
                                    <div class="form-check">
                                        <input
                                            type="checkbox"
                                            name="send_confirmation_email"
                                            id="send_confirmation_email"
                                            value="1"
                                            class="form-check-input"
                                            {{ old('send_confirmation_email') ? 'checked' : '' }} />
                                    </div><!--form-check-->
                                </div>
                            </div><!--form-group-->
                        </div>
                    </div> --}}

                    {{-- @include('backend.auth.includes.roles')

                    @if (!config('boilerplate.access.user.only_roles'))
                        @include('backend.auth.includes.permissions')
                    @endif --}}

            </x-slot>

            <x-slot name="footer">
                <button class="btn btn-sm btn-primary float-right" type="submit">@lang('Create User')</button>
            </x-slot>
        </x-backend.card>
    </x-forms.post>
@endsection

@push('after-scripts')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#imagePreview').css('background-image', 'url('+e.target.result +')');
                    $('#imagePreview').hide();
                    $('#imagePreview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#imageUpload").change(function() {
            readURL(this);
            $('<input>', {
                type: 'hidden',
                id: 'avatar_type',
                name: 'avatar_type',
                value: 'storage'
            }).appendTo('#imageUpload');
        });
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $('.tour_sector').select2({
            placeholder: 'Selectionnez Secteur de tournée ',
            multiple: 'multiple',
            minimumInputLength: 3,
            ajax: {
                url: "/admin/tour/searchTourSector ",
                type: 'post',
                dataType: 'json',
                delay: 250,
                data: function (data) {
                    return {
                        _token: CSRF_TOKEN,
                        search: data.term
                    };
                },
                processResults: function(data) {
                    var results = [];
                    $.each(data, function(index, item) {
                        results.push({
                            text: item,
                            id: item
                        });
                    });
                    return { results: results };
                },
            }
        });
    </script>
@endpush
