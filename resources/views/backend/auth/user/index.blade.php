@inject('model', '\App\Domains\Auth\Models\User')
@extends('backend.layouts.app')

@section('title', __('Active Users'))
@push('before-styles')
    <link href="{{ ('/css/_tagify.css') }}" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        .card {
            overflow: visible;
        }
    </style>
@endpush
@section('content')

    <x-backend.card>
        <x-slot name="header">
            @lang('Users')
        </x-slot>

        @if ($logged_in_user->hasAllAccess())
            <x-slot name="headerActions">
                <x-utils.link
                    icon="c-icon cil-plus"
                    class="card-header-action btn btn-primary btn-sm"
                    :href="route('admin.auth.user.create')"
                >
                    {{ __('Create User') }}
                </x-utils.link>
            </x-slot>
        @endif
        <x-slot name="body">
            <livewire:backend.users-table/>
        </x-slot>
    </x-backend.card>
    @push('before-scripts')
        <script src="{{ ('/resources/js/backend/tagify.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tagify/4.3.0/tagify.min.js" crossorigin="anonymous"
                referrerpolicy="no-referrer"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tagify/4.3.0/tagify.min.css"
              crossorigin="anonymous" referrerpolicy="no-referrer"/>

    @endpush
@endsection
