@props(['href' => '#', 'text' => __('Quitter'), 'permission' => false])

<x-utils.form-button
    :action="$href"
    method="delete"
    name="delete-item"
    button-class="btn btn-danger btn-sm"
    permission="{{ $permission }}"
>
    <i class="fa fa-window-close"></i> {{ $text }}
</x-utils.form-button>
