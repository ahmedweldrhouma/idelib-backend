<form method="post" {{ $attributes->merge(['action' => '#', 'class' => 'form-horizontal','id' => '#', 'enctype' => '' ]) }}>
    @csrf

    {{ $slot }}
</form>
