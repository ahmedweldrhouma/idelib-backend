<?php

namespace App\Domains\Message\Services;


use App\Domains\CovidTreatment\Models\CovidTreatment;
use App\Domains\Message\Models\Message;
use App\Services\BaseService;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Support\Arr;

/**
 * Class CovidTreatmentService.
 */
class MessageService extends BaseService
{

    public function __construct(Message $message)
    {
        $this->model = $message;
    }

    /**
     * @param  int  $id
     * @return CovidTreatment
     *
     */
    public function getCovidTreatById($id): CovidTreatment
    {
        $covid_treat = $this->getById($id);

        return $covid_treat;
    }

    public function getPictures($conversation)
    {
        return $conversation->messages()->where('type', 'picture')->paginate(10);
    }

    /**
     * @param  array  $data
     * @return Message
     *
     * @throws HttpException
     * @throws \Throwable
     */
    public function store(array $data = [],$sender_id): Message
    {
        DB::beginTransaction();

        try {
            $message = $this->model::create([
                "content" => $data['content'] ?? null,
                "attachment" => $data['attachment'] ?? null,
                "type" => $data['type'] ?? null,
                "sender_id" => $sender_id,
                "conversation_id" => $data['conversation_id'] ?? null,
            ]);
        } catch (Exception $e) {
            // dd($e);
            DB::rollback();
            // throw new HttpException(500, $e->getMessage());
            throw new HttpException(500, $e->getMessage());
            // throw new GeneralException('Un problème est survenu lors de la création du test pcr.');
        }
        DB::commit();
        return $message;
    }



}
