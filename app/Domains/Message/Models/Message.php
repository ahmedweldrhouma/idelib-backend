<?php

namespace App\Domains\Message\Models;


use App\Domains\Message\Models\Traits\Attribute\MessageAttribute;
use App\Domains\Message\Models\Traits\Relationship\MessageRelationship;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory,MessageRelationship,MessageAttribute;

    public const TYPE_FILE = 'file';
    public const TYPE_PICTURE = 'picture';
    public const TYPE_TEXT = 'text';
    public const TYPE_VOCAL = 'vocal';

    /**
     * @var string[]
     */
    protected $fillable = [
        'content',
        'type',
        'sender_id',
        'conversation_id',
        'attachment',
    ];

    /**
     * @var string[]
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];


}
