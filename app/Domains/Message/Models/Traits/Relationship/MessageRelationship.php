<?php

namespace App\Domains\Message\Models\Traits\Relationship;

use App\Domains\Auth\Models\User;
use App\Domains\Conversation\Models\Conversation;
use App\Domains\Message\Models\Message;

/**
 * Class CovidTreatmentRelationship.
 */
trait MessageRelationship
{
    public function conversation()
    {
        return $this->belongsTo(Conversation::class, 'conversation_id');
    }

    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_id');
    }
}
