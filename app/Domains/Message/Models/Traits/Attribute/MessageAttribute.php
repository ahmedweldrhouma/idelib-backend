<?php

namespace App\Domains\Message\Models\Traits\Attribute;

use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

/**
 * Traits UserAttribute.
 */
trait MessageAttribute
{

    /**
     * @return mixed
     */
    public function getContentAttribute()
    {
        if ($this->attributes['type'] !== "text") {
            return Storage::disk('s3')->url($this->attributes['content']);
        }
        return $this->attributes['content'];
    }
}
