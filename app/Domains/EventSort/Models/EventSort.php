<?php

namespace App\Domains\EventSort\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventSort extends Model
{
    use HasFactory;

    protected $guarded = ['id'];
}
