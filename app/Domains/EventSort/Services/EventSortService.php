<?php

namespace App\Domains\EventSort\Services;

use App\Domains\Appointment\Models\Appointment;
use App\Domains\EventSort\Models\EventSort;
use App\Services\BaseService;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Exception\HttpException;


/**
 * Class EventSortService.
 */
class EventSortService extends BaseService
{
    /**
     * EventSortService constructor.
     *
     * @param  EventSort  $eventSort
     */
    public function __construct(EventSort $eventSort)
    {
        $this->model = $eventSort;
    }

    public function all()
    {
        return $this->model->all();
    }
    public function getAllForUser($filters = [])
    {
        return $this->model::where('user_id',Auth::id())->whereBetween('day', [$filters['from'], $filters['to']])->get();
    }

}
