<?php

namespace App\Domains\Notification\Models;

use App\Domains\Notification\Models\Traits\Scope\NotificationScope;
use App\Domains\Notification\Models\Traits\Relationship\NotificationRelationship;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use NotificationRelationship,NotificationScope;

    /**
     * @var string[]
     */
    protected $fillable = [
        'user_id',
        'receiver_id',
        'model_id',
        'content',
        'model',
        'page',
        'subject',
        'sent_at',
        'seen_at',
    ];

    /**
     * @var string[]
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

}
