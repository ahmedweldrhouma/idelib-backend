<?php

namespace App\Domains\Notification\Models\Traits\Relationship;

use App\Domains\Auth\Models\User;
use App\Domains\Invitation\Models\Invitation;

/**
 * Class NotificationRelationship.
 */
trait NotificationRelationship
{
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function receiver()
    {
        return $this->belongsTo(User::class, 'receiver_id');
    }

    public function invitation()
    {
        return $this->belongsTo(Invitation::class, 'model_id');
    }
}
