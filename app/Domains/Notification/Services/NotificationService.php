<?php

namespace App\Domains\Notification\Services;

use App\Domains\Notification\Models\Notification;
use App\Exceptions\GeneralException;
use Carbon\Carbon;
use http\Params;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Services\BaseService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

/**
 * Class NotificationService.
 */
class NotificationService extends BaseService
{
    public function __construct(Notification $model)
    {
        $this->model = $model;
    }

    public function sendNotification($fcm_token, $page, $title, $body, $extraParams = [] )
    {
        $data = [
            "to" => $fcm_token,
            "data" =>
            [
                "page" => $page,
                "id" => Auth::id(),
            ],
            "notification" =>
            [
                "title" => $title,
                "body" => $body
            ]
        ];
        $data["data"] = array_merge($extraParams,$data["data"]);
        $response = Http::withHeaders([
            'Authorization' => "key=AAAAlaylEp4:APA91bGAHnsvolBihPnIBDXUEuzm91weKUHTYN97LS10en5VlKi9eKTqmdvuTWYnwFa7d-eJs18GjhN-f-fN0OVKN3_CP2MECWElbCksd25g0mwLe3sm9lTObLwT1O4nUlJZkL4lLRMZ",
            'Content-Type' => 'application/json',
        ])->post('https://fcm.googleapis.com/fcm/send', $data);

        if($response->failed() || $response->clientError() || $response->serverError()){
            var_dump([
                'fcm' => $fcm_token,
                'message' => $response->getReasonPhrase()
            ], $response->getStatusCode());
            return response()->json([
                'message' => $response->getReasonPhrase()
            ], $response->getStatusCode());
        }
        return $response;
    }

    public function getAllForUser()
    {
         $notifications  = Auth::user()->notification()->where(function ($q){
            $q->where('page','!=','notifications')->orWhere(function ($q){
                $q->whereHas("invitation")->where('page','=','notifications');
            });
        })->get();
         Auth::user()->notification()->whereNull('seen_at')->update(['seen_at' => Carbon::now()->format('Y-m-d H:i:s')]);
        return $notifications;
    }

    /**
     * @throws \Throwable
     */
    public function store($data =[] ) : Notification
    {

        DB::beginTransaction();

        try {

            $notification = $this->model::create([
                "model" => $data['model'] ?? null,
                "user_id" => $data['user_id'] ?? null,
                "page" => $data['page'] ?? null,
                "content" => $data['content'] ?? null,
                "receiver_id" => $data['receiver_id'] ?? null,
                "model_id" => $data['model_id'] ?? null,
                "subject" => $data['subject'] ?? null,
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            //throw new HttpException(422,  $e->getMessage());
            throw new HttpException(500,'Un problème est survenu lors de l\'enregistrement de notification. Veuillez réessayer.');
        }

        DB::commit();

        return $notification;
    }

    public function UnreadNotification()
    {
        return (bool)Auth::user()->notification->whereNull('seen_at')->first();
    }

}
