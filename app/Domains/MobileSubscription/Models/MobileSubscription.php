<?php

namespace App\Domains\MobileSubscription\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Domains\MobileSubscription\Models\Traits\Attribute\MobileSubscriptionAttribute;
use App\Domains\MobileSubscription\Models\Traits\Method\MobileSubscriptionMethod;
use App\Domains\MobileSubscription\Models\Traits\Relationship\MobileSubscriptionRelationship;
use App\Domains\MobileSubscription\Models\Traits\Scope\MobileSubscriptionScope;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Cashier\Subscription ;
use Illuminate\Database\Eloquent\Factories\HasFactory;



/**
 * Class MobileSubscription.
 */
class MobileSubscription extends  Subscription
{
    use MobileSubscriptionMethod,
        MobileSubscriptionRelationship,
        MobileSubscriptionScope,
        SoftDeletes, HasFactory;

    protected $primaryKey   = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'provider',
        'subscription_id',
        'receipt',
        'user_id',
        'users',
        'cascade',
        'plan_id',
        'plans',
        'cascade',
        'is_active',
        'transaction_id',
        'original_transaction_id',
        'operating_system',
        'subscribed_at',
        'expired_at',
        'cancelled_at',
        'stripe_id',
    ];


    /**
     * @var array
     */
    protected $dates = [
        'subscribed_at',
        'expired_at',
        'cancelled_at',
    ];

    public function cancelSub(){
        $stripeSubscription = $this->updateStripeSubscription([
            'cancel_at_period_end' => true,
        ]);

        $this->status = $stripeSubscription->status;

        // If the user was on trial, we will set the grace period to end when the trial
        // would have ended. Otherwise, we'll retrieve the end of the billing period
        // period and make that the end of the grace period for this current user.
        if ($this->onTrial()) {
            $this->expired_at = $this->trial_ends_at;
        } else {
            $this->expired_at = Carbon::createFromTimestamp(
                $stripeSubscription->current_period_end
            );
        }
        $this->cancelled_at = Carbon::now();

        $this->save();

        return $this;
    }


}
