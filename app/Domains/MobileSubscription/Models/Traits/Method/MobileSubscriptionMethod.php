<?php

namespace App\Domains\MobileSubscription\Models\Traits\Method;

use Illuminate\Support\Collection;

/**
 * Traits MobileSubscriptionMethod.
 */
trait MobileSubscriptionMethod
{

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->is_active;
    }

    /**
     * @return bool
     */
    public function isAnnual(): bool
    {
        return $this->annual;
    }

    /**
     * @return bool
     */
    public function isMonthly(): bool
    {
        return $this->monthly;
    }
}
