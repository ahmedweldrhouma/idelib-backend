<?php

namespace App\Domains\MobileSubscription\Models\Traits\Relationship;

use App\Domains\Auth\Models\PasswordHistory;
use App\Domains\Auth\Models\User;
use App\Domains\Plan\Models\Plan;

/**
 * Class MobileSubscriptionRelationship.
 */
trait MobileSubscriptionRelationship
{
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id')->withTrashed();
    }

    public function plan()
    {
        return $this->belongsTo(Plan::class, 'plan_id');
    }
}
