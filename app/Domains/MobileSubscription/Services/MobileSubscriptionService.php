<?php

namespace App\Domains\MobileSubscription\Services;

use App\Domains\Auth\Models\User;
use App\Domains\MobileSubscription\Models\MobileSubscription;
use App\Domains\Plan\Models\Plan;
use App\Exceptions\GeneralException;
use App\Services\BaseService;
use App\Services\RevenueCatService;
use Exception;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class MobileSubscriptionService.
 */
class MobileSubscriptionService extends BaseService
{
    /**
     * UserService constructor.
     *
     * @param MobileSubscription $mobileSubscription
     */
    public function __construct(MobileSubscription $mobileSubscription)
    {
        $this->model = $mobileSubscription;
    }

    public function all()
    {
        return $this->model->all();
    }

    /**
     * @param int $id
     * @return MobileSubscription
     *
     */

    public function getSubscriptionByid($id): MobileSubscription
    {
        $subscription = $this->getById($id);

        return $subscription;
    }

    public function getSubscriptionByStripeId($id)
    {
        $subscription = $this->model->where('stripe_id', $id)->first();

        return $subscription;
    }

    public function deactivateSubscriptionByStripeId($id, $dateDesabo): bool
    {
        $subscription = $this->getSubscriptionByStripeId($id);

        if ($subscription) {
            $subscription->update(['is_active' => false, 'expired_at' => $dateDesabo]);
            return true;
        }

        return false;
    }

    public function activateSubscriptionByStripeId($id, $dateAbo): bool
    {
        $subscription = $this->getSubscriptionByStripeId($id);

        if ($subscription) {
            $subscription->update(['is_active' => true, 'expired_at' => $dateAbo]);
            return true;
        }

        return false;
    }


    /**
     * @param string $user_id
     *
     */
    public function getSubscriptionByUser($user)
    {
        return $this->getByColumn($user->id, 'user_id');
    }


    public function getSubscriptionByTypeUser($type, $actif = true)
    {
        if ($actif)
            return MobileSubscription::selectRaw('"plans"."type" as "plan_type", count(*) as "nb"')->where('is_active', true)
                ->where('users.type', $type)
                ->join('users', 'users.id', '=', 'user_id')
                ->join('plans', 'plans.id', '=', 'plan_id')
                ->groupBy('plan_type')
                ->get();


        return MobileSubscription::selectRaw('"plans"."type" as "plan_type", count(*) as "nb"')
            ->where('users.type', $type)
            ->join('users', 'users.id', '=', 'user_id')
            ->join('plans', 'plans.id', '=', 'plan_id')
            ->groupBy('plan_type')
            ->get();
    }


    public function getLastPurchase($app_user_id)
    {
        return (RevenueCatService::get($app_user_id));
    }

    public function update(MobileSubscription $mobile_subscription, array $data = []): MobileSubscription
    {
        DB::beginTransaction();
        try {
            $mobile_subscription->update([
                "plan_id" => $data['plan_id'] ?? $mobile_subscription->plan_id,
                "transaction_id" => $data['transaction_id'] ?? $mobile_subscription->transaction_id,
                "is_active" => $data['is_active'] ?? $mobile_subscription->active,
                "original_transaction_id" => $data['original_transaction_id'] ?? $mobile_subscription->original_transaction_id,
                "subscribed_at" => $data['subscribed_at'] ?? $mobile_subscription->subscribed_at,
                "expired_at" => $data['expired_at'] ?? $mobile_subscription->expires_at,
                "operating_system" => $data['operating_system'] ?? $mobile_subscription->operating_system,
                "subscription_id" => $data['subscription_id'] ?? $mobile_subscription->subscription_id,
            ]);
        } catch (Exception $e) {
            DB::rollback();
            throw new HttpException(422, $e->getMessage());


            throw new GeneralException('Un problème est survenu lors de la mise à jour de l\'abonnement.');
        }

        DB::commit();

        return $mobile_subscription;
    }

    public function create(User $user, Plan $plan, array $data = []): MobileSubscription
    {
        DB::beginTransaction();
        try {
            $mobile_subscription = MobileSubscription::create([
                "plan_id" => $plan->id ?? null,
                "user_id" => $user->id ?? null,
                "operating_system" => $data['operating_system'] ?? null,
                "subscription_id" => $data['subscription_id'] ?? null,
                "provider" => $data['provider'] ?? null,
                "transaction_id" => $data['transaction_id'] ?? null,
                "original_transaction_id" => $data['original_transaction_id'] ?? null,
                "subscribed_at" => $data['subscribed_at'] ?? null,
                "is_active" => true
            ]);
        } catch (Exception $e) {
            DB::rollback();
            throw new HttpException(422, $e->getMessage());
            throw new GeneralException('Un problème est survenu lors de la mise à jour de l\'abonnement.');
        }

        DB::commit();

        return $mobile_subscription;
    }

    /**
     * @param MobileSubscription $mobile_subscription
     * @param array $data
     * @return MobileSubscription|false
     */
    public function disableSubscription(MobileSubscription $mobile_subscription, array $data = [])
    {
        DB::beginTransaction();
        try {
            $mobile_subscription->update(["is_active" => false]);
        } catch (Exception $e) {
            DB::rollback();
            throw new HttpException(422, $e->getMessage());
            return false;
        }
        DB::commit();
        return $mobile_subscription;
    }

    /**
     * @param MobileSubscription $mobileSubscription
     *
     * @return bool
     * @throws GeneralException
     */
    public function destroy(MobileSubscription $mobileSubscription): bool
    {
        if ($this->deleteById($mobileSubscription->id)) {

            return true;
        }

        throw new GeneralException(__('Un problème est survenu lors de la suppression de l\'abonnement.'));
    }
    /**
     *
     * @return MobileSubscriptionService
     * @throws GeneralException
     */
    public function withTrashed($plan,$user)
    {
        return $this->model::withTrashed()->where('is_active', 1)->where('plan_id', $plan->id)->where('user_id',$user->id)->first();
    }

}
