<?php

namespace App\Domains\Announcement\Services;

use App\Domains\Announcement\Models\Announcement;
use App\Services\BaseService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class AnnouncementService.
 */
class AnnouncementService extends BaseService
{
    /**
     * AnnouncementService constructor.
     *
     * @param  Announcement  $announcement
     */
    public function __construct(Announcement $announcement)
    {
        $this->model = $announcement;
    }

    public function getUserAnnouncements()
    {
        return Auth::user()->announcements;
    }

    public function getOffers($data = NULL)
    {
        $offers = $this->model->where('user_id','!=', Auth::id())->InTimeFrame();
        if(isset($data['sector']) && !empty($data['sector'])){
            $offers = $offers->BySector($data['sector']);
        }
        $offers = $offers->whereHas('user', function($user) {
            if(Auth::user()->isNurse()){
                $user->where('type', 'substitute');
            }elseif(Auth::user()->isSubstituteNurse()){
                $user->where('type', 'nurse');
            }
        });
        return $offers->get();
    }

    /**
     * @param  array  $data
     * @return Announcement
     *
     * @throws HttpException
     */
    public function store(array $data = []): Announcement
    {
        DB::beginTransaction();

        try {
            $announcement = $this->model->create([
                "start_date" => Carbon::parse($data['start_date'])->format('Y-m-d') ?? null,
                "end_date" => Carbon::parse($data['end_date'])->format('Y-m-d') ?? null,
                "sector" => $data['sector'] ?? null,
                "announcement" => $data['announcement'] ?? null,
                "has_transport" => $data['has_transport'] ?? null,
                "user_id" => Auth::id(),

            ]);
        } catch (Exception $e) {
            // dd($e);
            DB::rollback();
            throw new HttpException(422,  $e->getMessage());
            throw new HttpException(500,"Un problème est survenu lors de la création de l'annonce. Veuillez réessayer.");
        }

        DB::commit();

        return $announcement;
    }

    /**
     * @param  array  $data
     * @param  Announcement $announcement
     * @return Announcement
     *
     * @throws HttpException
     */
    public function update(array $data = [], Announcement $announcement): Announcement
    {
        DB::beginTransaction();

        try {
            $announcement->update([
                "start_date" => Carbon::parse($data['start_date'])->format('Y-m-d') ?? $announcement->start_date,
                "end_date" => Carbon::parse($data['end_date'])->format('Y-m-d') ?? $announcement->end_date,
                "sector" => $data['sector'] ?? $announcement->sector,
                "announcement" => $data['announcement'] ?? $announcement->announcement,
                "has_transport" => $data['has_transport'] ?? $announcement->has_transport,

            ]);
        } catch (Exception $e) {
            DB::rollback();
            throw new HttpException(422,  $e->getMessage());
            throw new HttpException(500,"Un problème est survenu lors de la mise à jour de l'annonce. Veuillez réessayer.");
        }

        DB::commit();

        return $announcement;
    }


    /**
     * @param  Announcement $announcement
     *
     * @return bool
     * @throws HttpException
     */
    public function destroy(Announcement $announcement): bool
    {
        if ($this->deleteById($announcement->id)) {

            return true;
        }

        // throw new HttpException(500, $e->getMessage());
        throw new HttpException(500,"Un problème est survenu lors de la suppression de l'annonce. Veuillez réessayer.");
    }
}
