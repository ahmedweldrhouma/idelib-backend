<?php

namespace App\Domains\Announcement\Models;

use App\Domains\Announcement\Models\Traits\Method\AnnoucementMethod;
use Illuminate\Database\Eloquent\Model;
use App\Domains\Announcement\Models\Traits\Scope\AnnouncementScope;
use App\Domains\Announcement\Models\Traits\Relationship\AnnouncementRelationship;

/**
 * Class Announcement.
 */
class Announcement extends Model
{
    use AnnouncementScope,
        AnnouncementRelationship,
        AnnoucementMethod;


    /**
     * @var string[]
     */
    protected $fillable = [
        'start_date',
        'end_date',
        'sector',
        'has_transport',
        'announcement',
        'user_id',
    ];

    /**
     * @var string[]
     */
    protected $dates = [
        'start_date',
        'end_date',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'has_transport' => 'boolean',
    ];

}
