<?php

namespace App\Domains\Announcement\Models\Traits\Method;

use Illuminate\Support\Facades\Auth;

trait AnnoucementMethod
{
    private function UserCanApply($user)
    {
            return $user->applications()
                ->where('announcement_id', $this->id)
                ->exists();
    }
}
