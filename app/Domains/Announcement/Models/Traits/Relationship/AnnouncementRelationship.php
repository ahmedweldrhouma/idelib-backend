<?php

namespace App\Domains\Announcement\Models\Traits\Relationship;

use App\Domains\Auth\Models\User;
use App\Domains\Treatment\Models\Treatment;

/**
 * Class AnnouncementRelationship.
 */
trait AnnouncementRelationship
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
