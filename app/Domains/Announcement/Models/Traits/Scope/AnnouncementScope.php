<?php

namespace App\Domains\Announcement\Models\Traits\Scope;

/**
 * Class AnnouncementScope.
 */
trait AnnouncementScope
{
    /**
     * @param $query
     * @param $area
     * @return mixed
     */
    public function scopeBySector($query, $sector)
    {
        return $query->where('sector', 'ilike', '%'.$sector.'%');
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeInTimeFrame($query)
    {
        return $query->where(function ($query) {
            $query->where(function ($query) {
                $query->whereNull('start_date')
                    ->whereNull('end_date');
            })->orWhere(function ($query) {
                $query->whereNotNull('start_date')
                    ->whereNotNull('end_date')
                    // ->where('start_date', '<=', now())
                    ->where('end_date', '>=', now());
            })->orWhere(function ($query) {
                $query->whereNotNull('start_date')
                    ->whereNull('end_date');
                    // ->where('start_date', '<=', now());
            })->orWhere(function ($query) {
                $query->whereNull('start_date')
                    ->whereNotNull('end_date')
                    ->where('end_date', '>=', now());
            });
        });
    }
}
