<?php

namespace App\Domains\Vaccine\Services;

use App\Domains\CovidTreatment\Models\CovidTreatment;
use App\Domains\Vaccine\Models\Vaccine;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Services\BaseService;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Arr;

/**
 * Class VaccineService.
 */
class VaccineService extends BaseService
{
    /**
     * VaccineService constructor.
     *
     * @param  Vaccine  $caccine
     */
    public function __construct(Vaccine $vaccine)
    {
        $this->model = $vaccine;
    }


    /**
     * @param  int  $id
     * @return Vaccine
     *
     */
    public function getVaccineById($id) : Vaccine
    {
        $vacc = $this->getById($id);

        return $vacc;
    }
    /**
     * @param  array  $data
     * @return CovidTreatment
     *
     * @throws HttpException
     * @throws \Throwable
     */
    public function store(array $data = [], $patient_id) : Vaccine
    {

        DB::beginTransaction();

        try {
            $vaccine = $this->model::create([
                "trod"          => $data['trod'],
                "date"          => Carbon::parse($data['date'])->format('Y-m-d'),
                "name"          => $data['name'],
                "batch_number"  => $data['batch_number'],
                "doses"         => $data['doses'],
                "doses_number"  => $data['doses_number'],
                "patient_id"    => $patient_id,
                "user_id"       => Auth::id() ?? null,
            ]);

        } catch (Exception $e) {
            // dd($e);
            DB::rollback();
            // throw new HttpException(500, $e->getMessage());
            throw new HttpException(500,"Un problème est survenu lors de la création d'un vaccin. Veuillez réessayer.");
            // throw new GeneralException("Un problème est survenu lors de la création d'un vaccin.");
        }

        DB::commit();

        return $vaccine;
    }

    public function getAllByPatient($patient_id)
    {
        return $this->model->where('patient_id', $patient_id)
                    ->where('user_id', Auth::id())
                    ->get();
    }

    public function getAllForNurse()
    {
        return Auth::user()->vaccines;
    }
    /**
     * @param  int  $id
     * @return int
     *
     */
    public function getPatientByIdVacc($id) : int
    {
        $vacc = $this->getById($id);

        return $vacc['patient_id'];
    }

    public function getAllForTeam($filters = [])
    {
        $teams = Auth::user()->team;
        return $teams->isNotEmpty()?Auth::user()->team->load('members.vaccines')->pluck('members.*.vaccines')->flatten():Auth::user()->load(['vaccines'=>function($query)use ($filters){
            if (isset($filters['from']) && isset($filters['to'])){
                $query->whereBetween('date',[$filters['from'],$filters['to']]);
            }
            if (isset($filters['date'])){
                $query->where('date','=',$filters['date']);
            }
            if (isset($filters['user_id'])){
                $query->where('nurse_id','=',$filters['user_id']);
            }
        }])->vaccines;
    }

    public function getTreatByNurse($user){
            $vaccines = $this->model->where('user_id',$user)->get();

        return $vaccines;
    }

    /**
     * @param  Vaccine  $vaccine
     * @param  array  $data
     * @return Vaccine
     *
     * @throws \Throwable
     */
    public function update(array $data = [], Vaccine $vaccine): Vaccine
    {
        DB::beginTransaction();

        try {
            $vaccine->update([
                'trod' => $data['trod'] ?? $vaccine->trod,
                'date' => Carbon::parse($data['date'])->format('Y-m-d') ?? $vaccine->date,
                'name' => $data['name'] ?? $vaccine->name,
                'batch_number' => $data['batch_number'] ?? $vaccine->batch_number,
                'doses' => $data['doses'] ?? $vaccine->doses,
                'doses_number' => $data['doses_number'] ?? $vaccine->doses,
            ]);
        } catch (Exception $e) {
            // dd($e);
            DB::rollBack();
            // throw new HttpException(500, $e->getMessage());
            throw new HttpException(500,"Un problème est survenu lors de la mise à jour d'un vaccin. Veuillez réessayer.");
            // throw new GeneralException(__('There was a problem updating this vaccine. Please try again.'));
        }

        DB::commit();

        return $vaccine;
    }

    public function destroy(Vaccine  $vacc): bool
    {
        if ($this->deleteById($vacc->id)) {

            return true;
        }

        return false;
    }

    public function getAllForNurseToday($today_date)
    {
        $all_treatments = Auth::user()->vaccines;
        $vaccs = [];
        foreach ($all_treatments as $trait)
            {
             if ($trait->date->format('Y-m-d') == $today_date ){

                 array_push ($vaccs,$trait);
             }
            }
        return $vaccs;
    }
}
