<?php

namespace App\Domains\Vaccine\Models;

use Illuminate\Database\Eloquent\Model;
use App\Domains\Vaccine\Models\Traits\Relationship\VaccineRelationship;
use App\Domains\Vaccine\Models\Traits\Scope\VaccineScope;

/**
 * Class Vaccine.
 */
class Vaccine extends Model
{
    use VaccineRelationship,
        VaccineScope;

    protected $primaryKey   = 'id';    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'trod',
        'date',
        'name',
        'batch_number',
        'doses',
        'doses_number',
        'patient_id',
        'user_id',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'date',
        'created_at',
        'updated_at',
    ];

    /**
     * @var string[]
     */
    protected $with = [
        'nurse',
        'patient'
    ];

}
