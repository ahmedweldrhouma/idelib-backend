<?php

namespace App\Domains\Vaccine\Models\Traits\Relationship;

use App\Domains\Auth\Models\User;
use App\Domains\Patient\Models\Patient;

/**
 * Class VaccineRelationship.
 */
trait VaccineRelationship
{
    public function patient()
    {
        return $this->belongsTo(Patient::class, 'patient_id');
    }

    
    public function nurse()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
