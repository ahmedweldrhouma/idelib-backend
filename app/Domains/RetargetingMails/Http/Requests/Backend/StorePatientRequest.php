<?php

namespace App\Domains\Patient\Http\Requests\Backend;

use App\Domains\Patient\Models\Enums\PatientType;
use App\Domains\Patient\Models\Patient;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;
use LangleyFoxall\LaravelNISTPasswordRules\PasswordRules;

/**
 * Class StorePatientRequest.
 */
class StorePatientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => ['required'],
            'user_type' => ['required', 'max:100'],
            'first_name' => ['required', 'max:100'],
            'last_name' => ['required', 'max:100'],
            'social_security_number' => ['required', 'max:100'],
            'email' => ['nullable', 'max:255', 'email', Rule::unique('users')],
            'phone' => ['sometimes', 'max:100'],
            'birth_date' => ['sometimes'],
            'doctor' => ['sometimes', 'max:100'],
            'doctor_phone' => ['sometimes', 'max:100'],
            'address' => ['sometimes', 'max:100'],
            'documents' =>  ['sometimes','array'],
            'mutual_card' => ['sometimes'],
            'vital_card' => ['sometimes', 'max:100'],
            'avatar_location' => ['sometimes','mimes:jpg,bmp,png']
        ];
    }

}
