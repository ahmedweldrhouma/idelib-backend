<?php

namespace App\Domains\Patient\Http\Requests\Backend;

use App\Domains\Patient\Models\Patient;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateDocumentsRequest.
 */
class UpdateDocumentsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'documents' =>  ['sometimes','array'],
        ];
    }

}
