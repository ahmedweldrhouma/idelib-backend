<?php

namespace App\Domains\RetargetingMails\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RetargetingMails extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'created_at', 'update_at', 'type_mail'];
}
