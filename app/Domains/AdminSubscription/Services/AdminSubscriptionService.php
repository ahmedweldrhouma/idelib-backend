<?php

namespace App\Domains\AdminSubscription\Services;

use App\Domains\AdminSubscription\Models\AdminSubscription;
use App\Exceptions\GeneralException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Services\BaseService;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

/**
 * Class AdminSubscriptionService.
 */
class AdminSubscriptionService extends BaseService
{
    /**
     * AdminSubscriptionService constructor.
     *
     * @param  AdminSubscription  $adminSubscription
     */
    public function __construct(AdminSubscription $adminSubscription)
    {
        $this->model = $adminSubscription;
    }

    /**
     * @param  array  $data
     * @return AdminSubscription
     *
     * @throws HttpException
     * @throws \Throwable
     */
    public function store(array $data = []) : AdminSubscription
    {

        DB::beginTransaction();

        try {

            $admin_subscription = $this->model::create([
                "plan_id" => $data['plan_id'] ?? null,
                "user_id" => $data['user_id'] ?? null,
                "subscribed_at" => $data['subscribed_at'] ?? null,
                "expires_at" => $data['expires_at'] ?? null,
            ]);

        } catch (Exception $e) {
            DB::rollback();
            throw new HttpException(422,  $e->getMessage());
            throw new HttpException(500,'Un problème est survenu lors de la création du patient. Veuillez réessayer.');

        }

        DB::commit();

        return $admin_subscription;
    }

    public function update(AdminSubscription $admin_subscription, array $data= []) : AdminSubscription
    {
        DB::beginTransaction();

        try {

            $admin_subscription->update([
                "plan_id" => $data['plan_id'] ?? $admin_subscription->plan_id,
                "subscribed_at" => $data['subscribed_at'] ?? $admin_subscription->subscribed_at,
                "expires_at" => $data['expires_at'] ?? $admin_subscription->expires_at
            ]);
        } catch (Exception $e) {
            // dd($e);
            DB::rollback();
            throw new HttpException(422,  $e->getMessage());

            throw new GeneralException('Un problème est survenu lors de la mise à jour de l\'abonnement.');
        }

        DB::commit();

        return $admin_subscription;
    }

    /**
     * @param  AdminSubscription  $admin_subscription
     *
     * @return bool
     * @throws GeneralException
     */
    public function destroy(AdminSubscription  $admin_subscription): bool
    {
        if ($this->deleteById($admin_subscription->id)) {

            return true;
        }

        throw new GeneralException(__('Un problème est survenu lors de la suppression de l\'abonnement.'));
    }

}
