<?php

namespace App\Domains\AdminSubscription\Models;

use App\Domains\AdminSubscription\Models\Traits\Scope\AdminSubscriptionScope;
use Illuminate\Database\Eloquent\Model;
use App\Domains\AdminSubscription\Models\Traits\Relationship\AdminSubscriptionRelationship;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class AdminSubscription.
 */
class AdminSubscription extends Model
{
    use AdminSubscriptionRelationship,AdminSubscriptionScope,SoftDeletes;
    protected $primaryKey   = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'plan_id',
        'subscribed_at',
        'expires_at',
        'iap',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'subscribed_at',
        'expires_at',
        'created_at',
        'updated_at',
    ];

    /**
     * @var string[]
     */
    protected $with = [
        'user',
        'plan'
    ];

}
