<?php

namespace App\Domains\AdminSubscription\Models\Traits\Scope;

/**
 * Class PatientScope.
 */
trait AdminSubscriptionScope
{
    /**
     * @param $query
     * @param $term
     *
     * @return mixed
     */
    public function scopeSearch($query, $term)
    {
        return $query->with('user')
            ->whereHas('user', function ($query) use ($term) {
                $query->where('first_name', 'ilike', '%' . $term . '%')
                    ->orWhere('last_name', 'ilike', '%' . $term . '%');
            });

    }

}
