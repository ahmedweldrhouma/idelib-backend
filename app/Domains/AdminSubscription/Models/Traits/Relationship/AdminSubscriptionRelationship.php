<?php

namespace App\Domains\AdminSubscription\Models\Traits\Relationship;

use App\Domains\Auth\Models\User;
use App\Domains\Plan\Models\Plan;

/**
 * Class AdminSubscriptionRelationship.
 */
trait AdminSubscriptionRelationship
{
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id')->withTrashed();
    }

    public function plan()
    {
        return $this->belongsTo(Plan::class, 'plan_id');
    }

}
