<?php

namespace App\Domains\AdminSubscription\Http\Requests\Backend;

use App\Domains\AdminSubscription\Models\AdminSubscription;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class StoreAdminSubscriptionRequest.
 */
class StoreAdminSubscriptionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => ['required'],
            'plan_id' => ['required'],
            'subscribed_at' => ['required'],
            'expires_at' => ['required']
        ];
    }

}
