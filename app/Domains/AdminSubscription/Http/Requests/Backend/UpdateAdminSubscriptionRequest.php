<?php

namespace App\Domains\AdminSubscription\Http\Requests\Backend;

use App\Domains\AdminSubscription\Models\AdminSubscription;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class UpdateAdminSubscriptionRequest.
 */
class UpdateAdminSubscriptionRequest extends FormRequest
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'plan_id' => ['required'],
            'subscribed_at' => ['required'],
            'expires_at' => ['required']
        ];
    }


    /**
     * Handle a failed authorization attempt.
     *
     * @return void
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    protected function failedAuthorization()
    {
        throw new AuthorizationException(__('Seul l\'administrateur peut mettre à jour cet abonnement.'));
    }
}
