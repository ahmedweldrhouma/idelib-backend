<?php

namespace App\Domains\Auth\Services;

use App\Domains\Auth\Events\User\UserCreated;
use App\Domains\Auth\Events\User\UserDeleted;
use App\Domains\Auth\Events\User\UserDestroyed;
use App\Domains\Auth\Events\User\UserRestored;
use App\Domains\Auth\Events\User\UserStatusChanged;
use App\Domains\Auth\Events\User\UserUpdated;
use App\Domains\Auth\Models\User;
use App\Domains\Team\Services\TeamService;
use App\Exceptions\GeneralException;
use App\Services\BaseService;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class UserService.
 */
class UserService extends BaseService
{
    /**
     * UserService constructor.
     *
     * @param User $user
     * @param TeamService $teamService
     */
    public function __construct(User $user, TeamService $teamService)
    {
        $this->model = $user;
        $this->teamService = $teamService;
    }

    /**
     * @param $type
     * @param bool|int $perPage
     * @return mixed
     */
    public function getByType($type, $perPage = false)
    {
        if (is_numeric($perPage)) {
            return $this->model::byType($type)->paginate($perPage);
        }
        return $this->model::byType($type)->get();
    }


    /**
     * @param $type 'mensuel' ou 'annuel'
     * @param bool|int $perPage
     * @return mixed
     */
    public function getByTypeAbo($type, $typeAbo)
    {

        return $this->model::byType($type)->with('activeSubscription')->get();
    }

    /**
     * @param $email
     *
     * @return mixed
     */
    public function getByEmail($email)
    {
        return $this->model::ByEmail($email)->first();
    }

    public function getColor()
    {
        return $this->get('color');
    }

    /**
     * @param $app_user_id
     *
     * @return mixed
     */
    public function getByAppUserId($app_user_id)
    {
        return $this->model::withTrashed()->where('app_user_id', $app_user_id)->first();
    }

    /**
     * @param array $data
     * @return mixed
     *
     * @throws HttpException
     */
    public function registerUser(array $data = []): User
    {
        DB::beginTransaction();
        try {
            $data['email_verified_at'] = Carbon::now()->format('Y-m-d H:i:s');
            $data['api_token'] = Str::random(80);
            $data['app_user_id'] = 'RC' . substr(str_shuffle(MD5(microtime())), 0, 8);;
            $user = $this->createUser($data);
        } catch (Exception $e) {
            DB::rollBack();
            throw new HttpException(422, $e->getMessage());
            throw new HttpException(500, 'Un problème est survenu lors de la création de votre compte. Veuillez réessayer.' . $e->getMessage());
        }

        DB::commit();

        return $user;
    }

    /**
     * @param $info
     * @param $provider
     * @return mixed
     *
     * @throws GeneralException
     */
    public function registerProvider($info, $provider): User
    {
        $user = $this->model::where('provider_id', $info->id)->first();

        if (!$user) {
            DB::beginTransaction();

            try {
                $user = $this->createUser([
                    'name' => $info->name,
                    'email' => $info->email,
                    'provider' => $provider,
                    'provider_id' => $info->id,
                    'email_verified_at' => now(),
                ]);
            } catch (Exception $e) {
                DB::rollBack();
                throw new HttpException(422, $e->getMessage());
                throw new GeneralException(__('There was a problem connecting to :provider', ['provider' => $provider]));
            }

            DB::commit();
        }

        return $user;
    }

    /**
     * @param array $data
     * @return User
     *
     * @throws GeneralException
     * @throws \Throwable
     */
    public function store(array $data = []): User
    {
        /*// Stockez toutes les lettres possibles dans une chaîne.
            $str = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $randomStr = '';
            $id_subscription='rc';

            // Générez une chaine de caractéres aléatoirement
            for ($i = 0; $i < 8; $i++) {
                $index = rand(0, strlen($str) - 1);
                $id_subscription .= $str[$index];
            }*/
        $id_subscription = 'RC' . substr(str_shuffle(MD5(microtime())), 0, 8);
        DB::beginTransaction();
        try {
            if (isset($data['avatar_location'])) {
                $avatar_location = $data['avatar_location']->store('avatars/', 's3');
                $data['avatar_location'] = $avatar_location;
            }
            if (isset($data['user_type'])) {
                $data['type'] = $data['user_type'];
            }

            if (is_array($data['tour_sector'])) {
                $data['tour_sector'] = implode(',', $data['tour_sector']);
            }

            $data['email_verified_at'] = Carbon::now()->format('Y-m-d H:i:s');
            $data['api_token'] = Str::random(80);
            $data['active'] = '1';
            $data['app_user_id'] = $id_subscription;
            $user = $this->createUser([
                'type' => $data['type'],
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'email' => $data['email'],
                'password' => $data['password'],
                'phone' => $data['phone'] ?? null,
                'app_user_id' => $data['app_user_id'],
                'adeli_number' => $data['adeli_number'] ?? null,
                'tour_sector' => $data['tour_sector'] ?? null,
                'longitude' => $data['longitude'] ?? null,
                'latitude' => $data['latitude'] ?? null,
                'api_token' => $data['api_token'] ?? null,
                'email_verified_at' => $data['email_verified_at'] ?? null,
                'birth_date' => isset($data['birth_date']) ? Carbon::parse($data['birth_date'])->format('Y-m-d') : null,
                'active' => isset($data['active']) && $data['active'] === '1',
                'avatar_location' => $avatar_location ?? null,
                'address' => $data['address'] ?? null,
                'created_by' => Auth::id() ?? null
            ]);

            $user->syncRoles($data['roles'] ?? []);

            if (!config('boilerplate.access.user.only_roles')) {
                $user->syncPermissions($data['permissions'] ?? []);
            }

            if ($user->isBiller()) {
                $user->sendBillerAccessNotification($data['password']);
            }

        } catch (Exception $e) {
            DB::rollBack();
            throw new HttpException(422, $e->getMessage());
            throw new GeneralException(__('There was a problem creating this user. Please try again.'));
        }

        event(new UserCreated($user));

        DB::commit();

        // They didn't want to auto verify the email, but do they want to send the confirmation email to do so?
        if (!isset($data['email_verified']) && isset($data['send_confirmation_email']) && $data['send_confirmation_email'] === '1') {
            $user->sendEmailVerificationNotification();
        }

        return $user;
    }

    /**
     * @param User $user
     * @param array $data
     * @return User
     *
     * @throws \Throwable
     */
    public function update(User $user, array $data = []): User
    {
        DB::beginTransaction();

        try {
            $avatar_location = $user->avatar_location;
            if (isset($data['avatar_location']) && $data['avatar_location'] != $user->avatar_location) {
                Storage::disk('s3')->delete($avatar_location);
                $avatar_location = $data['avatar_location']->store('avatars', 's3');
            }
            if (is_array($data['tour_sector'])) {
                $data['tour_sector'] = implode(',', $data['tour_sector']);
            }
            $user->update([
                'type' => $user->isMasterAdmin() ? $this->model::TYPE_SUPER_ADMIN : $data['type'] ?? $user->type,
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'email' => $data['email'],
                'phone' => $data['phone'],
                'adeli_number' => $data['adeli_number'],
                'avatar_location' => $avatar_location,
                'longitude' => $data['longitude'] ?? null,
                'latitude' => $data['latitude'] ?? null,
                'birth_date' => Carbon::parse($data['birth_date'])->format('Y-m-d') ?? null,
                'address' => $data['address'] ?? null,
                'tour_sector' => $data['tour_sector'] ?? null,
            ]);

        } catch (Exception $e) {
            // dd($e);
            DB::rollBack();
            throw new HttpException(422, $e->getMessage());
            if (request()->is('api/*')) {
                throw new HttpException(500, __('There was a problem updating this user. Please try again.'));
                // throw new HttpException(500, $e->getMessage());
            } else {
                throw new GeneralException(__('There was a problem updating this user. Please try again.'));
            }
        }

        event(new UserUpdated($user));

        DB::commit();

        return $user;
    }

    /**
     * @param User $user
     * @param array $data
     * @return User
     */
    public function updateProfile(User $user, array $data = []): User
    {
        $user->first_name = $data['first_name'] ?? null;
        $user->last_name = $data['last_name'] ?? null;
        $user->phone = $data['phone'] ?? null;
        $user->address = $data['address'] ?? null;
        $user->birth_date = $birth_date ?? null;
        $user->tour_sector = $data['tour_sector'] ?? null;
        $user->adeli_number = $data['adeli_number'] ?? null;
        $user->color = $data['color'] ?? $user->color;
        $user->rpps_number = $data['rpps_number'] ?? null;
        $user->longitude = $data['longitude'] ?? $user->longitude;
        $user->latitude = $data['latitude'] ?? $user->latitude;
        if (isset($data['avatar_location'])) {
            Storage::disk('s3')->delete($user->avatar_location);
            $user->avatar_location = $data['avatar_location']->store('avatars', 's3');
            $user->avatar_type = 'storage';
        }
        if ($user->isPatient() && isset($data['tour_sector'])) {
            $user->patient->zip_code = $data['tour_sector'];
            $user->patient->save();
        }

        return tap($user)->save();
    }

    /**
     * @param       $id
     * @param bool|UploadedFile $image
     *
     * @return array|bool
     * @throws HttpException
     */
    public function updateAvatar($id, $image)
    {
        $user = $this->getById($id);
        if ($image) {
            // $avatar_location = $image->storeAs('avatars', $data['first_name'].'_avatar_'.Carbon::now()->timestamp);
            Storage::disk('s3')->delete($user->avatar_location);
            $user->avatar_location = $image->store('avatars', 's3');
            $user->avatar_type = 'storage';
        }
        $user->save();
        return $user;
    }

    /**
     * @param User $user
     * @param $data
     * @param bool $expired
     * @return User
     *
     * @throws \Throwable
     */
    public function updatePassword(User $user, $data, $expired = false): User
    {
        // if (isset($data['current_password'])) {
        //     throw_if(
        //         ! Hash::check($data['current_password'], $user->password),
        //         new GeneralException(__('That is not your old password.'))
        //     );
        // }

        // Reset the expiration clock
        if ($expired) {
            $user->password_changed_at = now();
        }

        $user->password = $data['password'];

        return tap($user)->update();
    }

    /**
     * @param User $user
     * @param $status
     * @return User
     *
     * @throws GeneralException
     */
    public function mark(User $user, $status): User
    {
        if ($status === 0 && auth()->id() === $user->id) {
            throw new GeneralException(__('You can not do that to yourself.'));
        }

        if ($status === 0 && $user->isMasterAdmin()) {
            throw new GeneralException(__('You can not deactivate the administrator account.'));
        }

        $user->active = $status;

        if ($user->save()) {
            event(new UserStatusChanged($user, $status));

            return $user;
        }

        throw new GeneralException(__('There was a problem updating this user. Please try again.'));
    }

    /**
     * @param User $user
     * @return User
     *
     * @throws GeneralException
     */
    public function delete(User $user): User
    {
        /*        if ($user->id === auth()->id()) {
                    throw new GeneralException(__('You can not delete yourself.'));
                }*/

        if ($this->deleteById($user->id)) {
            event(new UserDeleted($user));

            return $user;
        }

        throw new GeneralException('There was a problem deleting this user. Please try again.');
    }

    /**
     * @param User $user
     * @return User
     *
     * @throws GeneralException
     */
    public function restore(User $user): User
    {
        if ($user->restore()) {
            event(new UserRestored($user));

            return $user;
        }

        throw new GeneralException(__('There was a problem restoring this user. Please try again.'));
    }

    /**
     * @param User $user
     * @return bool
     *
     * @throws GeneralException
     */
    public function destroy(User $user): bool
    {
        if ($user->forceDelete()) {
            event(new UserDestroyed($user));

            return true;
        }

        throw new GeneralException(__('There was a problem permanently deleting this user. Please try again.'));
    }

    /**
     * @param array $data
     * @return User
     */
    protected function createUser(array $data = []): User
    {
        return User::create([
            'type' => $data['type'] ?? $this->model::TYPE_USER,
            'first_name' => $data['first_name'] ?? null,
            'last_name' => $data['last_name'] ?? null,
            'email' => strtolower($data['email']) ?? null,
            'password' => $data['password'] ?? null,
            'phone' => $data['phone'] ?? null,
            'app_user_id' => $data['app_user_id'],
            'adeli_number' => $data['adeli_number'] ?? null,
            'birth_date' =>  Carbon::parse($data['birth_date'])->format('Y-m-d') ?? null,
            'email_verified_at' => $data['email_verified_at'] ?? null,
            'active' => $data['active'] ?? true,
            'avatar_location' => $data['avatar_location'] ?? null,
            'avatar_type' => $data['avatar_type'] ?? 'gravatar',
            'tour_sector' => $data['tour_sector'] ?? null,
            'address' => $data['address'] ?? null,
            'presentation' => $data['presentation'] ?? null,
            'api_token' => $data['api_token'] ?? null,
            'created_by' => $data['created_by'] ?? null,
            'rpps_number' => $data['rpps_number'] ?? null,
            'longitude' => $data['longitude'] ?? null,
            'latitude' => $data['latitude'] ?? null,
        ]);
    }


    /**
     * @param $email
     *
     * @return mixed
     */
    public function getNursesBySector()
    {
        $sector = Auth::user()->patient->zip_code;
        return $this->model::BySector($sector)->get();
    }

    /**
     * @param $email
     *
     * @return mixed
     */
    public function getNursesByPositions()
    {

        return $this->model::ByPosition()->get();
    }

    public function getAllForAutocomplete()
    {
        return $this->model->where('type', '!=', 'admin')
            ->where('type', '!=', 'biller')
            ->where('active', true)
            ->get();
    }

    public function search($str)
    {
        return $this->model->where('type', '=', User::TYPE_NURSE)
            ->where(function ($query) use ($str) {
                $query->where('last_name', 'ILIKE', "%" . strtolower($str) . "%")
                    ->orWhere('first_name', 'ILIKE', "%" . strtolower($str) . "%");
            })->get();
    }

    public function saveToken($user, $token): User
    {
        DB::beginTransaction();

        try {
            $user->fcm_token = $token;
            $user->save();

        } catch (Exception $e) {
            // dd($e);
            DB::rollBack();
            throw new HttpException(422, $e->getMessage());
            throw new HttpException(500, __('There was a problem updating this user. Please try again.'));
        }

        DB::commit();

        return $user;
    }

    /**
     * @param $id
     *
     * @throws HttpException
     */
    public function makeAdminOfTeam(User $user)
    {
        //Get the team of the user
        $team = $user->team()->first();

        //Update the admin id for this team
        $new_admin_team = $this->teamService->defineNewAdmin($team, $user->id);
        return $new_admin_team;
    }

    public function getMembersOfUserTeam()
    {
        $team = Auth::user()->team->first();
        $members = $team->members()->get();
        return $members;
    }

    public function getUsersMails()
    {
        $mails = $this->model->pluck('email');
        return $mails;
    }

    public function searchAdresse($adresse)
    {
        //https://nominatim.openstreetmap.org/search?q=45%20rue%20sainte&format=geojson&addressdetails=1&accept-language=fr&countrycodes=fr

        $baseUrl = 'https://nominatim.openstreetmap.org/search';
        $params = [
            'q' => htmlentities(removeAccent($adresse)),
            'format' => 'geojson',
            'addressdetails' => 1,
            'accept-language' => 'fr',
            'countrycodes' => 'fr'
        ];
        $response = Http::get($baseUrl, $params);

        $jsonData = $response->json();
        $tab = [];
        if (!empty($jsonData)) {
            foreach ($jsonData['features'] as $adresses) {
                $tab[] = array(
                    'display' =>
                        @$adresses['properties']['address']['house_number'] . ' ' .
                        @$adresses['properties']['address']['road'] . ' ' .
                        @$adresses['properties']['address']['postcode'] . ' ' .
                        @$adresses['properties']['address']['city'],
                    'lat' => $adresses['geometry']['coordinates'][1],
                    'lng' => $adresses['geometry']['coordinates'][0]
                );

            }
        }
        return $tab;
    }
}
