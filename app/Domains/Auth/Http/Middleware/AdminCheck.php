<?php

namespace App\Domains\Auth\Http\Middleware;

use App\Domains\Auth\Models\User;
use Closure;

/**
 * Class AdminCheck.
 */
class AdminCheck
{
    /**
     * @param $request
     * @param  Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user() &&
            ($request->user()->isType(User::TYPE_SUPER_ADMIN) ||
                $request->user()->isType(User::TYPE_ADMIN) ||
                $request->user()->isType(User::TYPE_NURSE) ||
                $request->user()->isType(User::TYPE_SUBSTITUTE_NURSE) ||
                $request->user()->isType(User::TYPE_BILLER))) {

            if (  ($request->user()->isType(User::TYPE_NURSE) ||
                $request->user()->isType(User::TYPE_SUBSTITUTE_NURSE) )  && !$request->user()->activeSubscription){
                //On verifie qu'il y a au moins un abonnement actif, sinon on redirige vers la page de prise d'abonnement

                return redirect()->route('frontend.auth.havetoSubscribe')->withFlashDanger(__('You have to subscribe to access this section'));
            }


            return $next($request);
        }

        return redirect()->route('frontend.index')->withFlashDanger(__('You do not have access to do that.'));
    }
}
