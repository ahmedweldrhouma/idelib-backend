<?php

namespace App\Domains\Auth\Http\Requests\Backend\User;

use App\Domains\Auth\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use LangleyFoxall\LaravelNISTPasswordRules\PasswordRules;

/**
 * Class StoreUserRequest.
 */
class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => ['required', Rule::in([
                User::TYPE_SUPER_ADMIN,
                User::TYPE_ADMIN,
                User::TYPE_PATIENT,
                User::TYPE_NURSE,
                User::TYPE_SUBSTITUTE_NURSE,
                User::TYPE_BILLER
            ])],
            'first_name' => ['required', 'max:100'],
            'last_name' => ['required', 'max:100'],
            'email' => ['required', 'max:255', 'email', Rule::unique('users')],
            'password' => ['max:100', PasswordRules::register($this->email)],
            'phone' => ['required', 'max:100'],
            'adeli_number' => ['sometimes', 'max:100'],
            'tour_sector' => ['sometimes', 'max:100'],
            'birth_date' => ['sometimes'],
            'tour_sector' => ['sometimes'],
            'longitude' => ['sometimes'],
            'latitude' => ['sometimes'],
            'active' => ['sometimes', 'in:1'],
            'address' => ['required'],
            'email_verified' => ['sometimes', 'in:1'],
            'send_confirmation_email' => ['sometimes', 'in:1'],
            'roles' => ['sometimes', 'array'],
            'roles.*' => [Rule::exists('roles', 'id')->where('type', $this->type)],
            'permissions' => ['sometimes', 'array'],
            'permissions.*' => [Rule::exists('permissions', 'id')->where('type', $this->type)],
            'avatar_type' => ['sometimes'],
            'avatar_location' => ['sometimes','mimes:jpg,bmp,png'],
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'roles.*.exists' => __('One or more roles were not found or are not allowed to be associated with this user type.'),
            'permissions.*.exists' => __('One or more permissions were not found or are not allowed to be associated with this user type.'),
        ];
    }
}
