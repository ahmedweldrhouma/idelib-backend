<?php

namespace App\Domains\Auth\Http\Controllers\Frontend\Auth;

use App\Domains\Auth\Models\User;
use App\Domains\Auth\Services\UserService;
use App\Domains\MobileSubscription\Models\MobileSubscription;
use App\Domains\MobileSubscription\Services\MobileSubscriptionService;
use App\Domains\Plan\Models\Plan;
use App\Domains\Plan\Services\PlanService;
use App\Http\Requests\Frontend\InscrRequest;
use App\Rules\Captcha;
use Cache;
use Carbon\Carbon;
use Exception;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use LangleyFoxall\LaravelNISTPasswordRules\PasswordRules;
use Stripe\PromotionCode;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class RegisterController.
 */
class RegisterController
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * @var UserService
     */
    protected $userService;

    /**
     * RegisterController constructor.
     *
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Where to redirect users after registration.
     *
     * @return string
     */
    public function redirectPath()
    {
        return route(homeRoute());
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showRegistrationForm()
    {
        //abort_unless(config('boilerplate.access.user.registration'), 404);

        return view('frontend.auth.register');
    }

    public function havetoSubscribe()
    {
        //abort_unless(config('boilerplate.access.user.registration'), 404);
        if (Auth::user() == null) {
            return redirect($this->redirectPath(route('frontend.auth.login')));
        }
        return view('frontend.auth.havetoSubscribe');
    }

    public function registerSubscription(Request $request)
    {
        DB::beginTransaction();

        $user = Auth::user();
        if ($user == null) {
            return redirect($this->redirectPath(route('frontend.auth.login')));
        }

        try {
            $data = $request->all();

            /*
            Remplaçant
            */
            if ($request->get('havetosubscribe') == 'remp') {
                if (isset($data['remp_promotional_code']) && !empty($data['remp_promotional_code'])) {
                    $promoCode = $data['remp_promotional_code'];
                }

                //On cherche le type de paiement avec l'offre correspondante
                $plan = PlanService::searchByName($data['remp_payment']);
            }




            /*
            Libéral
            */
            if ($request->get('havetosubscribe') == 'inf') {

                //On cherche le type de paiement avec l'offre correspondante
                $plan = PlanService::searchByName($data['lib_payment']);

                if (isset($data['lib_promotional_code']) && !empty($data['lib_promotional_code'])) {
                    $promoCode = $data['lib_promotional_code'];
                }
            }


            $subscription = $user->newSubscription($plan->product_name, $plan->name);


            if (!empty($promoCode)) {
                $subscription->withCoupon($promoCode);
            }

            $s = $subscription->create($request->payment_method);


            $s->update([
                'provider' => 'WEB',
                'plan_id' => $plan->id,
                'operating_system' => 'web version',
                'is_active' => true,
                'subscribed_at' => Carbon::now(),

            ]);


        } catch (Exception $e) {
            DB::rollBack();
            Log::warning('Erreur paiement ' . $e->getMessage());
            return redirect()->back()->withInput($data);

        }

        DB::commit();

        return view('frontend.user.registerConfirmation');

    }

    public function register(InscrRequest $request)
    {
        DB::beginTransaction();

        try {
            $data = $request->all();

            $createSubscription = false;
            //Création de l'utilisateur en base de données

            $promoCode = '';
            //On détermine le type de l'inscription
            if ($data['typeInsc'] == 'Infirmièr(e) libéral(e)') {
                $createSubscription = true;

                //Infirmiere libérale
                $createTab = [
                    'type' => 'nurse',
                    'tour_sector' => $data['lib_secteur'],
                    'first_name' => $data['lib_prenom'],
                    'last_name' => $data['lib_nom'],
                    'email' => $data['lib_mail'],
                    'password' => $data['lib_mdp'],
                    'phone' => $data['lib_telephone'],
                    'app_user_id' => null,
                    'adeli_number' => $data['lib_adeli'],
                    'birth_date' => $data['lib_birthday'],
                    'address' => $data['lib_adresse_inf_lib'],
                    'presentation' => $data['lib_prez_inf_liberal'],
                    'rpps_number' => $data['lib_rpps'],
                    'active' => 1,
                    'longitude' => $data['lib_lng'],
                    'latitude' => $data['lib_lat'],
                ];

                $user = $this->userService->registerUser($createTab);


                //On cherche le type de paiement avec l'offre correspondante
                $plan = PlanService::searchByName($data['lib_payment']);

                if (isset($data['lib_codepromo']) && !empty($data['lib_codepromo'])) {
                    $promoCode = $data['lib_codepromo'];
                }

            } elseif ($data['typeInsc'] == 'Infirmièr(e) remplaçant(e)') {

                $createSubscription = true;
                //Infirmiere remplaçante
                $createTab = [
                    'type' => 'substitute',
                    'birth_date' => null,
                    'first_name' => $data['remp_prenom'],
                    'last_name' => $data['remp_nom'],
                    'email' => $data['remp_mail'],
                    'password' => $data['remp_mdp'],
                    'phone' => $data['remp_telephone'],
                    'app_user_id' => null,
                    'adeli_number' => $data['remp_adeli'],
                    'active' => 1,
                    'longitude' => $data['remp_lng'],
                    'latitude' => $data['remp_lat'],


                ];

                $user = $this->userService->registerUser($createTab);

                if (isset($data['remp_promotional_code']) && !empty($data['remp_promotional_code'])) {
                    $promoCode = $data['remp_promotional_code'];
                }

                //On cherche le type de paiement avec l'offre correspondante
                $plan = PlanService::searchByName($data['remp_payment']);

            } elseif ($data['typeInsc'] == 'Patient(e)') {
                //Infirmiere remplaçante
                $createTab = [
                    'type' => 'patient',
                    'first_name' => $data['patient_prenom'],
                    'last_name' => $data['patient_nom'],
                    'email' => $data['patient_mail'],
                    'password' => $data['patient_mdp'],
                    'phone' => $data['patient_telephone'],
                    'app_user_id' => null,
                    'birth_date' => $data['patient_birthday'],
                    'address' => $data['patient_adresse'],
                    'active' => 1,


                ];

                $user = $this->userService->registerUser($createTab);


            }

            if ($createSubscription) {
                //Souscription sur stripe
                $subscription = $user->newSubscription($plan->product_name, $plan->name)
                    ->trialDays(env('STRIPE_TRIAL_DAYS', 7));


                if (!empty($promoCode)) {
                    $subscription->withCoupon($promoCode);
                }

                $s = $subscription->create($request->payment_method);


                $s->update([
                    'provider' => 'WEB',
                    'plan_id' => $plan->id,
                    'operating_system' => 'web version',
                    'is_active' => true,
                    'subscribed_at' => Carbon::now(),

                ]);
            }

            try {
                Mail::to($user->email)->send(new \App\Mail\WelcomeMail($user));
            } catch (\Exception $e) {
                Log::warning('Erreur mail ' . $e->getMessage());
            }

        } catch (Exception $e) {
            DB::rollBack();
            Log::warning('Erreur paiement ' . $e->getMessage());
            return redirect()->back()->withInput($data);

        }

        DB::commit();

        return view('frontend.user.registerConfirmation');
    }

    public function validatePromocode(Request $request)
    {
        try {

            \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
            $code = $request->get('code');
            $offre = $request->get('offre');
            $offre = PlanService::searchByName($offre);



            /*$coupons = Cache::remember('stripe-cache-coupons', 600, function () {
                // \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
                return \Stripe\Coupon::all(['expand' => ['data.applies_to']]);
            });*/

            $coupons = \Stripe\Coupon::all(['expand' => ['data.applies_to'],  'limit' => 100]);
           // $coupons = \Stripe\Coupon::retrieve( $code,  ['expand' => ['data.applies_to']]);

            $promoCodes = \Stripe\PromotionCode::all(["active" => true, 'expand' => ['data.coupon.applies_to']]);

            //  $coupons = \Stripe\Coupon::all(['expand' => ['data.applies_to']]);

            $tab = array('success' => false);

         //   $productId = \Stripe\Price::retrieve('price_1Nn1D3JB0RfkLVn1X7nnA7nH', ['expand' => ['data.product']]);

            $productId = \Stripe\Price::retrieve($offre->name, ['expand' => ['data.product']]);

            $tab['old_price'] = round($productId->unit_amount / 100, 2);
            $tab['old_price_men'] = round($tab['old_price'] / 12, 2);
            $tab['new_price'] = round($productId->unit_amount / 100, 2);
            $tab['new_price_men'] = round($tab['new_price'] / 12, 2);

            foreach ($coupons->data as $promoCode) {
                if ($promoCode->valid === true) {

                    if (strtolower($code) == strtolower($promoCode->name) ||
                        strtolower($code) == strtolower($promoCode->id)) {

                        // valid, on regarde si le code s'applique pour l'offre selectionnée
                        if ($promoCode->applies_to == null || in_array($productId->product, $promoCode->applies_to->products)) {
                            $tab['id'] = $promoCode->id;
                            $tab['success'] = true;
                            $tab['percent_off'] = $promoCode->percent_off;
                            $tab['amount_off'] = $promoCode->amount_off;
                            if (!empty($promoCode->percent_off))
                                $tab['new_price'] = round($productId->unit_amount * (1 - $promoCode->percent_off / 100) / 100, 2);

                            if (!empty($promoCode->amount_off))
                                $tab['new_price'] = round(($productId->unit_amount - $promoCode->amount_off) / 100, 2);

                            $tab['new_price_men'] = round($tab['new_price'] / 12, 2);
                        }


                    }
                }
            }



            if (!empty($tab)) {
                return response()->json($tab);
            }
            return 0;
        } catch (\Exception $e) {
            $tab['message'] = $e->getMessage().' '.$e->getLine();
            $tab['new_price'] = round($productId->unit_amount / 100, 2);
            return response()->json($tab);
        }
    }

    public function searchAddress(Request $request, UserService $userService)
    {
        //Search on https://nominatim.org/ adresse for lat / lng
        return response()->json($userService->searchAdresse($request->get('adresse')));

    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:100'],
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users')],
            'password' => array_merge(['max:100'], PasswordRules::register($data['email'] ?? null)),
            'terms' => ['required', 'in:1'],
            'g-recaptcha-response' => ['required_if:captcha_status,true', new Captcha],
        ], [
            'terms.required' => __('You must accept the Terms & Conditions.'),
            'g-recaptcha-response.required_if' => __('validation.required', ['attribute' => 'captcha']),
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\Domains\Auth\Models\User|mixed
     *
     * @throws \App\Domains\Auth\Exceptions\RegisterException
     */
    protected function create(array $data)
    {
        abort_unless(config('boilerplate.access.user.registration'), 404);

        return $this->userService->registerUser($data);
    }


}
