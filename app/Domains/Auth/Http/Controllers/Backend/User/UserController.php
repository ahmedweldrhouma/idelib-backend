<?php

namespace App\Domains\Auth\Http\Controllers\Backend\User;

use App\Domains\Auth\Http\Requests\Backend\User\DeleteUserRequest;
use App\Domains\Auth\Http\Requests\Backend\User\EditUserRequest;
use App\Domains\Auth\Http\Requests\Backend\User\StoreUserRequest;
use App\Domains\Auth\Http\Requests\Backend\User\UpdateStatusRequest;
use App\Domains\Auth\Http\Requests\Backend\User\UpdateUserRequest;
use App\Domains\Auth\Models\User;
use App\Domains\Auth\Services\PermissionService;
use App\Domains\Auth\Services\RoleService;
use App\Domains\Auth\Services\UserService;
use App\Domains\Invitation\Models\Invitation;
use App\Domains\Invitation\Services\InvitationService;
use App\Domains\MobileSubscription\Models\MobileSubscription;
use App\Domains\Team\Models\Team;
use App\Domains\Team\Models\TeamUser;
use App\Domains\Team\Services\TeamService;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use LangleyFoxall\LaravelNISTPasswordRules\PasswordRules;
use function Clue\StreamFilter\append;

/**
 * Class UserController.
 */
class UserController
{
    /**
     * @var UserService
     */
    protected $userService;

    /**
     * @var RoleService
     */
    protected $roleService;

    /**
     * @var PermissionService
     */
    protected $permissionService;
    /**
     * @var InvitationService
     */
    protected $invitationService;

    /**
     * UserController constructor.
     *
     * @param UserService $userService
     * @param RoleService $roleService
     * @param PermissionService $permissionService
     */
    public function __construct(UserService $userService, TeamService $teamService, InvitationService $invitationService, RoleService $roleService, PermissionService $permissionService)
    {
        $this->userService = $userService;
        $this->roleService = $roleService;
        $this->permissionService = $permissionService;
        $this->invitationService = $invitationService;
        $this->teamService = $teamService;
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('backend.auth.user.index');
    }


    /**
     * @return mixed
     */
    public function create()
    {
        return view('backend.auth.user.create')
            ->withRoles($this->roleService->get())
            ->withCategories($this->permissionService->getCategorizedPermissions())
            ->withGeneral($this->permissionService->getUncategorizedPermissions());
    }

    /*
    public function createBiller()
    {
    return view('backend.auth.user.create_biller');
    }
    */

    /**
     * @param StoreUserRequest $request
     * @return mixed
     *
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function store(StoreUserRequest $request)
    {
        $user = $this->userService->store($request->validated());
        return redirect()->back()->withFlashSuccess(__('L\'utilisateur a été crée avec succès.'));
    }

    /**
     * @param Request $request
     * @return mixed
     *
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function storeteamuser(Request $request)
    {
        $added_mails = 0;
        $exists_mails = 0;
        $invited_mails = 0;
        $arleady_in_team = 0;
        $has_invitation = 0;
        $error = 0;
        if ($request->email != null) {

            // ajout d'un facturier
            $user = $this->userService->getByEmail($request->email) ?? null;

            if ($user == null) {
                // to complete
                return redirect()->back()->withFlashSuccess(__('Une invitation a été envoyée par mail à cet utilisateur'));
            }
            $members_of_user_team = $this->userService->getMembersOfUserTeam();

            if ($members_of_user_team->contains($user)) {
                return redirect()->back()->withFlashWarning(__('L\'utilisateur est déja un membre dans cette équipe!'));
            }

            if ($user['type'] == 'biller') {
                // No one is biller in my team
                // Test if he is biller for another team or for this team

                if ($user->isBiller()) {
                    return redirect()->back()->withFlashWarning(__('l\'utilisateur est un facturier pour un autre équipe. '));
                } else {
                    return redirect()->back()->withFlashWarning(__('L\'utilisateur ne peut pas être un facturier.'));
                }

            } else {
                return redirect()->back()->withFlashWarning(__('L\'utilisateur ne peut pas être un facturier.'));
            }
        }
        if ($request->mails != null) {
            foreach (array_column(json_decode($request->mails), 'value') as $email) {
                $user = $this->userService->getByEmail($email);
                if ($user) {
                    if (Auth::user()->team->first()->getMemberOfTeam($user->id)) {
                        $exists_mails++;
                    } else if ($user->isMemberOfAnyTeam() && $user->isOwnerOfAnyTeam()) {
                        $arleady_in_team++;
                    } else if ($user->hasInvitationToTeam(Auth::user()->team->first()->id)) {
                        $has_invitation++;
                    } else {
                        try {
                            $invitation = $this->invitationService->invite(['email' => $email], Auth::user()->team->first()->id, $user->id);
                            $added_mails++;
                            Mail::to($email)->send(new \App\Mail\TeamInvitationMail(Auth::user()->team->first(), Auth::user()));
                        } catch (\Exception $e) {
                            dd($e->getMessage());
                            $error++;
                        }
                    }
                } else {
                    try {
                        $invitation = $this->invitationService->invite(['email' => $email], Auth::user()->team->first()->id);
                        Mail::to($email)->send(new \App\Mail\TeamInvitationMail(Auth::user()->team->first(), Auth::user()));
                        $invited_mails++;
                    } catch (\Exception $e) {
                        $error++;
                    }
                }
            }
        }
        die;
        return redirect()->back()->withFlashSuccess(__($added_mails . ' membres ajoutés et ' . $exists_mails . ' membres qui travaillent déja dans dautres équipes et ' . $invited_mails . ' membres invités par mail . ' . $has_invitation . ' utilisateur a deja une invitation en attente dans cette équipe'));
        // return dd($mails_users);
        // return redirect()->back()->withFlashSuccess(__('An invitation was sent to this member ! Thank you'));
    }

    /**
     * @param Request $request
     */
    public function createBiller(Request $request, Team $team)
    {
        $validated = $request->validate([
            'first_name' => ['required', 'max:100'],
            'last_name' => ['required', 'max:100'],
            'email' => ['required', 'max:255', 'email', Rule::unique('users')],
            'password' => ['max:100'],
        ]);

        if (Auth::user()->team()->first()->biller->first() != null) {

            return redirect()->back()->withFlashWarning(__('Il ya déja un facturier pour cette équipe.'));
        }
        $user = $this->userService->getByEmail($request->email) ?? null;
        if ($user != null) {
            if ($user['type'] == User::TYPE_BILLER) {
                if ($user->isBiller()) {
                    return redirect()->back()->withFlashWarning(__('l\'utilisateur est un facturier pour un autre équipe. '));
                } else {
                    return redirect()->back()->withFlashWarning(__('L\'utilisateur ne peut pas être un facturier.'));
                }
            } else {
                return redirect()->back()->withFlashWarning(__('L\'utilisateur ne peut pas être un facturier.'));
            }
        }
        $validated['type'] = User::TYPE_BILLER;
        $validated['birth_date'] = null;
        $user = $this->userService->registerUser($validated);

        if (!empty($user)) {
            $data = [
                'team_id' => $team->id,
                'user_id' => $user->id
            ];
            $this->teamService->storeteamuser($data);
        }
        return redirect()->route('admin.team.index')->withFlashSuccess(__('The biller has been successfully added'));
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function show(User $user)
    {
        return view('backend.auth.user.show')
            ->withUser($user);
    }

    /**
     * @param EditUserRequest $request
     * @param User $user
     * @return mixed
     */
    public function edit(EditUserRequest $request, User $user)
    {
        return view('backend.auth.user.edit')
            ->withUser($user)
            ->withRoles($this->roleService->get())
            ->withCategories($this->permissionService->getCategorizedPermissions())
            ->withGeneral($this->permissionService->getUncategorizedPermissions())
            ->withUsedPermissions($user->permissions->modelKeys());
    }

    /**
     * @param UpdateUserRequest $request
     * @param User $user
     * @return mixed
     *
     * @throws \Throwable
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $this->userService->update($user, $request->validated());
        return redirect()->route('admin.auth.user.edit', $user->id)->withUser($user)->withFlashSuccess(__('L\'utilisateur a été modifié avec succès.'));
    }

    /**
     * @param DeleteUserRequest $request
     * @param User $user
     * @return mixed
     *
     * @throws \App\Exceptions\GeneralException
     */
    public function destroy(DeleteUserRequest $request, User $user)
    {
        $this->userService->delete($user);

        return redirect()->route('admin.auth.user.deleted')->withFlashSuccess(__('L\'utilisateur a été supprimé avec succès.'));
    }

    /**
     * @param DeleteUserRequest $request
     * @param User $user
     * @return mixed
     *
     * @throws \App\Exceptions\GeneralException
     */
    public function delete(DeleteUserRequest $request, User $user)
    {
        $this->userService->destroy($user);

        return redirect()->route('admin.auth.user.index')->withFlashSuccess(__('L\'utilisateur a été supprimé avec succès.'));
    }

    public function getAllForAutocomplete()
    {
        $result = $this->userService->getAllForAutocomplete();
        $users = $result->map(function ($item, $key) {
            $type = $item->getTypeFormatted();
            $all = [];
            $all['id'] = $item->id;
            $all['value'] = $item->first_name . ' ' . $item->last_name;
            $all['label'] = $item->first_name . ' ' . $item->last_name . ' - ' . $type;
            return $all;
        });

        return response()->json($users);
    }

    public function search(Request $request)
    {
        $result = $this->userService->search($request->search);
        return response()->json($result);
    }

    public function searchTourSector(Request $request)
    {
        $search = $request->search;
        $zipCodes = include(resource_path('data/tourneList.php'));
        return collect($zipCodes)->filter(function ($item) use ($search) {
            return Str::startsWith($item, $search) !== false;
        })->toArray();
    }

    public function invoicesGetAll()
    {
        $user =  Auth::user();

        $subscriptions = MobileSubscription::where('user_id', $user->id)->get();

        $soon_expired = null;
        foreach ($subscriptions as $subscription) {
            $invoices = array(
                'id_subscription' => $subscription->id,
                'stripe_id' => $subscription->stripe_id,
                'invoices' => $user->invoices()->filter(function ($invoice) use ($subscription) {
                    return $invoice->subscription === $subscription->stripe_id;
                })
            );

            if ($subscription->expired_at != null)
                $soon_expired = $subscription->expired_at;


        }

        $tabNextInvoice =  null;
        $nextInvoice = $user->upcomingInvoice();
        if ($nextInvoice) {
            $tabNextInvoice['date'] = Carbon::parse($nextInvoice->created)->format('Y-m-d');
            $tabNextInvoice['amount'] = $nextInvoice->amount_due / 100;
        }

        //Is soon expired
        $message_soon_expired = null ;
        if ($soon_expired){
            $message_soon_expired = 'Votre abonnement prendra fin le '.Carbon::parse($soon_expired)->format('d-m-Y à H:i:s');
        }

        $oneAbo = true;
         if ($subscriptions->count() == 0){
             $oneAbo = false;
         }

        return view('backend.invoices.index', [
                'invoices' => $invoices ?? [],
                'tabNextInvoice' => $tabNextInvoice,
                'messageSoonExpired' => $message_soon_expired,
                'oneAbo' => $oneAbo

            ]
        );


    }

    public function unsubscribe(Request $request){
        $user =  Auth::user();
        $subscriptions = MobileSubscription::where('user_id', $user->id)->where('is_active', true)->with('plan')->first();

        if ($subscriptions) {
          $a =  $user->mobileSubscriptions()->first()->cancelSub();
            $message = 'Désabonnement pris en compte.';
            $request->session()->flash('message', $message);
        }
        else{
            $message = 'Aucun Abonnement actif';
            $request->session()->flash('message', $message);
        }
        return redirect()->back();
    }
}
