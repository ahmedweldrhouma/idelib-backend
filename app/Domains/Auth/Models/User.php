<?php

namespace App\Domains\Auth\Models;

use App\Domains\Auth\Models\Traits\Attribute\UserAttribute;
use App\Domains\Auth\Models\Traits\Method\UserMethod;
use App\Domains\Auth\Models\Traits\Relationship\UserRelationship;
use App\Domains\Auth\Models\Traits\Scope\UserScope;
use App\Domains\Auth\Notifications\Frontend\ResetPasswordNotification;
use App\Domains\Auth\Notifications\Api\ApiResetPasswordNotification;
use App\Domains\Auth\Notifications\Frontend\VerifyEmail;
use App\Domains\Auth\Notifications\Frontend\BillerAccessNotification;
use DarkGhostHunter\Laraguard\Contracts\TwoFactorAuthenticatable;
use DarkGhostHunter\Laraguard\TwoFactorAuthentication;
use Database\Factories\UserFactory;
use Illuminate\Auth\MustVerifyEmail as MustVerifyEmailTrait;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Lab404\Impersonate\Models\Impersonate;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Laravel\Cashier\Billable;

/**
 * Class User.
 */
class User extends Authenticatable implements MustVerifyEmail, TwoFactorAuthenticatable
{
    use HasApiTokens,
        HasFactory,
        HasRoles,
        Impersonate,
        MustVerifyEmailTrait,
        Notifiable,
        SoftDeletes,
        TwoFactorAuthentication,
        UserAttribute,
        UserMethod,
        UserRelationship,
        Billable,
        UserScope;

    public const TYPE_SUPER_ADMIN = 'super admin';
    public const TYPE_ADMIN = 'admin';
    public const TYPE_PATIENT = 'patient';
    public const TYPE_NURSE = 'nurse';
    public const TYPE_SUBSTITUTE_NURSE = 'substitute';
    public const TYPE_BILLER = 'biller';
    public const TYPE_USER = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'first_name',
        'last_name',
        'email',
        'email_verified_at',
        'password',
        'password_changed_at',
        'api_token',
        'active',
        'timezone',
        'last_login_at',
        'last_login_ip',
        'to_be_logged_out',
        'provider',
        'provider_id',
        'phone',
        'app_user_id',
        'adeli_number',
        'address',
        'birth_date',
        'tour_sector',
        'presentation',
        'subscription_type',
        'operating_system',
        'avatar_type',
        'avatar_location',
        'created_by',
        'color',
        'rpps_number',
        'longitude',
        'latitude',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'birth_date'=>"Y-m-d",
        'last_login_at',
        'email_verified_at',
        'password_changed_at',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'active' => 'boolean',
        'birth_date' => 'datetime',
        'last_login_at' => 'datetime',
        'email_verified_at' => 'datetime',
        'to_be_logged_out' => 'boolean',
    ];

    /**
     * @var array
     */
    protected $appends = [
        'avatar',
        'name',
    ];

    /**
     * @var string[]
     */
    protected $with = [
        //'team',
        // 'ratings'
    ];

    // /**
    //  * Send the password reset notification.
    //  *
    //  * @param  string  $token
    //  * @return void
    //  */
    // public function sendPasswordResetNotification($token): void
    // {
    //     $this->notify(new ResetPasswordNotification($token));
    // }
    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($user) {
            $user->mobileSubscriptions()->delete();
            $user->allAdminSubscription()->delete();
        });
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $code
     * @return void
     */
    public function sendPasswordResetNotification($code): void
    {
        $this->notify(new ResetPasswordNotification($code));
        /*if($this->isNurse()){
            $this->notify(new ResetPasswordNotification($code));
        }else{
            $this->notify(new ApiResetPasswordNotification($code, $this));
        }*/
    }

    /**
     * Send the registration verification email.
     */
    public function sendEmailVerificationNotification(): void
    {
        $this->notify(new VerifyEmail);
    }

    /**
     * Send the registration verification email.
     */
    public function sendBillerAccessNotification($password): void
    {
     //   $this->notify(new BillerAccessNotification($password));
    }

    /**
     * Return true or false if the user can impersonate an other user.
     *
     * @param void
     * @return bool
     */
    public function canImpersonate(): bool
    {
        return $this->can('admin.access.user.impersonate');
    }

    /**
     * Return true or false if the user can be impersonate.
     *
     * @param void
     * @return bool
     */
    public function canBeImpersonated(): bool
    {
        return ! $this->isMasterAdmin();
    }

    /**
     * Create a new factory instance for the model.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    protected static function newFactory()
    {
        return UserFactory::new();
    }
}
