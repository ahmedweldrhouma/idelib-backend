<?php

namespace App\Domains\Auth\Models\Traits\Method;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Domains\Team\Models\TeamUser;

/**
 * Traits UserMethod.
 */
trait UserMethod
{
    /**
     * @return bool
     */
    public function isMasterAdmin(): bool
    {
        return $this->id === 1;
    }

    /**
     * @return mixed
     */
    public function isSuperAdmin(): bool
    {
        return $this->type === self::TYPE_SUPER_ADMIN;
    }

    /**
     * @return mixed
     */
    public function isAdmin(): bool
    {
        return $this->type === self::TYPE_ADMIN;
    }

    /**
     * @return mixed
     */
    public function isNurse(): bool
    {
        return $this->type === self::TYPE_NURSE;
    }

    /**
     * @return mixed
     */
    public function isSubstituteNurse(): bool
    {
        return $this->type === self::TYPE_SUBSTITUTE_NURSE;
    }

    /**
     * @return mixed
     */
    public function isPatient(): bool
    {
        return $this->type === self::TYPE_PATIENT;
    }

    /**
     * @return mixed
     */
    public function isBiller(): bool
    {
        return $this->type === self::TYPE_BILLER;
    }

    /**
     * @return mixed
     */
    public function hasAllAccess(): bool
    {
        return $this->isSuperAdmin() && $this->hasRole(config('boilerplate.access.role.admin'));
    }

    /**
     * @param $type
     * @return bool
     */
    public function isType($type): bool
    {
        return $this->type === $type;
    }

    /**
     * @return mixed
     */
    public function canChangeEmail(): bool
    {
        return config('boilerplate.access.user.change_email');
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @return bool
     */
    public function isVerified(): bool
    {
        return $this->email_verified_at !== null;
    }

    /**
     * @return bool
     */
    public function isSocial(): bool
    {
        return $this->provider && $this->provider_id;
    }

    /**
     * @return Collection
     */
    public function getPermissionDescriptions(): Collection
    {
        return $this->permissions->pluck('description');
    }

    /**
     * @param  bool  $size
     * @return mixed|string
     *
     * @throws \Creativeorange\Gravatar\Exceptions\InvalidEmailException
     */
    public function getAvatar($size = null)
    {
        if($this->avatar_location == "") {
            return asset("img/default-avatar.png");
        }
        return Storage::disk('s3')->url($this->avatar_location);
    }

    public function getFullName()
    {
        return $this->first_name.' '. $this->last_name;
    }

    public function getColor()
    {
        return $this->color??"#4287f5";
    }

    public function getPicture($size = false)
    {
        if($this->avatar_location == "") {
            return asset("img/avatar.png");
        }
        return Storage::disk('s3')->url($this->avatar_location);
    }

    public function isOwnerOfTeam($team_id)
    {
        return ($this->teams()
            ->where('admin_id', $this->id)
            ->where('team_id', $team_id)->first()
        ) ? true : false;
    }

    public function isAdminOfTeam($team_id)
    {
        return ($this->team()
            ->where('is_admin',true)
            ->where('team_id', $team_id)->first()
        ) ? true : false;
    }

    public function isMemberOfTeam($team_id)
    {
        return ($this->teams()
            ->where('user_id', $this->id)
            ->where('team_id', $team_id)->first()
        ) ? true : false;
    }

    public function isMemberOfAnyTeam()
    {
        return ($this->team()->first()
        ) ? true : false;
    }
    public function isOwnerOfAnyTeam()
    {
        return (bool)$this->ownedTeam()->first();
    }

    public function getMemberColor($team_id)
    {
        return $this->team()
            ->where('user_id', $this->id)
            ->where('team_id', $team_id)
            ->first();
    }

    public function hasInvitationToTeam($team_id)
    {
        return ($this->invitations()
            ->where('invited_id', $this->id)
            ->where('team_id', $team_id)
            ->first()
        ) ? true : false;;
    }

    public function getTypeFormatted()
    {
        $type = '';
        switch ($this->type) {
            case 'nurse':
                $type = 'Infirmier';
                break;
            case 'substitute':
                $type = 'Remplaçant';
                break;
            case 'patient':
                $type = 'Patient';
                break;

            default:
                $type = '';
                break;
        }

        return $type;
    }

    public function isOwnerOfAnnouncement($announcement_id)
    {
        return ($this->announcements()
            ->where('user_id', $this->id)
            ->where('id', $announcement_id)->first()
        ) ? true : false;
    }

    public function isOwnerOfConversation($conversationId)
    {
        return (bool)$this->conversations()
            ->where('user_id', $this->id)
            ->where('id', $conversationId)->first();
    }

    public function isMemberOfConversation($conversation_id)
    {
        return (bool)$this->participants()
            ->where('participant_id', $this->id)
            ->where('conversation_id', $conversation_id)->first();
    }
}
