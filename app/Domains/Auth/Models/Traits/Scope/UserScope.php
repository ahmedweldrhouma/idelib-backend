<?php

namespace App\Domains\Auth\Models\Traits\Scope;

use App\Domains\Auth\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class UserScope.
 */
trait UserScope
{
    /**
     * @param $query
     * @param $term
     * @return mixed
     */
    public function scopeSearch($query, $term)
    {
        return $query->where(function ($query) use ($term) {
            $query->where('first_name', 'like', '%' . $term . '%')
                ->orWhere('last_name', 'like', '%' . $term . '%')
                ->orWhere('email', 'like', '%' . $term . '%')
                ->orWhere('address', 'like', '%' . $term . '%')
                ->orWhere('created_at', 'like', '%' . $term . '%')
                ->orWhere('operating_system', 'like', '%' . $term . '%')
                ->orWhere('subscription_type', 'like', '%' . $term . '%')
                ->orWhere('phone', 'like', '%' . $term . '%');
        });
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeOnlyDeactivated($query)
    {
        return $query->whereActive(false);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeOnlyActive($query)
    {
        return $query->whereActive(true);
    }

    /**
     * @param $query
     * @param $type
     * @return mixed
     */
    public function scopeByType($query, $type)
    {
        return $query->where('type', $type);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeAllAccess($query)
    {
        return $query->whereHas('roles', function ($query) {
            $query->where('name', config('boilerplate.access.role.admin'));
        });
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeByPlan($query,$plan)
    {
        return $query->whereHas('mobileSubscriptions', function ($query) use ($plan) {
            $query->where('plan_id', $plan);
        })->orWhereHas('adminSubscription', function ($query) use ($plan) {
            $query->where('plan_id', $plan);
        });
    }
    /**
     * @param $query
     * @return mixed
     */
    public function scopebySubscription($query,$subscriptionType)
    {
        if ($subscriptionType == "adminSubscription"){
            return $query->WhereHas('adminSubscription');
        }else if($subscriptionType == "mobileSubscription"){
            return $query->whereHas('mobileSubscriptions');
        }
        return $query;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeAdmins($query)
    {
        return $query->where('type', $this::TYPE_ADMIN);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeUsers($query)
    {
        return $query->where('type', $this::TYPE_USER);
    }

    /**
     * @param $query
     * @param $email
     *
     * @return mixed
     */
    public function scopeByEmail($query, $email)
    {
        return $query->where('email', $email);
    }

    /**
     * @param $query
     * @param $term
     * @return mixed
     */
    public function scopeBySector($query, $sector)
    {
        return $query->Where('tour_sector', 'ilike', '%' . $sector . '%')
            ->where('type', 'nurse')->where(function ($q) {
                $q->whereHas('activeSubscription')->orWhereHas('adminSubscription');
            });
    }
    /**
     * @param $query
     * @param $term
     * @return mixed
     */
    public function scopeByPosition($query)
    {
        $distance = 5;  //radius in kilometers
        $longitude = Auth::user()->longitude;
        $latitude = Auth::user()->latitude;
        [$maxLon,$minLon,$maxLat,$minLat] = self::getDegParams($latitude,$longitude);
        return $query->whereBetween('latitude', [$minLat, $maxLat])
            ->whereBetween('longitude', [$minLon,$maxLon])
            ->where('type', 'nurse')->where(function ($q) {
                $q->whereHas('activeSubscription')->orWhereHas('adminSubscription');
            });
    }
    public static function getDegParams ($lat,$lon)
    {
        $distance = 5; //your distance in KM
        $R = 6371; //constant earth radius. You can add precision here if you wish
        $maxLat = $lat + rad2deg($distance/$R);
        $minLat = $lat - rad2deg($distance/$R);
        $maxLon = $lon + rad2deg(asin($distance/$R) / cos(deg2rad($lat)));
        $minLon = $lon - rad2deg(asin($distance/$R) / cos(deg2rad($lat)));
        return [$maxLon,$minLon,$maxLat,$minLat];
    }
}
