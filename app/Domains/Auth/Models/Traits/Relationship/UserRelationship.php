<?php

namespace App\Domains\Auth\Models\Traits\Relationship;

use App\Domains\AdminSubscription\Models\AdminSubscription;
use App\Domains\Auth\Models\PasswordHistory;
use App\Domains\Conversation\Models\Conversation;
use App\Domains\Conversation\Models\ConversationUser;
use App\Domains\MobileSubscription\Models\MobileSubscription;
use App\Domains\Notification\Models\Notification;
use App\Domains\Patient\Models\Patient;
use App\Domains\Announcement\Models\Announcement;
use App\Domains\Application\Models\Application;
use App\Domains\Team\Models\Team;
use App\Domains\Invitation\Models\Invitation;
use App\Domains\Team\Models\TeamUser;
use App\Domains\Treatment\Models\TreatmentRequest;
use App\Domains\TreatmentComment\Models\TreatmentComment;
use App\Domains\Vaccine\Models\Vaccine;
use App\Domains\CovidTreatment\Models\CovidTreatment;
use App\Domains\Treatment\Models\Treatment;
use App\Domains\Auth\Models\User;
use App\Domains\Rating\Models\Rating;
use App\Domains\Appointment\Models\Appointment;
use App\Domains\Disponibilite\Models\Disponibilite;


/**
 * Class UserRelationship.
 */
trait UserRelationship
{
    /**
     * @return mixed
     */
    public function passwordHistories()
    {
        return $this->morphMany(PasswordHistory::class, 'model');
    }

    public function patient()
    {
        return $this->hasOne(Patient::class, 'user_id');
    }

    public function patients()
    {
        return $this->hasMany(Patient::class, 'nurse_id')->where('type', '!=', 'covid');
    }

    public function covidPatients()
    {
        return $this->hasMany(Patient::class, 'nurse_id')->where('type', 'covid');
    }

    public function announcements()
    {
        return $this->hasMany(Announcement::class, 'user_id');
    }

    public function applications()
    {
        return $this->hasMany(Application::class, 'user_id');
    }

    public function teams()
    {
        return $this->belongsToMany(Team::class)->using(TeamUser::class)->withPivot('color', 'id', 'is_admin');
    }

    public function ownedTeams()
    {
        return $this->belongsToMany(Team::class)->where('admin_id',$this->id)->using(TeamUser::class)->withPivot('color', 'id', 'is_admin');
    }

    public function team()
    {
        return $this->belongsToMany(Team::class)->using(TeamUser::class)->withPivot('color', 'id', 'is_admin')->withTimestamps();;
    }

    public function ownedTeam()
    {
        return $this->hasOne(Team::class, 'admin_id');
    }
    public function ownerOfTeam()
    {
        return $this->hasMany(Team::class, 'admin_id');
    }

    public function invitations()
    {
        return $this->hasMany(Invitation::class, 'invited_id');
    }

    public function treatmentRequests()
    {
        return $this->hasMany(TreatmentRequest::class, 'nurse_id');
    }

    public function vaccines()
    {
        return $this->hasMany(Vaccine::class, 'user_id');
    }

    public function covidTreatments()
    {
        return $this->hasMany(CovidTreatment::class, 'user_id');
    }

    public function covidTreatmentsType()
    {
        return $this->hasMany(Treatment::class, 'nurse_id')->where('type', 'covid');
    }

    public function treatments()
    {
        return $this->hasMany(Treatment::class, 'nurse_id')->where('type', '!=', 'covid')->orWhere('type', null);
    }

    public function treatmentsAll()
    {
        return $this->hasMany(Treatment::class, 'nurse_id')->where('type', '!=', 'covid');
    }

    public function billers()
    {
        return $this->hasMany(User::class, 'created_by')->where('type', 'biller');
    }

    public function nurse()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function ratings()
    {
        return $this->hasMany(Rating::class, 'nurse_id');
    }
    public function patientRating()
    {
        return $this->hasMany(Rating::class, 'nurse_id')->where('user_id',\Auth::id());
    }

    public function appointments()
    {
        return $this->hasMany(Appointment::class, 'nurse_id');
    }

    public function disponibilites()
    {
        return $this->hasMany(Disponibilite::class, 'nurse_id');
    }
    public function activeSubscription()
    {
        return $this->HasOne(MobileSubscription::class, 'user_id')->where('is_active',1);
    }

    public function mobileSubscriptions()
    {
        return $this->HasOne(MobileSubscription::class, 'user_id');
    }

    public function adminSubscriptions()
    {
        return $this->HasOne(AdminSubscription::class, 'user_id');
    }
    public function adminSubscription()
    {
        return $this->HasOne(AdminSubscription::class, 'user_id')->where('subscribed_at',"<=",today())->where('expires_at',">=",today());
    }

    public function allAdminSubscription()
    {
        return $this->HasOne(AdminSubscription::class, 'user_id');
    }
    public function treatmentComment()
    {
        return $this->hasMany(TreatmentComment::class, 'user_id');
    }

    public function notification()
    {
        return $this->hasMany(Notification::class, 'receiver_id');
    }
    public function conversations()
    {
        return $this->hasMany(Conversation::class,'user_id');
    }
    public function participants()
    {
        return $this->belongsToMany(Conversation::class,'conversation_user','participant_id','conversation_id')->withPivot( 'deleted_at')->wherePivot('deleted_at', '=', null);
    }
}
