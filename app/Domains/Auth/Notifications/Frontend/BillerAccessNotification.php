<?php

namespace App\Domains\Auth\Notifications\Frontend;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;
use App\Domains\Auth\Services\UserService;

/**
 * Class BillerAccessNotification.
 */
class BillerAccessNotification extends Notification
{
    /**
     * The password reset token.
     *
     * @var string
     */
    public $password;

    /**
     * Create a notification instance.
     *
     * @param  string  $password
     * @return void
     */
    public function __construct($password)
    {
        $this->password = $password;
    }

    /**
     * Get the notification's channels.
     *
     * @param  mixed  $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */

    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject(__('Creattion d\'un compte facturier'))
            ->line('Votre compte a été créer avec succés')
            ->line('Vous pouvez utiliser ce mot de passe pour acceder a votre compte.')
            ->line(new HtmlString('<p style="text-align:center;color:#3d4852;"><strong>' . $this->password .'</strong></p>'));
    }
}
