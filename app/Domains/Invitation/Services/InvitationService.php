<?php

namespace App\Domains\Invitation\Services;

use App\Domains\Invitation\Models\Invitation;
use App\Exceptions\GeneralException;
use Illuminate\Support\Arr;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Services\BaseService;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

/**
 * Class InvitationService.
 */
class InvitationService extends BaseService
{
    /**
     * InvitationService constructor.
     *
     * @param  Invitation  $invitation
     */
    public function __construct(Invitation $invitation)
    {
        $this->model = $invitation;
    }


    /**
     * @param  array  $data
     * @return Invitation
     *
     * @throws GeneralException
     * @throws \Throwable
     */
    public function invite(array $data = [], $team_id, $invited_id=null)
    {

        DB::beginTransaction();

        try {
            // $has_invitation = $this->model->byEmail($data['email'])->get();

            // // if($has_invitation->isNotEmpty()){
            // //     throw new HttpException(422,'Cet utilisateur a déjà une invitation en attente.');
            // //     // return response()->json(['error' => 'Cet utilisateur a déjà une invitation en attente'], 422);
            // // }
            $token = Str::random(80);

            $invitation = $this->model::create([
                "type"      => 'invite',
                "email"     => $data['email'],
                "token"     => $token,
                "user_id"   => Auth::id(),
                "team_id"   => $team_id,
                "invited_id"   => $invited_id,
            ]);

        } catch (Exception $e) {
            DB::rollback();
            throw new HttpException(422,  $e->getMessage());
            throw new HttpException(500,'Un problème est survenu lors de la création de l\'invitaion. Veuillez réessayer.');
        }

        DB::commit();

        return $invitation;
    }

    public function getPendingInvitations()
    {
        return $this->model->byEmail(Auth::user()->email)->get();
    }

    public function getInvitationbyToken($token)
    {
        return $this->model->byToken($token)->first();
    }
    public function getInvitationbyEmail($email)
    {
        return $this->model->where('email',$email)->first();
    }

    public function acceptInvite($token)
    {
        $allColors = collect(["#4287F5","#a5d6a7","#ff9800","#607d8b","#f06292","#ffeb3b","#ab47bc","#1b5e20"]);
        $invite = $this->model->byToken($token)->first();
        if($invite && !Auth::user()->team->first()){
            $user = Auth::user();
            $colors = $invite->team->members()->pluck('users.color');
            if ($colors->contains($user->color)) {
                $available = $allColors->except($colors);
                if($available->isNotEmpty()) {
                    $user->color = $available->random();
                    $user->save();
                }
            }
            $user->team()->attach($invite->team_id);
            return $invite->delete();
        }else{
            throw new HttpException(422,'cette invitation n\'est plus disponible');
        }

    }

    public function denyInvite($token)
    {
        $invite = $this->model->byToken($token)->first();
        $user = Auth::user();
        if($invite){
            $user->team()->detach($invite->team_id);

            return $invite->delete();
        }else{
            throw new HttpException(422,'cette invitation n\'est plus disponible');
            // return response()->json(['error' => 'cette invitation n\'est plus disponible'], 422);
        }

    }

}
