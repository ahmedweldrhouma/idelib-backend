<?php

namespace App\Domains\Invitation\Models\Traits\Scope;

/**
 * Class InvitationScope.
 */
trait InvitationScope
{

    /**
     * @param $query
     * @param $email
     *
     * @return mixed
     */
    public function scopeByEmail($query, $email)
    {
        return $query->where('email', $email);
    }

    /**
     * @param $query
     * @param $email
     *
     * @return mixed
     */
    public function scopeByToken($query, $token)
    {
        return $query->where('token', $token);
    }
}
