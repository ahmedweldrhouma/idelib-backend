<?php

namespace App\Domains\Invitation\Models\Traits\Relationship;

use App\Domains\Auth\Models\User;
use App\Domains\Team\Models\Team;

/**
 * Class InvitationRelationship.
 */
trait InvitationRelationship
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function team()
    {
        return $this->belongsTo(Team::class);
    }
}
