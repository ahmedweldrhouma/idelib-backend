<?php

namespace App\Domains\Invitation\Models;

use Illuminate\Database\Eloquent\Model;
use App\Domains\Invitation\Models\Traits\Relationship\InvitationRelationship;
use App\Domains\Invitation\Models\Traits\Scope\InvitationScope;

/**
 * Class Invitation.
 */
class Invitation extends Model
{
    use InvitationRelationship,
        InvitationScope;

    protected $primaryKey   = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'email',
        'token',
        'user_id',
        'team_id',
        'invited_id'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

}
