<?php

namespace App\Domains\Treatment\Models;

use Illuminate\Database\Eloquent\Model;
// use App\Domains\Treatment\Models\Traits\Attribute\TreatmentRequestAttribute;
// use App\Domains\Treatment\Models\Traits\Method\TreatmentRequestMethod;
use App\Domains\Treatment\Models\Traits\Relationship\TreatmentRequestRelationship;
use App\Domains\Treatment\Models\Traits\Scope\TreatmentRequestScope;

/**
 * Class TreatmentRequest.
 */
class TreatmentRequest extends Model
{
    use TreatmentRequestRelationship,
        TreatmentRequestScope;

    protected $primaryKey   = 'id';    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'patient_id',
        'note',
        'tour_sector',
        'is_pending',
        'is_available',
        'nurse_id'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
    ];

}
