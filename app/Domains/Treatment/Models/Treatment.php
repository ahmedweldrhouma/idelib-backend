<?php

namespace App\Domains\Treatment\Models;

use Illuminate\Database\Eloquent\Model;
use App\Domains\Treatment\Models\Traits\Attribute\TreatmentAttribute;
use App\Domains\Treatment\Models\Traits\Method\TreatmentMethod;
use App\Domains\Treatment\Models\Traits\Relationship\TreatmentRelationship;
use App\Domains\Treatment\Models\Traits\Scope\TreatmentScope;

/**
 * Class Treatment.
 */
class Treatment extends Model
{
    use TreatmentMethod,
        TreatmentRelationship,
        TreatmentScope;


    public const TYPE_CLASSIC = 'Classic';
    public const TYPE_DSI = 'dsi';
    public const TYPE_BSI = 'bsi';

    protected $primaryKey   = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'period',
        'start_at',
        'end_at',
        'daily_frequency',
        'monthly_frequency',
        'type',
        'classic_type',
        'bsi_type',
        'bsi_interval',
        'care_performed',
        'treatment',
        'stoped_at',
        'patient_id',
        'nurse_id',
        'absent_from',
        'absent_to',
        'prescription'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'start_at',
        'end_at',
        'stoped_at',
    ];

    /**
     * @var string[]
     */
    protected $with = [
        'patient',
        'passages'
    ];

}
