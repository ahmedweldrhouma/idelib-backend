<?php

namespace App\Domains\Treatment\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TreatmentRequest.
 */
class PatientAbsence extends Model
{

    protected $primaryKey   = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'absent_from',
        'absent_to',
        'patient_id'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'absent_from',
        'absent_to',
    ];

}
