<?php

namespace App\Domains\Treatment\Models\Traits\Method;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

/**
 * Traits TreatmentMethod.
 */
trait TreatmentMethod
{
    public function getStartDate() :String
    {
        return $this->start_date;
    }

    public function getPrescription($size = false)
    {
        if($this->prescription) {
            return Storage::disk('s3')->url($this->prescription);
        }
    }
    /**
     * @return String
     */
    public function getEndDate(): String
    {
        return $this->end_date;
    }

    public function getPeriod()
    {
        $period = explode(' ', $this->period);
       return $period[0] . ' ' . __($period[1]);
    }
}
