<?php

namespace App\Domains\Treatment\Models\Traits\Relationship;

use App\Domains\Auth\Models\User;
use App\Domains\EventSort\Models\EventSort;
use App\Domains\Passage\Models\Passage;
use App\Domains\Patient\Models\Patient;
use App\Domains\TreatmentComment\Models\TreatmentComment;

/**
 * Class TreatmentRelationship.
 */
trait TreatmentRelationship
{
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function nurse()
    {
        return $this->belongsTo(User::class, 'nurse_id');
    }

    public function patient()
    {
        return $this->belongsTo(Patient::class);
    }

    public function passages()
    {
        return $this->hasMany(Passage::class);
    }

    public function comments()
    {
        return $this->hasMany(TreatmentComment::class);
    }

    public function eventSort()
    {
        return $this->hasOne(EventSort::class, 'treatment_id')->where('user_id',\Auth::id());
    }
}
