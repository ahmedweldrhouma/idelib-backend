<?php

namespace App\Domains\Treatment\Models\Traits\Relationship;

use App\Domains\Patient\Models\Patient;
use App\Domains\Auth\Models\User;

/**
 * Class TreatmentRequestRelationship.
 */
trait TreatmentRequestRelationship
{
    public function patient()
    {
        return $this->belongsTo(Patient::class, 'patient_id');
    }

    public function nurse()
    {
        return $this->belongsTo(User::class, 'nurse_id');
    }
}
