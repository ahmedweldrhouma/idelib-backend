<?php

namespace App\Domains\Treatment\Models\Traits\Scope;

use Carbon\Carbon;

/**
 * Class TreatmentScope.
 */
trait TreatmentScope
{
    /**
     * @param $query
     * @param $term
     *
     * @return mixed
     */
    public function scopeSearch($query, $term)
    {
        return $query->with('patient')
            ->whereHas('patient', function ($query) use ($term){
                // dd($term);
                $query->where('first_name', 'ilike', '%'.$term.'%')
                    ->orWhere('last_name', 'ilike', '%'.$term.'%')
                    ->orWhere('birth_date', 'ilike', '%'.$term.'%')
                    ->orWhere('phone', 'ilike', '%'.$term.'%')
                    ->orWhere('social_security_number', 'ilike', '%'.$term.'%')
                    ->orWhere('doctor', 'ilike', '%'.$term.'%')
                    ->orWhere('doctor_phone', 'ilike', '%'.$term.'%')
                    // ->orWhere('antecedents', 'ilike', '%'.$term.'%')
                    ->orWhere('antecedents', 'ilike', '%'.$term.'%');
            })
            ->orWhere( fn ($query) =>
                $query->where('treatment', 'ilike', '%'.$term.'%')
                    ->orWhere('daily_frequency', 'ilike', '%'.$term.'%')
                    ->orWhere('start_at', 'ilike', '%'.$term.'%')
                    ->orWhere('end_at', 'ilike', '%'.$term.'%')
            );
    }

    /**
     * @param $id
     * @param $term
     *
     * @return mixed
     */
    public function scopeOnlyByNurse($query, $id)
    {
        return $query->where('nurse_id', $id);
    }

    /**
     * @return bool
     */
    public function scopeOnlyPunctual($query, $nurse_id)
    {
        return $query->where('type', 'classic')
                    ->where('nurse_id', $nurse_id)
                    ->where('classic_type', 'ponctuelle');
    }
    /**
     * @return bool
     */
    public function scopeOnlyCovid($query, $nurse_id)
    {
        return $query->where('type', 'covid')
                    ->where('nurse_id', $nurse_id)
                    ->where('classic_type', 'ponctuelle');
    }

    /**
     * @return bool
     */
    public function scopeOnlyChronic($query, $nurse_id)
    {
        return $query->where('type', '!=', 'classic')
                    ->where('type', '!=', 'covid')
                    ->where('nurse_id', $nurse_id)
                    ->whereIn('type', ['bsi', 'dsi'])
                    ->orWhere(function($query) {
                        $query->where('type', 'classic')
                            ->where('classic_type', '!=', 'ponctuelle');
                    });
    }

    public function scopePassageByPeriod($query, $period)
    {
        return $this->passages->map(function ($item) use ($period){
            return $item->period == $period ? $item : false;
        })->filter(function ($value, $key) {
            return $value != false;
        });
    }

}
