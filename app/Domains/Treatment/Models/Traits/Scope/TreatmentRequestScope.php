<?php

namespace App\Domains\Treatment\Models\Traits\Scope;

use Carbon\Carbon;

/**
 * Class TreatmentRequestScope.
 */
trait TreatmentRequestScope
{
    
    public function scopeBySector($query, $tour_sectors)
    {
        return $query->WhereIn('tour_sector', $tour_sectors);
    }
    
    public function scopeBySectorPending($query, $tour_sectors)
    {
        return $query->WhereIn('tour_sector', $tour_sectors)
                    ->where('is_pending', true);
    }

    /**
     * @return bool
     */
    public function scopeOnlyCovid()
    {
        return $this->where('type', 'covid');
    }

}
