<?php

namespace App\Domains\Treatment\Models\Enums;

use Illuminate\Support\Collection;

enum TreatmentBsiType: string
{

    case Bsi = 'BSA';
    case Bsb = 'BSB';
    case Bsc = 'BSC';

    static function values(): Collection {
        $collection = new Collection(self::cases());
        return $collection->pluck('value');
    }
}
