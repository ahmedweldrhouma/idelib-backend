<?php

namespace App\Domains\Treatment\Models\Enums;

use Illuminate\Support\Collection;

enum TreatmentClassicType: string
{
    case Punctuel = 'punctual';
    case Chronic = 'chronic';
    static function values(): Collection {
        $collection = new Collection(self::cases());
        return $collection->pluck('value');
    }
}
