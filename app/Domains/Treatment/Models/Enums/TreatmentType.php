<?php

namespace App\Domains\Treatment\Models\Enums;

use Illuminate\Support\Collection;

enum TreatmentType: string
{
    case Classic = 'classic';
    case Covid = 'covid';
    case Bsi = 'bsi';
    case Dsi = 'dsi';

    static function values(): Collection {
        $collection = new Collection(self::cases());
        return $collection->pluck('value');
    }
}