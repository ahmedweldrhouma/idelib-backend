<?php

namespace App\Domains\Treatment\Models\Enums;

use Illuminate\Support\Collection;

enum TreatmentCarePerformedType: string
{

    case Injections = 'injections';
    case SimpleBandage = 'simpleBandage';
    case HeavyBandage = 'heavyBandage';
    case Perfusion = 'perfusion';
    case Others = 'others';

    static function values(): Collection {
        $collection = new Collection(self::cases());
        return $collection->pluck('value');
    }
}
