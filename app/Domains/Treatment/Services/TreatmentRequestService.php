<?php

namespace App\Domains\Treatment\Services;

use App\Domains\Treatment\Models\TreatmentRequest;
use App\Exceptions\GeneralException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Services\BaseService;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

/**
 * Class TreatmentRequestService.
 */
class TreatmentRequestService extends BaseService
{
    /**
     * TreatmentRequestService constructor.
     *
     * @param  TreatmentRequest  $treatmentRequest
     */
    public function __construct(TreatmentRequest $treatmentRequest)
    {
        $this->model = $treatmentRequest;
    }

    public function all()
    {
        return $this->model->all();
    }

    public function getTreatmentRequestById($id)
    {
        return $this->getById($id);
    }

    /**
     * @param  array  $data
     * @return TreatmentRequest
     *
     */
    public function store(array $data = []) : TreatmentRequest
    {

        DB::beginTransaction();
        try {

            $treatment = $this->model::create([
                "note" => $data['note'] ?? null,
                "patient_id" => Auth::user()->patient->id,
                "is_pending" => true,
                "is_available" => true,
                "tour_sector" => Auth::user()->patient->zip_code,
            ]);

        } catch (Exception $e) {
            DB::rollback();
            throw new HttpException(422,  $e->getMessage());
            throw new HttpException(500,'Un problème est survenu lors de la création du prise en charge. Veuillez réessayer.');
        }

        DB::commit();

        return $treatment;
    }

    public function getTreatmentRequest()
    {
        if(Auth::user()->isPatient()){
            $treatment_requests = Auth::user()->patient->treatmentRequests;
        }else{
            $tour_sector = array_map('trim', explode(",",Auth::user()->tour_sector));
            $treatment_requests = $this->model->bySectorPending($tour_sector)->get();
        }
        return $treatment_requests;
    }

    public function getTreatmentRequestHistory()
    {
        return Auth::user()->treatmentRequests;
    }

    public function acceptTreatmentRequest($request)
    {
        DB::beginTransaction();
        try {
            $patient = $request->patient;
            $nurse = Auth::user();
            if($patient){
                $patient->nurse()->associate($nurse);
                $patient->save();
                $request->nurse_id = $nurse->id;
                $request->is_pending = false;
                $request->is_available = false;
                $request->save();
            }
        } catch (Exception $e) {
            // dd($e);
            DB::rollback();
            throw new HttpException(422,  $e->getMessage());
            throw new HttpException(500,'Un problème est survenu lors de l\'acceptation de la prise en charge. Veuillez réessayer.');
        }
        DB::commit();
        return $request;
    }


    public function denyTreatmentRequest($request)
    {
        DB::beginTransaction();

        try {
            $request->consulted_at = null;
            $request->consulted_by = null;
            $request->update();
        } catch (Exception $e) {
            // dd($e);
            DB::rollback();
            throw new HttpException(422,  $e->getMessage());
            throw new HttpException(500,'Un problème est survenu lors de l\'acceptation de la prise en charge. Veuillez réessayer.');
        }
        DB::commit();
        return $request;
    }

    public function updateAvailability($request)
    {
        DB::beginTransaction();

        try {
            if($request->is_available){
                $request->is_available = false;
            }else{
                $request->is_available = true;
            }

            DB::commit();

            return $request->save();
        } catch (Exception $e) {
            // dd($e);
            DB::rollback();
            throw new HttpException(422,  $e->getMessage());
            throw new HttpException(500,'Un problème est survenu lors de la mise à jour de la prise en charge. Veuillez réessayer.');
        }


    }

}
