<?php

namespace App\Domains\Treatment\Services;

use App\Domains\Patient\Models\Patient;
use App\Domains\Treatment\Models\Treatment;
use App\Domains\Treatment\Models\PatientAbsence;
use App\Domains\Treatment\Models\TreatmentAbsent;
use App\Domains\Treatment\Models\TreatmentRequest;
use App\Exceptions\GeneralException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Services\BaseService;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

/**
 * Class TreatmentRequestService.
 */
class PatientAbsenceService extends BaseService
{
    /**
     * TreatmentRequestService constructor.
     *
     * @param  PatientAbsence  $treatmentAbsence
     */
    public function __construct(PatientAbsence $treatmentAbsence)
    {
        $this->model = $treatmentAbsence;
    }

    public function all()
    {
        return $this->model->all();
    }

    /**
     * @param  array  $data
     * @return TreatmentRequest
     *
     */
    public function store(array $data = []) : PatientAbsence
    {

        DB::beginTransaction();

        try {

            $absence = $this->model::create([
                "absent_from" => $data['absent_from'] ?? null,
                "absent_to" => $data['absent_to'] ?? null,
                "patient_id" => $data['patient_id'] ?? null,
            ]);
        } catch (Exception $e) {
            // dd($e);
            DB::rollback();
            throw new HttpException(422,  $e->getMessage());
            throw new HttpException(500,"Un problème est survenu lors de la création d'absence. Veuillez réessayer.");
        }

        DB::commit();

        return $absence;
    }
    /**
     * @param  PatientAbsence  $passage
     *
     * @return bool
     * @throws HttpException
     */
    public function destroyForPatient(Patient $patient): bool
    {
        if ($this->where('patient_id',$patient->id)->deleteAll()) {
            return true;
        }
        return false;
    }
}
