<?php

namespace App\Domains\Treatment\Services;

use App\Domains\Patient\Models\Patient;
use App\Domains\Treatment\Models\Treatment;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Services\BaseService;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use function Clue\StreamFilter\fun;

/**
 * Class TreatmentService.
 */
class TreatmentService extends BaseService
{
    /**
     * TreatmentService constructor.
     *
     * @param Treatment $treatment
     */
    public function __construct(Treatment $treatment)
    {
        $this->model = $treatment;
    }

    public function all()
    {
        return $this->model->all();
    }

    public function getAllForNurseOfDay($day)
    {
        $all_treatments = Auth::user()->treatments()->where('start_at', "<=", $day)->where('end_at', '>=', $day)->get();

        return $all_treatments;
    }

    public function getAllForNurseWithDate($datedebut, $datefin)
    {
        $all_treatments = Auth::user()->treatments->merge(Auth::user()->covidTreatmentsType);
        $traits = [];
        foreach ($all_treatments as $trait) {
            if ($trait->start_at >= $datedebut && $trait->end_at <= $datefin) {
                array_push($traits, $trait);
            }
        }
        return $traits;
    }

    public function getTreatByNurse($user)
    {
        $treatments = $this->model->where('nurse_id', $user)->get();
        return $treatments;
    }

    public function getAllForTeam($filters = [])
    {
        $teams = Auth::user()->team;
        return $teams->isNotEmpty() ? Auth::user()->team->load(['members.treatmentsAll' => function ($query) use ($filters) {
            if (isset($filters['from']) && isset($filters['to'])) {
                $query->whereBetween('start_at', [$filters['from'], $filters['to']])
                    ->orWhereBetween('end_at', [$filters['from'], $filters['to']])
                    ->orWhere(fn($q) => $q->where(fn($q1) => $q1->where('start_at', '<=', $filters['from'])->where('end_at', '>=', $filters['to']))
                        ->orWhere(fn($q2) => $q2->where('start_at', '>=', $filters['from'])->where('end_at', '<=', $filters['to']))
                    );
            }
            if (isset($filters['date'])) {
                $query->where('start_at', '<=', $filters['date'])->where('end_at', '>=', $filters['date']);
            }

            if (isset($filters['user_id'])) {
                $query->where('nurse_id', '=', $filters['user_id']);
            }
        }])->pluck('members.*.treatmentsAll')->flatten() : Auth::user()->load(['treatmentsAll' => function ($query) use ($filters) {
            if (isset($filters['from']) && isset($filters['to'])) {
                $query->whereBetween('start_at', [$filters['from'], $filters['to']])
                    ->orWhereBetween('end_at', [$filters['from'], $filters['to']])
                    ->orWhere(fn($q) => $q->where(fn($q1) => $q1->where('start_at', '<=', $filters['from'])->where('end_at', '>=', $filters['to']))
                        ->orWhere(fn($q2) => $q2->where('start_at', '>=', $filters['from'])->where('end_at', '<=', $filters['to']))
                    );
            }
            if (isset($filters['date'])) {
                $query->where('start_at', '<=', $filters['date'])->where('end_at', '>=', $filters['date']);
            }
            if (isset($filters['user_id'])) {
                $query->where('nurse_id', '=', $filters['user_id']);
            }
        }])->treatmentsAll;
    }

    /**
     * @param int $id
     * @return Treatment
     *
     */
    public function getTreatmentById($id): Treatment
    {
        $treatment = $this->getById($id);

        return $treatment;
    }

    /**
     * @param int $id
     * @return int
     *
     */
    public function getPatientByIdTreat($id): int
    {
        $treatment = $this->getById($id);

        return $treatment['patient_id'];
    }

    /**
     * @param array $data
     * @return Treatment
     *
     * @throws HttpException
     * @throws \Throwable
     */
    public function store(array $data = [], $patient_id = null): Treatment
    {

        DB::beginTransaction();

        try {
            $prescription = null;
            if (isset($data['prescription'])) {
                $prescription = $data['prescription']->store('prescriptions');
            }

            if (isset($data['period'])) {
                $data['end_at'] = Carbon::parse($data['start_at'])->modify('+' . $data['period'])->format('Y-m-d');
            }
            $treatment = $this->model::create([
                "period" => $data['period'] ?? null,
                "start_at" => $data['start_at'] ?? null,
                "end_at" => $data['end_at'] ?? null,
                "daily_frequency" => $data['daily_frequency'] ?? null,
                "monthly_frequency" => $data['monthly_frequency'] ?? null,
                "type" => $data['type'] ?? null,
                "classic_type" => $data['classic_type'] ?? null,
                "bsi_type" => $data['bsi_type'] ?? null,
                "bsi_interval" => $data['bsi_interval'] ?? null,
                "treatment" => $data['treatment'] ?? null,
                "care_performed" => $data['care_performed'] ?? null,
                "stoped_at" => isset($data['stoped_at']) ? Carbon::parse($data['stoped_at'])->format('Y-m-d') : null,
                "patient_id" => $patient_id,
                "nurse_id" => Auth::id(),
                "prescription" => $prescription
            ]);


        } catch (Exception $e) {
            DB::rollback();
            throw new HttpException(422,  $e->getMessage());
            throw new HttpException(500, 'Un problème est survenu lors de la création du soin. Veuillez réessayer.');
            // throw new GeneralException('Un problème est survenu lors de la création du patient.');
        }

        DB::commit();

        return $treatment;
    }

    /**
     * @param array $data
     * @return Treatment
     *
     * @throws HttpException
     * @throws \Throwable
     */
    public function update(array $data = [], Treatment $treatment): Treatment
    {
        DB::beginTransaction();
        try {
            if (isset($data['period'])) {
                $data['end_at'] = Carbon::parse($data['start_at'])->modify('+' . $data['period'])->format('Y-m-d');
            }
            $treatment->update([
                "period" => $data['period'] ?? $treatment->period,
                "start_at" => Carbon::parse($data['start_at'])->format('Y-m-d') ?? $treatment->start_at,
                "end_at" => $data['end_at'] ?? $treatment->end_at,
                "daily_frequency" => $data['daily_frequency'] ?? $treatment->daily_frequency,
                "monthly_frequency" => $data['monthly_frequency'] ?? $treatment->monthly_frequency,
                "bsi_interval" => $data['bsi_interval'] ?? $treatment->bsi_interval,
                "treatment" => $data['treatment'] ?? $treatment->treatment,
                "stoped_at" => $data['stoped_at'] ?? $treatment->stoped_at,
                "care_performed" => $data['care_performed'] ?? $treatment->care_performed,
            ]);


        } catch (Exception $e) {
            // dd($e);
            DB::rollback();
            // throw new HttpException(500, $e->getMessage());
            throw new HttpException(500, 'Un problème est survenu lors de la création du soin. Veuillez réessayer.');
            // throw new GeneralException('Un problème est survenu lors de la création du patient.');
        }

        DB::commit();

        return $treatment;
    }

    /**
     * @param Treatment $treatment
     * @param  $image
     *
     * @return Treatment
     */
    public function updatePrescription(Treatment $treatment, $image): Treatment
    {
        if ($image) {
            Storage::disk('s3')->delete($treatment->prescription);
            $treatment->prescription = $image->store('prescriptions', 's3');
        }
        /*
         * $img = \Image::make($image->getRealPath());
            $img->resize(1024, 1024, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            Storage::disk('s3')->delete($treatment->prescription);
            $filename = uniqid() . '.' . $image->getClientOriginalName();
            if (Storage::disk('s3')->put('prescriptions/' . $filename, $img->stream()->__toString())) {
                $treatment->prescription = 'prescriptions/' . $filename;
            }
         */
        $treatment->save();
        return $treatment;
    }

    /**
     * @param Treatment $treatment
     *
     * @return bool
     * @throws HttpException
     */
    public function destroy(Treatment $treatment): bool
    {
        if ($this->deleteById($treatment->id)) {
            return true;
        }

        return false;
    }

    /**
     * @param Treatment $treatment
     *
     * @return Treatment
     */
    public function renewal(Treatment $treatment): Treatment
    {
        DB::beginTransaction();

        try {
            $end_at = Carbon::now()->modify('+' . $treatment->period)->format('Y-m-d');

            $treatment->start_at = Carbon::now()->format('Y-m-d');
            $treatment->end_at = $end_at;
            $treatment->save();

        } catch (Exception $e) {

            DB::rollback();
            throw new HttpException(500, $e->getMessage());
        }

        DB::commit();

        return $treatment;
    }

    public function getAllForNurseToday($today_date)
    {
        $all_treatments = Auth::user()->treatments;
        $traits = [];
        foreach ($all_treatments as $trait) {
            if ($trait->start_at->format('Y-m-d') == $today_date || $trait->end_at->format('Y-m-d') == $today_date || ($today_date >= $trait->start_at->format('Y-m-d') && $today_date <= $trait->end_at->format('Y-m-d'))) {

                array_push($traits, $trait);
            }
        }
        return $traits;
    }

}
