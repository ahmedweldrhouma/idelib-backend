<?php

namespace App\Domains\Passage\Models\Traits\Relationship;

use App\Domains\Patient\Models\Patient;
use App\Domains\Treatment\Models\Treatment;

/**
 * Class PassageRelationship.
 */
trait PassageRelationship
{   
    public function patient()
    {
        return $this->belongsTo(Patient::class, 'patient_id');
    }
    
    public function treatment()
    {
        return $this->belongsTo(Treatment::class);
    }


}
