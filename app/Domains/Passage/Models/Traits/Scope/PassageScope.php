<?php

namespace App\Domains\Passage\Models\Traits\Scope;

/**
 * Class PassageScope.
 */
trait PassageScope
{
    /**
     * @param $query
     * @param $term
     *
     * @return mixed
     */
    public function scopeSearch($query, $term)
    {
        return $query->where( fn ($query) => $query->where('name', 'ilike', '%'.$term.'%')
            ->orWhere('perice', 'ilike', '%'.$term.'%')
        );
    }
}
