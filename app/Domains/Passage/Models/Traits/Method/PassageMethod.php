<?php

namespace App\Domains\Passage\Models\Traits\Method;

use App\Domains\Treatment\Models\Treatment;
use Illuminate\Support\Collection;

/**
 * Traits PassageMethod.
 */
trait PassageMethod
{

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @return bool
     */
    public function isVerified(): bool
    {
        return $this->email_verified_at !== null;
    }
    /**
     * @return string
     */
    public function getForPatient() : string
    {
        if ($this->treatment->type.' '.$this->treatment->bsi_interval == Treatment::TYPE_BSI. ' '.'<'.'85'){
            return __($this->period) .' '.$this->time_slot.', AIS3: '.$this->ais;
        }else{
            return __($this->period) .' '.$this->time_slot;
        }
    }
}
