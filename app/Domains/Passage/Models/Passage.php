<?php

namespace App\Domains\Passage\Models;

use Illuminate\Database\Eloquent\Model;
use App\Domains\Passage\Models\Traits\Attribute\PassageAttribute;
use App\Domains\Passage\Models\Traits\Method\PassageMethod;
use App\Domains\Passage\Models\Traits\Relationship\PassageRelationship;
use App\Domains\Passage\Models\Traits\Scope\PassageScope;

/**
 * Class Passage.
 */
class Passage extends Model
{
    use PassageMethod,
        PassageRelationship,
        PassageScope;

    protected $primaryKey   = 'id';    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'period',
        'time_slot',
        'patient_id',
        'treatment_id',
        'ais'
    ];

}
