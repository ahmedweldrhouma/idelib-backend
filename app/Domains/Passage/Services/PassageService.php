<?php

namespace App\Domains\Passage\Services;

use App\Domains\Passage\Models\Passage;
use App\Domains\Treatment\Models\Treatment;
use App\Exceptions\GeneralException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Services\BaseService;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

/**
 * Class PassageService.
 */
class PassageService extends BaseService
{
    /**
     * PassageService constructor.
     *
     * @param  Passage  $passage
     */
    public function __construct(Passage $passage)
    {
        $this->model = $passage;
    }

    public function all()
    {
        return $this->model->all();
    }
    /**
     * @param  array  $data
     * @return Passage
     *
     * @throws HttpException
     * @throws \Throwable
     */
    public function store(array $data = [], $treatment_id = null) : Passage
    {

        DB::beginTransaction();

        try {
            $passage = $this->model::create([
                "period" => $data['period'] ?? null,
                "time_slot" => $data['time_slot'] ?? null,
                "ais" => $data['ais'] ?? null,
                "patient_id" => $data['patient_id'] ?? null,
                "treatment_id" => $treatment_id ?? null,
            ]);

        } catch (Exception $e) {
            // dd($e);
            DB::rollback();
            throw new HttpException(422,  $e->getMessage());
            throw new HttpException(500,'Un problème est survenu lors de la création du passage. Veuillez réessayer.');
            // throw new GeneralException('Un problème est survenu lors de la création du patient.');
        }

        DB::commit();

        return $passage;
    }



    /**
     * @param  Passage  $passage
     *
     * @return bool
     * @throws HttpException
     */
    public function destroy(Passage  $passage): bool
    {
        if ($this->deleteById($passage->id)) {

            return true;
        }
        // throw new HttpException(500, $e->getMessage());
        throw new HttpException(500,'Un problème est survenu lors de la suppression du passage. Veuillez réessayer.');
        // throw new GeneralException(__('Un problème est survenu lors de la suppression de patient.'));
    }

    /**
     * @param  Passage  $passage
     *
     * @return bool
     * @throws HttpException
     */
    public function destroyForTreatment(Treatment $treatment): bool
    {
        try{
            $this->where('treatment_id',$treatment->id)->deleteAll();
        }catch (Exception $e){
            // throw new HttpException(500, $e->getMessage());
            throw new HttpException(500,'Un problème est survenu lors de la suppression du passage. Veuillez réessayer.');
            // throw new GeneralException(__('Un problème est survenu lors de la suppression de patient.'));
            return false;
        }
        return true;

    }
}
