<?php

namespace App\Domains\CovidTreatment\Services;

use App\Domains\Appointment\Models\Appointment;
use App\Domains\CovidTreatment\Models\CovidTreatment;
use App\Services\BaseService;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Support\Arr;

/**
 * Class CovidTreatmentService.
 */
class CovidTreatmentService extends BaseService
{
    /**
     * CovidTreatmentService constructor.
     *
     * @param  CovidTreatment  $CovidTreatment
     */
    public function __construct(CovidTreatment $covidTreatment)
    {
        $this->model = $covidTreatment;
    }

    /**
     * @param  int  $id
     * @return CovidTreatment
     *
     */
    public function getCovidTreatById($id): CovidTreatment
    {
        $covid_treat = $this->getById($id);

        return $covid_treat;
    }


    /**
     * @param  array  $data
     * @return CovidTreatment
     *
     * @throws HttpException
     * @throws \Throwable
     */
    public function store(array $data = [], $patient_id, $type): CovidTreatment
    {

        DB::beginTransaction();
        try {
            $covidTreatment = $this->model::create([
                "type" => $type,
                "date" => Carbon::parse($data['date'])->format('Y-m-d') ?? null,
                "result" => $data['result'] ?? null,
                "prescription" => $data['prescription'] ?? null,
                "patient_id" => $patient_id,
                "user_id" => Auth::id() ?? null,
            ]);

        } catch (Exception $e) {
            DB::rollback();
            throw new HttpException(422,  $e->getMessage());
            throw new HttpException(500, 'Un problème est survenu lors de la création du test pcr. Veuillez réessayer.');
        }

        DB::commit();

        return $covidTreatment;
    }

    /**
     * @param       $id
     * @param bool|UploadedFile  $image
     *
     * @return array|bool
     */
    public function updatePrescription($covidTreatment, $image)
    {
        if ($image) {
            Storage::disk('s3')->delete($covidTreatment->prescription);
            $covidTreatment->prescription = $image->store('prescriptions', 's3');
        }
        $covidTreatment->save();
        return $covidTreatment;
    }

    public function getAllByPatient($patient_id, $type)
    {
        return $this->model->where('patient_id', $patient_id)
            ->where('type', $type)
            ->where('user_id', Auth::id())
            ->get();
    }

    public function getAllForNurse()
    {
        return Auth::user()->covidTreatments;
    }

    /**
     * @param  int  $id
     * @return int
     *
     */
    public function getPatientByIdCov($id): int
    {
        $covid = $this->getById($id);

        return $covid['patient_id'];
    }

    public function getAllForTeam($filters = [])
    {
        $teams = Auth::user()->team;
        return $teams->isNotEmpty()?Auth::user()->team->load('members.covidTreatments')->pluck('members.*.covidTreatments')->flatten():Auth::user()->load(['covidTreatments'=>function($query)use ($filters){
            if (isset($filters['from']) && isset($filters['to'])){
                $query->whereBetween('date',[$filters['from'],$filters['to']]);
            }
            if (isset($filters['date'])){
                $query->where('date','=',$filters['date']);
            }
            if (isset($filters['user_id'])){
                $query->where('nurse_id','=',$filters['user_id']);
            }
        }])->covidTreatments;
    }

    public function getTreatByNurse($user){
            $covid_treatments = $this->model->where('user_id',$user)->get();

        return $covid_treatments;
    }

    /**
     * @param  CovidTreatment $covidTreatment
     * @param  array  $data
     * @return CovidTreatment
     *
     * @throws \Throwable
     */
    public function update(array $data = [], CovidTreatment $covidTreatment): CovidTreatment
    {
        DB::beginTransaction();

        try {
            $covidTreatment->update([
                'date' => Carbon::parse($data['date'])->format('Y-m-d') ?? $covidTreatment->date,
                'result' => $data['result'] ?? $covidTreatment->result,
                'prescription' => $data['prescription'] ?? $covidTreatment->prescription,
            ]);

        } catch (Exception $e) {
            // dd($e);
            DB::rollBack();
            throw new HttpException(422,  $e->getMessage());
            throw new HttpException(500, 'Un problème est survenu lors de la mise à jour de ce traitement covid. Veuillez réessayer.');
            // throw new GeneralException(__('There was a problem updating this covid treatment. Please try again.'));
        }

        DB::commit();

        return $covidTreatment;
    }
    public function destroy(CovidTreatment $covidTreat): bool
    {
        if ($this->deleteById($covidTreat->id)) {

            return true;
        }

        return false;
    }

    public function getAllForNurseToday($today_date)
    {
        $all_treatments = Auth::user()->covidTreatments;
        $covid = [];
        foreach ($all_treatments as $trait)
            {
             if ($trait->date->format('Y-m-d') == $today_date ){

                 array_push ($covid,$trait);
             }
            }
        return $covid;
    }
}
