<?php

namespace App\Domains\CovidTreatment\Models;

use App\Domains\CovidTreatment\Models\Traits\Method\CovidTreatmentMethod;
use Illuminate\Database\Eloquent\Model;
use App\Domains\CovidTreatment\Models\Traits\Relationship\CovidTreatmentRelationship;
use App\Domains\CovidTreatment\Models\Traits\Scope\CovidTreatmentScope;

/**
 * Class CovidTreatment.
 */
class CovidTreatment extends Model
{
    use CovidTreatmentRelationship,
        CovidTreatmentMethod,
        CovidTreatmentScope;


    public const TYPE_ANTIGEN = 'antigen';
    public const TYPE_PCR = 'pcr';
    public const TYPE_VISIT = 'visit';

    protected $primaryKey   = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'date',
        'result',
        'prescription',
        'patient_id',
        'user_id',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'date',
        'created_at',
        'updated_at',
    ];

    /**
     * @var string[]
     */
    protected $with = [
        'nurse',
        'patient'
    ];

}
