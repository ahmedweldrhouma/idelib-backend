<?php

namespace App\Domains\CovidTreatment\Models\Traits\Method;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

/**
 * Traits PatientMethod.
 */
trait CovidTreatmentMethod
{
    public function getPrescription($size = false)
    {
        if($this->prescription) {
            return Storage::disk('s3')->url($this->prescription);
        }
    }
}
