<?php

namespace App\Domains\CovidTreatment\Models\Traits\Relationship;

use App\Domains\Auth\Models\User;
use App\Domains\Patient\Models\Patient;

/**
 * Class CovidTreatmentRelationship.
 */
trait CovidTreatmentRelationship
{
    public function patient()
    {
        return $this->belongsTo(Patient::class, 'patient_id');
    }

    
    public function nurse()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
