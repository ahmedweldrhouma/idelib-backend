<?php

namespace App\Domains\Conversation\Services;

use App\Domains\Auth\Models\User;
use App\Domains\Conversation\Models\ConversationUser;

class ConversationUserService
{
    /**
     * MessageService constructor.
     *
     * @param ConversationUser $conversationuser
     */
    public function __construct(ConversationUser $conversationuser)
    {
        $this->model = $conversationuser;
    }

    /**
     * @return User $user
     */
    public function detach($user)
    {
        $this->model->where('participant_id', $user->id)->update(['deleted_at'=>now()]);
        return $user;
    }


}
