<?php

namespace App\Domains\Conversation\Services;

use App\Domains\Appointment\Models\Appointment;
use App\Domains\Auth\Services\UserService;
use App\Domains\Conversation\Models\Conversation;
use App\Domains\CovidTreatment\Models\CovidTreatment;
use App\Services\BaseService;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Nette\Utils\Random;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Support\Arr;

/**
 * Class CovidTreatmentService.
 */
class ConversationService extends BaseService
{
    /**
     * MessageService constructor.
     *
     * @param  Conversation  $conversation
     */
    public function __construct(Conversation $conversation)
    {
        $this->model = $conversation;
    }

    public function getMemberConversation($user_id)
    {
        return Auth::user()->conversations()->has('participants', '<', 3)->whereHas('participants', function ($query) use ($user_id) {
                $query->where('participant_id', '=', $user_id);
            })->first();
    }
    public function getTeamMemberConversation($conversation)
    {
       return Auth::user()->team->first()->members->diff($conversation->participants)->whereNotIn('id',Auth::id());
    }


    /**
     * @param  array  $data
     * @return Conversation
     *
     * @throws HttpException
     * @throws \Throwable
     */
    public function store(array $data = [],$user_id = null): Conversation
    {
        DB::beginTransaction();
        $color =  '#' . substr(md5(rand()), 0, 6);
        try {
            $conversation = $this->model::create([
                'name' => $data['name'] ?? null,
                'color' => $color ?? null,
                'user_id' => Auth::check()?Auth::id():$user_id
            ]);
            $conversation->participants()->attach($data['participants']);
        } catch (Exception $e) {
            DB::rollback();
            dd($e->getMessage());
            throw new HttpException(500, 'Un problème est survenu lors de la création de la conversation. Veuillez réessayer.');
        }

        DB::commit();

        return $conversation;
    }

    public function update(array $data = [], $conversation): Conversation
    {
        DB::beginTransaction();

        try {
            $conversation->name = $data['name'] ?? $conversation->name;
            $conversation->color = $data['color'] ?? $conversation->color;
            $conversation->save();
            //$conversation->participants()->sync($data['participants']);
        } catch (Exception $e) {
            // dd($e);
            DB::rollback();
            throw new HttpException(500, 'Un problème est survenu lors de la mise à jour de la couleur. Veuillez réessayer.');
        }

        DB::commit();

        return $conversation;
    }



    /**
     * @return Conversation
     */
    public function destroy($conversation)
    {
        if ($this->deleteById($conversation->id)) {

            return true;
        }

        throw new HttpException(500,"Un problème est survenu lors de la suppression de la conversation. Veuillez réessayer.");
    }
    /**
     * @return Conversation
     */
    public function getUserConversation($user_id)
    {
        return $this->model->whereHas('participants', function($q) use ($user_id){
            $q->where('conversation_user.participant_id', $user_id)->where('conversation_user.deleted_at', null);
        })->get();
    }


    public function detachMember($conversation, $user)
    {
        $detach_memeber = $user->participants()->detach($conversation->id);
        return $detach_memeber;
    }
    public function addMember($conversation, $user)
    {
       return $conversation->participants()->attach($user);
    }


}
