<?php

namespace App\Domains\Conversation\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class ConversationUser extends Pivot
{
    protected $primaryKey   = 'id';
    public $incrementing = true;

    /**
     * @var string[]
     */
    protected $fillable = [
        'conversation_id',
        'participant_id'
    ];


}
