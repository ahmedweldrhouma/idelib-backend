<?php

namespace App\Domains\Conversation\Models;

use App\Domains\Conversation\Models\Traits\Relationship\ConversationRelationship;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    use HasFactory,ConversationRelationship;

    /**
     * @var string[]
     */
    protected $fillable = [
        'name',
        'color',
        'user_id',
    ];

    /**
     * @var string[]
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function isParticipant($user_id)
    {
        return ($this->participants()
            ->where('users.id', $user_id)->first()
        ) ? true : false;
    }


}
