<?php

namespace App\Domains\Conversation\Models\Traits\Relationship;

use App\Domains\Auth\Models\User;
use App\Domains\Conversation\Models\Conversation;
use App\Domains\Conversation\Models\ConversationParticipant;
use App\Domains\Conversation\Models\ConversationUser;
use App\Domains\Message\Models\Message;

/**
 * Class CovidTreatmentRelationship.
 */
trait ConversationRelationship
{
    public function messages()
    {
        return $this->hasMany(Message::class, 'conversation_id');
    }

    public function lastMessage()
    {
        return $this->hasOne(Message::class, 'conversation_id')->orderBy('created_at','desc');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function participants()
    {
        return $this->belongsToMany(User::class,'conversation_user','conversation_id','participant_id')->withPivot( 'deleted_at','participant_id')->wherePivot('deleted_at', '=', null);
    }

}
