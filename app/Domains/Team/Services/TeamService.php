<?php

namespace App\Domains\Team\Services;

use App\Domains\Team\Models\Team;
use App\Domains\Team\Models\TeamUser;
use App\Domains\Auth\Models\User;
use App\Exceptions\GeneralException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Services\BaseService;
use Exception;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Barryvdh\DomPDF\Facade\Pdf;

/**
 * Class TeamService.
 */
class TeamService extends BaseService
{
    /**
     * TeamService constructor.
     *
     * @param Team $team
     */
    public function __construct(Team $team, TeamUser $team_user)
    {
        $this->model = $team;
        $this->team_user = $team_user;
    }

    public function getOwnedTeams()
    {
        return Auth::user()->ownedTeams;
    }

    public function getTeams()
    {
        return Auth::user()->teams;
    }


    /**
     * @param array $data
     * @return CovidTreatment
     *
     * @throws GeneralException
     * @throws \Throwable
     */
    public function store(array $data = []): Team
    {

        DB::beginTransaction();

        try {
            $team = $this->model::create([
                "name" => $data['name'] ?? null,
                "admin_id" => Auth::id(),
            ]);

            $admin['color'] = "#000";
            $admin['is_admin'] = true;

            $team->members()->attach(Auth::id(), $admin);
        } catch (Exception $e) {
            // dd($e);
            DB::rollback();
            throw new HttpException(422,  $e->getMessage());
            throw new HttpException(500, 'Un problème est survenu lors de la création de l\'equipe. Veuillez réessayer.');
        }

        DB::commit();

        return $team;
    }

    public function updateColor($team, $user, $color)
    {
        DB::beginTransaction();

        try {
            $team_user = $user->getMemberColor($team->id)->pivot;
            $team_user->color = $color;
            $team_user->save();

        } catch (Exception $e) {
            // dd($e);
            DB::rollback();
            throw new HttpException(422,  $e->getMessage());
            throw new HttpException(500, 'Un problème est survenu lors de la mise à jour de la couleur. Veuillez réessayer.');
        }

        DB::commit();

        return $team_user;
    }

    public function update(array $data = [], $team): Team
    {
        DB::beginTransaction();

        try {
            $team->name = $data['name'] ?? $team->name;
            $team->admin_id = $data['admin_id'] ?? $team->admin_id;
            $team->save();

        } catch (Exception $e) {
            // dd($e);
            DB::rollback();
            throw new HttpException(422,  $e->getMessage());
            throw new HttpException(500, 'Un problème est survenu lors de la mise à jour de la couleur. Veuillez réessayer.');
        }

        DB::commit();

        return $team;
    }

    public function defineNewAdmin($team, $user_id)
    {
        DB::beginTransaction();
        try {
            $team_user = $team->getMemberOfTeam($user_id)->pivot;
            $team_user->is_admin = true;
            $team_user->save();

        } catch (Exception $e) {
            // dd($e);
            DB::rollback();
            throw new HttpException(500, $e->getMessage());
           // throw new HttpException(500, 'Un problème est survenu lors de la mise à jour de l\'admin. Veuillez réessayer.');
        }

        DB::commit();

        return $team_user;
    }

    public function defineNewOwner(Team $team, $user)
    {
        DB::beginTransaction();
        try {

            if ( Auth::user()->isOwnerOfTeam($team->id) && Auth::user()->team->first()->admins->except(Auth::id())){

                $result = $this->update(['admin_id' => $user->id], $team);

               $team_user =  $team->getMemberOfTeam($user->id)->pivot;
               $team_user->is_admin = true;
               $team_user->save();

                if ($result ){
                    $admin = $this->detachMember($team, Auth::user());
                }
            }
        } catch (Exception $e) {
            // dd($e);
            DB::rollback();
            throw new HttpException(500, 'Un problème est survenu lors de la mise à jour de l\'admin. Veuillez réessayer.');
        }

        DB::commit();

        return $admin;

    }

    public function detachMember($team, $user)
    {
        $detach_memeber = $user->team()->detach($team->id);
        if ($detach_memeber && Auth::user()->patients) {
            foreach (Auth::user()->patients as $patient) {
                $patient->nurse()->associate($team->admin);
                $patient->update();
                if ($patient->treatments->isNotEmpty()) {
                    foreach ($patient->treatments as $treatment) {
                        $treatment->nurse()->associate($team->admin);
                        $treatment->update();
                    }
                }
            }
        }
        return $detach_memeber;
    }

    public function detachAdmin($team, $user)
    {
        DB::beginTransaction();
        try {
            // dd($team->getMemberOfTeam($user->id)->pivot);
            $team_user = $team->getMemberOfTeam($user->id)->pivot;
            $team_user->is_admin = false;
            $team_user->save();
        } catch (Exception $e) {
            DB::rollback();
            throw new HttpException(500, 'Un problème est survenu lors de la mise à jour de l\'admin. Veuillez réessayer.');
        }

        DB::commit();
        return true;
    }

    public function generateContract(array $data = [])
    {
        $pdf = PDF::loadView('backend.team.contract', compact('data'));
        $file_name = $data['first_name_liberal'] . '_' . $data['last_name_liberal'] . '_and_' . $data['first_name_substitute'] . '_' . $data['last_name_substitute'] . '_' . Carbon::now()->timestamp . '.pdf';
        $content = $pdf->download()->getOriginalContent();
        Storage::disk('s3')->put('contracts/' . $file_name, $content);
        $pdf_location = Storage::disk('s3')->url('contracts/' . $file_name);
        return $pdf_location;
    }

    public function storeteamuser(array $data = []): TeamUser
    {

        DB::beginTransaction();
        try {
            $team_user = $this->team_user::create([
                "color" => $data['color'] ?? null,
                "is_admin" => 'false',
                "user_id" => $data['user_id'],
                "team_id" => $data['team_id']]);

        } catch (Exception $e) {
            // dd($e);
            DB::rollback();
            throw new HttpException(422,  $e->getMessage());
            throw new HttpException(500, 'Un problème est survenu lors de la création de membre de lequipe . Veuillez réessayer.');
        }

        DB::commit();

        return $team_user;
    }

}
