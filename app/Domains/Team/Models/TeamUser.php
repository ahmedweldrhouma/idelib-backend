<?php
 
 namespace App\Domains\Team\Models;
 
use Illuminate\Database\Eloquent\Relations\Pivot;
 
class TeamUser extends Pivot
{
    protected $primaryKey   = 'id';    
    public $incrementing = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'color',
        'is_admin',
        'user_id',
        'team_id',
    ];
}