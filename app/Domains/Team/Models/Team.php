<?php

namespace App\Domains\Team\Models;

use Illuminate\Database\Eloquent\Model;
use App\Domains\Team\Models\Traits\Relationship\TeamRelationship;
use App\Domains\Team\Models\Traits\Method\TeamMethod;

/**
 * Class Team.
 */
class Team extends Model
{
    use TeamRelationship,
        TeamMethod;

    protected $primaryKey   = 'id';    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'admin_id'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    
    /**
     * @var string[]
     */
    protected $with = [
        // 'members',
    ];



}
