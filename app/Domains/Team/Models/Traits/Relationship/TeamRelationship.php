<?php

namespace App\Domains\Team\Models\Traits\Relationship;

use App\Domains\Auth\Models\User;
use App\Domains\Team\Models\TeamUser;

/**
 * Class TeamRelationship.
 */
trait TeamRelationship
{
    public function admin()
    {
        return $this->belongsTo(User::class, 'admin_id');
    }

    public function biller()
    {
        return $this->belongsToMany(User::class)->using(TeamUser::class)->where('type','=','biller');
    }

    public function team_users()
    {
        return $this->belongsTo(TeamUser::class, 'user_id');
    }

    public function members()
    {
        return $this->belongsToMany(User::class)->withPivot('color', 'id', 'is_admin')->using(TeamUser::class)->withTimestamps();
    }

    public function admins()
    {
        return $this->belongsToMany(User::class)->withPivot('color', 'id', 'is_admin')->where('is_admin', true)->using(TeamUser::class)->withTimestamps();
    }
}
