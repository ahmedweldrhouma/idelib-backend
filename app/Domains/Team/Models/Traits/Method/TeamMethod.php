<?php

namespace App\Domains\Team\Models\Traits\Method;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

/**
 * Traits TeamMethod.
 */
trait TeamMethod
{
    /**
     * @return mixed
     */
    public function getOwnersOfTeam()
    {
        return $this->admins()
            ->where('team_id', $this->id)->get();
    }

    public function getMemberOfTeam($user_id)
    {
        return $this->members()
            ->where('team_id', $this->id)
            ->where('user_id', $user_id)->first();
    }
}
