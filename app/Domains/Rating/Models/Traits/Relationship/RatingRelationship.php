<?php

namespace App\Domains\Rating\Models\Traits\Relationship;

use App\Domains\Auth\Models\User;

/**
 * Class RatingRelationship.
 */
trait RatingRelationship
{
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    
    public function nurse()
    {
        return $this->belongsTo(User::class, 'nurse_id');
    }

}
