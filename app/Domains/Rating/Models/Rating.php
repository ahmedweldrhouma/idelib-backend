<?php

namespace App\Domains\Rating\Models;

use Illuminate\Database\Eloquent\Model;
use App\Domains\Rating\Models\Traits\Relationship\RatingRelationship;

/**
 * Class Rating.
 */
class Rating extends Model
{
    use RatingRelationship;

    protected $primaryKey   = 'id';    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'rating',
        'nurse_id',
        'user_id'
    ];

    /**
     * @var string[]
     */
    protected $with = [
        'user',
        'nurse'
    ];

}
