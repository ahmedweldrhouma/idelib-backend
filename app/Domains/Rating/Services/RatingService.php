<?php

namespace App\Domains\Rating\Services;

use App\Domains\Rating\Models\Rating;
use App\Exceptions\GeneralException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Services\BaseService;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

/**
 * Class RatingService.
 */
class RatingService extends BaseService
{
    /**
     * RatingService constructor.
     *
     * @param  Rating  $Rating
     */
    public function __construct(Rating $rating)
    {
        $this->model = $rating;
    }

    /**
     * @param  array  $data
     * @return Patient
     *
     * @throws HttpException
     * @throws \Throwable
     */
    public function store($nurse_id, array $data = []) : Rating
    {

        DB::beginTransaction();

        try {

            $rating = $this->model::create([
                "rating" => $data['rating'] ?? null,
                "user_id" => Auth::id(),
                "nurse_id" => $nurse_id,
            ]);

        } catch (Exception $e) {
            DB::rollback();
            throw new HttpException(422,  $e->getMessage());
            throw new HttpException(500,'Un problème est survenu lors de la création du patient. Veuillez réessayer.');

        }

        DB::commit();

        return $rating;
    }

}
