<?php

namespace App\Domains\TreatmentComment\Services;

use App\Domains\TreatmentComment\Models\TreatmentComment;
use App\Services\BaseService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TretmentCommentService extends BaseService
{
    /**
     * CovidTreatmentService constructor.
     *
     * @param  TreatmentComment $treatmentComment
     */
    public function __construct(TreatmentComment $treatmentComment)
    {
        $this->model = $treatmentComment;
    }


    public function getTreatmentComment($treatment_id)
    {
        $comments = $this->model->ByTreatmentId($treatment_id)->get();

        return $comments;
    }

    /**
     * @param  array  $data
     * @return TreatmentComment
     *
     * @throws HttpException
     * @throws \Throwable
     */
    public function store(array $data = [], $treatment_id): TreatmentComment
    {

        DB::beginTransaction();
        try {
            $treatmentComment =  $this->model::create([
                "content" => $data['content'],
                "treatment_id" => $treatment_id,
                "user_id" => Auth::id() ?? null,
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            throw new HttpException(422,  $e->getMessage());
            throw new HttpException(500, 'Un problème est survenu lors de la création du commentaire. Veuillez réessayer.');
        }

        DB::commit();

        return $treatmentComment;
    }


    /**
     * @param  TreatmentComment $treatmentComment
     * @param  array  $data
     * @return CovidTreatment
     *
     * @throws \Throwable
     */
    public function update(array $data = [], TreatmentComment $treatmentComment): TreatmentComment
    {
        DB::beginTransaction();

        try {
            $treatmentComment->update([
                'content' => $data['content'] ?? $treatmentComment->content,
            ]);

        } catch (\Exception $e) {
            // dd($e);
            DB::rollBack();
            throw new \HttpException(500, 'Un problème est survenu lors de la mise à jour de ce commentaire. Veuillez réessayer.');
        }

        DB::commit();

        return $treatmentComment;
    }

    public function destroy(TreatmentComment $comment): bool
    {

        if ($this->deleteById($comment->id)) {

            return true;
        }
        return false;
    }

}
