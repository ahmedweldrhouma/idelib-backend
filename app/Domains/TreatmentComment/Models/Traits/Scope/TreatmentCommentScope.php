<?php

namespace App\Domains\TreatmentComment\Models\Traits\Scope;

use Carbon\Carbon;

/**
 * Class CovidTreatmentScope.
 */
trait TreatmentCommentScope
{
    public function scopeByTreatmentId($query, $treatment_id)
    {
        return $query->where('treatment_id', $treatment_id);
    }

}
