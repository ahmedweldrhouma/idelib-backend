<?php

namespace App\Domains\TreatmentComment\Models\Traits\Relationship;

use App\Domains\Auth\Models\User;
use App\Domains\Treatment\Models\Treatment;

trait TreatmentCommentRelationship
{
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function treatment()
    {
        return $this->belongsTo(Treatment::class, 'treatment_id');
    }
}
