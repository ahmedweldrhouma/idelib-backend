<?php

namespace App\Domains\TreatmentComment\Models;

use App\Domains\TreatmentComment\Models\Traits\Relationship\TreatmentCommentRelationship;
use App\Domains\TreatmentComment\Models\Traits\Scope\TreatmentCommentScope;
use Illuminate\Database\Eloquent\Model;

class TreatmentComment extends Model
{
    use TreatmentCommentRelationship,TreatmentCommentScope;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'content',
        'treatment_id',
        'user_id',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];


}
