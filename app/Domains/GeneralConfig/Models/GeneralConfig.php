<?php

namespace App\Domains\GeneralConfig\Models;

use App\Domains\GeneralConfig\Models\Method\GeneralConfigMethod;
use Illuminate\Database\Eloquent\Model;

/**
 * Class GeneralConfig.
 */
class GeneralConfig extends Model
{
    use GeneralConfigMethod;

    protected $primaryKey   = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'value',
    ];
}
