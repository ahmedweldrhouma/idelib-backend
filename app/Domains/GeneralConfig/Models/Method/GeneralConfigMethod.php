<?php

namespace App\Domains\GeneralConfig\Models\Method;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

/**
 * Traits GeneralConfigMethod.
 */
trait GeneralConfigMethod
{
    /**
     * @return int
     */
    public function isActive(): int
    {
        return $this->value ;
    }
}
