<?php

namespace App\Domains\GeneralConfig\Services;

use App\Domains\GeneralConfig\Models\GeneralConfig;
use App\Domains\Disponibilite\Models\Method\DisponibiliteMethod;
use App\Domains\GeneralConfig\Models\Relationship\GeneralConfigRelationship;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Services\BaseService;


/**
 * Class GeneralConfigService.
 */
class GeneralConfigService extends BaseService
{
    /**
     * GeneralConfigService constructor.
     *
     * @param  GeneralConfig  $generalconfig
     */
    public function __construct(GeneralConfig $generalconfig)
    {
        $this->model = $generalconfig;
    }

    public function all()
    {
        return $this->model->all();
    }

    /**
     * @param  int  $id
     * @return Disponibilite
     *
     */
    public function getById($id) : GeneralConfig
    {
        $generalconfig = $this->getById($id);

        return $generalconfig;
    }

    public function update(String $name, $value) : bool
    {
        DB::beginTransaction();
        try {
            $this->model->where('name',$name)->update([
                "value" => $value ?? null,
            ]);
        } catch (Exception $e) {
            // dd($e);
            DB::rollback();
            throw new HttpException(422,  $e->getMessage());
            throw new GeneralException('Un problème est survenu lors de la mise à jour de l\'email.');
        }
        DB::commit();

        return true;
    }



    /**
     * @param  GeneralConfig  $generalconfig
     *
     * @return bool
     * @throws HttpException
     */
    public function destroy(GeneralConfig  $generalconfig): bool
    {
        if ($this->deleteById($generalconfig->id)) {

            return true;
        }

        return false;
    }

}
