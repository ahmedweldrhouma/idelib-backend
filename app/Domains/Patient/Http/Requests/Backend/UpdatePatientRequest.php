<?php

namespace App\Domains\Patient\Http\Requests\Backend;

use App\Domains\Patient\Models\Enums\PatientType;
use App\Domains\Patient\Models\Patient;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

/**
 * Class UpdatePatientRequest.
 */
class UpdatePatientRequest extends FormRequest
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => ['sometimes'],
            'first_name' => ['required', 'max:100'],
            'last_name' => ['required', 'max:100'],
            'social_security_number' => ['max:100'],
            'email' => ['sometimes', 'max:255', Rule::unique('patients','email')->ignore($this->patient),Rule::unique('users')->ignore($this->patient->user) ],
            'phone' => ['sometimes', 'max:100'],
            'avatar_location' => ['sometimes', 'mimes:jpg,bmp,png'],
            'vital_card' => ['sometimes', 'max:100'],
            'mutual_card' => ['sometimes', 'mimes:jpg,bmp,png'],
            'birth_date' => ['sometimes'],
            'doctor' => ['sometimes', 'max:100'],
            'doctor_phone' => ['sometimes', 'max:100'],
        ];
    }


    /**
     * Handle a failed authorization attempt.
     *
     * @return void
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    protected function failedAuthorization()
    {
        throw new AuthorizationException(__('Seul l\'administrateur peut mettre à jour cet abonnement.'));
    }
}
