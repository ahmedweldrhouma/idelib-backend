<?php

namespace App\Domains\Patient\Models\Enums;

use Illuminate\Support\Collection;

enum PatientType: string
{
    case Chronic = 'chronic';
    case Punctual = 'punctual';
    case Covid = 'covid';
    static function values(): Collection {
        $collection = new Collection(self::cases());

        return $collection->pluck('value');
    }
}
