<?php

namespace App\Domains\Patient\Models;

use App\Domains\Patient\Models\Enums\PatientType;
use Illuminate\Database\Eloquent\Model;
use App\Domains\Patient\Models\Traits\Attribute\PatientAttribute;
use App\Domains\Patient\Models\Traits\Method\PatientMethod;
use App\Domains\Patient\Models\Traits\Relationship\PatientRelationship;
use App\Domains\Patient\Models\Traits\Scope\PatientScope;

/**
 * Class Patient.
 */
class Patient extends Model
{
    use PatientMethod,
        PatientRelationship,
        PatientScope,
        PatientAttribute;
    public const TYPE_CHRONIC = 'Chronique';
    public const TYPE_PUNCTUAL = 'Penctuel';
    public const TYPE_COVID = 'Covid';
    protected $primaryKey   = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'first_name',
        'last_name',
        'email',
        'phone',
        'adeli_number',
        'address',
        'birth_date',
        'social_security_number',
        'doctor',
        'doctor_phone',
        'antecedents',
        'vital_card',
        'mutual_card',
        'user_id',
        'nurse_id',
        'is_replacement',
        'avatar_type',
        'avatar_location',
        'zip_code',
        'died_at'
    ];



    /**
     * @var array
     */
    protected $dates = [
        'replaced_at',
        'birth_date',
        'died_at',
    ];

    /**
     * @var string[]
     */
    protected $with = [
        'user'
    ];

}
