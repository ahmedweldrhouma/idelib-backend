<?php

namespace App\Domains\Patient\Models\Traits\Relationship;

use App\Domains\Auth\Models\User;
use App\Domains\Tracking\Models\Tracking;
use App\Domains\Treatment\Models\PatientAbsence;
use App\Domains\Treatment\Models\Treatment;
use App\Domains\Treatment\Models\TreatmentRequest;
use App\Domains\CovidTreatment\Models\CovidTreatment;
use App\Domains\Vaccine\Models\Vaccine;

/**
 * Class PatientRelationship.
 */
trait PatientRelationship
{
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }


    public function nurse()
    {
        return $this->belongsTo(User::class, 'nurse_id');
    }

    public function treatments()
    {
        return $this->hasMany(Treatment::class)->where('type', '!=', 'covid');
    }

    public function treatments_covid()
    {
        return $this->hasMany(Treatment::class)->where('type', 'covid');
    }

    public function covid_treatments()
    {
        return $this->hasMany(CovidTreatment::class);
    }

    public function vaccins()
    {
        return $this->hasMany(Vaccine::class);
    }

    public function absences()
    {
        return $this->hasMany(PatientAbsence::class);
    }

    public function trackings()
    {
        return $this->hasMany(Tracking::class);
    }

    public function treatmentRequests()
    {
        return $this->hasMany(TreatmentRequest::class,'patient_id');
    }
}
