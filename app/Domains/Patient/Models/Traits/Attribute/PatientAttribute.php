<?php

namespace App\Domains\Patient\Models\Traits\Attribute;

use Illuminate\Support\Facades\Hash;

/**
 * Traits PatientAttribute.
 */
trait PatientAttribute
{

    /**
     * @return string
     */
    public function getFormedAntecedentsAttribute()
    {
        $antecedents = [];
        if ($this->isDiabetic()) {
            $antecedents[] = 'Diabétique';
        }

        if ($this->isImmunocompromised()) {
            $antecedents[] = 'Immunodéprimé';
        }

        if ($this->isPalliative_care()) {
            $antecedents[] = 'Soins palliatifs';
        }

        return collect($antecedents)->implode('<br/>');
    }


    /**
     * @return mixed
     */
    public function getPictureAttribute()
    {
        return $this->getPicture();
    }
    /**
     * @return mixed
     */
    public function getAttachmentsAttribute()
    {
        return $this->getAttachments();
    }
}
