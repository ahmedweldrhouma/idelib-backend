<?php

namespace App\Domains\Patient\Models\Traits\Scope;

/**
 * Class PatientScope.
 */
trait PatientScope
{
    /**
     * @param $query
     * @param $term
     *
     * @return mixed
     */
    public function scopeSearch($query, $term)
    {
        return $query->where( fn ($query) => $query->where('first_name', 'ilike', '%'.$term.'%')
            ->orWhere('last_name', 'ilike', '%'.$term.'%')
            ->orWhere('email', 'ilike', '%'.$term.'%')
        );
    }
}
