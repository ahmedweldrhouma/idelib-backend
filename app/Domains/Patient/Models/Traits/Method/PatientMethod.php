<?php

namespace App\Domains\Patient\Models\Traits\Method;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

/**
 * Traits PatientMethod.
 */
trait PatientMethod
{

    /**
     * @return String
     */
    public function getFirstName():String
    {
        return $this->first_name;
    }
    /**
     * @return String
     */
    public function getName():String
    {
        return $this->first_name.' '.$this->last_name;
    }

    /**
     * @return String
     */
    public function getLastName():String
    {
        return $this->last_name;
    }


    /**
     * @return bool
     */
    public function isDiabetic(): bool
    {
        $antecedents = json_decode($this->antecedents, true);
        return isset($antecedents['diabetic']) &&$antecedents['diabetic'] == true ?? false;
    }

    public function isDied(): bool
    {
        return $this->died_at != null;
    }
    /**
     * @return string
     */
    public function getAntecedentsForHumans(): string
    {
        $antecedents = $this->antecedents?collect(json_decode($this->antecedents, true))->filter(fn($item) => $item)->keys():collect([]);
        $antecedents = $antecedents->map(function ($item){
            return __($item);
        });
        return $antecedents->implode(' , ');
    }

    /**
     * @return bool
     */
    public function isImmunocompromised(): bool
    {
        $antecedents = json_decode($this->antecedents, true);
        return isset($antecedents['immunocompromised']) &&$antecedents['immunocompromised'] == true ?? false;
    }

    /**
     * @return bool
     */
    public function isPalliative_care(): bool
    {
        $antecedents = json_decode($this->antecedents, true);
        return isset($antecedents['palliative_care']) &&$antecedents['palliative_care'] == true ?? false;
    }

    public function formedAnecedants(): string
    {
        $antecedents_str = '';
        $antecedents_str .= '<ul>';
        if($this->isDiabetic()){
            $antecedents_str .= '<li>Diabétique</li>';
        }
        if($this->isImmunocompromised()){
            $antecedents_str .= '<li>Immunodéprimé</li>';
        }
        if($this->isPalliative_care()){
            $antecedents_str .= '<li>Soins palliatifs</li>';
        }
        $antecedents_str .= '</ul>';
        if ($antecedents_str == '<ul></ul>') {
            return "Non renseigné";}
        return $antecedents_str;
    }


    public function getPicture($size = false)
    {
        if($this->avatar_location == "") {
            return 'https://gravatar.com/avatar/'.md5(strtolower(trim($this->email))).'?s='.config('boilerplate.avatar.size', $size).'&d=mp';
        }
        return Storage::disk('s3')->url($this->avatar_location);
    }
    public function getMutualCard($size = false)
    {
        if($this->mutual_card) {
            return Storage::disk('s3')->url($this->mutual_card);
        }
    }

    public function getAttachments()
    {
        return collect(Storage::disk('s3')->allFiles('attachments/patient'.$this->id))->map(fn($item) => Storage::disk('s3')->url($item));
    }

    public function hasTreatmentRequests()
    {

        return ($this->treatmentRequests()
            ->where('patient_id', $this->id)
            ->first()
        ) ? true : false;;
    }
}
