<?php

namespace App\Domains\Patient\Services;

use App\Domains\Patient\Models\Enums\PatientType;
use App\Domains\Patient\Models\Patient;
use App\Exceptions\GeneralException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Services\BaseService;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

/**
 * Class PatientService.
 */
class PatientService extends BaseService
{
    /**
     * PatientService constructor.
     *
     * @param  Patient  $patient
     */
    public function __construct(Patient $patient)
    {
        $this->model = $patient;
    }

    public function all()
    {
        return $this->model->all();
    }

    public function getPatientForNurse()
    {
        return Auth::user()->patients;
    }

    public function getPatientForTeam()
    {
        $team = Auth::user()->team()->first();
        if ($team){
            return $this->model->where(function ($query) {
                $query->where('type','!=','covid')
                    ->orWhereNull('type');
            })->whereHas('nurse', function ($query) use ($team) {
                $query->whereRelation('team', 'teams.id', '=', $team->id);
            })->whereDoesntHave('covid_treatments')->get();
        }
        return $this->model->where(function ($query) {
            $query->where('type','!=','covid')
                ->orWhereNull('type');
        })->whereRelation('nurse', 'id', '=', Auth::id())->whereDoesntHave('covid_treatments')->get();
    }

    public function getPatientForTeamWithCovid()
    {
        $team = Auth::user()->team()->first();
        if ($team){
            return $this->model->whereHas('nurse', function ($query) use ($team) {
                $query->whereRelation('team', 'teams.id', '=', $team->id);
            })->get();
        }
        return $this->model->whereRelation('nurse', 'id', '=', Auth::id())->get();
    }

    public function getCovidPatientsForNurse()
    {
        $team = Auth::user()->team()->first();
        if ($team){
            return $this->model->where('type','=','covid')->whereHas('nurse', function ($query) use ($team) {
                $query->whereRelation('team', 'teams.id', '=', $team->id);
            })->get();
        }
        return $this->model->where('type','=','covid')->whereRelation('nurse', 'id', '=', Auth::id())->get();
    }

    /**
     * @param  int  $patient_id
     * @return Patient
     *
     */
    public function getPatientById($id): Patient
    {
        $patient = $this->getById($id);

        return $patient;
    }

    /**
     * @param  string  $email
     *
     */
    public function getPatientByEmail($email)
    {
        return $this->getByColumn($email, 'email');
    }


    /**
     * @param  array  $data
     * @return Patient
     *
     * @throws HttpException
     * @throws \Throwable
     */
    public function store(array $data = [], $user_id = null): Patient
    {
        DB::beginTransaction();
        if (isset($data['type'])) {
            $type = $data['type'];
            if ($type == 'patient') {
                $type = null;
            }
        }
        try {
            $avatar_location = null;
            if (isset($data['avatar_location'])){
                $avatar_location = $data['avatar_location']->store('avatars', 's3');
            }
            $patient = $this->model::create([
                "type" => $type ?? null,
                "first_name" => $data['first_name'] ?? null,
                "last_name" => $data['last_name'] ?? null,
                "phone" => $data['phone'] ?? null,
                "email" => isset($data['email'])?strtolower($data['email']) : null,
                "address" => $data['address'] ?? null,
                "birth_date" => Carbon::parse($data['birth_date'])->format('Y-m-d') ?? null,
                "user_id" => $user_id,
                "social_security_number" => $data['social_security_number'] ?? null,
                "doctor" => $data['doctor'] ?? null,
                "doctor_phone" => $data['doctor_phone'] ?? null,
                "vital_card" => $data['vital_card'] ?? null,
                "antecedents" => isset($data['antecedents']) ? json_encode($data['antecedents']) : null,
                "nurse_id" => Auth::id() ?? null,
                "avatar_location" => $avatar_location,
                "zip_code" => $data['zip_code'] ?? null,
            ]);
        } catch (Exception $e) {
            DB::rollback();
            throw new HttpException(422,  $e->getMessage());
            if (request()->is('api/*')) {
                // throw new HttpException(500, $e->getMessage());
                throw new HttpException(500, 'Un problème est survenu lors de la création du patient. Veuillez réessayer :' . $e->getMessage());
            } else {
                throw new GeneralException('Un problème est survenu lors de la création du patient. Veuillez réessayer.');
            }
        }

        DB::commit();

        return $patient;
    }


    /**
     * @param  Patient  $patient
     * @param  array  $data
     * @return Patient
     *
     * @throws \Throwable
     */
    public function update(array $data = [], Patient $patient): Patient
    {

        DB::beginTransaction();

        try {
            if (PatientType::values()->contains($data['type'])) {
                $type = $data['type'];
                if ($type == 'patient') {
                    $type = null;
                }
            }
            $avatar_location = $patient->avatar_location;
            if (isset($data['avatar_location']) && $data['avatar_location'] != $patient->avatar_location) {
                Storage::disk('s3')->delete($avatar_location);
                $avatar_location = $data['avatar_location']->store('avatars', 's3');
            }
            if (isset($data['mutual_card'])) {
                Storage::disk('s3')->delete($patient->mutual_card);
                $mutual_card = $data['mutual_card']->store('mutualCards', 's3');
            }
            $patient->update([
                'type' => $type ?? $patient->type,
                'first_name' => $data['first_name'] ?? $patient->first_name,
                'last_name' => $data['last_name'] ?? $patient->last_name,
                'email' => $data['email'] ?? $patient->email,
                'phone' => $data['phone'] ?? $patient->phone,
                "address" => $data['address'] ?? $patient->address,
                "birth_date" =>  Carbon::parse($data['birth_date'])->format('Y-m-d') ?? $patient->birth_date,
                "social_security_number" => $data['social_security_number'] ?? $patient->social_security_number,
                "doctor" => $data['doctor'] ?? $patient->doctor,
                "doctor_phone" => $data['doctor_phone'] ?? $patient->doctor_phone,
                "vital_card" => $data['vital_card'] ?? $patient->vital_card,
                "mutual_card" => $mutual_card ?? $patient->mutual_card,
                "antecedents" => isset($data['antecedents']) ? json_encode($data['antecedents']) : $patient->antecedents,
                'avatar_location' => $avatar_location,
                "zip_code" => $data['zip_code'] ?? $patient->zip_code,
                "died_at" => $data['died_at'] ?? $patient->died_at,
            ]);
        } catch (Exception $e) {
            // dd($e);
            DB::rollBack();
            throw new HttpException(422,  $e->getMessage());
            throw new HttpException(500, 'Un problème est survenu lors de la mise à jours du patient. Veuillez réessayer.');
        }

        DB::commit();
        return $patient;
    }

    /**
     * @param       $id
     * @param bool|UploadedFile  $image
     *
     * @return array|bool
     */
    public function updateAvatar($id, $image)
    {
        $patient = $this->getById($id);
        if ($image) {
            Storage::disk('s3')->delete($patient->avatar_location);
            $patient->avatar_location = $image->store('avatars', 's3');
            $patient->avatar_type = 'storage';
        }
        $patient->save();
        return $patient;
    }


    /**
     * @param       $id
     * @param bool|UploadedFile  $image
     *
     * @return array|bool
     */
    public function updateMutual($id, $image)
    {
        $patient = $this->getById($id);
        if ($image) {
            Storage::disk('s3')->delete($patient->mutual_card);
            $patient->mutual_card = $image->store('mutualCards', 's3');
        }
        $patient->save();
        return $patient;
    }


    /**
     * @param       $id
     * @param bool|UploadedFile  $image
     *
     * @return array|bool
     */
    public function updateAttachments($id, $images)
    {
        $patient = $this->getById($id);
        $attachments = [];
        if ($images) {
            foreach ($images as $attachment) {
                $path = Storage::disk('s3')->putFileAs('attachments/patient' . $patient->id, $attachment, $attachment->getClientOriginalName());
                $attachments[] = ['name' => $attachment->getClientOriginalName(), "path" => Storage::disk('s3')->url($path)];
            }
        }
        return $attachments;
    }

    /**
     * @param  Patient  $patient
     *
     * @return bool
     * @throws HttpException
     */
    public function destroy(Patient $patient): bool
    {
        if ($this->deleteById($patient->id)) {

            return true;
        }

        // throw new GeneralException(__('Un problème est survenu lors de la suppression de patient.'));
        // throw new HttpException(500, $e->getMessage());
        throw new HttpException(500, 'Un problème est survenu lors de la suppression de patient. Veuillez réessayer.');
    }

    public function getTreatments(Patient $patient)
    {
        return $patient->treatments;
    }

    public function getCovidTreatments(Patient $patient)
    {
        return $patient->treatments_covid;
    }

    public function deleteAttachment($id, $attachment)
    {
        $patient = $this->getById($id);

        if ($attachment) {
             $path = Storage::disk('s3')->exists('attachments/patient' . $patient->id.'/'.$attachment);
            if ($path)
            {$supp = Storage::disk('s3')->delete('attachments/patient' . $patient->id.'/'.$attachment);
                return true;
                $patient->save();}
            else {
                return false;
            }
        }


    }

}
