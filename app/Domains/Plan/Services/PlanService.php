<?php

namespace App\Domains\Plan\Services;

use App\Domains\Plan\Models\Plan;
use App\Exceptions\GeneralException;
use App\Services\BaseService;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

/**
 * Class PlanService.
 */
class PlanService extends BaseService
{
    /**
     * UserService constructor.
     *
     * @param  Plan  $plan
     */
    public function __construct(Plan $plan)
    {
        $this->model = $plan;
    }

    public function all()
    {
        return $this->model->all();
    }

    /**
     * @param  array  $data
     * @return Plan
     *
     * @throws GeneralException
     * @throws \Throwable
     */
    public function store(array $data = []) : Plan
    {

        DB::beginTransaction();

        try {

            $plan = $this->model::create([
                "name" => $data['name'] ?? null,
                "display_price" => $data['display_price'] ?? null,
                "product_name" => $data['product_name'] ?? null,
                "apple_id" => $data['apple_id'] ?? null,
                "google_id" => $data['google_id'] ?? null
            ]);
        } catch (Exception $e) {
            // dd($e);
            DB::rollback();
            throw new HttpException(422,  $e->getMessage());
            throw new GeneralException('Un problème est survenu lors de la création du l\'abonnement.');
        }

        DB::commit();

        return $plan;
    }

    public function update(Plan $plan, array $data= []) : Plan
    {
        DB::beginTransaction();

        try {

            $plan->update([
                "name" => $data['name'] ?? $plan->name,
                "display_price" => $data['display_price'] ?? $plan->display_price,
                "product_name" => $data['product_name'] ?? $plan->product_name,
                "apple_id" => $data['apple_id'] ?? $plan->apple_id,
                "google_id" => $data['google_id'] ?? $plan->google_id
            ]);
        } catch (Exception $e) {
            // dd($e);
            DB::rollback();
            throw new HttpException(422,  $e->getMessage());

            throw new GeneralException('Un problème est survenu lors de la mise à jour de l\'abonnement.');
        }

        DB::commit();

        return $plan;
    }

    public static function searchByName($name){
        $plan = Plan::where('product_name', $name)->first();
        if (empty($plan)) {
            throw new Exception('Aucun tarif trouvé');
        }
        return $plan;
    }

    /**
     * @param  Plan  $plan
     *
     * @return bool
     * @throws GeneralException
     */
    public function destroy(Plan  $plan): bool
    {
        if ($this->deleteById($plan->id)) {

            return true;
        }

        throw new GeneralException(__('Un problème est survenu lors de la suppression de l\'abonnement.'));
    }

}
