<?php

namespace App\Domains\Plan\Models;

use Illuminate\Database\Eloquent\Model;
use App\Domains\Plan\Models\Traits\Attribute\PlanAttribute;
use App\Domains\Plan\Models\Traits\Method\PlanMethod;
use App\Domains\Plan\Models\Traits\Relationship\PlanRelationship;
use App\Domains\Plan\Models\Traits\Scope\PlanScope;

/**
 * Class Plan.
 */
class Plan extends Model
{
    use PlanMethod,
        PlanRelationship,
        PlanScope;

    protected $primaryKey   = 'id';    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'display_price',
        'product_name',
        'interval_count',
        'apple_id',
        'google_id',
        'display_price_google',
        'display_price_apple',
    ];
}
