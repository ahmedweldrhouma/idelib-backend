<?php

namespace App\Domains\Plan\Models\Traits\Method;

use Illuminate\Support\Collection;

/**
 * Traits PlanMethod.
 */
trait PlanMethod
{

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->is_active;
    }

    /**
     * @return bool
     */
    public function isAnnual(): bool
    {
        return $this->annual;
    }

    /**
     * @return bool
     */
    public function isMonthly(): bool
    {
        return $this->monthly;
    }
}
