<?php

namespace App\Domains\Plan\Models\Traits\Scope;

/**
 * Class PlanScope.
 */
trait PlanScope
{
    /**
     * @param $query
     * @param $term
     *
     * @return mixed
     */
    public function scopeSearch($query, $term)
    {
        return $query->where( fn ($query) => $query->where('name', 'ilike', '%'.$term.'%')
        );

    }
}
