<?php

namespace App\Domains\Plan\Models\Traits\Relationship;

use App\Domains\Auth\Models\PasswordHistory;
use App\Domains\MobileSubscription\Models\MobileSubscription;
use App\Domains\Plan\Models\Plan;

/**
 * Class PlanRelationship.
 */
trait PlanRelationship
{

    public function active()
    {
        return $this->hasMany(MobileSubscription::class,'plan_id')->where('is_active', '=',true);
    }

    public function inactive()
    {
        return $this->hasMany(MobileSubscription::class,'plan_id')->where('is_active' ,'=',false);
    }

    public function subscriptions()
    {
        return $this->hasMany(MobileSubscription::class,'plan_id');
    }

}
