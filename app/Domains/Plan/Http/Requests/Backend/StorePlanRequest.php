<?php

namespace App\Domains\Plan\Http\Requests\Backend;

use App\Domains\Plan\Models\Plan;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class StorePlanRequest.
 */
class StorePlanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'max:100', Rule::unique('plans')],
            'display_price' => ['required'],
            'apple_id' => ['required'],
            'google_id' => ['required'],
            'display_price_google' => ['sometimes'],
            'display_price_apple' => ['sometimes'],
            'interval_count' => ['sometimes'],
        ];
    }

}
