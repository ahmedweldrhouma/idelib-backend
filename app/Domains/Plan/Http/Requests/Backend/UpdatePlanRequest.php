<?php

namespace App\Domains\Plan\Http\Requests\Backend;

use App\Domains\Plan\Models\Plan;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class UpdatePlanRequest.
 */
class UpdatePlanRequest extends FormRequest
{
    

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'max:100'],
            'display_price' => ['required'],
            'apple_id' => ['required'],
            'google_id' => ['required'],
            'display_price_google' => ['sometimes'],
            'display_price_apple' => ['sometimes'],
            'interval_count' => ['sometimes'],
        ];
    }


    /**
     * Handle a failed authorization attempt.
     *
     * @return void
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    protected function failedAuthorization()
    {
        throw new AuthorizationException(__('Seul l\'administrateur peut mettre à jour cet abonnement.'));
    }
}
