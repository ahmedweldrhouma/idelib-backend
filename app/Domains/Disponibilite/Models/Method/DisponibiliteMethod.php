<?php

namespace App\Domains\Disponibilite\Models\Method;

use App\Domains\Disponibilite\Models\Disponibilite;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

/**
 * Traits UserMethod.
 */
trait DisponibiliteMethod
{

    /**
     * @return String
     */
    public function getStartDate() :String
    {
         return $this->start_time!= null?$this->start_date.'T'.$this->start_time:$this->start_date;
//        return $this->start_date;
    }

    /**
     * @return String
     */
    public function getEndDate(): String
    {
               return $this->end_time!= null?$this->end_date.'T'.$this->end_time:$this->end_date;
       // return $this->end_date;
    }

    /**
     * @return String
     */
    public function getStartTime(): String
    {

        return date('H:i', strtotime($this->start_time));
    }
    /**
     * @return String
     */
    public function getEndTime(): String
    {
        return date('H:i', strtotime($this->end_time));
    }

    /**
     * @return int
     */
    public function getNumberofDispByDay($date): int
    {
        $nbr_dispo = Disponibilite::where('start_date','<=', $date)->where('end_date','>=', $date)->count();
        return $nbr_dispo;
    }


}

