<?php

namespace App\Domains\Disponibilite\Models\Traits\Relationship;

use App\Domains\Auth\Models\User;

/**
 * Class DisponibiliteRelationship.
 */
trait DisponibiliteRelationship
{
    public function nurse()
    {
        return $this->belongsTo(User::class, 'nurse_id');
    }

}
