<?php

namespace App\Domains\Disponibilite\Models;

use App\Domains\Disponibilite\Models\Method\DisponibiliteMethod;
use Illuminate\Database\Eloquent\Model;
use App\Domains\Disponibilite\Models\Traits\Relationship\DisponibiliteRelationship;

/**
 * Class Disponibilite.
 */
class Disponibilite extends Model
{
    use DisponibiliteRelationship,DisponibiliteMethod;

    protected $primaryKey   = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'start_date',
        'end_date',
        'start_time',
        'end_time',
        'nurse_id'
    ];


}
