<?php

namespace App\Domains\Disponibilite\Services;

use App\Domains\Auth\Services\UserService;
use App\Domains\Disponibilite\Models\Disponibilite;
use App\Domains\Disponibilite\Models\Method\DisponibiliteMethod;
use App\Domains\Disponibilite\Models\Relationship\DisponibiliteRelationship;
use App\Domains\Notification\Services\NotificationService;
use phpDocumentor\Reflection\Types\Collection;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Services\BaseService;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Arr;

/**
 * Class DisponibiliteService.
 */
class DisponibiliteService extends BaseService
{
    /**
     * DisponibiliteService constructor.
     *
     * @param Disponibilite $disponibilite
     */
    public function __construct(Disponibilite $disponibilite,
                                UserService $userService,
                                NotificationService $notificationService
    )
    {
        $this->model = $disponibilite;
        $this->userService = $userService;
        $this->notificationService = $notificationService;
    }

    public function all()
    {
        return $this->model->all();
    }

    public function getAllForNurse()
    {
        $disponibilites = Auth::user()->disponibilites;

        return $disponibilites;
    }

    public function getAllForTeam($filters)
    {
        $team = Auth::user()->teams->first();
        $users = $team ? $team->load(['members.disponibilites' => function ($query) use ($filters) {
            if (isset($filters['from']) && isset($filters['to'])) {
                $query->whereBetween('start_date', [$filters['from'], $filters['to']])
                    ->orWhereBetween('end_date', [$filters['from'], $filters['to']])
                    ->orWhere(fn($q) => $q->where(fn($q1) => $q1->where('start_date', '<=', $filters['from'])->where('end_date', '>=', $filters['to']))
                        ->orWhere(fn($q2) => $q2->where('end_date', '>=', $filters['from'])->where('end_date', '<=', $filters['to']))
                    );
            }
            if (isset($filters['date'])) {
                $query->where('start_date', '<=', $filters['date'])->where('end_date', '>=', $filters['date']);
            }
            if (isset($filters['user_id'])) {
                $query->where('nurse_id', '=', $filters['user_id']);
            }
        }])->members->pluck("disponibilites")->flatten() : Auth::user()->load(['disponibilites' => function ($query) use ($filters) {
            if (isset($filters['from']) && isset($filters['to'])) {
                $query->whereBetween('start_date', [$filters['from'], $filters['to']])
                    ->orWhereBetween('end_date', [$filters['from'], $filters['to']])
                    ->orWhere(fn($q) => $q->where(fn($q1) => $q1->where('start_date', '<=', $filters['from'])->where('end_date', '>=', $filters['to']))
                        ->orWhere(fn($q2) => $q2->where('end_date', '>=', $filters['from'])->where('end_date', '<=', $filters['to']))
                    );
            }
            if (isset($filters['date'])) {
                $query->where('start_date', '<=', $filters['date'])->where('end_date', '>=', $filters['date']);
            }
            if (isset($filters['user_id'])) {
                $query->where('nurse_id', '=', $filters['user_id']);
            }
        }])->disponibilites;
        return $users;

    }

    /**
     * @param int $id
     * @return Disponibilite
     *
     */
    public function getDisponibiliteById($id): Disponibilite
    {
        $disponibilite = $this->getById($id);

        return $disponibilite;
    }


    public function getDispoByNurses($array_users)
    {
        foreach ($array_users as $user) {
            $disponibilites = $this->model->where('nurse_id', $user)->get();
        }
        return $disponibilites;
    }

    /**
     * @param array $data
     * @return Disponibilite
     *
     * @throws HttpException
     * @throws \Throwable
     */
    public function store(array $data = []): Disponibilite
    {

        DB::beginTransaction();
        try {
            $team = Auth::user()->teams->first();
            $user_id = Auth::id();
            if (isset($data['user_id']) && $user_id != $data['user_id']) {
                if (!$team) {
                    throw new HttpException(401, __('Vous n\'avez pas de team'));
                }
                if (!$team->getMemberOfTeam($data['user_id'])) {
                    throw new HttpException(401, __('L\'utilisateur n\'est as un membre'));
                }
                if (!Auth::user()->isOwnerOfTeam($team->id) && !Auth::user()->isAdminOfTeam($team->id)) {
                    throw new HttpException(401, __('Vous nêtes pas un administrateur encore.'));
                }
                $user_id = $data['user_id'];
            }
            if ($this->getNurseDisponibility($user_id, $data['start_date'], $data['end_date'])->count() > 0) {
                throw new HttpException(401, __('Vous êtes déja en poste durant cette periode !'));
            }
            $disponibilite = $this->model::create([
                "start_date" => Carbon::parse($data['start_date'])->format('Y-m-d') ?? null,
                "end_date" => Carbon::parse($data['end_date'])->format('Y-m-d') ?? null,
                "start_time" => $data['start_time'] ?? null,
                "end_time" => $data['end_time'] ?? null,
                "nurse_id" => $user_id ?? null,
            ]);
            if ($disponibilite){
                $user = $this->userService->getById($data['user_id']);
                $title = "Mise à jour de votre planning" ;
                $body = Auth::user()->getFullName() . " à mise a jour votre planning. Cliquez ici pour le consulter ";
                if ($user->fcm_token) {
                    $notification = $this->notificationService->sendNotification($user->fcm_token, "disponibilite", $title, $body,['disponibilite'=>$disponibilite, "user_id" => $user_id]);
                    if ($notification) {
                        $data = [
                            'model' => 'disponibilite',
                            'subject' => $title,
                            'page' => 'disponibilite',
                            'content' => $body,
                            'model_id' => $disponibilite->id,
                            'user_id' => Auth::id(),
                            'receiver_id' => $user->id,
                        ];
                        $this->notificationService->store($data);
                    }
                }
            }
        } catch (Exception $e) {
            // dd($e);
            DB::rollback();
            throw new HttpException(500, $e->getMessage());
            // throw new HttpException(500, 'Un problème est survenu lors de la création de votre disponibilite. Veuillez réessayer.');
        }

        DB::commit();

        return $disponibilite;
    }

    public function update(Disponibilite $disponibilite, array $data = []): Disponibilite
    {
        DB::beginTransaction();

        try {

            $disponibilite->update([
                "start_date" => $data['start_date'] ?? $disponibilite->start_date,
                "end_date" => $data['end_date'] ?? $disponibilite->end_date,
                "start_time" => $data['start_time'] ?? $disponibilite->start_time,
                "end_time" => $data['end_time'] ?? $disponibilite->end_time,
                "nurse_id" => $data['nurse_id'] ?? $disponibilite->nurse_id,
            ]);
        } catch (Exception $e) {
            // dd($e);
            DB::rollback();

            throw new HttpException(500, 'Un problème est survenu lors de la mise a jour de votre disponibilite. Veuillez réessayer.');
        }

        DB::commit();

        return $disponibilite;
    }

    /**
     * @return bool
     */
    public function getDispoByNurseAndDate($id_dispo, $id, $datestart, $dateend): bool
    {
        $nurse_dispo = null;
        $disponibilites = $this->getDispoByNurses([$id]);
        foreach ($disponibilites as $dispo) {
            if ($id_dispo != $dispo->id) {
                $dates = $dispo->start_date;
                $datee = $dispo->end_date;
                if (($datestart == $dates || $dateend == $datee) || ($dateend >= $dates && $dateend <= $datee) || ($dateend > $datee && $datestart <= $datee)) {
                    $nurse_dispo = "nonn dispo";
                    break;
                }
            }
        }
        return $nurse_dispo != null ? true : false;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getNurseDisponibility($id, $datestart, $dateend)
    {
        return $this->model->where('nurse_id',$id)->where(function ($query) use ($datestart,$dateend){
            $query->whereBetween('start_date', [$datestart, $dateend])
                ->orWhereBetween('end_date', [$datestart, $dateend])
                ->orWhere(fn($q) => $q->where(fn($q1) => $q1->where('start_date', '<=', $datestart)->where('end_date', '>=', $dateend))
                    ->orWhere(fn($q2) => $q2->where('end_date', '>=', $datestart)->where('end_date', '<=',  $dateend))
                );
        })->get();
/*        $disponibilites = $this->getDispoByNurses([$id]);
        foreach ($disponibilites as $dispo) {
            if ($id_dispo != $dispo->id) {
                $dates = $dispo->start_date;
                $datee = $dispo->end_date;
                if (($datestart == $dates || $dateend == $datee) || ($dateend >= $dates && $dateend <= $datee) || ($dateend > $datee && $datestart <= $datee)) {
                    $nurse_dispo = "nonn dispo";
                    break;
                }
            }
        }
        return $nurse_dispo != null ? true : false;*/
    }


    /**
     * @param Disponibilite $disponibilite
     *
     * @return bool
     * @throws HttpException
     */
    public function destroy(Disponibilite $disponibilite): bool
    {
        if ($this->deleteById($disponibilite->id)) {
                $title = "Mise à jour de votre planning" ;
                $body = Auth::user()->getFullName() . " à supprimer votre planning. Cliquez ici pour le consulter ";
                if ($disponibilite->nurse->fcm_token) {
                    $notification = $this->notificationService->sendNotification($disponibilite->nurse->fcm_token, "deleteDisponibilite", $title, $body,['disponibilite'=>$disponibilite]);
                    if ($notification) {
                        $data = [
                            'model' => 'disponibilite',
                            'subject' => $title,
                            'page' => 'deleteDisponibilite',
                            'content' => $body,
                            'model_id' => $disponibilite->id,
                            'user_id' => Auth::id(),
                            'receiver_id' => $disponibilite->nurse->id,
                        ];
                        $this->notificationService->store($data);
                    }
            }
            return true;
        }

        return false;
    }

}
