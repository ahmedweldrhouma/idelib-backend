<?php

namespace App\Domains\Appointment\Services;

use App\Domains\Appointment\Models\Appointment;
use App\Domains\Notification\Services\NotificationService;
use App\Domains\Patient\Services\PatientService;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Services\BaseService;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


/**
 * Class AppointmentService.
 */
class AppointmentService extends BaseService
{
    /**
     * AppointmentService constructor.
     *
     * @param  Appointment  $appointment
     */
    public function __construct(Appointment $appointment,NotificationService $notificationService,PatientService $patientService)
    {
        $this->model = $appointment;
        $this->notificationService = $notificationService;
        $this->patientService = $patientService;
    }

    public function all()
    {
        return $this->model->all();
    }

    public function getAllForNurse()
    {
        $appointments = Auth::user()->appointments;

        return $appointments;
    }
    public function getAllForNurseOfDay($date)
    {
        $appointments = Auth::user()->appointments()->where('start_date','<=',$date)->where('end_date','>=',$date)->get();

        return $appointments;
    }

    public function getAllForTeam($filters = [])
    {
        $teams = Auth::user()->team;
        return $teams->isNotEmpty()?Auth::user()->team->load(['members.appointments'=>function($query)use ($filters){
            if (isset($filters['from']) && isset($filters['to'])) {
                $query->whereBetween('start_date', [$filters['from'], $filters['to']])
                    ->orWhereBetween('end_date', [$filters['from'], $filters['to']])
                    ->orWhere(fn($q) => $q->where(fn($q1) => $q1->where('start_date', '<=', $filters['from'])->where('end_date', '>=', $filters['to']))
                        ->orWhere(fn($q2) => $q2->where('start_date', '>=', $filters['from'])->where('end_date', '<=', $filters['to']))
                    );
            }
            if (isset($filters['date'])) {
                $query->where('start_date', '<=', $filters['date'])->where('end_date', '>=', $filters['date']);
            }
            if (isset($filters['user_id'])) {
                $query->where('nurse_id','=',$filters['user_id']);
            }
    }])->pluck('members.*.appointments')->flatten():Auth::user()->load(['appointments'=>function($query)use ($filters){
            if (isset($filters['from']) && isset($filters['to'])) {
                $query->whereBetween('start_date', [$filters['from'], $filters['to']])
                    ->orWhereBetween('end_date', [$filters['from'], $filters['to']])
                    ->orWhere(fn($q) => $q->where(fn($q1) => $q1->where('start_date', '<=', $filters['from'])->where('end_date', '>=', $filters['to']))
                        ->orWhere(fn($q2) => $q2->where('start_date', '>=', $filters['from'])->where('end_date', '<=', $filters['to']))
                    );
            }
            if (isset($filters['date'])) {
                $query->where('start_date', '<=', $filters['date'])->where('end_date', '>=', $filters['date']);
            }
            if (isset($filters['user_id'])){
                $query->where('nurse_id','=',$filters['user_id']);
            }
        }])->appointments;
    }

    /**
     * @param  int  $id
     * @return Appointment
     *
     */
    public function getAppointmentById($id) : Appointment
    {
        $appointment = $this->getById($id);

        return $appointment;
    }

    /**
     * @param  array  $data
     * @return Appointment
     *
     * @throws HttpException
     * @throws \Throwable
     */
    public function store(array $data = []) : Appointment
    {

        DB::beginTransaction();

        try {

            $appointment = $this->model::create([
                "title" => $data['title'] ?? null,
                "address" => $data['address'] ?? null,
                "start_date" => Carbon::parse($data['start_date'])->format('Y-m-d') ?? null,
                "end_date" => Carbon::parse($data['end_date'])->format('Y-m-d') ?? null,
                "start_time" => $data['start_time'] ?? null,
                "end_time" => $data['end_time'] ?? null,
                "note" => $data['note'] ?? null,
                "patient_id" => $data['patient_id'] ?? null,
                "nurse_id" => Auth::id(),
            ]);
            if ($appointment && isset($data['patient_id']) ){
                    $patient = $this->patientService->getPatientById($data['patient_id']);
                    $title = "Mise à jour de votre tournée" ;
                    $body = Auth::user()->getFullName() . " à mise a jour votre tournée. Cliquez ici pour le consulter ";
                    if ($patient->user != null && $patient->user->fcm_token != null) {
                        $notification = $this->notificationService->sendNotification($patient->user->fcm_token, "appointment", $title, $body,['appointment'=>$appointment, "user_id" => $patient->user->id]);
                        if ($notification) {
                            $data = [
                                'model' => 'appointment',
                                'subject' => $title,
                                'page' => 'appointment',
                                'content' => $body,
                                'model_id' => $appointment->id,
                                'user_id' => Auth::id(),
                                'receiver_id' => $patient->user->id,
                            ];
                            $this->notificationService->store($data);
                        }
                    }
            }
        } catch (Exception $e) {
            // dd($e);
            DB::rollback();
            throw new HttpException(422,  $e->getMessage());
            throw new HttpException(500,'Un problème est survenu lors de la création de RDV. Veuillez réessayer.');
        }

        DB::commit();

        return $appointment;
    }

    public function update(Appointment $appointment, array $data= []) : Appointment
    {
        DB::beginTransaction();

        try {
            $appointment->update([
                "title" => $data['title'] ?? $appointment->title,
                "address" => $data['address'] ?? $appointment->address,
                "patient_id" => $data['patient_id'] ?? $appointment->patient_id,
                "start_date" => $data['start_date'] ?? $appointment->start_date,
                "end_date" => $data['end_date'] ?? $appointment->end_date,
                "start_time" => $data['start_time'] ?? $appointment->start_time,
                "end_time" => $data['end_time'] ?? $appointment->end_time,
                "note" => $data['note'] ?? $appointment->note
            ]);
            if ($appointment){
                $title = "Mise à jour de votre tournée" ;
                $body = Auth::user()->getFullName() . " à modifier votre tournée. Cliquez ici pour le consulter ";
                if (!is_null($appointment->patient->user->fcm_token)) {
                    $notification = $this->notificationService->sendNotification($appointment->patient->user->fcm_token, "updateAppointment", $title, $body,['appointment'=>$appointment, "user_id" => $appointment->patient->user->id]);
                    if ($notification) {
                        $data = [
                            'model' => 'appointment',
                            'subject' => $title,
                            'page' => 'updateAppointment',
                            'content' => $body,
                            'model_id' => $appointment->id,
                            'user_id' => Auth::id(),
                            'receiver_id' => $appointment->patient->user->id,
                        ];
                        $this->notificationService->store($data);
                    }
                }
            }
        } catch (Exception $e) {
            // dd($e);
            DB::rollback();
            throw new HttpException(422,  $e->getMessage());

            throw new HttpException(500,'Un problème est survenu lors de la mise a jour de RDV. Veuillez réessayer.');
        }

        DB::commit();

        return $appointment;
    }

    /**
     * @param  Appointment  $appointment
     *
     * @return bool
     * @throws HttpException
     */
    public function destroy(Appointment  $appointment): bool
    {
        if ($this->deleteById($appointment->id)) {
                $title = "Mise à jour de votre tournée" ;
                $body = Auth::user()->getFullName() . " à supprimer votre tournée. Cliquez ici pour le consulter ";
                if (!is_null($appointment->patient->user->fcm_token)) {
                    $notification = $this->notificationService->sendNotification($appointment->patient->user->fcm_token, "deleteAppointment", $title, $body,['appointment'=>$appointment, "user_id" => $appointment->patient->user->id]);
                    if ($notification) {
                        $data = [
                            'model' => 'appointment',
                            'subject' => $title,
                            'page' => 'deleteAppointment',
                            'content' => $body,
                            'model_id' => $appointment->id,
                            'user_id' => Auth::id(),
                            'receiver_id' => $appointment->patient->user->id,
                        ];
                        $this->notificationService->store($data);
                    }
                }

            return true;
        }

        return false;
    }

}
