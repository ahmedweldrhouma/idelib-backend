<?php

namespace App\Domains\Appointment\Models\Method;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

/**
 * Traits UserMethod.
 */
trait AppointementMethod
{
    /**
     * @return String
     */
    public function getStartDate() :String
    {
        return $this->start_time!= null?$this->start_date.'T'.$this->start_time:$this->start_date;
    }

    /**
     * @return String
     */
    public function getEndDate(): String
    {
        return $this->end_time!= null?$this->end_date.'T'.$this->end_time:$this->end_date;
    }

    /**
     * @return String
     */
    public function getStartTime(): String
    {
        return $this->start_time;
    }
    /**
     * @return String
     */
    public function getEndTime(): String
    {
        return $this->end_time;
    }
}

