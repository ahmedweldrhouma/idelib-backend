<?php

namespace App\Domains\Appointment\Models;

use App\Domains\Appointment\Models\Method\AppointementMethod;
use Illuminate\Database\Eloquent\Model;
use App\Domains\Appointment\Models\Traits\Relationship\AppointmentRelationship;

/**
 * Class Appointment.
 */
class Appointment extends Model
{
    use AppointmentRelationship,AppointementMethod;

    protected $primaryKey   = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'address',
        'start_date',
        'end_date',
        'start_time',
        'end_time',
        'note',
        'patient_id',
        'nurse_id'
    ];
}
