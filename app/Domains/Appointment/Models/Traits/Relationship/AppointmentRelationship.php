<?php

namespace App\Domains\Appointment\Models\Traits\Relationship;

use \App\Domains\EventSort\Models\EventSort;
use App\Domains\Auth\Models\User;
use App\Domains\Patient\Models\Patient;

/**
 * Class AppointmentRelationship.
 */
trait AppointmentRelationship
{
    public function nurse()
    {
        return $this->belongsTo(User::class, 'nurse_id');
    }
    public function patient()
    {
        return $this->belongsTo(Patient::class, 'patient_id');
    }

    public function eventSort()
    {
        return $this->hasOne(EventSort::class, 'appointment_id')->where('user_id',\Auth::id());
    }

}
