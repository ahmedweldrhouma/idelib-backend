<?php

namespace App\Domains\Application\Models\Traits\Relationship;

use App\Domains\Auth\Models\User;
use App\Domains\Announcement\Models\Announcement;

/**
 * Class ApplicationRelationship.
 */
trait ApplicationRelationship
{
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function announcement()
    {
        return $this->belongsTo(Announcement::class, 'announcement_id');
    }
}
 