<?php

namespace App\Domains\Application\Models;

use Illuminate\Database\Eloquent\Model;
use App\Domains\Application\Models\Traits\Relationship\ApplicationRelationship;

/**
 * Class Application.
 */
class Application extends Model
{
    use ApplicationRelationship;


    /**
     * @var string[]
     */
    protected $fillable = [
        'user_id',
        'announcement_id',
        'accepted',
    ];

    /**
     * @var string[]
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'accepted' => 'boolean',
    ];

    public function scopeByOwnedAnnouncements($query, $annoucements)
    {
        return $query->whereIn('announcement_id', $annoucements)
                ->where('accepted', null);
    }

    public function scopeByAnnouncementId($query, $annoucement_id)
    {
        return $query->where('announcement_id', $annoucement_id)
                ->where('accepted', null);
    }

}
