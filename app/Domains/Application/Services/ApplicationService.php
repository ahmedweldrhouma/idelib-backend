<?php

namespace App\Domains\Application\Services;

use App\Domains\Application\Models\Application;
use App\Services\BaseService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class ApplicationService.
 */
class ApplicationService extends BaseService
{
    /**
     * ApplicationService constructor.
     *
     * @param  Application  $application
     */
    public function __construct(Application $application)
    {
        $this->model = $application;
    }

    /**
     * @param  array  $announcement_id
     * @param  array  $user_id
     * @return Announcement
     *
     * @throws HttpException
     */
    public function store($announcement_id): Application
    {
        DB::beginTransaction();

        try {
            $application = $this->model->create([
                "user_id" => Auth::id(),
                "announcement_id" => $announcement_id,

            ]);
        } catch (Exception $e) {
            DB::rollback();
            throw new HttpException(422,  $e->getMessage());
            throw new HttpException(500,"Un problème est survenu lors de la création de l'annonce. Veuillez réessayer.");

        }

        DB::commit();

        return $application;
    }

    public function getOwnedApplications()
    {
        return Auth::user()->applications;
    }

    public function getApplications()
    {
        $ann = Auth::user()->announcements->pluck('id');
        $applications = $this->model->ByOwnedAnnouncements($ann)->get();

        return $applications;
    }

    public function getApplicationsToAnnouncement($announcement_id)
    {
        $applications = $this->model->ByAnnouncementId($announcement_id)->get();

        return $applications;
    }

    public function accept($application): Application
    {
        DB::beginTransaction();

        try {
            $application->update([
                "accepted" => true
            ]);
        } catch (Exception $e) {
            DB::rollback();
            throw new HttpException(422,  $e->getMessage());
            throw new HttpException(500,"Un problème est survenu lors de l'acceptation. Veuillez réessayer.");

        }

        DB::commit();

        return $application;
    }

    public function deny($application)
    {
        if ($this->deleteById($application->id)) {

            return true;
        }

        throw new HttpException(500,'Un problème est survenu lors de la réfus. Veuillez réessayer.');
    }

}
