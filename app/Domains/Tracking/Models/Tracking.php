<?php

namespace App\Domains\Tracking\Models;

use App\Domains\Tracking\Models\Traits\Attribute\TrackingAttribute;
use Illuminate\Database\Eloquent\Model;
use App\Domains\Tracking\Models\Traits\Relationship\TrackingRelationship;
use App\Domains\Tracking\Models\Traits\Scope\TrackingScope;

/**
 * Class Tracking.
 */
class Tracking extends Model
{
    use TrackingRelationship,
        TrackingAttribute,
        TrackingScope;

    protected $primaryKey   = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'prescription_url',
        'pickup_date',
        'duration',
        'duration_unit',
        'daily_frequency_period',
        'daily_frequency_time',
        'absences',
        'user_id',
        'patient_id',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'pickup_date',
        'created_at',
        'updated_at',
    ];

    /**
     * @var string[]
     */
    protected $with = [
        'nurse',
        'patient'
    ];

}
