<?php

namespace App\Domains\Tracking\Models\Traits\Scope;

use Carbon\Carbon;

/**
 * Class VaccineScope.
 */
trait TrackingScope
{
    /**
     * @param $query
     * @param $term
     *
     * @return mixed
     */
    public function scopeSearch($query, $term)
    {
        return $query->with('patient')
            ->whereHas('patient', function ($query) use ($term){
                $query->where('first_name', 'ilike', '%'.$term.'%')
                    ->orWhere('last_name', 'ilike', '%'.$term.'%')
                    ->orWhere('birth_date', 'ilike', '%'.$term.'%')
                    ->orWhere('email', 'ilike', '%'.$term.'%')
                    ->orWhere('phone', 'ilike', '%'.$term.'%')
                    ->orWhere('social_security_number', 'ilike', '%'.$term.'%')
                    ->orWhere('doctor', 'ilike', '%'.$term.'%')
                    ->orWhere('doctor_phone', 'ilike', '%'.$term.'%')
                    ->orWhere('antecedents', 'ilike', '%'.$term.'%');
                })
                ->orWhere( fn ($query) =>
                $query->where('date', 'ilike', '%'.$term.'%')
                      ->orWhere('name', 'ilike', '%'.$term.'%')
                      ->orWhere('batch_number', 'ilike', '%'.$term.'%')
                      ->orWhere('trod', 'ilike', '%'.$term.'%')
                      ->orWhere('doses', 'ilike', '%'.$term.'%')
            );
    }

    /**
     * @param $id
     * @param $term
     *
     * @return mixed
     */
    public function scopeOnlyByNurse($query, $id)
    {
        return $query->where('user_id', $id);
    }

    /**
     * @param $id
     * @param $term
     *
     * @return mixed
     */
    public function scopeByType($query, $type)
    {
        return $query->where('type', $type);
    }

}
