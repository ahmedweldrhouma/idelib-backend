<?php

namespace App\Domains\Tracking\Models\Traits\Attribute;

use Illuminate\Support\Facades\Hash;

/**
 * Traits PatientAttribute.
 */
trait TrackingAttribute
{

    /**
     * @return mixed
     */
    public function getPrescriptionUrlAttribute($prescriptionUrl)
    {
        return url($prescriptionUrl);
    }
}
