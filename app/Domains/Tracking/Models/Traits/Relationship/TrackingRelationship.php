<?php

namespace App\Domains\Tracking\Models\Traits\Relationship;

use App\Domains\Auth\Models\User;
use App\Domains\Patient\Models\Patient;

/**
 * Class TrackingRelationship.
 */
trait TrackingRelationship
{
    public function patient()
    {
        return $this->belongsTo(Patient::class, 'patient_id');
    }


    public function nurse()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
