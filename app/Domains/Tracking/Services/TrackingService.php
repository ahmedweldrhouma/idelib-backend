<?php

namespace App\Domains\Tracking\Services;

use App\Domains\Tracking\Models\Tracking;
use App\Domains\Vaccine\Models\Vaccine;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Services\BaseService;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Arr;

/**
 * Class VaccineService.
 */
class TrackingService extends BaseService
{
    /**
     * VaccineService constructor.
     *
     * @param Tracking $tracking
     */
    public function __construct(Tracking $tracking)
    {
        $this->model = $tracking;
    }


    /**
     * @param array $data
     * @return Tracking
     *
     * @throws HttpException
     * @throws \Throwable
     */
    public function store(array $data = [], $patient_id): Tracking
    {

        DB::beginTransaction();

        try {
            $prescription_url = null;
            if(isset($data['prescription_url'])){
                $prescription_url = Storage::disk('s3')->putFileAs('prescription/',$data['prescription_url'],'prescription_'.$patient_id.'.'.$data['prescription_url']->getClientOriginalExtension());
            }
            $vaccine = $this->model::create([
                "prescription_url" => $prescription_url,
                "pickup_date" => Carbon::parse($data['pickup_date'])->format('Y-m-d'),
                "duration" => $data['duration'],
                "duration_unit" => $data['duration_unit'],
                "daily_frequency_period" => $data['daily_frequency_period'],
                "daily_frequency_time" => $data['daily_frequency_time'],
                "absences" => $data['absences'],
                "patient_id" => $patient_id,
                "user_id" => Auth::id() ?? null,
            ]);

        } catch (Exception $e) {
             dd($e->getMessage());
            DB::rollback();
            // throw new HttpException(500, $e->getMessage());
            throw new HttpException(500, "Un problème est survenu lors de la création d'un vaccin. Veuillez réessayer.");
            // throw new GeneralException("Un problème est survenu lors de la création d'un vaccin.");
        }
        DB::commit();

        return $vaccine;
    }

    public function getAllByPatient($patient_id)
    {
        return $this->model->where('patient_id', $patient_id)
            ->where('user_id', Auth::id())
            ->get();
    }


    /**
     * @param Tracking $tracking
     * @param array $data
     * @return Tracking
     * @throws \Throwable
     */
    public function update(array $data = [], Tracking $tracking): Tracking
    {
        DB::beginTransaction();
        try {
            $prescription_url = $tracking->prescription_url;
            if(isset($data['prescription_url'])){
                $prescription_url = $data['prescription_url']->storeAs('prescriptions', 'prescription_'.$tracking->patient_id.'_'.Carbon::now()->timestamp.'.'.$data['prescription_url']->getClientOriginalExtension());
            }
            $tracking->update([
                "prescription_url" => $prescription_url,
                "pickup_date" => Carbon::parse($data['pickup_date'])->format('Y-m-d'),
                "duration" => $data['duration'],
                "duration_unit" => $data['duration_unit'],
                "daily_frequency_period" => $data['daily_frequency_period'],
                "daily_frequency_time" => $data['daily_frequency_time'],
                "absences" => $data['absences'],
            ]);
        } catch (Exception $e) {
            // dd($e);
            DB::rollBack();
            // throw new HttpException(500, $e->getMessage());
            throw new HttpException(500, "Un problème est survenu lors de la mise à jour d'un vaccin. Veuillez réessayer.");
            // throw new GeneralException(__('There was a problem updating this vaccine. Please try again.'));
        }

        DB::commit();

        return $tracking;
    }


}
