<?php

namespace App\Console\Commands;

use App\Domains\Auth\Services\UserService;
use App\Domains\Notification\Services\NotificationService;
use App\Domains\Patient\Services\PatientService;
use App\Domains\Treatment\Models\Treatment;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;

class CheckExpiredTreatment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:expired-treatments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check for expired treatments and send notifications';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(UserService $userService,NotificationService $notificationService)
    {
        parent::__construct();
        $this->userService = $userService;
        $this->notificationService = $notificationService;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $treatments = Treatment::where('end_at', '<', now())->get();
        foreach ($treatments as $treatment){
            $user = $this->userService->getById($treatment->patient->user->id);
            if ($user->isPatient()){
                $title = "Fin des soins infermier ";
                $body = "Votre ordonance est arrivé à terme en charge de soin se terminé ";
                if ($user->fcm_token){
                    $notification = $this->notificationService->sendNotification($user->fcm_token, "endTreatment", $title, $body);
                    if ($notification) {
                        $data = [
                            'model' => 'treatment',
                            'subject' => $title,
                            'page' => 'treatments',
                            'content' => $body,
                            'model_id' => $treatment->id,
                            'user_id' => Auth::id(),
                            'receiver_id' => $user->id,
                        ];
                        $this->notificationService->store($data);
                    }
                }
            }
        }
        return $notification;
    }
}
