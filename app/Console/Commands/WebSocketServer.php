<?php

namespace App\Console\Commands;
use App\Domains\Auth\Services\UserService;
use App\Domains\Conversation\Services\ConversationService;
use App\Domains\Message\Services\MessageService;
use App\Domains\Notification\Services\NotificationService;
use App\Http\Controllers\Chat\SocketController;
use Illuminate\Console\Command;
use React\EventLoop\Factory as LoopFactory;
use Ratchet\Server\IoServer;

use Ratchet\Http\HttpServer;

use Ratchet\WebSocket\WsServer;

class WebSocketServer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'websocket:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    protected $userService;
    protected $messageService;
    protected $conversationService;
    protected $notificationsService;
    public function __construct(UserService $userService,MessageService $messageService, NotificationService $notificationsService,ConversationService $conversationService)
    {
        $this->userService = $userService;
        $this->notificationsService = $notificationsService;
        $this->messageService = $messageService;
        $this->conversationService = $conversationService;
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $wsServer = new WsServer(
            new SocketController( $this->userService, $this->messageService, $this->notificationsService, $this->conversationService)
        );
        $server = IoServer::factory(
            new HttpServer(
                $wsServer
            ),
            8090
        );
        echo "socket is running ....";
        //$wsServer->enableKeepAlive($server->loop, 5);
        $server->run();
    }
}
