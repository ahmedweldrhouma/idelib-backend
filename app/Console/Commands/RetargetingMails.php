<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use DB;
use App\Domains\RetargetingMails\Models\RetargetingMails as RetMail;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class RetargetingMails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'retargetingMails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envoie les mails de retargeting';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {


        //Inscrits non abonnés
        //  24h apres inscription IDEL + REMPLA
        /**
         * Select users.id, users.email
         * FROM users
         * where users.id not in (select user_id from mobile_subscriptions )
         * AND users.type in ('substitue', 'nurse')
         * AND users.id in (select id from
         * (select id, created_at, (EXTRACT(EPOCH FROM current_timestamp) - EXTRACT(EPOCH FROM created_at))/3600 as hours from users ) as tmp
         * where hours > 18 AND hours < 19);
         */
        $type_mail = 'nonabo24';
        $query = DB::table('users')->selectRaw('users.id, users.email')
            ->whereRaw('users.id not in (select user_id from mobile_subscriptions )')
            ->whereIn('users.type', ['substitute', 'nurse'])
            ->whereRaw('users.id in
                    (select id from (select id, created_at, (EXTRACT(EPOCH FROM current_timestamp) - EXTRACT(EPOCH FROM created_at))/3600 as hours from users )
                    as tmp where hours > 18 AND hours < 25)')
            ->whereRaw('users.id not in (select user_id from retargeting_mails where type_mail = \'' . $type_mail . '\' )')
            ->get();

        if ($query->count()) {
            Log::channel('retargetingMails')->info('Mail non abonné 24 ');

            foreach ($query as $userNonAbo24) {
                //Envoie du mail
                //Mail::to($userNonAbo24->email)->send(new \App\Mail\NonAbonne24h());
                Log::channel('retargetingMails')->info($userNonAbo24->email);

                //On enregistre l'envoie de l'email
                RetMail::create(['user_id' => $userNonAbo24->id, 'type_mail' => $type_mail, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
            }
        } else {
            Log::channel('retargetingMails')->info('Aucun Mail non abonné 24 ');

        }
        Log::channel('retargetingMails')->info('============ ');


        //  7 jours apres inscriptions
        $type_mail = 'nonabo7j';
        $query = DB::table('users')->selectRaw('users.id, users.email')
            ->whereRaw('users.id not in (select user_id from mobile_subscriptions )')
            ->whereIn('users.type', ['substitute', 'nurse'])
            ->whereRaw('users.id in
                    (select id from (select id, created_at, ROUND((EXTRACT(EPOCH FROM current_timestamp) - EXTRACT(EPOCH FROM created_at))/86400) as jours from users )
                    as tmp where jours >= 7 AND jours < 8)')
            ->whereRaw('users.id not in (select user_id from retargeting_mails where type_mail = \'' . $type_mail . '\' )')
            ->get();


        if ($query->count()) {
            Log::channel('retargetingMails')->info('Mail non abonne apres 7 jours ');

            foreach ($query as $userNonAbo7J) {
                //Envoie du mail
                //Mail::to($userNonAbo7J->email)->send(new \App\Mail\NonAbonne7j());
                Log::channel('retargetingMails')->info($userNonAbo7J->email);

                //On enregistre l'envoie de l'email
                RetMail::create(['user_id' => $userNonAbo7J->id, 'type_mail' => $type_mail, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
            }
        } else {
            Log::channel('retargetingMails')->info('Aucun Mail '.$type_mail);

        }
        Log::channel('retargetingMails')->info('============ ');



        //Désabonnés suite aux 7 jours d'essai IDEL + REMPLA
        //  Mail 1 => 12 h après la fin des 7 jours d'essai
        $type_mail = 'desabo_after_try_1';
        $query = DB::table('users')->selectRaw('users.id, users.email')
            ->whereRaw('users.id  in (select user_id from (
							select user_id,subscribed_at, expired_at, ROUND((EXTRACT(EPOCH FROM expired_at) - EXTRACT(EPOCH FROM subscribed_at))/3600) as hours
							from mobile_subscriptions ) as tmp2
							WHERE hours > 180 and hours < 190
						 )')
            ->whereIn('users.type', ['substitute', 'nurse'])
            ->whereRaw('users.id not in (select user_id from retargeting_mails where type_mail = \'' . $type_mail . '\' )')
            ->get();
        if ($query->count()) {
            Log::channel('retargetingMails')->info('Mail desabo apres 7 jours ');

            foreach ($query as $userNonAbo7J) {
                //Envoie du mail
                //Mail::to($userNonAbo7J->email)->send(new \App\Mail\Desabonne7j());
                Log::channel('retargetingMails')->info($userNonAbo7J->email);

                //On enregistre l'envoie de l'email
                RetMail::create(['user_id' => $userNonAbo7J->id, 'type_mail' => $type_mail, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
            }
        } else {
            Log::channel('retargetingMails')->info('Aucun Mail '.$type_mail);

        }
        Log::channel('retargetingMails')->info('============ ');


        //  Mail 2 48h après le mail 1 et si tjs pas abonné
        $type_mail = 'desabo_after_try_2';
        $query = DB::table('users')->selectRaw('users.id, users.email')
            ->whereRaw('users.id  in (select user_id from (
							select user_id,created_at,
							 ROUND((EXTRACT(EPOCH FROM current_timestamp) - EXTRACT(EPOCH FROM created_at))/3600) as hours
							from retargeting_mails WHERE type_mail=\'desabo_after_try_1\' ) as tmp2
							WHERE hours = 48
						) ')
            ->whereRaw('users.id not in (select user_id from retargeting_mails where type_mail = \'' . $type_mail . '\' )')->get();
        if ($query->count()) {
            Log::channel('retargetingMails')->info('Mail desabo apres 7 jours 48h apres 1ere relance  ');

            foreach ($query as $userNonAbo7J) {
                //Envoie du mail
                //Mail::to($userNonAbo7J->email)->send(new \App\Mail\Desabonne7j48h());
                Log::channel('retargetingMails')->info($userNonAbo7J->email);

                //On enregistre l'envoie de l'email
                RetMail::create(['user_id' => $userNonAbo7J->id, 'type_mail' => $type_mail, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
            }
        } else {
            Log::channel('retargetingMails')->info('Aucun Mail '.$type_mail);

        }
        Log::channel('retargetingMails')->info('============ ');


        // Abonnés actifs => 10 jours apres la prise effective d'abonnement
        $type_mail = 'abo_actif_10j';
        $query = DB::table('users')->selectRaw('users.id, users.email')
            ->whereRaw('users.id  in (select user_id from (
							select user_id,created_at,
							 ROUND((EXTRACT(EPOCH FROM current_timestamp) - EXTRACT(EPOCH FROM subscribed_at))/86400) as jours
							from mobile_subscriptions WHERE expired_at is null ) as tmp2
							WHERE jours = 10
						) ')
            ->whereRaw('users.id not in (select user_id from retargeting_mails where type_mail = \'' . $type_mail . '\' )')->get();

        if ($query->count()) {
            Log::channel('retargetingMails')->info('Mail abonne actif 10 jours apres abo');

            foreach ($query as $userNonAbo7J) {
                //Envoie du mail
                //Mail::to($userNonAbo7J->email)->send(new \App\Mail\Abonne10JActif());
                Log::channel('retargetingMails')->info($userNonAbo7J->email);

                //On enregistre l'envoie de l'email
                RetMail::create(['user_id' => $userNonAbo7J->id, 'type_mail' => $type_mail, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
            }
        } else {
            Log::channel('retargetingMails')->info('Aucun Mail '.$type_mail);

        }
        Log::channel('retargetingMails')->info('============ ');
        return 0;
    }
}
