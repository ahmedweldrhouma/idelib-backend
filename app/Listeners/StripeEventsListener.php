<?php

namespace App\Listeners;

use App\Domains\Auth\Models\User;
use App\Domains\MobileSubscription\Models\MobileSubscription;
use App\Domains\MobileSubscription\Services\MobileSubscriptionService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Laravel\Cashier\Events\WebhookReceived;
use IlluminateLog;

class StripeEventsListener
{
    /**
     * Handle received Stripe webhooks.
     */
    public function handle(WebhookReceived $event): void
    {
        Log::debug('STRIPE event => ' . $event->payload['type']);
        $mobileSubscription = new MobileSubscription();


        $mobileSubService = new MobileSubscriptionService($mobileSubscription);

        if ($event->payload['type'] === 'customer.subscription.deleted') {
            //Le client se désabonne

            Log::debug('customer => ' . $event->payload['data']['object']['customer']);
            Log::debug('heure canceled => ' . Carbon::parse($event->payload['data']['object']['canceled_at'])->format('Y-m-d H:i'));
            Log::debug('id subscription => ' . $event->payload['data']['object']['id']);
            Log::debug('date fin abo => ' . Carbon::parse($event->payload['data']['object']['current_period_end'])->format('Y-m-d H:i'));
            Log::debug( $event->payload['data']['object']);

            //renseigné le expired_at
            //On cherche le client
            $user = User::where('stripe_id', $event->payload['data']['object']['customer'])->first();
            if ($user) {
                //On cherche l'abonnement
                $sub = $mobileSubService->deactivateSubscriptionByStripeId($event->payload['data']['object']['id'], Carbon::parse($event->payload['data']['object']['current_period_end'])->format('Y-m-d H:i'));
                Log::debug('Abonnement desactive => ' . $sub);
            }
        }


        if ($event->payload['type'] === 'invoice.paid') {

            Log::debug('STRIPE event => ' . $event->payload['type']);
            Log::debug($event->payload['data']);
            //invoice.paid
            //Quand le user a payé une facture
            //il faut passer l'abonnemenent actif
            $now = Carbon::now()->addDays(30)->format('Y-m-d H:i:s');

            $sub = $mobileSubService->activateSubscriptionByStripeId($event->payload['data']['object']['id'], $now);
        }

        if ($event->payload['type'] === 'invoice.payment_failed') {
            //invoice.payment_failed
            //Paiement echec, on suspend l'abonnement jusqu'à ce qu'on ait un "invoice paid"
          //  $sub = $mobileSubService->deactivateSubscriptionByStripeId($event->payload['data']['object']['id'], Carbon::parse($event->payload['data']['object']['current_period_end'])->format('Y-m-d H:i'));
         //   Log::debug('Abonnement desactive payment failed => ' . $sub);
        }

        //invoice.updated
        /*
         * Envoyé lorsqu’un paiement aboutit ou échoue. Si le paiement aboutit,
         * l’attribut paid est défini sur true et le status sur paid. Si le paiement échoue,
         * paid est défini sur false et le status reste open. Les échecs de paiement
         * déclenchent par ailleurs un événement invoice.payment_failed.

         */

        //customer.subscription.updated
        if ($event->payload['type'] === 'customer.subscription.updated') {

        }

        //customer.subscription.created

        //payment_method.automatically_updated

        //invoice.payment_action_required
    }
}
