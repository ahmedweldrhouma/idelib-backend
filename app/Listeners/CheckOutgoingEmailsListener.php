<?php

namespace App\Listeners;

use App\Domains\GeneralConfig\Services\GeneralConfigService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Events\MessageSending;
use Illuminate\Queue\InteractsWithQueue;

class CheckOutgoingEmailsListener
{
    private $generalConfigService;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(GeneralConfigService $generalConfigService)
    {
        $this->generalConfigService = $generalConfigService;
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return null|bool
     */
    public function handle(MessageSending $event): null|bool
    {
        if (isset($event->message->mailable) && !$this->shouldSendEmail($event->message->mailable)) {
            return false;
        }
        return null;
    }

    protected function shouldSendEmail($eventName): bool
    {
        $config = $this->generalConfigService->getByColumn($eventName,'name')->first();
        return !is_null($config) && $config->value == 1;
    }
}
