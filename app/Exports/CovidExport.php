<?php

namespace App\Exports;

use App\Domains\CovidTreatment\Models\CovidTreatment;
use App\Domains\Vaccine\Models\Vaccine;
use App\Domains\Treatment\Models\Treatment;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Shared\Date;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\Auth;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class CovidExport implements
    FromView,
    WithEvents,
    WithColumnFormatting,
    ShouldAutoSize
{

    use Exportable;
    protected $patients;

    public function __construct(
        $patients
    )
    {
        $this->patients = $patients;
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getDelegate()->getStyle('D1')
                    ->getAlignment()
                    ->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
            }
        ];
    }

    public function view(): View
    {
        return view('backend.exports.covid.covid_treatments', [
            'patients' => $this->patients
        ]);
    }

    public function columnFormats(): array
    {
        return [
            'D' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'E' => NumberFormat::FORMAT_NUMBER,
            'G' => NumberFormat::FORMAT_NUMBER,
        ];
    }
/*    public function view(): View
    {
        switch ($this->type) {
            case ('covid_treatments'):
                if (($this->start_date != null) && ($this->end_date != null)) {
                    $treatments = CovidTreatment::where([['user_id', '=', Auth::user()->getQueueableId()], ['type', $this->covid_type], ['date', '>=', $this->start_date], ['date', '<=', $this->end_date]])->get();
                } else if ($this->start_date != null) {
                    $treatments = CovidTreatment::where([['user_id', '=', Auth::user()->getQueueableId()], ['type', $this->covid_type], ['date', '>=', $this->start_date]])->get();
                } else if ($this->end_date != null) {
                    $treatments = CovidTreatment::where([['user_id', '=', Auth::user()->getQueueableId()], ['type', $this->covid_type], ['date', '<=', $this->end_date]])->get();
                } else {
                    $treatments = CovidTreatment::where([['user_id', '=', Auth::user()->getQueueableId()], ['type', $this->covid_type]])->get();
                }
                $type = $this->covid_type;

                return view('backend.exports.covid.covid_treatments', compact('treatments', 'type'));

            case ('vaccines'):
                if (($this->start_date != null) && ($this->end_date != null)) {
                    $treatments = Vaccine::where([['user_id', '=', Auth::user()->getQueueableId()], ['date', '>=', $this->start_date], ['date', '<=', $this->end_date]])->get();
                } else if ($this->start_date != null) {
                    $treatments = Vaccine::where([['user_id', '=', Auth::user()->getQueueableId()], ['date', '>=', $this->start_date]])->get();
                } else if ($this->end_date != null) {
                    $treatments = Vaccine::where([['user_id', '=', Auth::user()->getQueueableId()], ['date', '<=', $this->end_date]])->get();
                } else {
                    $treatments = Vaccine::where('user_id', '=', Auth::user()->getQueueableId())->get();
                }
                return view('backend.exports.covid.vaccines', compact('treatments'));

            default:
                $treatments = 'Something went wrong.';
        }

    }*/
}
