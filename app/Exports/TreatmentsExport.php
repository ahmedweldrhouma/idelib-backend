<?php

namespace App\Exports;

use App\Domains\Treatment\Models\Treatment;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class TreatmentsExport implements 
    FromView,
    ShouldAutoSize
{

    use Exportable;
    protected $treatments;

    public function __construct($treatments)
    {
        $this->treatments = $treatments;
    }

    public function view(): View
    {
        $treatments = Treatment::whereKey($this->treatments)->with('passages')->get();
        $morning_passages = $treatments->flatMap(function ($item) {
            return $item->PassageByPeriod('matin');
        });
        $morning_passages = $morning_passages->isEmpty() ? null : $morning_passages[0];
        
        $midday_passages = $treatments->flatMap(function ($item) {
            return $item->PassageByPeriod('midi');
        });
        $midday_passages = $midday_passages->isEmpty() ? null : $midday_passages[0];

        $night_passages = $treatments->flatMap(function ($item) {
            return $item->PassageByPeriod('soir');
        });
        $night_passages = $night_passages->isEmpty() ? null : $night_passages[0];
        
        return view('backend.exports.treatments',compact(
            'treatments', 
            'morning_passages', 
            'midday_passages',
            'night_passages'
        ));
    }
}
