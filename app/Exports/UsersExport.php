<?php

namespace App\Exports;

use App\Domains\Auth\Models\User;
use App\Http\Livewire\Backend\UsersTable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Ramsey\Collection\Collection;

class UsersExport implements  FromCollection

/*
    FromQuery,
    WithHeadings,
    WithMapping,
    ShouldAutoSize
    */
{
    use Exportable;
    protected $users;

    public function __construct($users)
    {
        $this->users = $users;
    }
    public function collection()
    {
        return  $this->users;
    }

/*
    public function __construct($users)
    {
        $this->users = $users;
    }

    public function headings(): array
    {
        return [
            "#",
            "Email",
            "Prénom",
            "Nom",
            "Type",
            "Abonnement",
            "Statut",
            "Téléphone",
            "Date d'inscription",
            "Souscription via iap",
            "Admin subscription",
            "Abonné(e) le",
            "Expire le",
            "Équipe",
            "Id associés",
        ];
    }

    public function map($user): array
    {
        return [
            $user->id,
            $user->email,
            $user->first_name,
            $user->last_name,
            $user->type,
            $user->subscription_type,
            $user->active,
            $user->phone,
            date("d-m-Y",strtotime($user->created_at)),
            $user->operating_system, // TODO: Souscription via iap
            '---', //TODO: Admin subscription
            '---', //TODO: Subscription date
            '---', //TODO: Subscription expire date
            '---', //TODO: Team name
            '---', //TODO: Members ids
        ];
    }

    public function query()
    {
        return User::query()->whereKey($this->users);
    }*/
}
