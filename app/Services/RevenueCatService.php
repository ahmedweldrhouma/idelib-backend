<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class RevenueCatService
{
    /**
     * Revenue cat api Token
     */
    const API_KEY = "Bearer sk_ObmwnYEMdOXFfcWpYVKNwOEHRQIFs";
    /**
     * Revenue cat endpoint
     */
    const END_POINT = "https://api.revenuecat.com/v1/subscribers/:app_user_id";

    /**
     * @param $app_user_id
     * @return array|false[]
     */
    public static function get($app_user_id){
        return self::call('GET',['app_user_id' => $app_user_id]);
    }

    /**
     * @param $method
     * @param $params
     * @param $data
     * @return array|false[]
     */
    private static function call($method,$params,$data = []){
        $url = self::END_POINT;
        foreach($params as $key=>$param){
            $url = str_replace(':'.$key , $param,$url);
        }
        $request = Http::withHeaders([
            'Authorization' => self::API_KEY
        ])->{$method}($url,$data);
        if ($request->successful()){
            return [
                'success' => true,
                'data' => json_decode($request->body(),true)];
        }
        return [
                'success' => false,
        ];
    }



}
