<?php

namespace App\Http\Controllers\Chat;

use App\Domains\Auth\Services\UserService;
use App\Domains\Conversation\Services\ConversationService;
use App\Domains\Message\Models\Message;
use App\Domains\Message\Services\MessageService;
use App\Domains\Notification\Services\NotificationService;
use App\Http\Controllers\Controller;
use App\Http\Resources\ConversationResource;
use App\Http\Resources\MessageResource;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;

class SocketController extends Controller implements MessageComponentInterface
{
    const SUCCESS_STATUS = 201;
    const ERROR_STATUS = 422;
    protected $users;

    protected $clients;
    protected $conversations;
    protected $userService;
    protected $messageService;
    protected $conversationService;
    protected $notificationsService;
    protected $liveConversations;

    public function __construct(UserService $userService, MessageService $messageService, NotificationService $notificationsService, ConversationService $conversationService)
    {
        $this->userService = $userService;
        $this->notificationsService = $notificationsService;
        $this->messageService = $messageService;
        $this->conversationService = $conversationService;
        $this->clients = [];
        $this->users = [];
        $this->conversations = [];
        $this->liveConversations = [];
    }

    public function onOpen(ConnectionInterface $conn)
    {
        $query = $conn->httpRequest->getUri()->getQuery();
        parse_str($query, $data);
        if (isset($data['role']) && $data['role']=="system"){
            $conn->send("connexion established");
            echo "system connected";
            return 0;
        }
        $user_id = $data['user_id'];
        $user = $this->userService->where('id', $user_id)->first();
        if (!$user) {
            echo "error : no user with id $user_id";
            $conn->close();
        }
        try {
            $conversations = $this->conversationService->getUserConversation($user_id);
        } catch (\Exception $e) {
            echo "error : {$e->getMessage()}";
        }

        foreach ($conversations as $conversation) {
            // check if conversation already exists
            if (!array_key_exists($conversation->id, $this->conversations)) {
                $this->conversations[$conversation->id] = array();
            }
            // check if user already exists in the conversation
            if (!array_key_exists($user_id, $this->conversations[$conversation->id])) {
                // add user to the conversation
                $this->conversations[$conversation->id][$user_id] = $conn->resourceId;
            }
        }
        $conn->user_id = $user_id;
        $conn->fcm_token = $user->fcm_token;
        $conn->conversations = $conversations->pluck('id');
        $this->clients[$conn->resourceId] = $conn;
        $this->users[$user_id] = $conn->resourceId;
        $conn->send("connexion established");
        echo $user->name . " connected";
        var_dump($this->users, array_keys($this->clients), $this->conversations);
    }

    public function onMessage(ConnectionInterface $from, $msg)
    {
        try {
            $user_id = isset($this->clients[$from->resourceId])?$this->clients[$from->resourceId]->user_id:null;
        var_dump("received msg : $msg, id:$user_id");
            $data = json_decode($msg, true);
            switch ($data["action"]) {
                case "get_messages":
                    $response['status'] = self::SUCCESS_STATUS;
                    $response['messages'] = MessageResource::collection($this->conversationService->getById($data['conversation_id'])->messages()->with('sender:id,first_name,last_name,avatar_location')->orderBy('created_at', 'desc')->get());
                    $response['action'] = "conversation_messages";
                    $from->send(json_encode($response));
                    break;
                case "new_message":
                    if (!isset($this->conversations[$data['conversation_id']])){
                        $conversation = $this->conversationService->getById($data['conversation_id']);
                        if ($conversation && $conversation->isParticipant($user_id)){
                            $response1['status'] = self::SUCCESS_STATUS;
                            $response1['conversation'] = [
                                "id" => $conversation->id,
                                "name" => $conversation->name,
                                "color" => $conversation->color,
                                "participants" => $conversation->participants->map->only(['id', 'first_name', 'first_name', 'avatar'])
                            ];
                            $response1['action'] = "new_conversation";
                            $this->conversations[$data['conversation_id']] = array();
                            foreach ($conversation->participants as $participant) {
                                if (isset($this->users[$participant->id])) {
                                    $this->conversations[$conversation->id][$participant->id] = $this->users[$participant->id];
                                    $this->clients[$this->users[$participant->id]]->send(json_encode($response1));
                                }
                            }
                        }
                    }
                    $response['status'] = self::SUCCESS_STATUS;
                    $response['conversation_id'] = $data['conversation_id'];
                    $message = $this->messageService->store($data, $user_id);
                    if ($message && isset($this->conversations[$data['conversation_id']])) {
                        $response['messages'] = [new MessageResource($message)];
                        $response['action'] = "new_message";
                        foreach ($this->conversations[$data['conversation_id']] as $id => $user) {
                            if (isset($this->clients[$user])) {
                                $this->clients[$user]->send(json_encode($response));
                                if ($id != $user_id && $this->clients[$user]->fcm_token != null) {
                                    $title = "Nouveau message" ;
                                    $body = $message->sender->name . " : ".($message->type==Message::TYPE_TEXT?$message->content:"a envoyer une piéce jointe");
                                    $this->notificationsService->sendNotification($this->clients[$user]->fcm_token,'newMessage',$title,$body,['avatar'=>$message->sender->avatar,'color'=>$message->sender->color??"#4287f5",'conversation'=>new ConversationResource($message->conversation)]);
                                }
                            }
                        }
                    }
                    break;
                case "new_conversation":
                    $response['status'] = self::SUCCESS_STATUS;
                    $conversation = $this->conversationService->store($data, $user_id);
                    $response['conversation'] = [
                        "id" => $conversation->id,
                        "name" => $conversation->name,
                        "color" => $conversation->color,
                        "participants" => $conversation->participants->map->only(['id', 'first_name', 'first_name', 'picture'])
                    ];
                    $response['action'] = "new_conversation";
                    if ($conversation && !isset($this->conversations[$conversation->id])) {
                        $this->conversations[$conversation->id] = array();
                        foreach ($conversation->participants as $participant) {
                            if (isset($this->users[$participant->id])) {
                                $this->conversations[$conversation->id][$participant->id] = $this->users[$participant->id];
                                $this->clients[$this->users[$participant->id]]->send(json_encode($response));
                            }
                        }
                    }
                    break;
                case "get_conversations":
                    $response['status'] = self::SUCCESS_STATUS;
                    $response['action'] = "get_conversations";
                    $conversations = $this->conversationService->getUserConversation($user_id);
                    $response['conversations'] = ConversationResource::collection($conversations->sortBy('lastMessage.created_at'));
                    $from->send(json_encode($response));
                    break;
                case "leaveConversations":
                    $response['status'] = self::SUCCESS_STATUS;
                    foreach ($data['conversations'] as $conversation){
                        if (isset($this->conversations[$conversation]) && $this->conversations[$conversation][$data['user_id']]){
                            unset($this->conversations[$conversation][$data['user_id']]);
                        }
                    }
                    $from->send(json_encode($response));
                    break;
                case "get_connected":
                    $response['status'] = self::SUCCESS_STATUS;
                    $response['connected'] = [];
                    $response['action'] = "get_connected";
                    foreach ($data['conversations'] as $conversation) {
                        array_push($response['connected'], ['conversation_id' => $conversation, 'connected' => array_keys($this->conversations[$conversation] ?? [])]);
                    }
                    $from->send(json_encode($response));
                    break;
                case "ping":
                    $from->send(json_encode(['action'=>"pong"]));
                    break;
                default:
                    break;
            }

        } catch (\Exception $e) {
            $data['status'] = self::ERROR_STATUS;
            $from->send(json_encode($data));
            echo $e->getMessage();
        }
    }

    public function onClose(ConnectionInterface $conn)
    {
        $user_id = $this->clients[$conn->resourceId]->user_id;
        var_dump("logout", $this->clients[$conn->resourceId]->conversations);
        foreach ($this->clients[$conn->resourceId]->conversations as $conversation_id) {
            unset($this->conversations[$conversation_id][$user_id]);
            if (empty($this->conversations[$conversation_id])) {
                unset($this->conversations[$conversation_id]);
            }
            /*foreach ($this->conversations[$conversation_id] as $user) {
                if ($conn->ressourceId !== $user) {
                    $this->clients[$user]->send(json_encode([
                        'action' => "disconnection",
                        'conversation_id' => $conversation_id,
                        'user_id' => $user_id,
                    ]));
                }
            }*/
        }
        unset($this->users[$user_id]);
        unset($this->clients[$conn->resourceId]);

    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo "An error has occurred: {$e->getMessage()} \n";
        $conn->close();
    }
}
