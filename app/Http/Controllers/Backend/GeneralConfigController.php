<?php

namespace App\Http\Controllers\Backend;

use App\Domains\GeneralConfig\Models\GeneralConfig;
use App\Domains\GeneralConfig\Services\GeneralConfigService;
use App\Domains\MobileSubscription\Models\MobileSubscription;
use App\Domains\MobileSubscription\Services\MobileSubscriptionService;
use Database\Factories\TeamFactory;
use Database\Factories\UserFactory;
use Illuminate\Http\Request;

/**
 * Class SubscriptionController.
 */
class GeneralConfigController
{
    /**
     * @var GeneralConfigService
     */
    protected $generalConfigService;

    /**
     * SubscriptionController constructor.
     *
     * @param  MobileSubscriptionService  $mobileSubscriptionService
     */
    public function __construct(GeneralConfigService $generalConfigService)
    {
        $this->generalConfigService = $generalConfigService;
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $generalConfig = GeneralConfig::all();
        $team =  TeamFactory::new()->make();
        $user =  UserFactory::new()->make();
        $sender =  UserFactory::new()->make();
        return view('backend.email.index',compact('generalConfig','team','user','sender'));
    }

    public function update(Request $request)
    {
        $this->generalConfigService->update($request->name, $request->value);
        return Response()->json([
            'success' => true
        ]);
    }
}
