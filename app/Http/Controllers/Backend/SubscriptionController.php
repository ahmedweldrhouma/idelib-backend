<?php

namespace App\Http\Controllers\Backend;

use App\Domains\MobileSubscription\Models\MobileSubscription;
use App\Domains\MobileSubscription\Services\MobileSubscriptionService;
use App\Domains\Patient\Models\Patient;
use Illuminate\Http\Request;
use Laravel\Cashier\Subscription;

/**
 * Class SubscriptionController.
 */
class SubscriptionController
{
    /**
     * @var MobileSubscriptionService
     */
    protected $mobileSubscriptionService;

    /**
     * SubscriptionController constructor.
     *
     * @param  MobileSubscriptionService  $mobileSubscriptionService
     */
    public function __construct(MobileSubscriptionService $mobileSubscriptionService)
    {
        $this->mobileSubscriptionService = $mobileSubscriptionService;
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('backend.subscription.index');
    }

    public function edit( MobileSubscription $mobileSubscription)
    {
        $user = $mobileSubscription->user();
        return view('backend.subscription.edit', [
            'mobileSubscription' => $mobileSubscription
        ])->withUser($user);
    }


    /**
     * @param  Plan  $plan
     * @return mixed
     *
     * @throws \App\Exceptions\GeneralException
     */
    public function destroy(Plan  $plan)
    {
        $this->planService->destroy($plan);

        return redirect()->route('admin.subscription.index')->withFlashSuccess('L\'abonnement a été supprimé avec succès.');
    }
}
