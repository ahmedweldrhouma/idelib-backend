<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Domains\Appointment\Models\Appointment;
use App\Domains\Appointment\Services\AppointmentService;
use App\Http\Resources\AppointmentResource;
use App\Http\Requests\Api\Appointment\StoreAppointmentRequest;
use App\Http\Requests\Api\Appointment\UpdateAppointmentRequest;
use DB;


/**
 * Class AppointmentController.
 */
class AppointmentController
{
    /**
     * @var AppointmentService
     */
    protected $AppointmentService;

    /**
     * AppointmentController constructor.
     *
     * @param  AppointmentService  $AppointmentService
     */
    public function __construct(AppointmentService $AppointmentService)
    {
        $this->AppointmentService = $AppointmentService;
    }

    public function getDetails(Appointment $Appointment)
    {
        return new AppointmentResource($Appointment);
    }

    public function getUserAppointments()
    {
        return AppointmentResource::collection($this->AppointmentService->getAllForNurse());
    }

    /**
     * @param  StoreAppointmentRequest  $request
     * @return mixed
     *
     */
    public function store(StoreAppointmentRequest $request)
    {
        try {
            $Appointment = $this->AppointmentService->store($request->validated());

            return back()->withFlashSuccess('Le rendez vous a été ajouté avec succès.');

        } catch (Exception $e) {
            // dd($e);
            return response()->json(['error' => 'Un problème est survenu lors de la création de lannonce.'], 422);
        }
    }

    /**
     * @param  UpdateAppointmentRequest  $request
     * @param  Appointment  $Appointment
     * @return mixed
     *
     */
    public function update(UpdateAppointmentRequest $request, Appointment $Appointment)
    {
        try {
            $Appointment = $this->AppointmentService->update($Appointment,$request->validated());

            return new AppointmentResource($Appointment);

        } catch (Exception $e) {
            // dd($e);
            return response()->json(['error' => 'Un problème est survenu lors de la mise a jour de lannonce.'], 422);
        }
    }


    /**
     * @param  Appointment  $Appointment
     * @return mixed
     *
     * @throws \App\Exceptions\GeneralException
     */
    public function destroy(Appointment  $Appointment)
    {
        $this->AppointmentService->destroy($Appointment);

        return response()->json([
            'status' => 'success'
        ], 200);
    }
}
