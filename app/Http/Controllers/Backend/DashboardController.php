<?php

namespace App\Http\Controllers\Backend;

use App\Domains\Appointment\Services\AppointmentService;
use App\Domains\Auth\Services\UserService;
use App\Domains\CovidTreatment\Services\CovidTreatmentService;
use App\Domains\MobileSubscription\Models\MobileSubscription;
use App\Domains\MobileSubscription\Services\MobileSubscriptionService;
use App\Domains\Patient\Models\Enums\PatientType;
use App\Domains\Patient\Models\Patient;
use App\Domains\Patient\Services\PatientService;
use App\Domains\Treatment\Services\TreatmentService;
use App\Domains\Vaccine\Services\VaccineService;
use App\Http\Requests\Api\Calendar\getCalendarRequest;
use App\Http\Resources\AbsenceResource;
use App\Http\Resources\PassageResource;
use App\Http\Resources\UserResource;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Exports\UsersExport;
use Excel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use function Clue\StreamFilter\fun;

/**
 * Class DashboardController.
 */
class DashboardController
{
    protected $appointmentService;
    protected $frequency = [
        "L" => "1",
        "M" => "2",
        "Me" => "3",
        "J" => "4",
        "V" => "5",
        "S" => "6",
        "D" => "7"
    ];
    public function __construct(AppointmentService $appointmentService,PatientService $patientService,
                                TreatmentService $treatmentService, CovidTreatmentService $covidTreatmentService,
                                VaccineService $vaccineService, UserService $userService, MobileSubscriptionService $mobileSubscription)
    {
        $this->patientService = $patientService;
        $this->treatmentService = $treatmentService;
        $this->covidTreatmentService = $covidTreatmentService;
        $this->vaccineService = $vaccineService;
        $this->appointmentService = $appointmentService;
        $this->userService = $userService;
        $this->mobileSubscription = $mobileSubscription;
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        $patients = $this->patientService->getPatientForTeamWithCovid()->groupBy(function ($query){
            return $query->treatments->first()->classic_type??($query->type!=""?$query->type:"chronic");
        })->map(function ($type, $key) {
            return ['name' => __($key), 'y' => $type->count()];
        });
        $statsAdmin = null;
        if( Auth::user()->isSuperAdmin() || Auth::user()->isAdmin()){
            $statsAdmin['nbNurse'] = $this->userService->getByType('nurse')->count();
            $statsAdmin['nbPatient'] = $this->userService->getByType('patient')->count();
            $statsAdmin['nbSubstitute'] = $this->userService->getByType('substitute')->count();

            $totalInscr=  $statsAdmin['nbNurse'] +  $statsAdmin['nbPatient'] +$statsAdmin['nbSubstitute'];





            $statsAdmin['totalRegister'] = $statsAdmin['nbNurse'] + $statsAdmin['nbPatient'] + $statsAdmin['nbSubstitute'];

            $getSubscriptionNurse = $this->mobileSubscription->getSubscriptionByTypeUser('nurse');
            $statsAdmin['nbNurseMensuel'] = $getSubscriptionNurse->where('plan_type', 'mensuel')->first()->nb;
            $statsAdmin['nbNurseAnnuel'] = $getSubscriptionNurse->where('plan_type', 'annuel')->first()->nb;
            $tmpTot = $statsAdmin['nbNurseAnnuel'] + $statsAdmin['nbNurseMensuel'];

            $statsAdmin['nbNurseAbonne'] = $tmpTot;

            $statsAdmin['nbNurseMensuelPercent'] = ($statsAdmin['nbNurseMensuel'] * 100) / $tmpTot ;
            $statsAdmin['nbNurseAnnuelPercent'] =  ($statsAdmin['nbNurseAnnuel'] * 100) / $tmpTot ;


            $getSubscriptionSubstitue = $this->mobileSubscription->getSubscriptionByTypeUser('substitute');
            $statsAdmin['nbSubstituteAnnuel'] = $getSubscriptionSubstitue->where('plan_type', 'annuel')->first()->nb;
            $statsAdmin['nbSubstituteMensuel'] = $getSubscriptionSubstitue->where('plan_type', 'mensuel')->first()->nb;
            $tmpTot = $statsAdmin['nbSubstituteAnnuel'] + $statsAdmin['nbSubstituteMensuel'];
            $statsAdmin['nbSubstituteAbonne'] = $tmpTot;

            $nbTotuserAbonne = $statsAdmin['nbSubstituteAbonne'] + $statsAdmin['nbNurseAbonne'];

            $statsAdmin['nbSubstituteAbonnePercent'] = ($statsAdmin['nbSubstituteAbonne'] * 100) / $nbTotuserAbonne ;
            $statsAdmin['nbNurseAbonnePercent'] = ($statsAdmin['nbNurseAbonne'] * 100) / $nbTotuserAbonne ;


            $statsAdmin['nbSubstituteMensuelPercent'] = ($statsAdmin['nbSubstituteMensuel'] * 100) / $tmpTot ;
            $statsAdmin['nbSubstituteAnnuelPercent'] =  ($statsAdmin['nbSubstituteAnnuel'] * 100) / $tmpTot ;



            $getSubscriptionNurseAll = $this->mobileSubscription->getSubscriptionByTypeUser('nurse', false);
            $statsAdmin['nbNurseMensuelAll'] = $getSubscriptionNurseAll->where('plan_type', 'mensuel')->first()->nb;
            $statsAdmin['nbNurseAnnuelAll'] = $getSubscriptionNurseAll->where('plan_type', 'annuel')->first()->nb;
            $tmpTot = $statsAdmin['nbNurseAnnuelAll'] + $statsAdmin['nbNurseMensuelAll'];

            $statsAdmin['nbNurseAbonneAll'] = $tmpTot;

            $statsAdmin['nbNurseMensuelPercentAll'] = ($statsAdmin['nbNurseMensuelAll'] * 100) / $tmpTot ;
            $statsAdmin['nbNurseAnnuelPercentAll'] =  ($statsAdmin['nbNurseAnnuelAll'] * 100) / $tmpTot ;




            $getSubscriptionSubstitueAll = $this->mobileSubscription->getSubscriptionByTypeUser('substitute', false);
            $statsAdmin['nbSubstituteAnnuelAll'] = $getSubscriptionSubstitueAll->where('plan_type', 'annuel')->first()->nb;
            $statsAdmin['nbSubstituteMensuelAll'] = $getSubscriptionSubstitueAll->where('plan_type', 'mensuel')->first()->nb;
            $tmpTot = $statsAdmin['nbSubstituteAnnuelAll'] + $statsAdmin['nbSubstituteMensuelAll'];
            $statsAdmin['nbSubstituteAbonneAll'] = $tmpTot;

            $nbTotuserAbonneAll = $statsAdmin['nbSubstituteAbonneAll'] + $statsAdmin['nbNurseAbonneAll'];

            $statsAdmin['nbSubstituteAbonnePercentAll'] = ($statsAdmin['nbSubstituteAbonneAll'] * 100) / $nbTotuserAbonneAll ;
            $statsAdmin['nbNurseAbonnePercentAll'] = ($statsAdmin['nbNurseAbonneAll'] * 100) / $nbTotuserAbonneAll ;


            $statsAdmin['nbSubstituteMensuelPercentAll'] = ($statsAdmin['nbSubstituteMensuelAll'] * 100) / $tmpTot ;
            $statsAdmin['nbSubstituteAnnuelPercentAll'] =  ($statsAdmin['nbSubstituteAnnuelAll'] * 100) / $tmpTot ;



        }
        return view('backend.dashboard', ['stats' => $patients??[], 'statsAdmin' => $statsAdmin??[]]);
    }

    public function events(getCalendarRequest $request)
    {
        $date = $request->date;
        $treatments = $this->treatmentService->getAllForNurseOfDay($date)->map(function ($treatment) use ($date) {
            $patient = $treatment->patient;
            $absences = $patient?$patient->absences:collect([]);
            $data= collect();
            if ($treatment->type == 'classic') {
                $title = 'Soin Classique ' . __($treatment->classic_type);
            } else {
                $title = 'Soin ' . strtoupper(__($treatment->type));
                if ($treatment->type == 'bsi') {
                    $title .= '-' . strtoupper($treatment->bsi_type);
                }
            }
            $dows = explode(',', $treatment->monthly_frequency);
            $days_name = [];
            setlocale(LC_ALL, 'en');
            foreach ($dows as $k => $dow) {
                $days_name[] = strtolower(strftime('%A', strtotime("Sunday +{$this->frequency[trim($dow)]} days")));
            }
            if (in_array(strtolower(Carbon::parse($date)->format('l')), $days_name)) {
                        foreach ($treatment->passages as $passage) {
                            if ( (is_null($patient->died_at) || Carbon::parse($date)->isBefore(Carbon::parse($patient->died_at))) && (( is_null($treatment->stoped_at) || Carbon::parse($date)->isBefore($treatment->stoped_at) ))  ) {
                                $data->push([
                                    'type' => "treatment",
                                    'id' => $treatment->id,
                                    'title' => $title,
                                    'absent' => $absences->filter(fn($item) => Carbon::parse($date)->between($item->absent_from, $item->absent_to))->isNotEmpty(),
                                    'start' => __($passage->period) . ($passage->time_slot ? ', ' . __($passage->time_slot) : ""),
                                    'patient_name' => $patient->getName(),
                                    'patient_url' => route('admin.patient.view', $patient->id),
                                ]);
                            }
                        }
            }
            return $data;
        })->flatten(1);
        $appointments = $this->appointmentService->getAllForNurseOfDay($date)->map(function ($appointment){
            return [
                'type' => "appointment",
                'id' => $appointment->id,
                'title' => $appointment->title,
                'address' => $appointment->address,
                'note' => $appointment->note,
                'start' => Carbon::parse($appointment->start_time)->format('H:i'),
                'end' => Carbon::parse($appointment->end_time)->format('H:i'),
            ];
        });
        return response()->json($treatments->merge($appointments));
    }
}
