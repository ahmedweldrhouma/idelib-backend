<?php

namespace App\Http\Controllers\Backend;

use App\Domains\Treatment\Http\Requests\Backend\StoreTreatmentRequest;
use App\Domains\Treatment\Http\Requests\Backend\UpdateTreatmentRequest;
use Illuminate\Http\Request;
use App\Domains\Treatment\Models\Treatment;
use App\Domains\Treatment\Services\TreatmentService;
use App\Domains\Auth\Services\UserService;
use Illuminate\Support\Facades\DB;

/**
 * Class TreatmentController.
 */
class TreatmentController
{
    /**
     * @var TreatmentService
     * @var UserService
     */
    protected $treatmentService;
    protected $userService;

    /**
     * TreatmentController constructor.
     *
     * @param  TreatmentService  $treatmentService
     * @param  UserService  $userService
     */
    public function __construct(TreatmentService $treatmentService, UserService $userService)
    {
        $this->treatmentService = $treatmentService;
        $this->userService = $userService;
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('backend.treatment.index');
    }

    public function getByType(Request $request)
    {
        $type = $request->type;

        return view('backend.treatment.index', compact('type'));
    }


    public function getAllByNurse(Request $request)
    {
        $type = 'covid';

        return view('backend.treatment.index', compact('type'));
    }

    /**
     * @return mixed
     */
    public function create()
    {
        return view('backend.treatment.create');
    }

    /**
     * @param  StoreTreatmentRequest  $request
     * @return mixed
     *
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function store(StoreTreatmentRequest $request)
    {
        // DB::beginTransaction();
        try {
            $user = $this->userService->store($request->only([
                'user_type',
                'first_name',
                'last_name',
                'email',
                'password',
                'phone',
                'address',
                'birth_date',
            ], ));
            $treatment = $this->TreatmentService->store($request->validated(), $user->id);

            return redirect()->route('admin.Treatment.index', $treatment)->withFlashSuccess(__('Le Treatment a été créé avec succès.'));

        } catch (Exception $e) {
            throw new HttpException(422,  $e->getMessage());
            return redirect()->back()->withFlashDanger($e);
            // DB::rollback();
        }

        // DB::commit();

    }

    /**
     * @param  Treatment  $treatment
     * @return mixed
     *
     * @throws \App\Exceptions\GeneralException
     */
    public function destroy(Treatment  $treatment)
    {
        $this->treatmentService->destroy($treatment);
        $this->userService->delete($treatment->user);

        return redirect()->route('admin.treatment.index')->withFlashSuccess('Le Treatment a été supprimé avec succès.');
    }
}
