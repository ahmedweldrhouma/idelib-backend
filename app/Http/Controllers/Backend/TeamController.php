<?php

namespace App\Http\Controllers\Backend;

use App\Domains\Auth\Http\Requests\Backend\User\UpdateStatusRequest;
use App\Domains\Auth\Models\User;
use App\Domains\Auth\Services\UserService;
use App\Domains\Team\Requests\StoreTeamRequest;
use Illuminate\Http\Request;
use App\Domains\Team\Models\Team;
use App\Domains\Team\Services\TeamService;
use Illuminate\Support\Facades\Auth;

class TeamController
{
    /**
     * @var TeamService
     */
    protected $teamservice;

    /**
     * TeamController constructor.
     *
     * @param TeamService $teamservice
     * @param UserService $userService
     */
    public function __construct(TeamService $teamService, UserService $userService)
    {
        $this->teamService = $teamService;
        $this->userService = $userService;
    }

    /**
     * @param  StoreTeamRequest  $request
     * @return mixed
     *
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function store(StoreTeamRequest $request)
    {
        $team = $this->teamService->store($request->validated());
        return redirect()->back()->withFlashSuccess(__('L\'équipe a été créé avec succès.'));
    }

    public function detachMember(Team $team, $user_id)
    {
        if (!Auth::user()->isAdminOfTeam($team->id)) {
            return redirect()->back()->withFlashDanger(__('Vous n\'étes pas un administrateur de cette équipe.'));
        }
        $user = $this->userService->getById($user_id);
        if ($user->isMemberOfTeam($team->id)) {
            if ($this->teamService->detachMember($team, $user)) {
                return redirect()->back()->withFlashSuccess(__('Membre a été supprimer de cette équipe avec succès.'));
            }
        }
        return redirect()->back()->withFlashDanger(__('Cet utilisateur n\'est pas membre de cette équipe.'));
    }

    public function leaveteam()
    {
        if ($this->teamService->detachMember(Auth::user()->team->first(), Auth::user())) {
            return Response()->json([__('Membre a été supprimer de cette équipe avec succès.')]);
        }
        return Response()->json([__('error')],422);
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
            return view('backend.team.index');

    }
    public function makeadmin(UpdateStatusRequest $request, User $user)
    {

        $res = $this->userService->makeAdminOfTeam($user);
        return view('backend.team.index');
    }

    public function defineNewOwner(UpdateStatusRequest $request,Team $team)
    {
        $user = $this->userService->getById($request->user_id);
        $res = $this->teamService->defineNewOwner($team,$user);
        return redirect()->back();
    }

    /**
     * @return mixed
     */
    public function createTeamUser()
    {
        $nurses = $this->userService->getByType("nurse");
        $admins = $this->userService->getByType("admin");
        $users = collect($nurses, $admins);
        return view('backend.auth.user.includes.create_team_user', ['users' => $users]);
    }
}
