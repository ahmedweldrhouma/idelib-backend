<?php

namespace App\Http\Controllers\Backend;

use App\Domains\Plan\Http\Requests\Backend\StorePlanRequest;
use App\Domains\Plan\Http\Requests\Backend\UpdatePlanRequest;
use Illuminate\Http\Request;
use App\Domains\Plan\Models\Plan;
use App\Domains\Plan\Services\PlanService;

/**
 * Class PlanController.
 */
class PlanController
{
    /**
     * @var PlanService
     */
    protected $PlanService;

    /**
     * PlanController constructor.
     *
     * @param  PlanService  $planService
     */
    public function __construct(PlanService $planService)
    {
        $this->planService = $planService;
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        //dd($this->PlanService->mobile());
        return view('backend.plan.index');
    }

    /**
     * @return mixed
     */
    public function create()
    {
        return view('backend.plan.create');
    }

    /**
     * @param  StoreUserRequest  $request
     * @return mixed
     *
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function store(StorePlanRequest $request)
    {
        $plan = $this->planService->store($request->validated());

        return redirect()->route('admin.plan.index', $plan)->withFlashSuccess(__('L\'abonnement a été créé avec succès.'));
    }

    /**
     * @param  Plan  $plan
     * @return mixed
     */
    public function edit(Request $request, Plan $plan)
    {
        return view('backend.plan.edit', compact('plan'));
    }

    /**
     * @param  UpdatePlanRequest  $request
     * @param  Plan  $plan
     * @return mixed
     *
     * @throws \Throwable
     */
    public function update(UpdatePlanRequest $request, Plan $plan)
    {
        $this->planService->update($plan, $request->validated());

        return redirect()->route('admin.plan.edit', $plan->id)->withFlashSuccess('L\'abonnement a été mis à jour avec succès.');
    }

    /**
     * @param  Plan  $plan
     * @return mixed
     *
     * @throws \App\Exceptions\GeneralException
     */
    public function destroy(Plan  $plan)
    {
        $this->planService->destroy($plan);

        return redirect()->route('admin.plan.index')->withFlashSuccess('L\'abonnement a été supprimé avec succès.');
    }
}
