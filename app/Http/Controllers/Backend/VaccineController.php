<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Domains\Vaccine\Models\Vaccine;
use App\Domains\Patient\Models\Patient;
use App\Domains\Vaccine\Services\VaccineService;
// use App\Http\Requests\Api\Vaccine\StoreVaccineRequest;
// use App\Http\Requests\Api\Vaccine\UpdateVaccineRequest;


/**
 * Class VaccineController.
 */
class VaccineController
{
    /**
     * @var VaccineService
     */
    protected $vaccineService;

    /**
     * VaccineController constructor.
     *
     * @param  VaccineService  $vaccineService
     */
    public function __construct(VaccineService $vaccineService)
    {
        $this->vaccineService = $vaccineService;
    }

    
    public function getAllByNurse()
    {
        return view('backend.vaccine.index');
    }
    
}
