<?php

namespace App\Http\Controllers\Backend;

use App\Domains\AdminSubscription\Http\Requests\Backend\StoreAdminSubscriptionRequest;
use App\Domains\AdminSubscription\Http\Requests\Backend\UpdateAdminSubscriptionRequest;
use App\Domains\Auth\Services\UserService;
use Illuminate\Http\Request;
use App\Domains\AdminSubscription\Models\AdminSubscription;
use App\Domains\AdminSubscription\Services\AdminSubscriptionService;
use App\Domains\Plan\Services\PlanService;

/**
 * Class AdminSubscriptionController.
 */
class AdminSubscriptionController
{
    /**
     * @var AdminSubscriptionService
     */
    protected $adminSubscriptionService;
    protected $planService;
    protected $userService;

    /**
     * AdminSubscriptionController constructor.
     *
     * @param  AdminSubscriptionService  $adminSubscriptionService
     * @param  PlanService  $planService
     */
    public function __construct(AdminSubscriptionService $adminSubscriptionService, PlanService  $planService, UserService  $userService)
    {
        $this->adminSubscriptionService = $adminSubscriptionService;
        $this->planService = $planService;
        $this->userService = $userService;
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('backend.adminSubscription.index');
    }

    /**
     * @return mixed
     */
    public function create()
    {
        $plans = $this->planService->all();

        return view('backend.adminSubscription.create', compact('plans'));
    }

    /**
     * @param  adminSubscriptionService  $request
     * @return mixed
     *
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function store(StoreAdminSubscriptionRequest $request)
    {
        $user = $this->userService->getById($request->user_id);
        if (!$user){
            return redirect()->back()->withFlashDanger(__('Utilisateur n\'existe pas.'));
        }
        if ($user->allAdminSubscription){
            return redirect()->back()->withFlashDanger(__('Utilisateur possede deja un abonnement.'));
        }
        $admin_subscription = $this->adminSubscriptionService->store($request->validated());
        return redirect()->route('admin.admin_subscription.index', $admin_subscription)->withFlashSuccess(__('L\'abonnement a été créé avec succès.'));
    }

    /**
     * @param  Plan  $plan
     * @return mixed
     */
    public function edit(Request $request, AdminSubscription $admin_subscription)
    {
        $plans = $this->planService->all();
        return view('backend.adminSubscription.edit', compact('admin_subscription', 'plans'));
    }

    /**
     * @param  adminSubscriptionService  $request
     * @param  AdminSubscription  $adminSubscription
     * @return mixed
     *
     * @throws \Throwable
     */
    public function update(UpdateAdminSubscriptionRequest $request, AdminSubscription $admin_subscription)
    {
        $this->adminSubscriptionService->update($admin_subscription, $request->validated());
        return redirect()->route('admin.admin_subscription.edit', $admin_subscription->id)->withFlashSuccess('L\'abonnement a été mis à jour avec succès.');
    }

    /**
     * @param  AdminSubscription  $admin_subscription
     * @return mixed
     *
     * @throws \App\Exceptions\GeneralException
     */
    public function destroy(AdminSubscription $admin_subscription)
    {
        $this->adminSubscriptionService->destroy($admin_subscription);
        return redirect()->route('admin.admin_subscription.index')->withFlashSuccess('L\'abonnement a été supprimé avec succès.');
    }
}
