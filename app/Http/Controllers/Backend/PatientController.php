<?php

namespace App\Http\Controllers\Backend;

use App\Domains\Patient\Http\Requests\Backend\StorePatientRequest;
use App\Domains\Patient\Http\Requests\Backend\UpdateDocumentsRequest;
use App\Domains\Patient\Http\Requests\Backend\UpdatePatientRequest;
use Illuminate\Http\Request;
use App\Domains\Patient\Models\Patient;
use App\Domains\Patient\Services\PatientService;
use App\Domains\Auth\Services\UserService;
use App\Domains\Patient\Models\Enums\PatientType;
use Exception;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

/**
 * Class PatientController.
 */
class PatientController
{
    /**
     * @var PatientService
     * @var UserService
     */
    protected $patientService;
    protected $userService;

    /**
     * PatientController constructor.
     *
     * @param  PatientService  $patientService
     * @param  UserService  $userService
     */
    public function __construct(PatientService $patientService, UserService $userService)
    {
        $this->patientService = $patientService;
        $this->userService = $userService;
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('backend.patient.index');
    }

    public function getByType(Request $request)
    {
        $type = $request->type;
        //$this->checkType($type);
        return view('backend.patient.index', [
            'type' => $type
        ]);
    }

    /**
     * @return mixed
     */
    public function create(Request $request)
    {
        $type = $request->type;
        //$this->checkType($type);
        return view('backend.patient.create', [
            'type' => $type
        ]);
    }

    public function edit(Patient $patient)
    {
        return view('backend.patient.edit', [
            'patient' => $patient
        ]);
    }

    public function view(Patient $patient)
    {
        return view('backend.patient.view', [
            'patient' => $patient
        ]);
    }

    /**
     * @param  StorePatientRequest  $request
     * @return mixed
     *
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function store(StorePatientRequest $request)
    {
        try {

            $patient = $this->patientService->store($request->validated());
            $output = $this->patientService->updateAttachments(
                $patient->id,
                $request->has('documents') ? $request->documents : false
            );
            $mutual_card = $this->patientService->updateMutual(
                $patient->id,
                $request->has('mutual_card') ? $request->file('mutual_card') : false
            );
            return redirect()->back()->withFlashSuccess(__('Le patient a été créé avec succès.'));
            //return redirect()->route('admin.patient.index', $patient)->withFlashSuccess(__('Le patient a été créé avec succès.'));
        } catch (Exception $e) {
            return redirect()->back()->withFlashDanger($e->getMessage());
        }
    }
        /**
     * @param Request $request
     *
     * @return mixed
    */
    public function uploadMutual($patient_id, Request $request)
    {
        $output = $this->patientService->updateMutual(
            $patient_id,
            $request->has('mutual_card') ? $request->file('mutual_card') : false
        );
        return [
            'mutual_card' => $output->getMutualCard()
        ];
    }

    public function update(Patient $patient, UpdatePatientRequest $request)
    {
        try {
            $this->patientService->update($request->validated(), $patient);

            return redirect()->back()->withFlashSuccess(__('Le patient a été modifiée avec succès.'));
        } catch (Exception $e) {
            return redirect()->back()->withFlashDanger($e);
        }
    }

    /**
     * @param  Patient  $patient
     * @return mixed
     *
     * @throws \App\Exceptions\GeneralException
     */
    public function destroy(Patient $patient)
    {
        $this->patientService->destroy($patient);
        if ($patient->user) {
            $this->userService->delete($patient->user);
        }
        return back()->withFlashSuccess('Le patient a été supprimé avec succès.');
    }

    public function checkType(string $type)
    {
        if (PatientType::values()->doesntContain($type)) {
            App::abort(404);
        }
    }

    public function updateAttachments(Patient $patient, UpdateDocumentsRequest $request)

 {
     $output = $this->patientService->updateAttachments(
        $patient->id,
        $request->has('documents') ? $request->documents : false
    );
    return back()->withFlashSuccess('Les documents ont été mis à jour avec succès .');

}
     /**
      * @param Request $req
     * @return mixed
     *
     */
public function deleteAttachments(Patient $patient,Request $req)
    {
      $attachment_deleted = $this->patientService->deleteAttachment(
       $patient->id,
       $req->filename
      );
      if ($attachment_deleted)

 { return back()->withFlashSuccess('Le document a été supprimé avec succès .');}
 else
 {
    return back()->withErrors('Le document ne peut pas être supprimé.');
 }
}
}
