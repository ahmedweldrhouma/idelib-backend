<?php

namespace App\Http\Controllers\Backend;

use App\Domains\Disponibilite\Models\Disponibilite;
use App\Domains\Disponibilite\Services\DisponibiliteService;
use App\Domains\GeneralConfig\Models\GeneralConfig;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DisponibilityController extends Controller
{
    /**
     * @var DisponibiliteService
     */
    protected $disponibiliteService;

    /**
     * @param DisponibiliteService $disponibiliteService
     */
    public function __construct(DisponibiliteService $disponibiliteService)
    {
        $this->disponibiliteService = $disponibiliteService;

    }
    public function store(Request $request)
    {
        $start_date = $request->start_date;
        // $periode = $request->periode;
        $end_date = $request->end_date;
        $config = GeneralConfig::find(1);
        $max_nurses_by_day = 2; //$config->getMaxNursesByDay();
        //Vérifier si je suis disponible ou nn
        $disponibilites = $this->disponibiliteService->getAllForNurse();
        if ($this->disponibiliteService->getDispoByNurseAndDate(null,Auth::user()->id, $start_date,$end_date)) {
            return redirect()->back()->with('message' , 'Vous êtes déja en poste durant cette periode ! ');
        }

        if ($request->periode == 'matin') {
            $start_time = '08:00';
            $end_time = '12:00';
        } else if ($request->periode == 'apresmidi') {
            $start_time = '13:00';
            $end_time = '18:00';
        } else if ($request->periode == 'jour') {
            $start_time = '08:00';
            $end_time = '18:00';
        } else if ($request->periode == 'semaine') {
            // Il va travailler une semaine complète
            $start_time = '08:00';
            $end_time = '18:00';
            $end_date = date("Y-m-d", strtotime($start_date . "+6 days"));
        }

        foreach ($disponibilites as $diso) {
            if ($diso->getNumberofDispByDay($start_date) == $max_nurses_by_day) {
                return redirect()->back()->with('message' , 'Nombre Maximum de nurses par jour atteint  ! ');
            }
        }
        $title = Auth::user()->getFullName();

        $diponibility = $this->disponibiliteService->store([
            "start_date" => $start_date,
            "end_date" => $end_date,
            "start_time" => $start_time,
            "end_time" => $end_time,
            "user_id" => $request->get('nurse_id')
        ]);
        return redirect()->route('admin.calendar.getAllByType', 'planning');
    }


    public function update(Request $request)
    {
        try {
            $dispo = $this->disponibiliteService->getDisponibiliteById($request->id);

            $disponibilites = $this->disponibiliteService->getAllForNurse();

            if ($this->disponibiliteService->getDispoByNurseAndDate($request->id, Auth::user()->id, $request->start_date, $request->end_date)) {
                return Response()->json(['message'=> 'Vous êtes déja en poste durant cette periode ! '],422);
                //return redirect()->back()->with('message', 'Vous êtes déja en poste durant cette periode ! ');
            }

            foreach ($disponibilites as $diso) {
                $max_nurses_by_day = 2;
                if ($diso->getNumberofDispByDay($request->start_date) == $max_nurses_by_day) {
                   // return redirect()->back()->with('message', 'Nombre Maximum de nurses par jour atteint  ! ');
                    return Response()->json(['message'=>'Nombre Maximum de nurses par jour atteint  ! '],422);

                }
            }
            $team = Auth::user()->team->first();
            if (Auth::id() == $dispo->nurse_id || ($team && $team->admin_id == Auth::id())) {
               $disponibilite = $this->disponibiliteService->update($dispo, $request->all());
                //$dispo->update($request->all());
                return response()->json(['disponibilite' => $disponibilite]);
            } else {
                return Response()->json(['message'=>'Vous n\'etes pas l\'admin de cette équipe! '],422);
 //               return redirect()->back()->with('message', 'Vous n\'etes pas l\'admin de cette équipe! ');
            }
        }catch(\Exception $e){
            dd($e->getMessage());
        }

    }

  function destroy(Request $request)
    {
        $disponibility = $this->disponibiliteService->getDisponibiliteById($request->id);
        $this->disponibiliteService->destroy($disponibility);

        return response()->json([
            'status' => 'success'
        ], 200);
    }
}
