<?php

namespace App\Http\Controllers\Backend;

use App\Domains\Appointment\Services\AppointmentService;
use App\Domains\Disponibilite\Models\Disponibilite;
use App\Domains\Disponibilite\Services\DisponibiliteService;
use App\Domains\Auth\Services\UserService;
use App\Domains\CovidTreatment\Models\CovidTreatment;
use App\Domains\GeneralConfig\Models\GeneralConfig;
use App\Domains\GeneralConfig\Services\GeneralConfigService;
use App\Domains\Patient\Services\PatientService;
use App\Domains\Treatment\Models\Treatment;
use App\Domains\Vaccine\Models\Vaccine;
use App\Http\Requests\Api\Calendar\getCalendarRequest;
use App\Http\Resources\UserResource;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Domains\Treatment\Services\TreatmentService;
use App\Domains\CovidTreatment\Services\CovidTreatmentService;
use App\Domains\Vaccine\Services\VaccineService;
use Illuminate\Support\Facades\Auth;
use function Sentry\startTransaction;


/**
 * Class CalendarController.
 */
class CalendarController
{
    protected $frequency = [
        "L" => "1",
        "M" => "2",
        "Me" => "3",
        "J" => "4",
        "V" => "5",
        "S" => "6",
        "D" => "7"
    ];
    protected $period = [
        'morning' => [
            "< 7h" =>
                [
                    'start_time' => "05:00",
                    'end_time' => "07:00"
                ],
            "> 7h" =>
                [
                    'start_time' => "07:00",
                    'end_time' => "10:00"
                ],
        ],
        'evening' =>
            [
                'start_time' => "12:00",
                'end_time' => "14:00"
            ],
        'night' =>
            [
                "< 20h" =>
                    [
                        'start_time' => "18:00",
                        'end_time' => "20:00"
                    ],
                "> 20h" =>
                    [
                        'start_time' => "20:00",
                        'end_time' => "22:00"
                    ]
            ],
    ];
    /**
     * @var VaccineService
     * @var TreatmentService
     * @var CovidTreatmentService
     * @var PatientService
     * @var DisponibiliteService
     */
    protected $vaccineService;
    protected $covidTreatmentService;
    protected $treatmentService;
    protected $disponibiliteService;
    protected $patientService;
    protected $userService;
    protected $appointmentService;

    /**
     * CalendarController constructor.
     *
     * @param VaccineService $vaccineService
     * @param TreatmentService $treatmentService
     * @param CovidTreatmentService $covidTreatmentService
     * @param PatientService $patientService
     * @param UserService $userService
     * @param DisponibiliteService $disponibiliteService
     */
    public function __construct(AppointmentService $appointmentService, CovidTreatmentService $covidTreatmentService, TreatmentService $treatmentService, VaccineService $vaccineService, DisponibiliteService $disponibiliteService, PatientService $patientService, UserService $userService)
    {
        $this->treatmentService = $treatmentService;
        $this->covidTreatmentService = $covidTreatmentService;
        $this->vaccineService = $vaccineService;
        $this->disponibiliteService = $disponibiliteService;
        $this->appointmentService = $appointmentService;
        $this->patientService = $patientService;
        $this->userService = $userService;

    }


    public function index()
    {
        return view('backend.calendar.index');
    }

    public function getAllByType($type)
    {
        $team = Auth::user()->team->first();
        $users = $team ? $team->members : collect([Auth::user()]);
        if ($type == 'tour') {
            return view('backend.calendar.index', ['users' => $users]);
        } else if ($type == 'planning') {
            return view('backend.calendar.planning', ['users' => $users]);
            $team = Auth::user()->team;
            $members = ($team->isNotEmpty() ? $team->pluck('members') : collect([Auth::user()]))->map(function ($item, $key) {
                return [
                    'id' => $item->id,
                    'name' => $item->getFullName(),
                    'color' => $item->pivot->color ?? $item->getColor(),
                ];
            });
            return view('backend.calendar.planning', ['members' => $members]);
        }
        abort(404);
    }

    public function getData(getCalendarRequest $request, $type)
    {
        $data = [];
        $filters = $request->validated();
        $filters['from'] = Carbon::parse($filters['start'])->format("Y-m-d");
        $filters['to'] = Carbon::parse($filters['end'])->format("Y-m-d");
        if ($type == 'tour') {
            if (!isset($filters['filtre_trait']) || $filters['filtre_trait'] == "soin") {
                $treatments = $this->treatmentService->getAllForTeam($filters);
                foreach ($treatments as $trait) {
                    //$patient_id = $this->treatmentService->getPatientByIdTreat($trait->id);
                    $patient = $trait->patient;
                    $absences = $patient->absences;

                    $title = '';
                    if ($trait->type == 'classic') {
                        $title = 'Soin Classique ' . $trait->classic_type;
                    } else {
                        $title = 'Soin ' . strtoupper($trait->type);
                        if ($trait->type == 'bsi') {
                            $title .= '-' . strtoupper($trait->bsi_type);
                        }
                    }
                    $dows = explode(',', $trait->monthly_frequency);
                    $days_name = [];
                    setlocale(LC_ALL, 'en');
                    foreach ($dows as $k => $dow) {
                        $days_name[] = strtolower(strftime('%A', strtotime("Sunday +{$this->frequency[trim($dow)]} days")));
                    }
                    $period = explode(" ", $trait->period);
                    $passages = $trait->passages->map(fn($passage) => $passage->getForPatient());
                    $startDate = Carbon::parse($filters['start']); // Get the first friday. If $fromDate is a friday, it will include $fromDate as a friday
                    $endDate = Carbon::parse($filters['end']);
                    for ($date = $startDate; $date->lte($endDate); $date->addWeek()) {
                        foreach ($days_name as $day) {
                            $event_day = $date->clone();
                            $event_day = $event_day->modify("this {$day}");
                            if ($event_day->between(Carbon::parse($trait->start_at), Carbon::parse($trait->end_at)) && (!is_null($trait->stoped_at) ? $event_day->isBefore(Carbon::parse($trait->stoped_at)) : true) && (!is_null($patient->died_at) ? $event_day->isBefore(Carbon::parse($patient->died_at)) : true)) {
                                foreach ($trait->passages as $passage) {
                                    $start_time = $passage->period == "evening" ? $this->period[$passage->period]['start_time'] : $this->period[$passage->period][$passage->time_slot]['start_time'];
                                    $end_time = $passage->period == "evening" ? $this->period[$passage->period]['end_time'] : $this->period[$passage->period][$passage->time_slot]['end_time'];
                                    $data[] = [
                                        'id' => $trait->id,
                                        'title' => $title,
                                        'start' => $event_day->format('Y-m-d') . ' ' . $start_time,
                                        'end' => $event_day->format('Y-m-d') . ' ' . $end_time,
                                        'extendedProps' => [
                                            'patient_name' => $patient->getName(),
                                            'nurse_name' => $trait->nurse->getFullName(),
                                            'creation_date' => $trait->created_at->format("H:i d-m-Y"),
                                            'start_date' => $trait->start_at->format("d-m-Y"),
                                            'passages' => $passages,
                                            'end_date' => $trait->end_at->format("d-m-Y"),
                                            'edit_link' => route('admin.patient.edit', $patient),
                                            'show_link' => route('admin.patient.view', $patient),
                                            'period' => $period[0] . ' ' . __($period[1]),
                                            'heb_frequency' => array_map(fn($item) => __($item), $days_name),
                                            'patient_img' => $patient->getPicture(),
                                            'absent' => $absences->filter(fn($item) => $event_day->between($item->absent_from->format("Y-m-d"), $item->absent_to->format("Y-m-d")))->isNotEmpty(),
                                            'patient_mail' => $patient->email,
                                            'patient_phone' => $patient->phone,
                                            'patient_type' => __($patient->type),
                                            'original_start' => $trait->start_at->format('Y-m-d'),
                                            'original_end' => $trait->end_at->format('Y-m-d')
                                        ],
                                        //'daysOfWeek' => $dows,
                                        'color' => '#17bddb'
                                    ];
                                }
                            }
                        }
                    }
                }
            }
            if (!isset($filters['filtre_trait']) || $filters['filtre_trait'] == "covid") {
                $covid_treatments = $this->covidTreatmentService->getAllForTeam($filters);
                foreach ($covid_treatments as $covid) {
                    $patient_id_cov = $this->covidTreatmentService->getPatientByIdCov($covid->id);
                    $patient_cov = $this->patientService->getPatientById($patient_id_cov);
                    if ($covid->type == 'visite') {
                        $type = 'Visite à domicile';
                    } else if ($covid->type == 'pcr') {
                        $type = 'Test PCR';
                    } else if ($covid->type == 'antigen') {
                        $type = 'Test antigénique';
                    } else {
                        $type = '';
                    }

                    $data[] = [
                        'id' => $covid->id,
                        'title' => $type,
                        'start' => $covid->date,
                        'end' => date('Y-m-d', strtotime($covid->date . ' +1 day')),
                        'extendedProps' => [
                            'patient_name' => $patient_cov->getName(),
                            'patient_img' => $patient_cov->getPicture(),
                            'patient_mail' => $patient_cov->email,
                            'patient_phone' => $patient_cov->phone,
                            'patient_type' => $patient_cov->type,
                        ],
                        'color' => '#9c27b0'
                    ];
                }
            }
            if (!isset($filters['filtre_trait']) || $filters['filtre_trait'] == "vaccin") {
                $vaccines = $this->vaccineService->getAllForTeam($filters);
                foreach ($vaccines as $vacc) {
                    $patient_id_vacc = $this->vaccineService->getPatientByIdVacc($vacc->id);
                    $patient_vacc = $this->patientService->getPatientById($patient_id_vacc);
                    $data[] = [
                        'id' => $vacc->id,
                        'title' => 'Vaccin',
                        'start' => $vacc->date,
                        'end' => date('Y-m-d', strtotime($vacc->date . ' +1 day')),
                        'extendedProps' => [
                            'patient_name' => $patient_vacc->getName(),
                            'patient_img' => $patient_vacc->getPicture(),
                            'patient_mail' => $patient_vacc->email,
                            'patient_phone' => $patient_vacc->phone,
                            'patient_type' => $patient_vacc->type,
                        ],
                        'color' => '#df5da4'
                    ];
                }
            }
            if (!isset($filters['filtre_trait']) || $filters['filtre_trait'] == "appointment") {
                foreach ($this->appointmentService->getAllForTeam($filters) as $key => $item) {
                    $team = $item->nurse->team;
                    $patient = $item->patient;
                    $all = [];
                    $all['id'] = $item->id;
                    $all['title'] = 'Appointment';
                    $all['color'] = '#bf9824';
                    $all['extendedProps'] = [
                        'title' => $item->title,
                        'original_start' => $item->start_date,
                        'original_end' => $item->end_date,
                        'original_start_time' => $item->start_time,
                        'original_end_time' => $item->end_time,
                    ];
                    $period = CarbonPeriod::create($filters['from'], $filters['to']);
                    foreach ($period as $key1 => $date) {
                        if ($date->between(Carbon::parse($item['start_date']), Carbon::parse($item['end_date'])) && (!$patient || (!is_null($item->stoped_at) || $date->isBefore(Carbon::parse($item->stoped_at))) && (!is_null($patient->died_at) || $date->lte(Carbon::parse($patient->died_at))))) {
                            $all['start'] = $date->format('Y-m-d') . ' ' . $item->start_time;
                            $all['end'] = $date->format('Y-m-d') . ' ' . $item->end_time;
                            $data[] = $all;
                        }
                    }
                };
            }
            return response()->json($data);
        } else {
            $search = $filters['user_id'] ?? "";
            $events = [];
            if ($search != "") {
                $array_users = explode(',', $search);
                $disponibilites = $this->disponibiliteService->getDispoByNurses($array_users);
            } else {
                // Param for getAllforTeam ???????????????
                $disponibilites = $this->disponibiliteService->getAllForTeam($search);
            }
            foreach ($disponibilites as $dispo) {
                for ($date = Carbon::parse($dispo->start_date); $date->lte(Carbon::parse($dispo->end_date)); $date->addDay()) {
                    $team_user = $this->userService->getById($dispo->nurse_id);
                    $image_user = $team_user->getPicture();
                    $color_user = $team_user->getColor();
                    $events[] = [
                        'id' => $dispo->id,
                        'title' => $team_user->getFullName(),
                        'start' => $dispo->start_time != null ? $date->format('Y-m-d') . 'T' . $dispo->start_time : $date->format('Y-m-d'),
                        'end' => $dispo->end_time != null ? $date->format('Y-m-d') . 'T' . $dispo->end_time : $date->format('Y-m-d'),
                        'color' => $color_user,
                        'extendedProps' => ['image_url' => $image_user, 'start_time' => $dispo->getStartTime(), 'end_time' => $dispo->getEndTime()]
                    ];
                }
            }
            return response()->json($events);
        }


    }

    public function getAllPlanning()
    {
        return collect();
    }


    public
    function deleteEvent(
        Request $request
    )
    {
        $event = $this->covidTreatmentService->getCovidTreatById($request->id);
        $this->covidTreatmentService->destroy($event);
        return response()->json([
            'status' => 'covid deleted with success'
        ], 200);
    }

    public
    function deleteVaccEvent(
        Request $request
    )
    {
        $event = $this->vaccineService->getVaccineById($request->id);
        $this->vaccineService->destroy($event);
        return response()->json([
            'status' => 'covid deleted with success'
        ], 200);
    }

    public
    function deleteTraitEvent(
        Request $request
    )
    {
        $event = $this->treatmentService->getTreatmentById($request->id);
        $this->treatmentService->destroy($event);
        return response()->json([
            'status' => 'Treat deleted with success'
        ], 200);
    }


    public
    function treatUpdate(
        Request $request
    )
    {
        $treat = $this->treatmentService->getTreatmentById($request->id);
        $treat->update($request->all());
        return response()->json(['traitment' => $treat]);
    }

    public
    function vaccUpdate(
        Request $request
    )
    {
        $vacc = $this->vaccineService->getVaccineById($request->id);
        $date = $request->start_at;
        $vacc->update(['date' => $date]);

        return response()->json(['vaccin' => $vacc]);
    }

    public
    function covUpdate(
        Request $request
    )
    {
        $covid = $this->covidTreatmentService->getCovidTreatById($request->id);
        $date = $request->start_at;
        $covid->update(['date' => $date]);
        return response()->json(['covid' => $covid]);
    }

    public
    function appUpdate(
        Request $request
    )
    {
        $appointment = $this->appointmentService->getById($request->id);
        if ($appointment) {
            $start = Carbon::parse($request->start_at);
            $end = Carbon::parse($request->end_at);
            $result = $this->appointmentService->update($appointment, [
                "start_date" => $start->format('Y-m-d'),
                "end_date" => $end->format('Y-m-d'),
                "start_time" => $start->format('H:i'),
                "end_time" => $end->format('H:i'),
            ]);
            return response()->json(['success' => true]);
        }
        return response()->json(['success' => false]);
    }

    public
    function appRemove(
        Request $request
    )
    {
        $appointment = $this->appointmentService->deleteById($request->id);
        if ($appointment) {
            return response()->json(['success' => true]);
        }
        return response()->json(['success' => false]);
    }


}
