<?php

namespace App\Http\Controllers\Backend;

use App\Domains\CovidTreatment\Services\CovidTreatmentService;
use App\Domains\Treatment\Http\Requests\Backend\StoreTreatmentRequest;
use App\Domains\Treatment\Http\Requests\Backend\UpdateTreatmentRequest;
use Illuminate\Http\Request;
use App\Domains\CovidTreatment\Models\CovidTreatment;
use App\Domains\CovidTreatment\Services\ConversationService;
use App\Domains\Auth\Services\UserService;
use Illuminate\Support\Facades\DB;

/**
 * Class CovidTreatmentController.
 */
class CovidTreatmentController
{
    /**
     * @var TreatmentService
     * @var UserService
     */
    protected $covidTreatmentService;
    protected $userService;

    /**
     * CovidTreatmentController constructor.
     *
     * @param  ConversationService  $covidTreatmentService
     * @param  UserService  $userService
     */
    public function __construct(CovidTreatmentService $covidTreatmentService, UserService $userService)
    {
        $this->covidTreatmentService = $covidTreatmentService;
        $this->userService = $userService;
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    // public function index()
    // {
    //     return view('backend.treatment.index');
    // }

    public function getByType(Request $request)
    {
        $type = $request->type;

        return view('backend.covidTreatment.index', compact('type'));
    }

    /**
     * @return mixed
     */
    public function create()
    {
        return view('backend.treatment.create');
    }

    /**
     * @param  StoreTreatmentRequest  $request
     * @return mixed
     *
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function store(StoreTreatmentRequest $request)
    {
        // DB::beginTransaction();
        try {
            $user = $this->userService->store($request->only([
                'user_type',
                'first_name',
                'last_name',
                'email',
                'password',
                'phone',
                'address',
                'birth_date',
            ], ));
            // dd($user);
            $treatment = $this->TreatmentService->store($request->validated(), $user->id);

            return redirect()->route('admin.Treatment.index', $treatment)->withFlashSuccess(__('Le Treatment a été créé avec succès.'));

        } catch (Exception $e) {
            dd($e);
            return redirect()->back()->withFlashDanger($e);
            // DB::rollback();
        }

        // DB::commit();

    }

    /**
     * @param  Treatment  $treatment
     * @return mixed
     *
     * @throws \App\Exceptions\GeneralException
     */
    public function destroy(Treatment  $treatment)
    {
        $this->treatmentService->destroy($treatment);
        $this->userService->delete($treatment->user);

        return redirect()->route('admin.treatment.index')->withFlashSuccess('Le Treatment a été supprimé avec succès.');
    }
}
