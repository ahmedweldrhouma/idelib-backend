<?php

namespace App\Http\Controllers\Frontend\User;
use App\Domains\Auth\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rules\In;

/**
 * Class AccountController.
 */
class AccountController
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        if (Auth::user()->isNurse() || AUth::user()->isAdmin()) {
            $patients_number = count(Auth::user()->patients);
            $dispo_number = count(Auth::user()->disponibilites);
            //return dd($patients_number);
            return view('frontend.user.account', ['patients_number' => $patients_number, 'dispo_numbers' => $dispo_number]);
        } else {
            return view('frontend.user.account');
        }
    }
}
