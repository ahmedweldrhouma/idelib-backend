<?php

namespace App\Http\Controllers\Frontend;

use App\Domains\Patient\Services\PatientService;
use Illuminate\Support\Facades\Auth;

/**
 * Class HomeController.
 */
class HomeController
{
    public function __construct(PatientService$patientService)
    {
        $this->patientService = $patientService;
    }
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        if (Auth::user() != null) {
            $patients = $this->patientService->getPatientForNurse()->groupBy('type')->map(function ($type, $key) {
                return ['name' => $key, 'y' => $type->count()];
            });
            return view('backend.dashboard', ['stats' => $patients]);
        } else {
            return view('frontend.auth.login');
        }
    }
}
