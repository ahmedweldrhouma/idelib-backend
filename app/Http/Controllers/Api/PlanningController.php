<?php

namespace App\Http\Controllers\Api;

use App\Domains\Auth\Services\UserService;
use App\Domains\Disponibilite\Services\DisponibiliteService;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Calendar\getCalendarRequest;
use Illuminate\Http\Request;

class PlanningController extends Controller
{
    /**
     * @var DisponibiliteService
     * @var userService
     */

    protected $disponibiliteService;
    protected $userService;

    /**
     * CalendarController constructor.
     *
     * @param DisponibiliteService $disponibiliteService
     * @param UserService $userService
     */
    public function __construct(DisponibiliteService $disponibiliteService, UserService $userService)
    {
        $this->disponibiliteService = $disponibiliteService;
        $this->userService = $userService;
    }

    public function getData(getCalendarRequest $request)
    {
        $filters = $request->validated();
        $disponibilites = $this->disponibiliteService->getAllForTeam($filters);
            $events = [];
        foreach ($disponibilites as $dispo) {
            $team_user = $this->userService->getById($dispo->nurse_id);
            $image_user = $team_user->getPicture();
            $color_user = $team_user->getColor();
            $events[] = [
                'id' => $dispo->id,
                'user_id' => $dispo->nurse_id,
                'title' => $team_user->getFullName(),
                'start_date' => $dispo->start_date,
                'end_date' => $dispo->end_date,
                'start_time' => $dispo->start_time,
                'end_time' => $dispo->end_time,
                'color' => $color_user,
                'avatar' => $image_user
            ];
        }
        return response()->json($events);
    }

}
