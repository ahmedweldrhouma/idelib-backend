<?php

namespace App\Http\Controllers\Api;

use App\Domains\Auth\Models\User;
use App\Domains\Patient\Models\Patient;
use App\Domains\Patient\Services\PatientService;
use App\Domains\Treatment\Models\TreatmentRequest;
use App\Domains\Treatment\Services\PatientAbsenceService;
use App\Http\Requests\Api\Treatment\UpdateTreatmentRequest;
use App\Http\Resources\PatientResource;
use App\Http\Resources\UserResource;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Domains\Treatment\Models\Treatment;
use App\Domains\Treatment\Services\TreatmentService;
use App\Domains\Treatment\Services\TreatmentRequestService;
use App\Domains\Passage\Services\PassageService;
use App\Domains\Notification\Services\NotificationService;
use App\Domains\Auth\Services\UserService;
use App\Http\Resources\TreatmentResource;
use App\Http\Resources\TreatmentRequestResource;
use App\Http\Requests\Api\Treatment\StoreTreatmentRequest;
use App\Http\Requests\Api\Treatment\StoreRequestTreatmentRequest;

// use App\Http\Requests\Api\Treatment\UpdateTreatmentRequest;
use DB;
use Illuminate\Support\Facades\Auth;
use PHPUnit\Exception;
use function Clue\StreamFilter\fun;


/**
 * Class TreatmentController.
 */
class TreatmentController
{
    /**
     * @var TreatmentService
     * @var PassageService
     * @var TreatmentRequestService
     * @var NotificationService
     * @var UserService
     */
    protected $treatmentService;
    protected $passageService;
    protected $treatmentRequestService;
    protected $notificationService;
    protected $userService;
    protected $patientService;

    /**
     * TreatmentController constructor.
     *
     * @param TreatmentService $treatmentService
     * @param PassageService $passageService
     * @param TreatmentRequestService $treatmentRequestService
     * @param NotificationServicee $notificationServicee
     * @param UserService $userService
     * @param PatientService $patientService
     */
    public function __construct(
        TreatmentService        $treatmentService,
        PassageService          $passageService,
        PatientAbsenceService   $absenceService,
        TreatmentRequestService $treatmentRequestService,
        NotificationService     $notificationService,
        UserService             $userService,
        PatientService          $patientService,
    )
    {
        $this->treatmentService = $treatmentService;
        $this->passageService = $passageService;
        $this->absenceService = $absenceService;
        $this->treatmentRequestService = $treatmentRequestService;
        $this->notificationService = $notificationService;
        $this->userService = $userService;
        $this->patientService = $patientService;
    }

    public function getAll()
    {
        return TreatmentResource::collection($this->treatmentService->all());
    }

    public function getFinishedTreatments(User $user)
    {
        try {
            $users = Treatment::where('patient_id', '=', $user->patient->id)->where(function ($q) {
                $q->where('end_at', "<", today())->orWhere('stoped_at', "<", today());
            })->with('nurse',function($query){
                return $query->where('type','nurse')->doesntHave('team')->doesntHave('patientRating');
            })->get()->pluck('nurse')->unique();
            $users1 = Treatment::where('patient_id', '=', $user->patient->id)->where(function ($q) {
                $q->where('end_at', "<", today())->orWhere('stoped_at', "<", today());
            })->with('nurse.team.members',function($query){
                return $query->doesntHave('patientRating')->where('type','nurse');
            })->get()->pluck('nurse.team.*.members')->flatten()->unique();
            return UserResource::collection($users->merge($users1)->unique()->filter());
        }catch (Exception $e){
            return Response()->json(['error'=>$e->getMessage()]);
        }

    }

    /**
     * @param StoreTreatmentRequest $request
     * @return mixed
     *
     */
    public function store(StoreTreatmentRequest $request, $patient_id)
    {
        DB::beginTransaction();
        $data = $request->validated();
        try {
            $patient = $this->patientService->getPatientById($patient_id);
            if ($patient->isDied()) return response()->json(['error' => 'Ce patient est décédé'], 422);

            $data = $request->validated();
            if (isset($data['start_at'])) {
                $data['start_at'] = Carbon::createFromFormat('d/m/Y', $data['start_at'])->format('Y-m-d');
            }
            if (isset($data['stoped_at'])) {
                $data['stoped_at'] = Carbon::createFromFormat('d/m/Y', $data['stoped_at'])->format('Y-m-d');
            }
            $treatment = $this->treatmentService->store($data, $patient_id);

            foreach ($data['passages'] ?? [] as $passage) {
                $dataPass = [];
                $dataPass['period'] = $passage['period'];
                $dataPass['time_slot'] = $passage['time_slot'];
                $dataPass['ais'] = isset($passage['ais']) ?? null;
                $dataPass['patient_id'] = $patient_id;
                $this->passageService->store($dataPass, $treatment->id);
            }
        } catch (Exception $e) {
            throw new HttpException(422, $e->getMessage());
            DB::rollback();
            return response()->json(['error' => 'Un problème est survenu lors de la création du patient.'], 422);
        }

        DB::commit();

        return new TreatmentResource($treatment);
    }


    /**
     * @param UpdateTreatmentRequest $request
     * @return mixed
     *
     */
    public function update(UpdateTreatmentRequest $request, $patient_id, Treatment $treatment)
    {
        DB::beginTransaction();
        $data = $request->validated();
        try {
            $treatment = $this->treatmentService->update($data, $treatment);
            if (isset($request['stoped_at']) && $treatment) {
                $patient = $this->patientService->getPatientById($patient_id);
                $body = "Votre  prise en charge de soins se termine ";
                $title = "Fin d'un soin " . $treatment->type;
                if ($patient->user && !is_null($patient->user->fcm_token)) {
                    $notification = $this->notificationService->sendNotification($patient->user->fcm_token, "stoppedTreatment", $title, $body, ['treatment' => $treatment, "patient_id" => $patient_id]);
                    if ($notification) {
                        $data = [
                            'model' => 'treatment',
                            'subject' => $title,
                            'page' => 'stoppedTreatment',
                            'content' => $body,
                            'model_id' => $treatment->id,
                            'user_id' => Auth::id(),
                            'receiver_id' => $patient->user->id,
                        ];
                        $this->notificationService->store($data);
                    }
                }
            }
            $this->passageService->destroyForTreatment($treatment);
            foreach ($data['passages'] ?? [] as $passage) {
                $dataPass = [];
                $dataPass['period'] = $passage['period'];
                $dataPass['time_slot'] = $passage['time_slot'];
                $dataPass['ais'] = isset($passage['ais']) ?? null;
                $dataPass['patient_id'] = $treatment->patient_id;
                $this->passageService->store($dataPass, $treatment->id);
            }
        } catch (Exception $e) {
            throw new HttpException(422, $e->getMessage());
            DB::rollback();
            return response()->json(['error' => 'Un problème est survenu lors de la mise a jour du traitement.'], 422);
        }

        DB::commit();

        return new TreatmentResource($treatment->refresh());
    }

    /**
     * @param StoreRequestTreatmentRequest $request
     * @return mixed
     *
     */
    public function storeRequest(StoreRequestTreatmentRequest $request)
    {
        DB::beginTransaction();

        try {
            if (Auth::user()->isPatient() && isset(Auth::user()->patient)) {


                if (Auth::user()->patient->hasTreatmentRequests()) {
                    return response()->json([
                        'message' => __('you already have a request')
                    ], 422);
                }

                $treatment = $this->treatmentRequestService->store($request->validated());
                $nurses = $this->userService->getNursesBySector();

                $title = "Nouvelle demande de prise charge";
                $body = "Vite dépêchez-vous de consulter la demande avant qu'il ne soit pas tard !";
                foreach ($nurses as $nurse) {
                    if ($nurse->fcm_token) {
                        $notif = $this->notificationService->sendNotification($nurse->fcm_token, "treatmentRequests", $title, $body, ['treatment' => $treatment, "patient_id" => Auth::id()]);
                        if ($notif) {
                            $data = [
                                'model' => 'treatment ',
                                'subject' => $title,
                                'page' => 'treatmentRequests',
                                'content' => $body,
                                //'content' => $body,
                                'model_id' => $treatment->id,
                                'user_id' => Auth::id(),
                                'receiver_id' => $nurse->id,
                            ];
                            $this->notificationService->store($data);
                        }
                    }
                }
            } else {
                return response()->json([
                    'error' => 'this user is not a patient.'
                ], 422);
            }

        } catch (Exception $e) {
            throw new HttpException(422, $e->getMessage());
            DB::rollback();

            return response()->json(['error' => 'Un problème est survenu lors de la création du demande de prise en charge.'], 422);
        }

        DB::commit();
        return new TreatmentRequestResource($treatment);
    }

    /**
     * @param Treatment $treatment
     * @param  $patient_id
     * @return mixed
     *
     */
    public function destroy(Treatment $treatment)
    {
        $this->treatmentService->destroy($treatment);

        return response()->json([
            'status' => 'success'
        ], 200);
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function uploadPrescription(Request $request, $patient_id, Treatment $treatment)
    {
        try {
            $output = $this->treatmentService->updatePrescription($treatment,
                $request->has('prescription') ? $request->file('prescription') : false
            );
            return [
                'prescription' => $output->getPrescription()
            ];
        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage()
            ], 422);
        }
    }

    public function getTreatmentRequest()
    {
        return TreatmentRequestResource::collection($this->treatmentRequestService->getTreatmentRequest());
    }

    public function showRequest(TreatmentRequest $request)
    {
        if ($request->nurse) {
            return Response()->json([
                'title' => 'Trop tard !',
                'error' => 'Désolé, quelqu\'un été plus rapide que vous.Soyez plus réactif la prochaine fois.',
            ], 422);
        }
        if ($request->consulted_by && Auth::id() != $request->consulted_by && Carbon::now()->diffInMinutes($request->consulted_at) < 10) {
            return Response()->json([
                'title' => 'Un instant',
                'error' => 'Cette fiche patient est en train d\'être consultée'
            ], 422);
        }
        $request->consulted_at = Carbon::now()->format('Y-m-d H:i');
        $request->consulted_by = Auth::id();
        $request->save();
        return new TreatmentRequestResource($request);
    }

    public function acceptTreatment($request_id)
    {
        $request = $this->treatmentRequestService->getTreatmentRequestById($request_id);
        $this->treatmentRequestService->acceptTreatmentRequest($request);
    }

    public function denyTreatment($request_id)
    {
        $request = $this->treatmentRequestService->getTreatmentRequestById($request_id);
        if ($request->consulted_by && Auth::id() == $request->consulted_by && Carbon::now()->diffInMinutes($request->consulted_at) < 10) {
            $this->treatmentRequestService->denyTreatmentRequest($request);
            return response()->json([
                'status' => 'success'
            ], 200);
        }
        return response()->json([
            'error' => 'this request is no longer available'
        ], 404);
    }

    public function checkAvailability($request_id)
    {
        $request = $this->treatmentRequestService->getTreatmentRequestById($request_id);

        if ($request) {
            if ($request->is_available) {
                return response()->json([
                    'is_available' => true
                ], 200);
            } else {
                return response()->json([
                    'is_available' => false
                ], 200);
            }
        } else {
            return response()->json([
                'error' => 'this request does not exist'
            ], 404);
        }
    }

    public function updateAvailability($request_id)
    {
        $request = $this->treatmentRequestService->getTreatmentRequestById($request_id);

        if ($request) {
            $this->treatmentRequestService->updateAvailability($request);

            return response()->json([
                'is_available' => $request->is_available
            ], 200);
        } else {
            return response()->json([
                'error' => 'this request does not exist'
            ], 404);
        }
    }

    public function getTreatmentRequestHistory()
    {
        return TreatmentRequestResource::collection($this->treatmentRequestService->getTreatmentRequestHistory());
    }

    public function renewal(Treatment $treatment)
    {
        return new TreatmentResource($treatment = $this->treatmentService->renewal($treatment));
    }
}
