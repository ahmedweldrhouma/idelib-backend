<?php

namespace App\Http\Controllers\Api;

use App\Domains\Notification\Services\NotificationService;
use App\Mail\TeamInvitationMail;
use Illuminate\Http\Request;
use App\Domains\Invitation\Models\Invitation;
use App\Domains\Invitation\Services\InvitationService;
use App\Domains\Team\Services\TeamService;
use App\Domains\Auth\Services\UserService;
use App\Http\Requests\Api\Invitation\StoreInvitationRequest;
use App\Http\Resources\InvitationResource;
use App\Http\Resources\TeamResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

/**
 * Class InvitationController.
 */
class InvitationController
{
    /**
     * @var InvitationService
     */
    protected $invitationService;
    protected $userService;
    protected $teamService;

    /**
     * InvitationController constructor.
     *
     * @param InvitationService $invitationService
     * @param UserService $userService
     * @param TeamService $teamService
     */
    public function __construct(InvitationService $invitationService,
                                UserService $userService,
                                TeamService $teamService,
                                NotificationService $notificationService
    )
    {
        $this->invitationService = $invitationService;
        $this->userService = $userService;
        $this->teamService = $teamService;
        $this->notificationService = $notificationService;
    }

    public function invite(StoreInvitationRequest $request, $team_id)
    {

        if (!Auth::user()->isAdminOfTeam($team_id)) {
            return response()->json([
                'error' => __('You don\'t have the right to make this action.')
            ], 422);
        }
        $user = $this->userService->getByEmail($request->email);
        if ($user) {
            if ($user->hasInvitationToTeam($team_id)) {
                return response()->json([
                    'message' => __('This user already has a pending invitation in this team.')
                ], 422);
            }

            if ($user->isMemberOfTeam($team_id)) {
                return response()->json([
                    'message' => __('This user is already a team member.')
                ], 422);
            }
            $invitation = $this->invitationService->invite($request->all(), $team_id, $user->id);
            $team = $this->teamService->getById($team_id);
            $title ="Invitation à rejoindre une équipe " ;
            $body ="Vous avez reçus une invitation à rejoindre l'équipe" . " " . $invitation->team->name . " de "  . $invitation->team->admin->getFullName() ;
            $notification = $this->notificationService->sendNotification($user->fcm_token, "notifications", $title, $body);
            //dd($user->fcm_token);
            if ($notification) {
                $data= [
                    'model' => 'invitation ',
                    'subject' => $title,
                    'page' => 'notifications',
                    'content' => $body,
                    'model_id' => $invitation->id,
                    'user_id' => Auth::id(),
                    'receiver_id' => $user->id,
                ];
                $this->notificationService->store($data);
            }

            if ($invitation) {
                try {
                    Mail::to($request->email)->send(new TeamInvitationMail(Auth::user()->team->first(), Auth::user()));
                } catch (\Exception $e) {
                    return response()->json([
                        'error' => "Email not found"
                    ], 422);
                }
                return response()->json(['success' => 'success'], 200);
            }
        } else {
            if ($this->invitationService->getInvitationbyEmail($request->email)) {
                return response()->json([
                    'message' => __('This user already has a pending invitation in this team.')
                ], 422);
            }
            $invitation = $this->invitationService->invite(['email' => $request->email], $team_id);
            try {
                Mail::to($request->email)->send(new TeamInvitationMail(Auth::user()->team->first(), Auth::user()));
            } catch (\Exception $e) {
                return response()->json([
                    'error' => "Email not found"
                ], 422);
            }
            return response()->json(['success' => 'success', 'message' => __('Member successfully invited by e-mail')], 200);
        }
    }

    public function getPendingInvitations()
    {
        return InvitationResource::collection($this->invitationService->getPendingInvitations());
    }

    /**
     * @param  $team_id
     * @param  $token
     * @return mixed
     *
     */
    public function accept($token)
    {
        $invite = $this->invitationService->getInvitationbyToken($token);
        if (!$invite) {
            return response()->json(['error' => __('This invitation is not available')], 422);
        }
        if (Auth::user()->team->isNotEmpty()) {
            return response()->json(['error' => __('You already belong to a team')], 422);
        }
        if (($invite->invited_id != null && $invite->invited_id !== Auth::id()) || $invite->email != Auth::user()->email) {
            return response()->json([
                'message' =>  __('You don\'t have the right to make this action.')
            ], 422);
        }
        $team_id = $invite->team_id;
        $this->invitationService->acceptInvite($token);
        $team = $this->teamService->getById($team_id);

        return new TeamResource($team);
    }

    /**
     * @param  $team_id
     * @param  $token
     * @return mixed
     *
     */
    public function deny($token)
    {
        $invite = $this->invitationService->getInvitationbyToken($token);
        if (!$invite) {
            return response()->json(['error' => __('This invitation is not available')], 422);
        }
        if ($invite->invited_id !== Auth::id()) {
            return response()->json([
                'message' => __('You don\'t have the right to make this action.')
            ], 422);
        }
        $this->invitationService->denyInvite($token);
        return response()->json(['success' => 'success'], 200);
    }
}
