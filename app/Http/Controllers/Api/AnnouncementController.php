<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Domains\Announcement\Models\Announcement;
use App\Domains\Announcement\Services\AnnouncementService;
use App\Http\Resources\AnnouncementResource;
use App\Http\Requests\Api\Announcement\StoreAnnouncementRequest;
use App\Http\Requests\Api\Announcement\UpdateAnnouncementRequest;
use DB;


/**
 * Class AnnouncementController.
 */
class AnnouncementController
{
    /**
     * @var AnnouncementService
     */
    protected $announcementService;

    /**
     * AnnouncementController constructor.
     *
     * @param  AnnouncementService  $announcementService
     */
    public function __construct(AnnouncementService $announcementService)
    {
        $this->announcementService = $announcementService;
    }

    public function getDetails(Announcement $announcement)
    {
        return new AnnouncementResource($announcement);
    }

    public function getUserAnnouncements()
    {
        return AnnouncementResource::collection($this->announcementService->getUserAnnouncements());
    }

    public function getOffers(Request $request)
    {
        return AnnouncementResource::collection($this->announcementService->getOffers($request->all()));
    }

    /**
     * @param  AnnouncementStoreRequest  $request
     * @return mixed
     *
     */
    public function store(StoreAnnouncementRequest $request)
    {
        try {
            $announcement = $this->announcementService->store($request->validated());

            return new AnnouncementResource($announcement);

        } catch (Exception $e) {
            throw new HttpException(422,  $e->getMessage());
            return response()->json(['error' => 'Un problème est survenu lors de la création de lannonce.'], 422);
        }
    }

    /**
     * @param  UpdateAnnouncementRequest  $request
     * @param  Announcement  $announcement
     * @return mixed
     *
     */
    public function update(UpdateAnnouncementRequest $request, Announcement $announcement)
    {
        try {
            $announcement = $this->announcementService->update($request->validated(), $announcement);

            return new AnnouncementResource($announcement);

        } catch (Exception $e) {
            throw new HttpException(422,  $e->getMessage());
            return response()->json(['error' => 'Un problème est survenu lors de la mise a jour de lannonce.'], 422);
        }
    }


    /**
     * @param  Announcement  $announcement
     * @return mixed
     *
     * @throws \App\Exceptions\GeneralException
     */
    public function destroy(Announcement  $announcement)
    {
        $this->announcementService->destroy($announcement);

        return response()->json([
            'status' => 'success'
        ], 200);
    }
}
