<?php

namespace App\Http\Controllers\Api;

use App\Domains\Conversation\Models\Conversation;
use App\Domains\Message\Services\MessageService;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Message\StoreMessageRequest;
use App\Http\Resources\MessageResource;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    public function __construct(MessageService $messageService)
    {
        $this->messageService = $messageService;
    }

    public function getMessagePictures(Conversation $conversation)
    {
        try {
            if (Auth::user()->isOwnerOfConversation($conversation->id) || Auth::user()->isMemberOfConversation($conversation->id)) {
                $pictures = $this->messageService->getPictures($conversation);
                $updatedItems = $pictures->getCollection();
                $pictures->setCollection($updatedItems->pluck('content'));
                return $pictures;
            }
            return response()->json(['error' => 'access dined.'], 422);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 422);
        }

    }

    public function storeFile(StoreMessageRequest $request,int $conversation_id)
    {
        try {
            if (Auth::user()->isOwnerOfConversation($conversation_id) || Auth::user()->isMemberOfConversation($conversation_id)) {
                if ($request->hasFile('attachment')) {
                    $name = \Str::random(10).'_'.$request->attachment->getClientOriginalName();
                    $request->attachment->storeAs('messages',$name, 's3');
                    return Response()->json(["path"=>'messages/'.$name]);
                }
            }
            return response()->json(['error' => 'Fichier introuvable ou vide.'], 422);
        } catch (\Exception $e) {
            // dd($e);
            return response()->json(['error' => $e->getMessage()], 422);
        }
    }
}
