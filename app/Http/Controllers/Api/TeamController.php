<?php

namespace App\Http\Controllers\Api;

use App\Domains\Conversation\Services\ConversationService;
use App\Domains\Conversation\Services\ConversationUserService;
use App\Domains\Notification\Services\NotificationService;
use App\Domains\Team\Models\TeamUser;
use Illuminate\Http\Request;
use App\Domains\Team\Models\Team;
use App\Domains\Auth\Models\User;
use App\Domains\Team\Services\TeamService;
use App\Domains\Auth\Services\UserService;
use App\Http\Resources\TeamResource;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Api\Team\StoreContactRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

/**
 * Class TeamController.
 */
class TeamController
{
    /**
     * @var TeamService
     * @var UserService
     */
    protected $teamService;
    protected $userService;
    protected $notificationService;

    /**
     * TeamController constructor.
     *
     * @param TeamService $teamService
     * @param UserService $userService
     */
    public function __construct(TeamService $teamService, ConversationUserService $conversationUserService, UserService $userService, NotificationService $notificationService)
    {
        $this->teamService = $teamService;
        $this->userService = $userService;
        $this->notificationService = $notificationService;
        $this->conversationUserService = $conversationUserService;
    }

    public function getOwnedTeams()
    {
        $team = $this->teamService->getOwnedTeams()->first();
        return $team ? new TeamResource($team) : [];
        //return TeamResource::collection($this->teamService->getOwnedTeams());
    }

    public function getTeams()
    {
        $team = $this->teamService->getTeams()->first();
        return $team ? new TeamResource($team) : [];
        //return TeamResource::collection($this->teamService->getTeams());
    }


    public function store(Request $request)
    {
        if (Auth::user()->team->isEmpty()) {
            $team = $this->teamService->store($request->all());
            if ($team) {
                return new TeamResource($team);
            }
        }
        return response()->json([
            'error' => __('You already belong to a team')
        ], 422);
    }

    public function updateColor(Team $team, $user_id, Request $request)
    {
        $request->validate([
            'color' => ['required', 'string', 'max:7'],
        ]);

        $user = $this->userService->getById($user_id);

        if ($user->isMemberOfTeam($team->id)) {
            $color = $this->teamService->updateColor($team, $user, $request->color);

            if ($color->wasChanged('color')) {
                return response()->json(['success' => 'success'], 200);
            } else {
                return response()->json([
                    'error' => 'La couleur n\'a pas été mise à jour.'
                ], 422);
            }
        } else {
            return response()->json([
                'error' => __('This person does not belong to your team.')
            ], 422);
        }
    }

    public function update(Request $request, Team $team)
    {
        $request->validate([
            'name' => ['required', 'string'],
        ]);

        try {
            $team = $this->teamService->update($request->all(), $team);

            return new TeamResource($team);

        } catch (Exception $e) {
            throw new HttpException(422, $e->getMessage());
            return response()->json(['error' => 'Un problème est survenu lors de la mise a jour de l\'équipe.'], 422);
        }
    }

    public function detachMember(Request $request, Team $team, $user_id)
    {
        if (!Auth::user()->isAdminOfTeam($team->id)) {
            return response()->json([
                'error' => __('You are not an administrator of this team!')
            ], 422);
            //redirect()->back()->withFlashDanger(__('Vous n\'étes pas un administrateur de cette équipe.'));
        }
        $user = $this->userService->getById($user_id);
        if ($user->isMemberOfTeam($team->id)) {
            if ($this->teamService->detachMember($team, $user)) {
                $title = "Sortir de l'équipe";
                $body = "Vous avez sortir de l'équipe " . " " . $team->name;
                if ($user->fcm_token) {
                    if ($request->has('delay')) {
                        sleep(intval($request->delay));
                    }
                    $notif = $this->notificationService->sendNotification($user->fcm_token, "detachMember", $title, $body, ['team' => $team]);
                    if ($notif) {
                        $data = [
                            'model' => 'detachMember',
                            'subject' => $title,
                            'page' => 'team',
                            'content' => $body,
                            'model_id' => $team->id,
                            'user_id' => Auth::id(),
                            'receiver_id' => $user->id,
                        ];
                        $this->notificationService->store($data);
                    }
                }
                //Mail::to($user->email)->send(new \App\Mail\DetachedFromTeamMail($user));
                return response()->json([
                    'success' => "success"
                ]);
                //redirect()->back()->withFlashSuccess(__('Membre a été supprimer de cette équipe avec succès.'));
            }
        }
        return response()->json([
            'error' => __('This person does not belong to your team.')
        ], 422);
        //return redirect()->back()->withFlashDanger(__('Cet utilisateur n\'est pas membre de cette équipe.'));
    }

    /**
     * @throws \Throwable
     */
    public function leaveTeam(Team $team)
    {
        try {
            if (Auth::user()->isOwnerOfTeam($team->id)) {
                return Response()->json(["error" => __('You must set a new group owner to quit.')], 422);
            }
            if (Auth::user()->isAdminOfTeam($team->id)) {
                $admins = $team->getOwnersOfTeam()->pluck('id');
                if ($admins->count() < 2 && $admins->contains(Auth::id())) {
                    return response()->json([
                        'error' => __('You must set a new group owner to quit.')
                    ], 422);
                } else {
                    $delete = $this->teamService->detachAdmin($team, Auth::user());
                }
            }

            if (Auth::user()->isMemberOfTeam($team->id)) {
                $conversations = Auth::user()->conversations->pluck('id');

                if ($this->teamService->detachMember($team, Auth::user()) && $this->conversationUserService->detach(Auth::user())) {
                    \Ratchet\Client\connect(env('WS_URL') . '?role=system')->then(function ($conn) use ($conversations) {
                        $conn->on('message', function ($msg) use ($conn) {
                            $conn->close();
                        });
                        $conn->send(json_encode(['user_id' => Auth::id(), 'action' => 'leaveConversations', 'conversations' => $conversations]));
                    }, function ($e) {
                        echo "Could not connect: {$e->getMessage()}\n";
                    });
                    try {
                        $title = "Un membre a quitté l'équipe";
                        $body = Auth::user()->getFullName() . " a quitté l'equipe";
                        if (Auth::user()->isSubstituteNurse()) {
                            $title = "Un remplaçant a quitté l'équipe";
                            $teamAdmin = $team->admin;
                            if ($teamAdmin->fcm_token) {
                                $notif = $this->notificationService->sendNotification($teamAdmin->fcm_token, "substituteLeaveTeam", $title, $body, ["user" => Auth::user()]);
                                if ($notif) {
                                    $data = [
                                        'model' => 'team',
                                        'subject' => $title,
                                        'page' => 'substituteLeaveTeam',
                                        'content' => $body,
                                        'model_id' => $team->id,
                                        'user_id' => Auth::id(),
                                        'receiver_id' => $teamAdmin->id,
                                    ];
                                    $this->notificationService->store($data);
                                }
                            }
                        } else {
                            $members = $team->members->except(Auth::id());
                            foreach ($members as $member) {
                                if ($member->fcm_token) {
                                    $notif = $this->notificationService->sendNotification($member->fcm_token, "team", $title, $body, ["user" => Auth::user()]);
                                    if ($notif) {
                                        $data = [
                                            'model' => 'team',
                                            'subject' => $title,
                                            'page' => 'team',
                                            'content' => $body,
                                            'model_id' => $team->id,
                                            'user_id' => Auth::id(),
                                            'receiver_id' => $member->id,
                                        ];
                                        $this->notificationService->store($data);
                                    }
                                }
                            }
                        }
                        try {
                            Mail::to(Auth::user()->email)->send(new \App\Mail\LeaveTeamMail($team, Auth::user()));
                        } catch (\Exception $e) {
                            return response()->json([
                                'error' => "Email not found"
                            ], 422);
                        }
                    } catch (\Exception $e) {
                        return response()->json([
                            'error' => $e->getMessage()
                        ], 422);
                    }
                    return Response()->json(["success" => "success", "message" => 'You have successfully left this team']);
                }
            }
        } catch (\Exception $e) {

            return response()->json([
                'error' => $e->getMessage()
            ], 422);
        }

        return response()->json(['error' => __('You\'re not a member of this team.')], 422);
    }

    public function defineNewAdmin(Team $team, $user_id)
    {
        if (!Auth::user()->isOwnerOfTeam($team->id) && !Auth::user()->isAdminOfTeam($team->id)) {
            return response()->json([
                'error' => __('You\'re not part of this team.')
            ], 422);
        }
        $user = $this->userService->getById($user_id);
        if ($user->isMemberOfTeam($team->id)) {
            if ($user->isNurse()) {
                $admins = $team->getOwnersOfTeam()->pluck('id');
                if ($admins->contains($user->id)) {
                    return response()->json([
                        'error' => __('This user is already an administrator in this team.')
                    ], 422);
                } else {
                    $admin = $this->teamService->defineNewAdmin($team, $user->id);

                    if ($admin->wasChanged('is_admin')) {
                        return response()->json(['success' => 'success'], 200);
                    } else {
                        return response()->json([
                            'error' => 'La couleur n\'a pas été mise à jour.'
                        ], 422);
                    }
                }
            } else {
                return response()->json([
                    'error' => 'Cet utilisateur n\'est pas un infirmier libéral.'
                ], 422);
            }
        } else {
            return response()->json([
                'error' => __('This person does not belong to your team.')
            ], 422);
        }
    }

    public function defineNewOwner(Team $team, $user_id)
    {
        if (!Auth::user()->isOwnerOfTeam($team->id)) {
            return response()->json([
                'error' => __('You\'re not part of this team.')
            ], 422);
        }
        $user = $this->userService->getById($user_id);
        if ($user->isMemberOfTeam($team->id)) {
            if ($user->isNurse() || $user->isSubstituteNurse() || $user->isAdmin()) {
                $result = $this->teamService->update(['admin_id' => $user_id], $team);
                $admin = $this->teamService->defineNewAdmin($team, $user_id);
                $admin = $this->teamService->detachAdmin($team, Auth::user());
                if ($admin) {
                    return response()->json(['success' => 'success'], 200);
                }
            } else {
                return response()->json([
                    'error' => 'Cet utilisateur n\'est pas un infirmier libéral.'
                ], 422);
            }
        } else {
            return response()->json([
                'error' => __('This person does not belong to your team.')
            ], 422);
        }
    }

    public function createContract(StoreContactRequest $request, Team $team, $user_id)
    {
        $user = $this->userService->getById($user_id);
        if ($user->isSubstituteNurse()) {
            $contract_url = $this->teamService->generateContract($request->all());
            return response()->json([
                'contract_url' => $contract_url
            ], 200);

        } else {
            return response()->json([
                'error' => 'Cet utilisateur n\'est pas un infirmier remplaçant.'
            ], 422);
        }
    }

}
