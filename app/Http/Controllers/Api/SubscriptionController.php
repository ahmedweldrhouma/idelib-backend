<?php

namespace App\Http\Controllers\Api;

use App\Domains\Auth\Services\UserService;
use App\Domains\MobileSubscription\Services\MobileSubscriptionService;
use App\Domains\Plan\Models\Plan;
use App\Domains\Plan\Services\PlanService;
use App\Domains\Vaccine\Models\MobileSubscription;
use App\Http\Resources\MobileSubscriptionResource;
use App\Services\RevenueCatService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SubscriptionController
{
    /**
     * @var MobileSubscriptionService
     */
    protected $mobilesubscriptionService;

    /**
     * @var PlanService
     */
    protected $planService;
    /**
     * @var UserService
     */
    protected $userService;

    /**
     * SubscriptionController constructor.
     *
     * @param MobileSubscriptionService $mobilesubscriptionservice
     */
    public function __construct(MobileSubscriptionService $mobilesubscriptionservice, PlanService $planService, UserService $userService)
    {
        $this->mobilesubscriptionService = $mobilesubscriptionservice;
        $this->planService = $planService;
        $this->userService = $userService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //retourner les subscriptions existe
        $mobilesubscriptions = $this->mobilesubscriptionService->all();
        return new MobileSubscriptionResource($mobilesubscriptions);
    }

    public function getSubscriptionByid($id)
    {
        return ($this->mobilesubscriptionService->get_Revenue_Cat($id));
    }

    public function updateSubscription(Request $request, Plan $plan)
    {
        try {
            $user = Auth::user();
            //Get the last subscription
            $result = RevenueCatService::get(12);
            $plan = $this->planService->getById($plan);
            //If data retrieved successfully
            if (!$result['success']) {
                return response()->json(['error' => 'Un problème est survenu lors de la mise a jour du subscription.'], 422);
            }
            $subscriptions = $result['data']['subscriber']['subscriptions'];
            return $this->mobilesubscriptionService->create($user, $plan, $subscriptions[$plan->apple_id] ?? $subscriptions[$plan->google_id]);
        } catch (Exception $e) {
            return response()->json(['error' => 'Un problème est survenu lors de la mise a jour du subscription.'], 422);
        }
        // return new MobileSubscriptionResource($mobilesubscription);
    }

    public function getSubscriptionByUser()
    {
        $user = Auth::user();
        return new MobileSubscriptionResource($this->mobilesubscriptionService->getSubscriptionByUser($user));
    }

    public function subscriptionEvent(Request $request)
    {
        try {
            $data = $request->get('event');
            $user = $this->userService->getByAppUserId($data['app_user_id']);
            if(!$user){
                return Response()->json(['error'=> 'user not found'],422);
            }
            $plan = $this->planService->getByColumn($data['product_id'], $data['store'] == "APP_STORE" ? 'apple_id' : 'google_id');
            if(!$plan){
                return Response()->json(['error'=> 'plan not found'],422);
            }
            if ($data['type'] == "PROMOTIONAL" || $data['type'] == "RENEWAL" || $data['type'] == "INITIAL_PURCHASE" || $data['type'] == "NON_RENEWING_PURCHASE") {
                $subscription = $this->mobilesubscriptionService->withTrashed($plan,$user);
                if ($subscription) {
                    return $this->mobilesubscriptionService->update($subscription, [
                        'type' => $data['type'],
                        'is_active' => true,
                        'period_type' => $data['period_type'],
                        'provider' => $data['store'],
                        'subscription_id' => $data['id'],
                        'transaction_id' => $data['transaction_id'],
                        'original_transaction_id' => $data['original_transaction_id'],
                        'operating_system' => $data['store'] == "APP_STORE" ? "ios" : "android"
                    ]);
                } else if (is_null($user->activeSubscription) || $this->mobilesubscriptionService->disableSubscription($user->activeSubscription)) {
                    return $this->mobilesubscriptionService->create($user, $plan, [
                        'type' => $data['type'],
                        'period_type' => $data['period_type'],
                        'provider' => $data['store'],
                        'subscription_id' => $data['id'],
                        'transaction_id' => $data['transaction_id'],
                        'original_transaction_id' => $data['original_transaction_id'],
                        'operating_system' => $data['store'] == "APP_STORE" ? "ios" : "android",
                        'subscribed_at' => date('Y-m-d H:i:s', $data['purchased_at_ms']),
                        'expired_at' => date('Y-m-d H:i:s', $data['expiration_at_ms']),
                    ]);
                }
            } elseif ($data['type'] == "CANCELLATION") {
                $subscription = $this->mobilesubscriptionService->withTrashed($plan,$user);
                if ($subscription) {
                    $this->mobilesubscriptionService->update($subscription, [
                        "cancelled_at" => date('Y-m-d H:i:s'),
                        "is_active" => false
                    ]);
                }
            } elseif ($data['type'] == "UNCANCELLATION") {
                $subscription = $this->mobilesubscriptionService->withTrashed($plan,$user);
                if ($subscription) {
                    $this->mobilesubscriptionService->update($subscription, [
                        "cancelled_at" => null,
                        "is_active" => true
                    ]);
                }
            } elseif ($data['type'] == "EXPIRATION") {
                $subscription = $this->mobilesubscriptionService->withTrashed($plan,$user);
                if ($subscription) {
                    $this->mobilesubscriptionService->update($subscription, [
                        "expired_at" => date('Y-m-d H:i:s'),
                        "is_active" => false
                    ]);
                }
            }
        } catch (Exception $e) {
            return Response()->json(['message' => $e->getMessage()], 422);
        }
    }
}
