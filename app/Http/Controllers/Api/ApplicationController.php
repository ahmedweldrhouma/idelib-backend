<?php

namespace App\Http\Controllers\Api;

use App\Mail\NewApplication;
use Illuminate\Http\Request;
use App\Domains\Application\Models\Application;
use App\Domains\Announcement\Models\Announcement;
use App\Domains\Application\Services\ApplicationService;
use App\Domains\Announcement\Services\AnnouncementService;
use App\Domains\Notification\Services\NotificationService;
use App\Http\Resources\ApplicationResource;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;

/**
 * Class ApplicationController.
 */
class ApplicationController
{
    /**
     * @var ApplicationService
     */
    protected $applicationService;
    protected $announcementService;
    protected $notificationService;

    /**
     * ApplicationController constructor.
     *
     * @param ApplicationService $applicationService
     * @param AnnouncementService $announcementService
     * @param NotificationService $notificationService
     */
    public function __construct(
        ApplicationService  $applicationService,
        AnnouncementService $announcementService,
        NotificationService $notificationService
    )
    {
        $this->applicationService = $applicationService;
        $this->announcementService = $announcementService;
        $this->notificationService = $notificationService;
    }

    /**
     * @param Announcement $announcement
     * @return mixed
     *
     */
    public function store(Announcement $announcement, Request $request)
    {
        $applications = $this->applicationService->getOwnedApplications()->where('announcement_id', $announcement->id);

        if ($applications->count() >= 1) {
            return response()->json(['error' => 'Vous Avez déja postuler à cette offre.']);
        }

        $application = $this->applicationService->store($announcement->id);

        if ($announcement->user->fcm_token) {
            $title = "Nouvelle application";
            $body = Auth::user()->first_name . " " . Auth::user()->last_name . " a postulé à votre offre";
            $notif = $this->notificationService->sendNotification($announcement->user->fcm_token, "applications", $title, $body, ['announcement' => $announcement, 'user' => Auth::id()]);
            if ($notif) {
                $data = [
                    'model' => 'application',
                    'subject' => $title,
                    'page' => 'applications',
                    'content' => $body,
                    'model_id' => $announcement->id,
                    'user_id' => Auth::id(),
                    'receiver_id' => $announcement->user->id,
                ];
                $this->notificationService->store($data);
            }
        }
        if (!empty($application)) {
            try {
                Mail::to($announcement->user->email)->send(new NewApplication($announcement->user, Auth::user()));
            } catch (\Exception $e) {
                return response()->json([
                    'error' => "Email not found"
                ], 422);
            }
        }
        // $request->merge([
        //     "to" => $announcement->user->fcm_token,
        //     "data" =>
        //     [
        //         "page" => "profil",
        //         "id" => Auth::id()
        //     ],
        //     "notification" =>
        //     [
        //         "title" => "IDELIB",
        //         "body" => Auth::user()->first_name." ".Auth::user()->last_name ." a postulé à votre offre"
        //     ]
        // ]);

        // $response_notif = Http::withHeaders([
        //     'Authorization' => env('FIREBASE_SERVER_KEY'),
        //     'Content-Type' => 'application/json',
        // ])->post('https://fcm.googleapis.com/fcm/send', $request->all());

        // if($response_notif->failed() || $response_notif->clientError() || $response_notif->serverError()){
        //     return response()->json([
        //         'message' => $response_notif->getReasonPhrase()
        //     ], $response_notif->getStatusCode());
        // }


        return new ApplicationResource($application);
    }

    public function getApplications()
    {
        $applications = $this->applicationService->getApplications();

        return ApplicationResource::collection($applications);
    }

    public function getApplicationsToAnnouncement(Announcement $announcement)
    {
        if (Auth::user()->isOwnerOfAnnouncement($announcement->id)) {
            $applications = $this->applicationService->getApplicationsToAnnouncement($announcement->id);

            return ApplicationResource::collection($applications);
        } else {
            return response()->json(['error' => 'this offers is not yours']);
        }

    }

    public function accept(Application $application)
    {
        $applications = $this->applicationService->getApplications()->pluck('id')->toArray();

        if (in_array($application->id, $applications)) {
            return new ApplicationResource($this->applicationService->accept($application));
        } else {
            return response()->json(['error' => 'Cette demande n\'est pas disponible']);
        }
    }

    /**
     * @param Application $application
     * @return mixed
     *
     */
    public function deny(Application $application)
    {
        $applications = $this->applicationService->getApplications()->pluck('id')->toArray();
        if (in_array($application->id, $applications)) {
            $this->applicationService->deny($application);

            return response()->json(['success' => 'success'], 200);
        } else {
            return response()->json(['error' => 'Cette demande n\'est pas disponible']);
        }
    }
}
