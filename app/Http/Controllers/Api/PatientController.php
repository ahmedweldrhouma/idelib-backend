<?php

namespace App\Http\Controllers\Api;

use App\Domains\Treatment\Services\PatientAbsenceService;
use App\Http\Requests\Api\Patient\StorePatientRequest;
use App\Http\Requests\Api\Patient\UpdatePatientRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Domains\Patient\Models\Patient;
use App\Domains\Patient\Services\PatientService;
use App\Domains\Auth\Services\UserService;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\PatientResource;
use App\Http\Resources\TreatmentResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class PatientController.
 */
class PatientController
{
    /**
     * @var PatientService
     * @var UserService
     * @var PatientAbsenceService
     */
    protected $patientService;
    protected $userService;
    protected $patientAbsenceService;

    /**
     * PatientController constructor.
     *
     * @param  PatientService  $patientService
     * @param  UserService  $userService
     */
    public function __construct(PatientService $patientService,PatientAbsenceService $patientAbsenceService, UserService $userService)
    {
        $this->patientService = $patientService;
        $this->userService = $userService;
        $this->patientAbsenceService = $patientAbsenceService;
    }

    /**
     * @return PatientResource
     */
    public function index()
    {
        return PatientResource::collection($this->patientService->getPatientForTeam());
    }

    /**
     * @return PatientResource
     */
    public function getCovidPatients()
    {
        return PatientResource::collection($this->patientService->getCovidPatientsForNurse());
    }

    public function getPatientById($id)
    {
        return new PatientResource($this->patientService->getPatientById($id));
    }

    /**
     * @param $id
     * @return array
     */
    public function getPatientAttachments(Patient $patient)
    {
        return collect($patient->attachments)->map(fn ($attachment) => ['name' => basename($attachment),'path' => Storage::disk('s3')->url($attachment) ]);
    }

    /**
     * @param  StorePatientRequest  $request
     * @return mixed
     *
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function store(StorePatientRequest $request)
    {
        try {
            $data = $request->validated();
            if (isset($data['birth_date'])){
                $data['birth_date'] = Carbon::createFromFormat('d/m/Y',$data['birth_date'])->format('Y-m-d');
            }
            $patient = $this->patientService->store($data);
            $user = $this->userService->getByEmail($request->email);
            foreach ($data['absents'] ?? [] as $absent) {
                $dataAbs = [];
                $dataAbs['absent_from'] = Carbon::parse($absent['absent_from'])->format('Y-m-d');
                $dataAbs['absent_to'] = Carbon::parse($absent['absent_to'])->format('Y-m-d');
                $dataAbs['patient_id'] = $patient->id;
                $this->patientAbsenceService->store($dataAbs);
            }
            if($user){
                $patient->user()->associate($user);
                $patient->save();
            }

            return new PatientResource($patient);

        } catch (Exception $e) {
            throw new HttpException(422,  $e->getMessage());
            return response()->json(['error' => 'Un problème est survenu lors de la création du patient.'], 422);
        }
    }

    public function update(UpdatePatientRequest $request, Patient $patient)
    {
        try {
            $data = $request->validated();
            if (isset($data['birth_date'])){
                $data['birth_date'] = Carbon::createFromFormat('d/m/Y',$data['birth_date'])->format('Y-m-d');
            }
            if (isset($data['died_at'])){
                $data['died_at'] = Carbon::createFromFormat('d/m/Y',$data['died_at'])->format('Y-m-d');
            }
            $patient = $this->patientService->update($data, $patient);
            //$this->patientAbsenceService->destroyForPatient($patient);
            foreach ($data['absents'] ?? [] as $absent) {
                $dataAbs = [];
                $dataAbs['absent_from'] = Carbon::parse($absent['absent_from'])->format('Y-m-d');
                $dataAbs['absent_to'] = Carbon::parse($absent['absent_to'])->format('Y-m-d');
                $dataAbs['patient_id'] = $patient->id;
                $this->patientAbsenceService->store($dataAbs);
            }
            return new PatientResource($patient);
        } catch (\Exception $e) {
            throw new HttpException(422,  $e->getMessage());
            return response()->json(['error' => 'Un problème est survenu lors de la mise a jour du patient.'], 422);
        }
    }

    /**
     * @param  Patient  $patient
     * @return mixed
     *
     * @throws \App\Exceptions\GeneralException
     */
    public function destroy(Patient  $patient)
    {
        $this->patientService->destroy($patient);

        return response()->json([
            'status' => 'success'
        ], 200);
    }

    public function getTreatments(Patient $patient)
    {
        return TreatmentResource::collection($this->patientService->getTreatments($patient));
    }

    public function getCovidTreatments(Patient $patient)
    {
        return TreatmentResource::collection($this->patientService->getCovidTreatments($patient));
    }

    /**
     * @param Request $request
     *
     * @return mixed
    */
    public function uploadAvatar($patient_id, Request $request)
    {
        $output = $this->patientService->updateAvatar(
            $patient_id,
            $request->has('avatar_location') ? $request->file('avatar_location') : false
        );
        return [
            'avatar_location' =>  url($output->avatar_location)
        ];
    }

    /**
     * @param Request $request
     *
     * @return mixed
    */
    public function uploadMutual($patient_id, Request $request)
    {
        $output = $this->patientService->updateMutual(
            $patient_id,
            $request->has('mutual_card') ? $request->file('mutual_card') : false
        );

        return [
            'mutual_card' => $output->getMutualCard()
        ];
    }
    /**
     * @param Request $request
     *
     * @return mixed
    */
    public function uploadAttachments($patient_id, Request $request)
    {
        $output = $this->patientService->updateAttachments(
            $patient_id,
            $request->has('attachments') ? $request->attachments : false
        );
        return $output;
    }
}
