<?php

namespace App\Http\Controllers\Api;

use App\Domains\Appointment\Services\AppointmentService;
use App\Domains\CovidTreatment\Services\CovidTreatmentService;
use App\Domains\EventSort\Services\EventSortService;
use App\Domains\Treatment\Services\TreatmentService;
use App\Domains\Vaccine\Services\VaccineService;
use App\Http\Requests\Api\Calendar\getCalendarRequest;
use App\Http\Resources\AppointmentResource;
use App\Http\Resources\PassageResource;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Request;


/**
 * Class CalendarController.
 */
class CalendarController
{
    protected $frequency = [
        "L" => "1",
        "M" => "2",
        "Me" => "3",
        "J" => "4",
        "V" => "5",
        "S" => "6",
        "D" => "7"
    ];
    protected $period = [
        'morning' => [
            "< 7h" =>
                [
                    'start_time' => "05:00",
                    'end_time' => "07:00"
                ],
            "> 7h" => [
                'start_time' => "07:00",
                'end_time' => "10:00"
            ],
        ],
        'evening' => [
            'start_time' => "12:00",
            'end_time' => "14:00"
        ],
        'night' => [
            "< 20h" => [
                'start_time' => "18:00",
                'end_time' => "20:00"
            ],
            "> 20h" => [
                'start_time' => "20:00",
                'end_time' => "22:00"
            ]
        ],
    ];
    /**
     * @var VaccineService
     * @var TreatmentService
     * @var CovidTreatmentService
     * @var AppointmentService
     * @var EventSortService
     */
    protected $vaccineService;
    protected $covidTreatmentService;
    protected $treatmentService;
    protected $appointmentService;
    protected $eventSortService;

    /**
     * CalendarController constructor.
     *
     * @param VaccineService $vaccineService
     * @param TreatmentService $treatmentService
     * @param CovidTreatmentService $covidTreatmentService
     * @param AppointmentService $appointmentService
     * @param EventSortService $eventSortService
     */
    public function __construct(
        CovidTreatmentService $covidTreatmentService,
        TreatmentService      $treatmentService,
        VaccineService        $vaccineService,
        EventSortService      $eventSortService,
        AppointmentService    $appointmentService
    )
    {
        $this->eventSortService = $eventSortService;
        $this->treatmentService = $treatmentService;
        $this->covidTreatmentService = $covidTreatmentService;
        $this->vaccineService = $vaccineService;
        $this->appointmentService = $appointmentService;
    }

    public function getData(getCalendarRequest $request)
    {
        $filters = $request->validated();
        return response()->json(AppointmentResource::collection($this->appointmentService->getAllForTeam($filters)->sortBy('date')->values()));
    }

    public function getNewData(getCalendarRequest $request)
    {
        try {
            $data = collect();
            $filters = $request->validated();
            $eventSort = $this->eventSortService->getAllForUser($filters);
            foreach ($this->treatmentService->getAllForTeam($filters) as $item) {
                $patient = $item->patient;
                $absences = $item->patient->absences;
                if ($item->type == 'classic') {
                    $title = 'Soin Classique ' . $item->classic_type;
                } else {
                    $title = 'Soin ' . strtoupper($item->type);
                    if ($item->type == 'bsi') {
                        $title .= '-' . strtoupper($item->bsi_type);
                    }
                }

                $dows = explode(',', $item->monthly_frequency);
                $days_name = [];
                setlocale(LC_ALL, 'en');
                foreach ($dows as $k => $dow) {
                    $days_name[] = strtolower(strftime('%A', strtotime("Sunday +{$this->frequency[trim($dow)]} days")));
                }
                $all = [];
                $all['type'] = 'treatment';
                $all['nurse'] = [
                    "id" => $item->nurse->id,
                    "type" => $item->nurse->type,
                    "first_name" => $item->nurse->first_name,
                    "last_name" => $item->nurse->last_name,
                    "color" => Auth::user()->getColor()
                ];
                $all['patient'] = [
                    "id" => $item->patient->id,
                    "type" => $item->patient->type,
                    "first_name" => $item->patient->first_name,
                    "last_name" => $item->patient->last_name
                ];
                $all['title'] = $title;
                $startDate = Carbon::parse($filters['from']); // Get the first friday. If $fromDate is a friday, it will include $fromDate as a friday
                $endDate = Carbon::parse($filters['to']);
                for ($date = $startDate; $date->lte($endDate); $date->addWeek()) {
                    $start = $date;
                    foreach ($days_name as $day) {
                        $event_day = $start->clone();
                        $event_day = $event_day->modify("this {$day}");
                        if ($event_day->between(Carbon::parse($item->start_at), Carbon::parse($item->end_at)) && $event_day->between(Carbon::parse($filters['from']), Carbon::parse($filters['to'])) && (is_null($item->stoped_at) || $event_day->isBefore(Carbon::parse($item->stoped_at))) && (is_null($patient->died_at) || $event_day->lte(Carbon::parse($patient->died_at)))) {
                            foreach ($item->passages as $passage) {
                                $item->start_time = $passage->period == "evening" ? $this->period[$passage->period]['start_time'] : $this->period[$passage->period][$passage->time_slot]['start_time'];
                                $item->end_time = $passage->period == "evening" ? $this->period[$passage->period]['end_time'] : $this->period[$passage->period][$passage->time_slot]['end_time'];
                                $all['eventSort'] = $eventSort->where('day',$event_day->format('Y-m-d'))->where('treatment_id',$item->id)->where('time_from',$item->start_time.':00')->where('time_to',$item->end_time.':00')->first();
                                $all['details'] = [
                                    'id' => $item->id,
                                    'type' => $item->type,
                                    'period' => $item->period,
                                    'classic_type' => $item->classic_type,
                                    'start_at' => $event_day->format('Y-m-d'),
                                    'absent' => $absences->filter(fn($item) => $event_day->between($item->absent_from, $item->absent_to))->isNotEmpty(),
                                    'died' => !is_null($patient->died_at) && $event_day->format('Y-m-d') == $patient->died_at->format('Y-m-d'),
                                    'end_at' => $event_day->format('Y-m-d'),
                                    'start_time' => $item->start_time,
                                    'end_time' => $item->end_time,
                                    'treatment' => $item->treatment,
                                ];
                                $data->push($all);
                            }
                        }
                    }
                }
            };
            foreach ($this->appointmentService->getAllForTeam($filters) as $key => $item) {
                $all = [];
                $patient = $item->patient;
                $absences = $patient ? $patient->absences : collect([]);
                $all['type'] = 'appointment';
                $all['nurse'] = [
                    "id" => $item->nurse->id,
                    "type" => $item->nurse->type,
                    "first_name" => $item->nurse->first_name,
                    "last_name" => $item->nurse->last_name,
                    "color" => Auth::user()->getColor()
                ];
                $period = CarbonPeriod::create($filters['from'], $filters['to']);
                foreach ($period as $date) {
                    if ($date->between(Carbon::parse($filters['from']), Carbon::parse($filters['to'])) && (!$patient || (!is_null($item->stoped_at) || $date->isBefore(Carbon::parse($item->stoped_at))) && (!is_null($patient->died_at) || $date->lte(Carbon::parse($patient->died_at))))) {
                        $item->start_date = $date->format('Y-m-d');
                        $item->end_date = $item->start_date;
                        $start_time = Carbon::parse($item->start_time)->format('H:i:S');
                        $end_time = Carbon::parse($item->end_time)->format('H:i:s');
                        $all['patient'] = $item->patient ? [
                            "id" => $item->patient->id,
                            "type" => "patient",
                            "first_name" => $item->patient->first_name,
                            "last_name" => $item->patient->last_name
                        ] : null;
                        $dateEvent = Carbon::parse($item->start_date)->format('Y-m-d');
                        $all['eventSort'] = $eventSort->where('day',$dateEvent)->where('appointment_id',$item->id)->where('time_from',$start_time)->where('time_to',$end_time)->first();
                        $all['details'] = [
                            'id' => $item->id,
                            'title' => $item->title,
                            'address' => $item->address,
                            'absent' => $absences->filter(fn($item) => $date->between($item->absent_from, $item->absent_to))->isNotEmpty(),
                            'died' => $patient && $date->eq(Carbon::parse($patient->died_at)),
                            'start_at' => $dateEvent,
                            'end_at' => $dateEvent,
                            'start_time' => $start_time,
                            'end_time' => $end_time,
                            'note' => $item->note
                        ];
                        $data->push($all);
                    }
                }
            };
            return response()->json($data->sortBy('eventSort.index', SORT_ASC)->values());
        } catch (\Exception $e) {
            return Response()->json(['error' => $e->getMessage()], 422);
        }
    }

    public function getDayData(Request $request, $date)
    {
        $appointments = $this->appointmentService->getAllForTeam(['date' => $date])->groupBy(function ($reg) {
            return date('H', strtotime($reg->start_time));
        })->map(fn($item) => ['count' => $item->count(), 'start_time' => date('H:i', strtotime($item->first()->start_time)), 'end_time' => date('H:i', strtotime($item->first()->end_time))]);
        return response()->json($appointments->values());
    }

    public function getNewDayData(Request $request, $date)
    {
        $data = collect();
        foreach ($this->treatmentService->getAllForTeam(['date' => $date]) as $key => $item) {
            $patient = $item->patient;
            $absences = $item->patient->absences;
            $title = '';
            if ($item->type == 'classic') {
                $title = $item->treatment;
                $title = 'Soin Classique ' . $item->classic_type;
            } else {
                $title = 'Soin ' . strtoupper($item->type);
                if ($item->type == '_') {
                    $title .= '-' . strtoupper($item->bsi_type);
                }
            }

            $dows = explode(',', $item->monthly_frequency);
            $days_name = [];
            setlocale(LC_ALL, 'en');
            foreach ($dows as $k => $dow) {
                $days_name[] = strtolower(strftime('%A', strtotime("Sunday +{$this->frequency[trim($dow)]} days")));
            }
            $all = [];
            $team = $item->nurse->team;
            $all['type'] = 'treatment';
            $all['nurse'] = [
                "id" => $item->nurse->id,
                "type" => $item->nurse->type,
                "first_name" => $item->nurse->first_name,
                "last_name" => $item->nurse->last_name,
                "color" => Auth::user()->getColor()
            ];
            $all['patient'] = [
                "id" => $item->patient->id,
                "type" => $item->patient->type,
                "first_name" => $item->patient->first_name,
                "last_name" => $item->patient->last_name,
            ];
            $all['eventSort'] = $item->eventSort;
            $all['title'] = $title;
            $all['color'] = $team->isNotEmpty() ? $item->nurse->getMemberColor($team[0]->id)->pivot->color : Auth::user()->getColor();
            $event_day = Carbon::parse($date);
            if (in_array(strtolower($event_day->format('l')), $days_name) && (!is_null($item->stoped_at) || $event_day->isBefore(Carbon::parse($item->stoped_at))) && (!is_null($patient->died_at) || $event_day->lte(Carbon::parse($patient->died_at)))) {
                $item->start_at = $item->end_at = Carbon::parse($date)->format('Y-m-d');
                foreach ($item->passages as $passage) {
                    $item->start_time = $passage->period == "evening" ? $this->period[$passage->period]['start_time'] : $this->period[$passage->period][$passage->time_slot]['start_time'];
                    $item->end_time = $passage->period == "evening" ? $this->period[$passage->period]['end_time'] : $this->period[$passage->period][$passage->time_slot]['end_time'];
                    $all['details'] = [
                        'id' => $item->id,
                        'period' => $item->period,
                        'start_at' => Carbon::parse($date)->format('Y-m-d'),
                        'end_at' => Carbon::parse($date)->format('Y-m-d'),
                        'start_time' => $item->start_time,
                        'end_time' => $item->end_time,
                        'absent' => $absences->filter(fn($item) => $event_day->between($item->absent_from, $item->absent_to))->isNotEmpty(),
                        'died' => $event_day->eq(Carbon::parse($patient->died_at)),
                        'treatment' => $item->treatment,
                        'daily_frequency' => $item->daily_frequency,
                        'monthly_frequency' => $item->monthly_frequency,
                        'type' => $item->type,
                        'care_performed' => $item->care_performed,
                        'classic_type' => $item->classic_type,
                        'bsi_type' => $item->bsi_type,
                        'bsi_interval' => $item->bsi_interval,
                        'stoped_at' => $item->stoped_at,
                        'prescription' => $item->prescription,
                        // 'absents' => AbsenceResource::collection($item->absences),
                        'passages' => PassageResource::collection($item->passages)
                    ];
                    $data->push($all);
                }
            }
        };
        foreach ($this->appointmentService->getAllForTeam(['date' => $date]) as $key => $item) {
            $team = $item->nurse->team;
            $all = [];
            $date = Carbon::parse($date);
            if ($date->between(Carbon::parse($item->start_at), Carbon::parse($item->end_at)) && (!$patient || (!is_null($item->stoped_at) || $date->isBefore(Carbon::parse($item->stoped_at))) && (!is_null($patient->died_at) || $event_day->lte(Carbon::parse($patient->died_at))))) {
                $all['type'] = 'appointment';
                $all['patient'] = $item->patient ? [
                    "id" => $item->patient->id,
                    "type" => "patient",
                    "first_name" => $item->patient->first_name,
                    "last_name" => $item->patient->last_name,
                ] : null;
                $all['nurse'] = [
                    "id" => $item->nurse->id,
                    "type" => $item->nurse->type,
                    "first_name" => $item->nurse->first_name,
                    "last_name" => $item->nurse->last_name,
                    "color" => Auth::user()->getColor()
                ];
                $all['eventSort'] = $item->eventSort;
                $all['details'] = [
                    'id' => $item->id,
                    'title' => $item->title,
                    'address' => $item->address,
                    'start_at' => Carbon::parse($item->start_date)->format('Y-m-d'),
                    'end_at' => Carbon::parse($item->end_date)->format('Y-m-d'),
                    'start_time' => Carbon::parse($item->start_time)->format('H:i'),
                    'end_time' => Carbon::parse($item->end_time)->format('H:i'),
                    'note' => $item->note,
                ];
                $all['color'] = $team->isNotEmpty() ? $item->nurse->getMemberColor($team[0]->id)->pivot->color : Auth::user()->getColor();
                $data->push($all);
            }
        };
        return response()->json($data->groupBy(fn($item) => $item['details']['start_time'] . ' ' . $item['details']['end_time'])->map(fn($item) => ["start_time" => $item->first()['details']['start_time'], "end_time" => $item->first()['details']['end_time'], "data" => $item])->map(function ($item) {
            $last = new Carbon($item['data'][0]['details']['start_time']);
            $item['data'] = $item['data']->map(function ($treat) use ($last) {
                $treat['details']['start_time'] = $last->format('H:i');
                $treat['details']['end_time'] = $last->addMinute(20)->format('H:i');
                return $treat;
            });
            return $item;
        })->values());
    }

    public function getHoursData(Request $request, $date)
    {
        $appointments = $this->appointmentService->getAllForTeam(['date' => $date])->map(fn($item) => new AppointmentResource($item));
        return response()->json($appointments);
    }

}
