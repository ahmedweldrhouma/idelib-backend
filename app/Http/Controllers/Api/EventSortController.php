<?php

namespace App\Http\Controllers\Api;

use App\Domains\EventSort\Models\EventSort;
use App\Domains\EventSort\Services\EventSortService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EventSortController extends Controller
{
    protected EventSortService $eventSortService;
    /**
     * @param EventSortService $eventSortService
     */
    public function __construct(EventSortService $eventSortService)
    {
        $this->eventSortService =$eventSortService;
    }

    public function store(Request $request){
        \DB::beginTransaction();
        try {
            $data = $request->all();
            $sorts = $this->eventSortService->getAllForUser(['from'=>$data[0]['date'],'to'=>$data[0]['date']]);
            foreach($data as $key => $event){
                $currentSort = $event['type'] == "appointment"?$sorts->where('appointment_id',$event['id'])->where('time_from',$event['start_time'].':00')->where('time_to',$event['end_time'].':00')->first():$sorts->where('treatment_id',$event['id'])->where('time_from',$event['start_time'].':00')->where('time_to',$event['end_time'].':00')->first();
                if($currentSort){
                    $currentSort->update([
                        'index' => $key
                    ]);
                }else{
                    EventSort::create([
                        'index'=>$key,
                        'user_id'=>\Auth::id(),
                        'appointment_id'=>$event['type'] == "appointment"?$event['id']:null,
                        'treatment_id'=>$event['type'] == "treatment"?$event['id']:null,
                        'day'=>$event['date']??null,
                        'time_from'=>$event['start_time']??null,
                        'time_to'=>$event['end_time']??null,
                    ]);
                }
            }
        }catch (\Exception $e){
            \DB::rollBack();
            return Response()->json(['success'=>false,"message"=> $e->getMessage()]);
        }
        \DB::commit();
        return Response()->json(['success'=>true]);
    }
}
