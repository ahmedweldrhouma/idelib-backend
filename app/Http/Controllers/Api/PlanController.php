<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Domains\Plan\Models\Plan;
use App\Domains\Plan\Services\PlanService;
use App\Http\Resources\PlanResource;

/**
 * Class PlanController.
 */
class PlanController
{
    /**
     * @var PlanService
     */
    protected $planService;

    /**
     * PlanController constructor.
     *
     * @param  PlanService  $planService
     */
    public function __construct(PlanService $planService)
    {
        $this->planService = $planService;
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function getAll()
    {
        return PlanResource::collection($this->planService->all());
    }
}
