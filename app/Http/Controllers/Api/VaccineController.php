<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Domains\Vaccine\Models\Vaccine;
use App\Domains\Patient\Models\Patient;
use App\Domains\Vaccine\Services\VaccineService;
use App\Http\Resources\VaccineResource;
use App\Http\Requests\Api\Vaccine\StoreVaccineRequest;
use App\Http\Requests\Api\Vaccine\UpdateVaccineRequest;
use DB;


/**
 * Class VaccineController.
 */
class VaccineController
{
    /**
     * @var VaccineService
     */
    protected $vaccineService;

    /**
     * VaccineController constructor.
     *
     * @param  VaccineService  $vaccineService
     */
    public function __construct(VaccineService $vaccineService)
    {
        $this->vaccineService = $vaccineService;
    }

    /**
     * @param  StoreVaccineRequest  $request
     * @return mixed
     *
     */
    public function store(StoreVaccineRequest $request, Patient $patient)
    {
        DB::beginTransaction();

        try {
            $vaccine = $this->vaccineService->store($request->validated(), $patient->id);

        } catch (Exception $e) {

            DB::rollback();
            throw new HttpException(422,  $e->getMessage());

            return response()->json(['error' => "Un problème est survenu lors de la création d'un vaccin."], 422);
        }

        DB::commit();

        return new VaccineResource($vaccine);

    }

    /**
     * @param  UpdateVaccineRequest  $request
     * @return mixed
     *
     */
    public function update(UpdateVaccineRequest $request, Patient $patient, Vaccine $vaccine)
    {
        DB::beginTransaction();

        try {
            $vaccine = $this->vaccineService->update($request->validated(), $vaccine);

        } catch (Exception $e) {
            DB::rollback();
            throw new HttpException(422,  $e->getMessage());

            return response()->json(['error' => "Un problème est survenu lors de la mise a jour d'un vaccin."], 422);
        }

        DB::commit();

        return new VaccineResource($vaccine);

    }

    public function getAllByPatient($patient_id)
    {
        $vaccines = $this->vaccineService->getAllByPatient($patient_id);

        return VaccineResource::collection($vaccines);
    }

    public function destroy($patient_id, Vaccine $vaccine)
    {

        if ($vaccine->patient_id != $patient_id) {
            return response()->json(['error' => 'Le patient ne convient pas.'], 422);
        }
        $response = $this->vaccineService->destroy($vaccine);
        if ($response) {
            return Response()->json([
                'success' => 'success'
            ]);
        }
        return response()->json(['error' => 'Un problème est survenu lors de la suppression.'], 422);

    }

}
