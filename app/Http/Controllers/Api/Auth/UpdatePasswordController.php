<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Requests\Api\Auth\UpdatePasswordRequest;
use App\Domains\Auth\Services\UserService;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

/**
 * Class UpdatePasswordController.
 */
class UpdatePasswordController
{
    /**
     * @var UserService
     */
    protected $userService;

    /**
     * ChangePasswordController constructor.
     *
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @param  UpdatePasswordRequest  $request
     *
     * @return mixed
     * @throws \Throwable
     */
    public function update(UpdatePasswordRequest $request)
    {
        if(! Hash::check($request->current_password, Auth::user()->password)){
            return response()->json([
                'error' => 'That is not your old password.'
            ], 422);
        }else{
            $valid = $this->userService->updatePassword($request->user(), $request->validated());
            
            if($valid){
                return response()->json([
                    'status' => 'success'
                ], 200);
            }else{
                return response()->json([
                    'error' => 'That is not your old password.'
                ], 422);
            }
        }
    }
}
