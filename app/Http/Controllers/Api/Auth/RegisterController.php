<?php

namespace App\Http\Controllers\Api\Auth;

use App\Domains\Auth\Services\UserService;
use App\Domains\Patient\Services\PatientService;
use App\Domains\Team\Models\TeamUser;
use App\Rules\Captcha;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\RegistersUsers;

// use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use LangleyFoxall\LaravelNISTPasswordRules\PasswordRules;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;

/**
 * Class RegisterController.
 */
class RegisterController
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * @var UserService
     */
    protected $userService;
    protected $patientService;

    /**
     * RegisterController constructor.
     *
     * @param UserService $userService
     */
    public function __construct(UserService $userService, PatientService $patientService)
    {
        $this->userService = $userService;
        $this->patientService = $patientService;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     *
     * @return \App\Domains\Auth\Models\User|mixed
     * @throws \App\Domains\Auth\Exceptions\RegisterException
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => ['required', 'string', 'max:100'],
            'last_name' => ['required', 'string', 'max:100'],
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users')],
            'password' => array_merge(['max:100']),
            'phone' => ['required'],
            'birth_date' => ['required'],
            'address' => ['required'],
            'tour_sector' => ['sometimes'],
            'longitude' => ['sometimes'],
            'latitude' => ['sometimes'],
            'adeli_number' => ['sometimes'],
            'presentation' => ['sometimes'],
            'zip_code' => ['sometimes'],
            'rpps_number' => ['sometimes'],
        ]);
        if ($validator->fails()) {
            if (in_array('email', $validator->errors()->keys())) {
                return Response()->json([
                    'status' => 420,
                    'errors' => $validator->errors()
                ], 420);
            }
            return Response()->json([
                'status' => 422,
                'errors' => $validator->errors()
            ], 422);
        }

        try {
            $data = $request->all();
            if (isset($data['birth_date'])) {
                $data['birth_date'] = Carbon::createFromFormat('d/m/Y', $data['birth_date'])->format('Y-m-d');
            }
            $user = $this->userService->registerUser($data);
            if ($request->type == "patient") {
                $patient = $this->patientService->getPatientByEmail($request->email);
                if (!$patient) {
                    $data['zip_code'] = $data['tour_sector'];
                    $this->patientService->store($data, $user->id);
                } else {
                    $patient->user()->associate($user);
                    $patient->zip_code = $request->zip_code;
                    $patient->save();
                }
            }
            $errors = [];
            try {
                Mail::to($user->email)->send(new \App\Mail\WelcomeMail($user));
            } catch (\Exception $e) {
                $errors[]= "Email not found";
            }
            return new UserResource($user);
        } catch (Exception $e) {
            throw new HttpException(422, $e->getMessage());
            return response()->json(['error' => 'Un problème est survenu lors de l\inscription.'], 422);
        }

    }
}
