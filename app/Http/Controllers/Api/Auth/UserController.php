<?php
namespace App\Http\Controllers\Api\Auth;

use App\Domains\Auth\Services\UserService;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Auth;
use App\Domains\Auth\Models\User;
use Illuminate\Http\Request;

/**
 * Class UserController.
 */
class UserController
{
    /**
     * @var UserService
     */
    protected $userService;


    /**
     * UserController constructor.
     *
     * @param  UserService  $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function getByType($type)
    {
        $users = $this->userService->getByType($type);

        return UserResource::collection($users);
    }

    public function getNearNurses()
    {
        if(Auth::user()->isPatient() && isset(Auth::user()->patient)){
            $nurses = $this->userService->getNursesBySector();
            return UserResource::collection($nurses);
        }else{
            return response()->json([
                'error' => 'this user is not a patient.'
            ], 422);
        }
    }
    public function getNearNursesPositions()
    {
        if(Auth::user()->isPatient()){
            $nurses = $this->userService->getNursesByPositions();
            return UserResource::collection($nurses);
        }else{
            return response()->json([
                'error' => 'this user is not a patient.'
            ], 422);
        }
    }

    public function saveToken(User $user, Request $request)
    {
        if($user->exists){
            return new UserResource($this->userService->saveToken($user, $request->token));
        }else{
            return response()->json([
                'error' => 'Cet utilisateur n\'existe pas.'
            ], 422);
        }
    }
}
