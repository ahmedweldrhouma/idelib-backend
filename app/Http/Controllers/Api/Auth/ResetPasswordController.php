<?php

namespace App\Http\Controllers\Api\Auth;

use App\Domains\Auth\Rules\UnusedPassword;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use LangleyFoxall\LaravelNISTPasswordRules\PasswordRules;
use App\Domains\Auth\Services\UserService;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;

/**
 * Class ResetPasswordController.
 */
class ResetPasswordController
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * @var UserService
     */
    protected $userService;

    /**
     * ResetPasswordController constructor.
     *
     * @param  UserService  $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;


    /**
     * Get the password reset validation rules.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'email' => ['required', 'max:255', 'email'],
            'password' => array_merge(
                [
                    'max:100',
                    new UnusedPassword(request('email')),
                ]
                // PasswordRules::changePassword(request('email'))
            ),
        ];
    }

    public function verifyCode(Request $request)
    {
        $request->validate([
            'code' => 'required|min:6',
            'email' => 'required'
        ]);

        $user = $this->userService->getByEmail($request->email);
        if($user){
            if($user->confirmation_code == $request->code){
                if($user->is_used){
                    return response()->json([
                        'error' => 'expired'
                    ], 422);
                }else{
                    $user->is_used = true;
                    $user->save();

                    return response()->json([
                        'status' => 'success'
                    ]);
                }
            }else{
                return response()->json([
                    'error' => 'invalid code'
                ], 404);
            }
        }else{
            return response()->json([
                'error' => 'Invalid email'
            ], 422);
        }
    }

    /**
     * Reset the given user's password.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function resetApi(Request $request)
    {
        $user = $this->userService->getByEmail($request->email);
        $response = $this->resetPassword($user, $request->password);

        return response()->json([
            'status' => 'success'
        ], 200);
    }


}
