<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Requests\Api\Auth\UpdateProfileRequest;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use App\Domains\Auth\Services\UserService;
use Illuminate\Support\Facades\Mail;

/**
 * Class ProfileController.
 */
class ProfileController
{

    public function me()
    {
        $user = Auth::user();

        return new UserResource($user);
    }

    /**
     *
     * @throws \App\Exceptions\GeneralException
     */
    public function destroy(Request $request, UserService $userService)
    {
        if ($userService->delete($request->user())) {
            try {
                Mail::to($request->user()->email)->send(new \App\Mail\DestroyAccount($request->user()));
            } catch (\Exception $e) {
                return response()->json([
                    'error' => "Email not found"
                ], 422);
            }
            return response()->json(['status' => 'success'], 200);
        };
    }

    /**
     * @param UpdateProfileRequest $request
     * @param UserService $userService
     *
     * @return mixed
     */
    public function update(UpdateProfileRequest $request, UserService $userService)
    {
        $data = $request->validated();
        if (isset($data['birth_date'])) {
            $data['birth_date'] = Carbon::createFromFormat('d/m/Y', $data['birth_date'])->format('Y-m-d');
        }
        $user = $userService->updateProfile($request->user(), $data);
        return new UserResource($user);
    }

    /**
     * @param Request $request
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function uploadAvatar(Request $request, UserService $userService)
    {
        $size = $request->file('avatar_location')->getSize();

        if (!$size || $size > 2097152) {
            return response()->json(['error' => 'La limit maximale de téléchargement est atteinte, veuillez télécharger une image inférieure à 2 Mo.'], 422);
        }

        $output = $userService->updateAvatar(
            Auth::id(),
            $request->has('avatar_location') ? $request->file('avatar_location') : false
        );

        return [
            'avatar_location' => $output->picture
        ];

    }


    public function convertUploadedFileToHumanReadable($size, $precision = 2)
    {
        if ($size > 0) {
            $size = (int)$size;
            $base = log($size) / log(1024);
            $suffixes = array(' bytes', ' KB', ' MB', ' GB', ' TB');
            return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
        }

        return $size;
    }

}
