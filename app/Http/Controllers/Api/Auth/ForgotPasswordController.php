<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use App\Domains\Auth\Services\UserService;

/**
 * Class ForgotPasswordController.
 */
class ForgotPasswordController
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;
    /**
     * @var UserService
     */
    protected $userService;

    /**
     * ForgotPasswordController constructor.
     *
     * @param  UserService  $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function sendResetLinkEmailApi(Request $request)
    {
        $this->validateEmail($request);
        $code = random_int(100000, 999999);
        $user = $this->userService->getByEmail($request->email);
        if($user){
            $user->forceFill([
                'confirmation_code' => $code,
                'is_used' => false
            ])->save();
            $response = $this->broker()->sendResetLink(
                $this->credentials($request)
            );
            // dd($this->broker());
            return $response == Password::RESET_LINK_SENT
                ? response()->json([
                    'status' => 'success',
                    'code' => $code
                ])
                : response()->json(['email' => trans($response)]);
        }else{
            return response()->json([
                'error' => 'Invalid email'
            ], 422);
        }
    }

}
