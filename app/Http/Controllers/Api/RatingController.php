<?php

namespace App\Http\Controllers\Api;

use App\Domains\Notification\Services\NotificationService;
use Illuminate\Http\Request;
use App\Domains\Rating\Models\Rating;
use App\Domains\Auth\Services\UserService;
use App\Domains\Rating\Services\RatingService;
use App\Http\Resources\RatingResource;
use Illuminate\Support\Facades\Auth;

/**
 * Class RatingController.
 */
class RatingController
{
    /**
     * @var RatingService
     * @var UserService
     */
    protected $ratingService;
    protected $userService;

    /**
     * RatingController constructor.
     *
     * @param  RatingService  $ratingService
     * @param  UserService  $userService
     */
    public function __construct(RatingService $ratingService, UserService  $userService, NotificationService $notificationService)
    {
        $this->ratingService = $ratingService;
        $this->userService = $userService;
        $this->notificationService = $notificationService;
    }

    public function store($nurse_id, Request $request)
    {
        if(Auth::user()->isNurse() || Auth::user()->isPatient() && $nurse_id != Auth::id()){
            if ($this->ratingService->where("nurse_id",$nurse_id)->where("user_id",Auth::id())->first()){
                return response()->json([
                    'error' => 'Vous avez déja donner votre avis.'
                ], 422);
            }
            $user = $this->userService->getById($nurse_id);
            if($user->isSubstituteNurse() || $user->isNurse()|| $user->isAdmin()){
                $rate = $this->ratingService->store($nurse_id, $request->all());
               if ($rate){
                   $title ="Nouvelle evaluation " ;
                   $body = "vous a attribué une évaluation de " . $rate->rating ;
                   $notification = $this->notificationService->sendNotification($user->fcm_token, "profil", $title, $body,['user' => $user->id,'rate' => $rate] );

                   if ($notification) {
                       $data= [
                           'model' => 'rating',
                           'subject' => $title,
                           'content' => $body,
                           'page' => 'profil',
                           'model_id' => $rate->id,
                           'user_id' => Auth::id(),
                           'receiver_id' => $user->id,
                       ];
                       $this->notificationService->store($data);
                   }
               }
                return new RatingResource($rate);
            }else{
                return response()->json([
                    'error' => 'Cet utilisateur n\'est pas un infirmier.'
                ], 422);
            }
        }else{
            return response()->json([
                'error' => 'Vous ne pouvez pas noter cet infirmièr.'
            ], 422);
        }
    }

    public function show(Request $request,$nurse_id)
    {
            $user = $this->userService->getById($nurse_id);
        if($user->isNurse() || $user->isAdmin() || $user->isSubstituteNurse()){
            $ratings = $user->ratings;
            return Response()->json([
                'rating' => round(floatval($ratings->avg('rating'))??0,2),
                'count' => $ratings->count(),
            ]);

        }else{
            return response()->json([
                'error' => "Utilisateur n'est pas un infirmièr."
            ], 422);
        }
    }
}
