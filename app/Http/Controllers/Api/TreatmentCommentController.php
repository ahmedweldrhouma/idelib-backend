<?php

namespace App\Http\Controllers\Api;

use App\Domains\Treatment\Models\Treatment;
use App\Domains\Treatment\Services\TreatmentService;
use App\Domains\TreatmentComment\Models\TreatmentComment;
use App\Domains\TreatmentComment\Services\TretmentCommentService;
use App\Http\Requests\Api\TreatmentComment\StoreTreamtmentCommentRequest;
use App\Http\Requests\Api\TreatmentComment\updateTreamtmentCommentRequest;
use App\Http\Requests\Api\TreatmentComment\UpdateTreatmentCommentRequest;
use App\Http\Resources\TreatmentCommentResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


/**
 * Class CovidTreatmentController.
 */
class TreatmentCommentController
{
    /**
     * @var TretmentCommentService
     */
    protected $treatmentCommentService;

    /**
     * CovidTreatmentController constructor.
     *
     * @param TretmentCommentService $treatmentCommentService
     */
    public function __construct(TretmentCommentService $treatmentCommentService , TreatmentService $treatmentService)
    {
        $this->treatmentCommentService = $treatmentCommentService;
        $this->treatmentService = $treatmentService;
    }

    public function getTreatmentComment(Treatment $treatment)
    {
        $comments = $this->treatmentCommentService->getTreatmentComment($treatment->id);
        return TreatmentCommentResource::collection($comments);
    }

    /**
     * @param StoreTreamtmentCommentRequest $request
     * @return mixed
     *
     */
    public function store(StoreTreamtmentCommentRequest $request, $treatment_id)
    {

        DB::beginTransaction();
        try {
            $treatment  = $this->treatmentService->getTreatmentById($treatment_id);
            $team = $treatment->nurse->team->first();
            if(($team && !Auth::user()->isMemberOfTeam($treatment->nurse->team->first()->id) || $treatment->nurse == Auth::user())){
                return response()->json(['error' => "Tu n'as pas le droit de commenter ce traitement."], 422);
            }
            $comment = $this->treatmentCommentService->store($request->validated(), $treatment_id);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['error' => 'Un problème est survenu lors de la création du commentaire.'], 422);
        }

        DB::commit();

        return new TreatmentCommentResource($comment);

    }

    /**
     * @param Uptret $request
     * @param int $patient_id
     * @param string $type
     * @param CovidTreatment $covidTreatment
     * @return mixed
     *
     */
    public function update(UpdateTreatmentCommentRequest $request, $treatment_id ,TreatmentComment $comment)
    {
        DB::beginTransaction();
        try {
            if($comment->user->id != Auth::id()){
                return response()->json(['error' => "L'accès est refusé, le commentaire ne t'appartient pas."], 422);
            }
            $comment = $this->treatmentCommentService->update($request->validated(), $comment);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['error' => "Un problème est survenu lors de la mise a jour d'un commentaire"], 422);
        }
        DB::commit();
        return new TreatmentCommentResource($comment);
    }

    public function destroy($treatment_id, TreatmentComment $comment)
    {
        if ($comment->user->id != Auth::id()) {
            return response()->json(['error' => 'Le commentaire ne convient pas.'], 422);
        }
        $response = $this->treatmentCommentService->destroy($comment);
        if ($response) {
            return Response()->json([
                'success' => 'success'
            ]);
        }
        return response()->json(['error' => 'Un problème est survenu lors de la suppression.'], 422);
    }
}
