<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Domains\Team\Models\TeamUser;
use App\Domains\Team\Services\TeamUserService;
use App\Http\Resources\TeamUserResource;

/**
 * Class TeamUserController.
 */

class TeamUserController {

/**
* @var TeamUserService\
*/
protected $teamUserService;

/**
* PlanController constructor.
*
* @param  TeamUserService  $teamUserService
*/
public function __construct(TeamUserService $teamUserService)
{
$this->teamUserService = $teamUserService;
}

    /**
     * @param  Request  $request
     * @return mixed
     *
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function store(Request $request)
    {
        try {
            $teamuser = $this->teamUserService->store($request->validated());
            return redirect()->back()->withFlashSuccess(__('Le membre a été créé avec succès.'));
        } catch (Exception $e) {
            return redirect()->back()->withFlashDanger($e);
        }
    }
}
