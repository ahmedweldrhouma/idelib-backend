<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Domains\Tracking\Models\Tracking;
use App\Domains\Patient\Models\Patient;
use App\Domains\Tracking\Services\TrackingService;
use App\Http\Resources\TrackingResource;
use App\Http\Requests\Api\Tracking\StoreTrackingRequest;
use App\Http\Requests\Api\Tracking\UpdateTrackingRequest;
use DB;


/**
 * Class TrackingController.
 */
class TrackingController
{
    /**
     * @var TrackingService
     */
    protected $trackingService;

    /**
     * TrackingController constructor.
     *
     * @param  TrackingService  $trackingService
     */
    public function __construct(TrackingService $trackingService)
    {
        $this->trackingService = $trackingService;
    }

    /**
     * @param  StoreTrackingRequest  $request
     * @return mixed
     *
     */
    public function store(StoreTrackingRequest $request, Patient $patient)
    {
        DB::beginTransaction();
        try {
            $tracking = $this->trackingService->store($request->validated(), $patient->id);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['error' => "Un problème est survenu lors de la création d'un vaccin."], 422);
        }

        DB::commit();

        return new TrackingResource($tracking);

    }

    /**
     * @param  UpdateTrackingRequest  $request
     * @return mixed
     *
     */
    public function update(UpdateTrackingRequest $request, Patient $patient, Tracking $tracking)
    {
        DB::beginTransaction();

        try {
            $tracking = $this->trackingService->update($request->validated(), $tracking);

        } catch (Exception $e) {
            dd($e);
            DB::rollback();

            return response()->json(['error' => "Un problème est survenu lors de la mise a jour d'un vaccin."], 422);
        }

        DB::commit();

        return new TrackingResource($tracking);

    }

    public function getAllByPatient($patient_id)
    {
        $trackings = $this->trackingService->getAllByPatient($patient_id);

        return TrackingResource::collection($trackings);
    }

}
