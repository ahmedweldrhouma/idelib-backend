<?php

namespace App\Http\Controllers\Api;

use App\Domains\Auth\Models\User;
use App\Domains\Auth\Services\UserService;
use App\Domains\Conversation\Models\Conversation;
use App\Domains\Conversation\Services\ConversationService;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Conversation\StoreConversationRequest;
use App\Http\Requests\Api\Conversation\StoreMemberConversationRequest;
use App\Http\Requests\Api\Conversation\UpdateConversationRequest;
use App\Http\Resources\ConversationResource;
use App\Http\Resources\MessageResource;
use App\Http\Resources\UserResource;
use Dotenv\Validator;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ConversationController extends Controller
{
    public function __construct(ConversationService $conversationService,UserService $userService)
    {
        $this->conversationService = $conversationService;
        $this->userService = $userService;
    }

    public function getConversations()
    {
       return ConversationResource::collection(Auth::user()->conversations);
    }

    public function getMemberConversations($user_id)
    {
        $user = $this->userService->getById($user_id);
        if ($user->isSubstituteNurse()){
            $conversation = $this->conversationService->getMemberConversation($user->id);
            if(!$conversation){
                return  Response()->json(['error'=>true,'message' => 'no conversation found'],422);
            }
            return new ConversationResource($conversation);
        }
        return Response()->json(['message' => 'access dined']);
    }

    public function getTeamMemberConversations($conversation_id)
    {
        $conversation = $this->conversationService->getById($conversation_id);
        $team = Auth::user()->team()->first();
        if (!$team){
            return Response()->json(['message' => __('You are not a member of any team')],422);
        }
        if (Auth::user()->isOwnerOfConversation($conversation->id)){
            $members = $this->conversationService->getTeamMemberConversation($conversation);
            if($members->isEmpty()){
                return  Response()->json(['error'=>true,'message' => __('No members found')],422);
            }
            return UserResource::collection($members);
        }
        return Response()->json(['message' => __('You are not authorized to obtain the list of team members')],422);
    }

    public function messages(Conversation $conversation)
    {
       return Response()->json($conversation->messages()->with('sender:id,first_name,last_name,color')->select(['id','content','type','sender_id','created_at','updated_at'])->simplePaginate());
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getConversation(Conversation $conversation)
    {
        if (Auth::user()->isOwnerOfConversation($conversation->id) ||
            Auth::user()->isMemberOfConversation($conversation->id))
        {
            return $conversation ? new ConversationResource($conversation) : [];
        }else{
            return response()->json(['message' => 'error.'], 422);
        }
    }

    /**
     * @param  Conversation  $conversation
     * @return mixed
     *
     */
    public function store(StoreConversationRequest $request)
    {
        if (!Auth::user()->isNurse() && !Auth::user()->isAdmin()){
            return  Response()->json(["message" => __('You are not authorized to start a conversation')],422);
        }
        $team = Auth::user()->teams->first();
        $data = $request->validated();
        foreach($data['participants'] as $participant){
            if($participant == Auth::id()){
                continue;
            }
            $user = $this->userService->getById($participant);
            if (!$team && !$user->isSubstituteNurse()){
                return  Response()->json(["message" => __('You don\'t have a team')]);
            }
            if ($team){
                if ($user->isNurse() && !$user->isMemberOfTeam($team->id) ){
                    return  Response()->json(["message" => __('Members do not belong to your team')]);
                }
            }
        }
        try {
            $conversation = $this->conversationService->store($data);
            return new ConversationResource($conversation);

        } catch (Exception $e) {
            return response()->json(['error' => __('A problem occurred when creating the conversation')], 422);
        }
    }

    /**
     * @param  Conversation  $conversation
     * @return mixed
     *
     */
    public function update(UpdateConversationRequest $request, Conversation $conversation)
    {

        try {
            if (Auth::user()->isOwnerOfConversation($conversation->id)){

            $conversation = $this->conversationService->update($request->validated(),$conversation);

            }else{

            return response()->json(['error' => __('You are not allowed to update this conversation')], 422);

            }
            return new ConversationResource($conversation);

        } catch (Exception $e) {
            // dd($e);
            return response()->json(['error' => __('A problem occurred when updating the conversation.')], 422);
        }
    }

    /**
     * @param  Conversation  $conversation
     * @return mixed
     *
     */
    public function delete(Conversation  $conversation)
    {
        if (Auth::user()->isOwnerOfConversation($conversation->id)) {

        $this->conversationService->destroy($conversation);

        return response()->json([
            'status' => 'success'
        ], 200);
    }
        return response()->json([
            'message' => 'access denied '
        ], 200);
    }

    public function removeMember(Conversation $conversation, $user_id)
    {
        if (!Auth::user()->isOwnerOfConversation($conversation->id)) {
            return response()->json([
                'error' => 'Vous n\'étes pas un administrateur de cette conversation.'
            ], 422);
        }
        $user = $this->userService->getById($user_id);
        if ($user->isMemberOfConversation($conversation->id)) {
            if ($this->conversationService->detachMember($conversation, $user)) {
                return response()->json([
                    'success' => "success"
                ]);
            }
        }
        return response()->json([
            'error' => 'Cet utilisateur n\'est pas membre de cette conversation.'
        ], 422);
    }

    public function addMember(StoreMemberConversationRequest $request)
    {
        $data = $request->validated();
        if (!Auth::user()->isOwnerOfConversation($data['conversation_id'])) {
            return response()->json([
                'error' => __('You are not an administrator of this conversation !')
            ], 422);
        }
        $user = $this->userService->getById($data['user_id']);
       if ($user->isNurse() &&  !$user->isMemberOfTeam(Auth::user()->team->first()->id)) {
            return response()->json([
                'error' => __('This person does not belong to your team.')
            ], 422);
        }
        $conversation = $this->conversationService->getById($data['conversation_id']);
        if (!$user->isMemberOfConversation($data['conversation_id'])) {
            $this->conversationService->addMember($conversation, $user);
            return response()->json([
                'success' => "success"
            ]);
        }

        return response()->json([
            'error' => __('This user is already a member of this conversation')
        ], 422);
    }



}
