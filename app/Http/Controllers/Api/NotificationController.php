<?php

namespace App\Http\Controllers\Api;

use App\Domains\Notification\Services\NotificationService;
use App\Http\Resources\NotificationResource;
use Illuminate\Http\Request;
use App\Domains\Invitation\Models\Invitation;
use App\Domains\Invitation\Services\InvitationService;
use App\Domains\Team\Services\TeamService;
use App\Domains\Auth\Services\UserService;
use App\Http\Requests\Api\Invitation\StoreInvitationRequest;
use App\Http\Resources\InvitationResource;
use App\Http\Resources\TeamResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use phpDocumentor\Reflection\Types\Boolean;

/**
 * Class InvitationController.
 */
class NotificationController
{
    /**
     * @var NotificationService
     */
    protected $notificationService;
    protected $invitationService;

    /**
     * InvitationController constructor.
     *
     * @param NotificationService $notificationService
     * @param InvitationService $invitationService
     */
    public function __construct(NotificationService $notificationService,InvitationService $invitationService)
    {
        $this->notificationService = $notificationService;
        $this->invitationService = $invitationService;
    }

    public function getUserNotifications()
    {
        $notifications =  NotificationResource::collection($this->notificationService->getAllForUser());
        return $notifications;
    }

    public function UnreadNotification()
    {
         $notifications = $this->notificationService->UnreadNotification();
        return [
            'status' => $notifications
        ];
    }
}
