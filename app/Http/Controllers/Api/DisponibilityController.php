<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Domains\Disponibilite\Services\DisponibiliteService;
use App\Http\Requests\Api\Disponibilite\StoreDisponibiliteRequest;
use App\Http\Requests\Api\Disponibilite\UpdateDisponibiliteRequest;
use Illuminate\Http\Request;
use App\Domains\Disponibilite\Models\Disponibilite;
use App\Http\Resources\DisponibiliteResource;

class DisponibilityController extends Controller
{
    /**
     * @var DisponibiliteService
     */

    protected $disponibiliteService;

    /**
     * CalendarController constructor.
     *
     * @param DisponibiliteService $disponibiliteService
     */
    public function __construct(DisponibiliteService $disponibiliteService)
    {
        $this->disponibiliteService = $disponibiliteService;
    }

    public function index()
    {
        return DisponibiliteResource::collection($this->disponibiliteService->getAllForNurse());
    }

    public function store(StoreDisponibiliteRequest $request)
    {
        try {
            $disponibilite = $this->disponibiliteService->store($request->validated());
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 422);
            //return response()->json(['error' => 'Un problème est survenu lors de la création de la disponibilité.'], 422);
        }
        return new DisponibiliteResource($disponibilite);
    }

    public function update(Disponibilite $disponibilite, UpdateDisponibiliteRequest $request)
    {
        return new DisponibiliteResource($this->disponibiliteService->update($disponibilite, $request->validated()));
    }

    public function get($disponibilite_id)
    {
        return new DisponibiliteResource($this->disponibiliteService->getDisponibiliteById($disponibilite_id));
    }

    public function destroy(Disponibilite $disponibilite)
    {
        $this->disponibiliteService->destroy($disponibilite);
        return response()->json([
            'status' => 'success'
        ], 200);
    }
}
