<?php

namespace App\Http\Controllers\Api;

use App\Domains\CovidTreatment\Services\CovidTreatmentService;
use Illuminate\Http\Request;
use App\Domains\CovidTreatment\Models\CovidTreatment;
use App\Domains\CovidTreatment\Services\ConversationService;
use App\Domains\Passage\Services\PassageService;
use App\Http\Resources\CovidTreatmentResource;
use App\Http\Requests\Api\CovidTreatment\StoreCovidTreatmentRequest;

// use App\Http\Requests\Api\Treatment\UpdateTreatmentRequest;
use DB;


/**
 * Class CovidTreatmentController.
 */
class CovidTreatmentController
{
    /**
     * @var TreatmentService
     */
    protected $covidTreatmentService;
    protected $passageService;

    /**
     * CovidTreatmentController constructor.
     *
     * @param CovidTreatmentService $covidTreatmentService
     * @param PassageService $passageService
     */
    public function __construct(CovidTreatmentService $covidTreatmentService, PassageService $passageService)
    {
        $this->covidTreatmentService = $covidTreatmentService;
        $this->passageService = $passageService;
    }

    public function getAll()
    {
        return TreatmentResource::collection($this->treatmentService->all());
    }

    /**
     * @param StoreCovidTreatmentRequest $request
     * @return mixed
     *
     */
    public function store(StoreCovidTreatmentRequest $request, $patient_id, $type)
    {
        DB::beginTransaction();

        try {

            $treatment = $this->covidTreatmentService->store($request->validated(), $patient_id, $type);

        } catch (Exception $e) {
            DB::rollback();
            throw new HttpException(422,  $e->getMessage());
            return response()->json(['error' => 'Un problème est survenu lors de la création du patient.'], 422);
        }

        DB::commit();

        return new CovidTreatmentResource($treatment);

    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function uploadPrescription($patient_id, $type, CovidTreatment $covidTreatment, Request $request)
    {

        $output = $this->covidTreatmentService->updatePrescription(
            $covidTreatment,
            $request->has('prescription') ? $request->file('prescription') : false
        );

        return [
            'prescription' => $output->getPrescription()
        ];
    }

    /**
     * @param UpdateCovidTreatmentRequest $request
     * @param int $patient_id
     * @param string $type
     * @param CovidTreatment $covidTreatment
     * @return mixed
     *
     */
    public function update(UpdateCovidTreatmentRequest $request, $patient_id, $type, CovidTreatment $covidTreatment)
    {
        DB::beginTransaction();

        try {
            // dd($covidTreatment);
            $treatment = $this->covidTreatmentService->update($request->validated(), $covidTreatment);

        } catch (Exception $e) {
            DB::rollback();
            throw new HttpException(422,  $e->getMessage());
            return response()->json(['error' => "Un problème est survenu lors de la mise a jour d'un " . $type . "."], 422);
        }
        DB::commit();
        return new CovidTreatmentResource($treatment);

    }


    public function getAllByPatient($patient_id, $type)
    {
        $treatments = $this->covidTreatmentService->getAllByPatient($patient_id, $type);

        return CovidTreatmentResource::collection($treatments);
    }


    public function destroy($patient_id, $type, CovidTreatment $covidTreatment)
    {

        if ($covidTreatment->patient_id != $patient_id) {
            return response()->json(['error' => 'Le patient ne convient pas.'], 422);
        }
        $response = $this->covidTreatmentService->destroy($covidTreatment);
        if ($response) {
            return Response()->json([
                'success' => 'success'
            ]);
        }
        return response()->json(['error' => 'Un problème est survenu lors de la suppression.'], 422);

    }
}
