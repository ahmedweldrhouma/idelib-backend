<?php

namespace App\Http\Requests\Api\Calendar;

use App\Domains\CovidTreatment\Models\CovidTreatment;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use LangleyFoxall\LaravelNISTPasswordRules\PasswordRules;

/**
 * Class StoreCovidTreatmentRequest.
 */
class getCalendarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'from' => ['sometimes', 'date'],
            'to' => ['sometimes', 'date'],
            'start' => ['sometimes', 'date'],
            'end' => ['sometimes', 'date'],
            'user_id' => ['sometimes'],
            'filtre_trait' => ['sometimes'],
        ];
    }

}
