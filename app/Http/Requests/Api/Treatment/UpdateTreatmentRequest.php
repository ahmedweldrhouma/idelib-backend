<?php

namespace App\Http\Requests\Api\Treatment;

use App\Domains\Treatment\Models\Enums\TreatmentBsiType;
use App\Domains\Treatment\Models\Enums\TreatmentCarePerformedType;
use App\Domains\Treatment\Models\Enums\TreatmentClassicType;
use App\Domains\Treatment\Models\Enums\TreatmentType;
use App\Domains\Treatment\Models\Treatment;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

/**
 * Class UpdateTreatmentRequest.
 */
class UpdateTreatmentRequest extends FormRequest
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_at' => ['required', 'max:100'],
            'period' => ['required', 'max:100'],
            'treatment' => ['sometimes','max:250'],
            'daily_frequency' => ['sometimes', 'max:100'],
            'passages' => ['sometimes'],
            'bsi_interval' => ['sometimes'],
            'monthly_frequency' => ['sometimes', 'max:100'],
            'care_performed' => ['sometimes','nullable', new Enum(TreatmentCarePerformedType::class)],
            'stoped_at' => ['sometimes', 'max:100'],
            'prescription' => ['sometimes', 'mimes:png,jpg,jpeg, gif', 'max:2048'],
        ];
    }
}
