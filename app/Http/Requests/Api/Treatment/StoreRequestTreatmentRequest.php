<?php

namespace App\Http\Requests\Api\Treatment;

use App\Domains\TreatmentRequest\Models\TreatmentRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use LangleyFoxall\LaravelNISTPasswordRules\PasswordRules;

/**
 * Class StoreRequestTreatmentRequest.
 */
class StoreRequestTreatmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'note' => ['required','max:250']
        ];
    }

}
