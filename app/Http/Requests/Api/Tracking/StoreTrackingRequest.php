<?php

namespace App\Http\Requests\Api\Tracking;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use LangleyFoxall\LaravelNISTPasswordRules\PasswordRules;

/**
 * Class StoreTrackingRequest.
 */
class StoreTrackingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'prescription_url' => ['required','mimes:jpg,bmp,png'],
            'pickup_date' => ['required', 'date'],
            'duration' => ['required'],
            'duration_unit' => ['required', 'in:hours,days,minutes'],
            'daily_frequency_period' => ['required', 'in:morning,evening,night'],
            'daily_frequency_time' => ['required', 'in:before_20,after_20'],
            'absences' => ['required', 'boolean']
        ];
    }

}
