<?php

namespace App\Http\Requests\Api\Announcement;

use App\Domains\Announcement\Models\Announcement;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use LangleyFoxall\LaravelNISTPasswordRules\PasswordRules;

/**
 * Class UpdateAnnouncementRequest.
 */
class UpdateAnnouncementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_date' => ['required', 'max:100'],
            'end_date' => ['required', 'max:100'],
            'sector' => ['required', 'max:100'],
            'announcement' => ['sometimes','max:250'],
            'has_transport' => ['sometimes', 'max:100'],
        ];
    }

}
