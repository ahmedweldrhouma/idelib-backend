<?php

namespace App\Http\Requests\Api\CovidTreatment;

use App\Domains\CovidTreatment\Models\CovidTreatment;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use LangleyFoxall\LaravelNISTPasswordRules\PasswordRules;

/**
 * Class StoreCovidTreatmentRequest.
 */
class StoreCovidTreatmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => ['required', 'max:100'],
            'result' => ['sometimes', 'max:100'],
            'prescription' => ['sometimes'],
        ];
    }

}
