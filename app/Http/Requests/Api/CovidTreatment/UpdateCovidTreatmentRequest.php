<?php

namespace App\Http\Controllers\Api;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateCovidTreatmentRequest.
 */
class UpdateCovidTreatmentRequest extends FormRequest
{
    

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => ['required', 'max:100'],
            'result' => ['sometimes', 'max:100'],
            'prescription' => ['sometimes'],
        ];
    }

}
