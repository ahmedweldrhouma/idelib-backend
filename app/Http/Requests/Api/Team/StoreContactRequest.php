<?php

namespace App\Http\Requests\Api\Team;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class StoreContactRequest.
 */
class StoreContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => ['required', 'max:100'],
            'date_start' => ['required', 'max:100'],
            'date_end' => ['sometimes', 'max:100'],
            'first_name_liberal' => ['required', 'max:255'],
            'last_name_liberal' => ['required', 'max:255'],
            'adeli_number_liberal' => ['sometimes', 'max:100'],
            'ordinal_number_liberal' => ['sometimes', 'max:100'],
            'cabinet_sis_liberal' => ['sometimes'],
            'first_name_substitute' => ['required', 'max:255'],
            'last_name_substitute' => ['required', 'max:255'],
            'adeli_number_substitute' => ['sometimes', 'max:100'],
            'ordinal_number_substitute' => ['sometimes', 'max:100'],
            'cabinet_sis_substitute' => ['sometimes']
        ];
    }

}
