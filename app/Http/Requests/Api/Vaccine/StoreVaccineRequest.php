<?php

namespace App\Http\Requests\Api\Vaccine;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use LangleyFoxall\LaravelNISTPasswordRules\PasswordRules;

/**
 * Class StoreVaccineRequest.
 */
class StoreVaccineRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'trod' => ['required', 'max:100'],
            'date' => ['required', 'max:100'],
            'name' => ['required', 'max:100'],
            'batch_number' => ['required', 'max:100'],
            'doses' => ['required', 'max:100'],
            'doses_number' => ['required', 'max:100']
        ];
    }

}
