<?php

namespace App\Http\Requests\Api\Patient;

use App\Domains\Patient\Models\Patient;
use Illuminate\Foundation\Http\FormRequest;
use App\Domains\Patient\Models\Enums\PatientType;
use Illuminate\Validation\Rules\Enum;

use Illuminate\Validation\Rule;

use LangleyFoxall\LaravelNISTPasswordRules\PasswordRules;

/**
 * Class StorePatientRequest.
 */
class StorePatientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'type' => ['required', new Enum(PatientType::class)],
            'first_name' => ['required', 'max:100'],
            'last_name' => ['required', 'max:100'],
            'email' => ['sometimes','nullable', 'max:255', 'email', Rule::unique('patients')],
            'phone' => ['sometimes', 'max:100'],
            'birth_date' => ['sometimes'],
            'doctor' => ['sometimes', 'max:100'],
            'doctor_phone' => ['sometimes', 'max:100'],
            'vital_card' => ['sometimes', 'max:100'],
            'mutual_card' => ['sometimes','mimes:jpg,bmp,png'],
            'social_security_number' => ['sometimes', 'max:100'],
            'antecedents' => ['sometimes', 'max:100'],
            'address' => ['sometimes', 'max:100'],
            'absents' => ['sometimes','array'],
            'zip_code' => ['sometimes', 'max:100'],
            'avatar_location' => ['sometimes','mimes:jpg,bmp,png']
        ];
    }

}
