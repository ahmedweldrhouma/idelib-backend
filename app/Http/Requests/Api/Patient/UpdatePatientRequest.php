<?php

namespace App\Http\Requests\Api\Patient;

use App\Domains\Patient\Models\Patient;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class UpdatePatientRequest.
 */
class UpdatePatientRequest extends FormRequest
{


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => ['required', 'max:100'],
            'last_name' => ['required', 'max:100'],
            'email' => ['sometimes', 'max:255', 'email', Rule::unique('patients')->ignore($this->patient->id)],
            'phone' => ['sometimes', 'max:100'],
            'birth_date' => ['sometimes'],
            'doctor' => ['sometimes', 'max:100'],
            'doctor_phone' => ['sometimes', 'max:100'],
            'vital_card' => ['sometimes', 'max:100'],
            'mutual_card' => ['sometimes','mimes:jpg,bmp,png'],
            'antecedents' => ['sometimes', 'max:100'],
            'address' => ['sometimes', 'max:100'],
            'absents' => ['sometimes','array'],
            'zip_code' => ['sometimes', 'max:100'],
            'died_at' => ['sometimes'],
            'avatar_location' => ['sometimes','mimes:jpg,bmp,png']
        ];
    }
}
