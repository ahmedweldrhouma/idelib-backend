<?php

namespace App\Http\Requests\Frontend;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Password;

class InscrRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {



        if ($this->request->get('typeInsc') == 'Infirmièr(e) libéral(e)' ) {
            return [
                // Step 1
                'lib_prenom' => 'required_if:typeInsc,Infirmièr(e) libéral(e)',
                'lib_telephone' => 'required_if:typeInsc,Infirmièr(e) libéral(e)',
                'lib_nom' => 'required_if:typeInsc,Infirmièr(e) libéral(e)',
                'lib_mdp' => 'required_if:typeInsc,Infirmièr(e) libéral(e)|confirmed',
                'lib_mail' => 'required_if:typeInsc,Infirmièr(e) libéral(e)|email',
                'lib_conf_mail' => 'required_if:typeInsc,Infirmièr(e) libéral(e)|same:lib_mail',
                'lib_adresse_inf_lib' => 'required_if:typeInsc,Infirmièr(e) libéral(e)',
                'lib_birthday' => 'nullable|date',
                'lib_lat' => 'required',
                'lib_lng' => 'required',
            ];
        }
        if ($this->request->get('typeInsc') == 'Infirmièr(e) remplaçant(e)' ) {
            return [


                'remp_prenom' => 'required_if:typeInsc,Infirmièr(e) remplaçant(e)',
                'remp_adresse' => 'required_if:typeInsc,Infirmièr(e) remplaçant(e)',
                'remp_nom' => 'required_if:typeInsc,Infirmièr(e) remplaçant(e)',
                'remp_mail' => 'required_if:typeInsc,Infirmièr(e) remplaçant(e)|email',
                'remp_mdp' => 'required_if:typeInsc,Infirmièr(e) remplaçant(e)|confirmed',
                'remp_conf_mail' => 'required_if:typeInsc,Infirmièr(e) remplaçant(e)|same:remp_mail',
                'remp_telephone' => 'required_if:typeInsc,Infirmièr(e) remplaçant(e)',
                'remp_lat' => 'required',
                'remp_lng' => 'required',


            ];
        }
        if ($this->request->get('typeInsc') == 'Patient(e)' ) {
            return [


                'patient_prenom' => 'required_if:typeInsc,Patient(e)',
                'patient_adresse' => 'required_if:typeInsc,Patient(e)',
                'patient_nom' => 'required_if:typeInsc,Patient(e)',
                'patient_birthday' => 'required_if:typeInsc,Patient(e)|date',
                'patient_mail' => 'required_if:typeInsc,Patient(e)|email',
                'patient_mdp' => 'required_if:typeInsc,Patient(e)|confirmed',
                'patient_conf_mail' => 'required_if:typeInsc,Patient(e)|same:patient_mail',
                'patient_telephone' => 'required_if:typeInsc,Patient(e)',


            ];
        }
    }
}
