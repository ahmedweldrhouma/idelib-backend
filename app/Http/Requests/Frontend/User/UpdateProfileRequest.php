<?php

namespace App\Http\Requests\Frontend\User;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdatePasswordRequest.
 */
class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => [ 'required','max:100'],
            'last_name' => ['required','max:100'],
            'phone' => ['required'],
            'address' => ['required'],
            'birth_date' => ['required'],
            'email' => ['sometimes'],
            'tour_sector' => ['sometimes'],
            'adeli_number' => ['sometimes'],
            'zip_code' => ['sometimes'],
            'rpps_number' => ['sometimes','nullable'],
            'avatar_location' => ['sometimes','mimes:jpg,bmp,png']
        ];
    }
}
