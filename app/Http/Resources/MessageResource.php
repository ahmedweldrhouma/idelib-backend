<?php

namespace App\Http\Resources;

use App\Domains\Message\Models\Message;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class MessageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'content' => $this->content,
            'type' => $this->type,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'sender' => $this->when($this->conversation->participants()->where('participant_id',$this->sender->id)->where('conversation_user.deleted_at',null)->first(), function (){
                return $this->sender->only(["id","first_name","last_name",'avatar']);
            },["id"=>$this->sender->id,"first_name"=> "ustilisateur","last_name" => "de Idelib",'avatar' =>asset("img/avatar.png")]),
        ];
    }
}
