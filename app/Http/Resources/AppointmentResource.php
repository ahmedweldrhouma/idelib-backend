<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\UserResource;
use Carbon\Carbon;

class AppointmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->when($this->title,function (){
                return $this->title;
            }),
            'patient' => $this->when($this->patient_id != null,function (){
                return new PatientResource($this->patient);
            },null),
            'address' => $this->address,
            'start_date' => Carbon::parse($this->start_date)->format('Y-m-d'),
            'end_date' => Carbon::parse($this->end_date)->format('Y-m-d'),
            'start_at' => Carbon::parse($this->start_date)->format('Y-m-d'),
            'end_at' => Carbon::parse($this->end_date)->format('Y-m-d'),
            'start_time' => Carbon::parse($this->start_time)->format('H:i'),
            'end_time' => Carbon::parse($this->end_time)->format('H:i'),
            'note' => $this->note,
            'nurse' => new UserResource($this->nurse)
        ];
    }
}
