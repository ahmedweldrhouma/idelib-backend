<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\PatientResource;
use Carbon\Carbon;

class VaccineResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'date' => Carbon::parse($this->date)->format('Y-m-d'),
            'trod' => $this->trod,
            'name' => $this->name,
            'batch_number' => $this->batch_number,
            'doses' => $this->when($this->doses != null, function(){ 
                return $this->doses;
            }),
            'doses_number' => $this->when($this->doses_number != null, function(){ 
                return $this->doses_number;
            }),
            'patient' => new PatientResource($this->patient),
        ];
    }
}
