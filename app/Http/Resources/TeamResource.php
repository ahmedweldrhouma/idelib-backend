<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\PatientResource;
use App\Http\Resources\UserResource;
use App\Http\Resources\TeamUserResource;
use Carbon\Carbon;

class TeamResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'owner' => new UserResource($this->admin),
            'admins' => UserResource::collection($this->admins->except($this->admin_id)),
            'members' => UserResource::collection($this->members()->where('is_admin', false)->get())
        ];
    }
}
