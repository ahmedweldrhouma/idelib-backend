<?php

namespace App\Http\Resources;

use App\Domains\Conversation\Models\Conversation;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class ConversationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'created_at' => $this->start_date,
            'updated_at' => $this->end_date,
            'last_message' => new MessageResource($this->lastMessage),
            'owner' => $this->user,
            'participants' => $this->participants->map->only(['id','first_name','last_name','color','avatar'])
        ];
    }
}
