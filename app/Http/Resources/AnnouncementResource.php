<?php

namespace App\Http\Resources;

use App\Domains\Announcement\Models\Traits\Method\AnnoucementMethod;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Auth;

class AnnouncementResource extends JsonResource
{
    use AnnoucementMethod;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'start_date' => date("Y-m-d", strtotime($this->start_date)),
            'end_date' => date("Y-m-d", strtotime($this->end_date)),
            'has_transport' => $this->has_transport,
            'announcement' => $this->announcement,
            'sector' => $this->sector,
            'owner' => $this->when($this->user_id != Auth::id(), function(){
                return new UserResource($this->user);
            }),
            'canapply' => !$this->UserCanApply(Auth::user()),
        ];
    }
}
