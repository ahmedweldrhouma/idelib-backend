<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\PatientResource;
use Carbon\Carbon;

class CovidTreatmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'date' => Carbon::parse($this->date)->format('Y-m-d'),
            'type' => $this->type,
            'result' => $this->result,
            'prescription' => $this->when($this->prescription != null, function(){
                return $this->getPrescription();
            }),
            'patient' => new PatientResource($this->patient),
        ];
    }
}
