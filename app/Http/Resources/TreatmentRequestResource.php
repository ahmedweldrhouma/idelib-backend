<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;
use App\Http\Resources\PatientResource;
use Illuminate\Support\Facades\Auth;

class TreatmentRequestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'note' => $this->when($this->note, function(){
                return $this->note;
            }),
            'patient' => $this->when(Auth::user()->type != 'patient', function(){
                return new PatientResource($this->patient);
            }),
            'tour_sector' => $this->tour_sector,
            "is_pending" => $this->is_pending,
            "is_available" => $this->is_available,
            'created_at' => Carbon::parse($this->created_at)->format('d-m-Y'),
        ];
    }
}
