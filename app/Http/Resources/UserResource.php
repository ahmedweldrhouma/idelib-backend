<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\AddressResource;
use Illuminate\Support\Facades\Auth;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'phone' => $this->phone,
            'birth_date' => $this->birth_date,
            'api_token' => $this->api_token,
            'fcm_token' => $this->fcm_token,
            'avatar_location' => $this->picture,
            'tour_sector' => $this->tour_sector,
            'address' => $this->address,
            'adeli_number' => $this->adeli_number,
            'presentation' => $this->presentation,
            'longitude' => $this->longitude,
            'latitude' => $this->latitude,
            'subscription_type' => $this->subscription_type,
            'operating_system' => $this->operating_system,
            'rc_id' => $this->when($this->app_user_id != null, function(){
                return $this->app_user_id;
            }),
            'subscription' => $this->when($this->adminSubscription || $this->activeSubscription, function(){
                return true;
            },false),
            'zip_code' => $this->when($this->isPatient(), function(){
                return $this->patient->zip_code;
            }),
            'rpps_number' => $this->when($this->isNurse() || $this->isSubstituteNurse(), function(){
                return $this->rpps_number;
            }),
            'team_color' => $this->whenPivotLoaded('team_user', function () {
                return $this->pivot->color;
            }),
            'rating' => $this->when($this->isNurse() || $this->isAdmin() || $this->isSubstituteNurse(), function(){
                return [
                    'rating' => floatval($this->ratings()->avg('rating'))??0,
                    'count' => $this->ratings()->count(),
                ];
            }),
            'color' => $this->when($this->color, function(){
                return $this->color;
            },"#4287f5"),
            'conversations_count' => $this->participants()->count()
        ];
    }
}
