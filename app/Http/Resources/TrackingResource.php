<?php

namespace App\Http\Resources;

use App\Http\Resources\PatientResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class TrackingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'prescription_url' => url($this->prescription_url),
            'pickup_date' => $this->pickup_date,
            'duration' => $this->duration,
            'duration_unit' => $this->duration_unit,
            'daily_frequency_period' => $this->daily_frequency_period,
            'daily_frequency_time' => $this->daily_frequency_time,
            //'absences' => $this->absences,
            'patient' => new PatientResource($this->patient)
        ];
    }
}
