<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\PassageResource;
use Carbon\Carbon;

class TreatmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'period' => $this->when($this->period,function (){
                $period = explode(' ', $this->period);
                return $period[0] . ' ' . __($period[1]);
            }),
            'start_at' => Carbon::parse($this->start_at)->format('Y-m-d'),
            'end_at' => Carbon::parse($this->end_at)->format('Y-m-d'),
            'treatment' => $this->when($this->treatment, function(){
                return $this->treatment;
            }),
            'daily_frequency' => $this->when($this->daily_frequency, function(){
                return $this->daily_frequency;
            }),
            'monthly_frequency' => $this->when($this->monthly_frequency, function(){
                return $this->monthly_frequency;
            }),
            'type' => $this->type,
            'care_performed' => $this->when($this->care_performed, function(){
                return __($this->care_performed);
            }),
            'classic_type' => $this->when($this->classic_type, function(){
                return $this->classic_type;
            }),
            'bsi_type' => $this->when($this->bsi_type, function(){
                return $this->bsi_type;
            }),
            'bsi_interval' => $this->when($this->bsi_interval, function(){
                return $this->bsi_interval;
            }),
            'stoped_at' => $this->when($this->stoped_at, function(){
                return Carbon::parse($this->stoped_at)->format('Y-m-d');
            }),
            'prescription' => $this->when($this->prescription, function(){
                return $this->getPrescription();
            }),
            //'absents' => AbsenceResource::collection($this->absences),
            'passages' => PassageResource::collection($this->passages)
        ];
    }
}
