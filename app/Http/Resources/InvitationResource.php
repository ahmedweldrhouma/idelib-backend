<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\TeamResource;
use Carbon\Carbon;

class InvitationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'token' => $this->token,
            'user' => [
                'first_name' => $this->user->first_name,
                'last_name' => $this->user->last_name,
                'avatar_location' => $this->user->picture,
                'color' => $this->when($this->user->color, function(){
                    return $this->user->color;
                },"#4287f5")
            ],
            'team' => new TeamResource($this->team)
        ];
    }
}
