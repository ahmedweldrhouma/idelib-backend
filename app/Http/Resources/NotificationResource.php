<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use function Clue\StreamFilter\fun;

class NotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
       // dd(Auth::user()->invitations);
        return [
            'id' => $this->id,
            'model' => $this->model,
            'subject' => $this->subject,
            'content' => $this->content,
            'page' => $this->page,
            'token' => $this->when($this->page == "notifications" && $this->invitation,function(){
                return $this->invitation->token;
            }),
            'user' => new UserResource($this->user),
        ];
    }
}
