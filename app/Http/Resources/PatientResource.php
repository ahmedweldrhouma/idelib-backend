<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PatientResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => $this->when($this->type != null, function(){
                return $this->type;
            }),
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'phone' => $this->phone,
            'birth_date' => $this->birth_date,
            'address' => $this->address,
            'zip_code' => $this->zip_code,
            'nurse_id' => $this->nurse_id,
            'avatar_location' => $this->when($this->avatar_location != null, function(){
                return $this->picture;
            }),
            'social_security_number' => $this->when($this->social_security_number != null, function(){
                return $this->social_security_number;
            }),
            'doctor' => $this->when($this->doctor != null, function(){
                return $this->doctor;
            }),
            'doctor_phone' => $this->when($this->doctor_phone != null, function(){
                return $this->doctor_phone;
            }),
            'vital_card' => $this->when($this->vital_card != null, function(){
                return $this->vital_card;
            }),
            'mutual_card' =>  $this->when($this->mutual_card != null, function(){
                return $this->getMutualCard();
            }),
            'attachments' =>  $this->getAttachments(),
            'absents' => AbsenceResource::collection($this->absences),
            'died_at' => $this->died_at,
            'antecedents' => $this->when($this->antecedents != null, function(){
                return json_decode($this->antecedents);
            }),
        ];
    }
}
