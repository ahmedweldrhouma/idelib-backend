<?php

namespace App\Http\Livewire\Backend;

use App\Domains\AdminSubscription\Models\AdminSubscription;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

/**
 * Class AdminSubscriptionsTable.
 */
class AdminSubscriptionsTable extends DataTableComponent
{
    /**
     * @return Builder
     */
    public function query(): Builder
    {
        return AdminSubscription::query()
            ->when($this->getFilter('search'), fn ($query, $term) => $query->search($term));
    }

    public function columns(): array
    {
        return [
            Column::make(__('User'), 'user_id')
                ->sortable(),
            Column::make(__('Abonnement'), 'plan_id'),
            Column::make(__('Type utilisateur créé'), 'user_id'),
            Column::make(__('Souscrit le'), 'subscribed_at'),
            Column::make(__('Expire le'), 'expires_at'),
            // Column::make(__('Number of Users'), 'users_count')
            //     ->sortable(),
            Column::make(__('Actions')),
        ];
    }

    public function rowView(): string
    {
        return 'backend.adminSubscription.includes.row';
    }
}
