<?php

namespace App\Http\Livewire\Backend;

use App\Domains\Vaccine\Models\Vaccine;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\Views\Filter;
use Illuminate\Support\Facades\Auth;
use App\Exports\TreatmentsExport;
use App\Exports\CovidExport;
use Excel;
/**
 * Class VaccinesTable.
 */
class VaccinesTable extends DataTableComponent
{
    /**
     * @var array|string[]
     */
    public array $filterNames = [
        'date_range' => 'date range',
    ];


    public array $bulkActions = [
        'exportSelected' => 'Export',
    ];


    /**
     * @return Builder
     */
    public function query(): Builder
    {
        $nurse = Auth::user();
        if(Auth::user()->isBiller()){
            $nurse = Auth::user()->nurse;
        }
        
        if($nurse->vaccines->isNotEmpty()){
            $query = $nurse->vaccines->toQuery();
        }else{
            $query = Vaccine::query();
        }
        
        return $query->when($this->getFilter('search'), fn ($query, $term) => $query->search($term))
        ->when($this->getFilter('start_date'), fn ($query, $start_date) => $query->where('date', '>=',date('Y-m-d',strtotime($start_date))))
        ->when($this->getFilter('end_date'), fn ($query, $end_date) => $query->where('date', '<=',date('Y-m-d',strtotime($end_date))));
}

    /**
     * @return array
     */
    public function filters(): array
    {
        return [
            'start_date' => Filter::make('Date début')
            ->date([
                'min' => now()->subYear()->format('Y-m-d'), // Optional
                'max' => now()->format('Y-m-d') // Optional
            ]),
            'end_date' => Filter::make('Date fin')
            ->date([
                'min' => now()->subYear()->format('Y-m-d'), // Optional
                'max' => now()->format('Y-m-d') // Optional
            ]),
        ];
    }

    public function columns(): array
    {
        return [
            Column::make(__('Nom'), 'patient_id')
                ->sortable(),
            Column::make(__('Prénom'), 'patient_id')
                ->sortable(),
            Column::make(__('date de naissance'), 'patient_id'),
            Column::make(__('E-mail'), 'patient_id')
                ->sortable(),
            Column::make(__('Numéro de téléphone'), 'patient_id')
                ->sortable(),
            Column::make(__('N° de sécurité sociale'), 'social_security_number'),
            Column::make(__('Adresse'), 'address'),
            Column::make(__('Médecin traitant'), 'doctor'),
            Column::make(__('Tel médecin traitant'), 'doctor_phone'),
            Column::make(__('Antécédents'), 'antecedents'),
            Column::make(__('Date de prise en charge'), 'start_at'),
            // Column::make(__('Number of Users'), 'users_count')
            //     ->sortable(),
            Column::make(__('Actions')),
        ];
    }

    public function rowView(): string
    {
        return 'backend.vaccine.includes.row';
    }
    
    public function exportSelected()
    {

            return (new CovidExport($this->selectedKeys, 'vaccines',null, $this->getFilter('start_date'),
            $this->getFilter('end_date')))->download('liste_des_soins_vaccins.xlsx');
        
    }
}
