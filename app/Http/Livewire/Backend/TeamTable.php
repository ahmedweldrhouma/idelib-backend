<?php

namespace App\Http\Livewire\Backend;

use App\Domains\Auth\Models\User;
use App\Domains\Team\Models\Team;
use App\Domains\Team\Models\TeamUser;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\Views\Filter;
use Illuminate\Support\Facades\Auth;

/**
 * Class TeamTable.
 */
class TeamTable extends DataTableComponent
{
    /**
     * @var
     */
    public $status;




    /**
     * @param  string  $status
     */
    public function mount($status = 'active'): void
    {
        $this->status = $status;
    }

    /**
     * @return Builder
     */
    public function query(): Builder
    {
        $user = Auth::user();
        $team = $user->team()->first();
        $query = User::query();
        if ($team) {
            //If he had a team we get only the users in that team OR himself
            $query = $query->whereRelation('team', 'teams.id', '=', $team->id)->orWhere('id','=',$user->id);
        }else {
            //If he had no team so return only his account
            $query = $query->where('id',$user->id);
        }
        //Add the biller account if exists
        $query->orWhere('type','biller')->where('created_by',$user->id);
        return $query
           ->when($this->getFilter('search'), fn ($query, $term) => $query->search($term));
    }


    /**
     * @return array
     */
    public function columns(): array
    {
        if (Auth::user()->type == User::TYPE_BILLER){
            return [
                Column::make(__('Type'))
                    ->sortable(),
                Column::make(__('Name'), 'first_name')
                    ->sortable(),
                Column::make(__('Role'))
                    ->sortable(),
                Column::make(__('E-mail'), 'email')
                    ->sortable(),
                Column::make(__('Date d’ajout'), 'created_at')
                    ->sortable(),
            ];
        }else{
            return [
                Column::make(__('Type'))
                    ->sortable(),
                Column::make(__('Name'), 'first_name')
                    ->sortable(),
                Column::make(__('Role'))
                    ->sortable(),
                Column::make(__('E-mail'), 'email')
                    ->sortable(),
                Column::make(__('Date d’ajout'), 'created_at')
                    ->sortable(),
                Column::make(__('Actions')),
            ];
        }
    }

    /**
     * @return string
     */
    public function rowView(): string
    {
        return 'backend.auth.user.includes.team_row';
    }

}
