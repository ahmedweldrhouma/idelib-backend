<?php

namespace App\Http\Livewire\Backend;

use App\Domains\Treatment\Models\Treatment;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\Views\Filter;
use Illuminate\Support\Facades\Auth;
use App\Exports\TreatmentsExport;
use App\Exports\CovidExport;

/**
 * Class TreatmentsTable.
 */
class TreatmentsTable extends DataTableComponent
{
    /**
     * @var array|string[]
     */
    public array $filterNames = [
        'date_range' => 'date range',
    ];


    public array $bulkActions = [
        'exportSelected' => 'Export',
    ];

    /**
     * @param  string  $treatmentType
     */
    public function mount($treatmentType): void
    {
        $this->treatmentType = $treatmentType;
    }

    /**
     * @return Builder
     */
    public function query(): Builder
    {
        $nurse = Auth::user();
        if(Auth::user()->isBiller()){
            $nurse = Auth::user()->nurse;
        }

        $query = Treatment::query();
        if($this->treatmentType === 'chronic'){
            $query = $query->OnlyChronic($nurse->id);
        }elseif($this->treatmentType === 'punctual'){
            $query = $query->OnlyPunctual($nurse->id);
        }elseif($this->treatmentType === 'covid'){
            $query = $query->OnlyCovid($nurse->id);
        }

        return $query->when($this->getFilter('search'), fn ($query, $term) => $query->search($term))
                    ->when($this->getFilter('start_date'), fn ($query, $start_date) => $query->where('created_at', '>=',date('Y-m-d',strtotime($start_date))))
                    ->when($this->getFilter('end_date'), fn ($query, $end_date) => $query->where('created_at', '<=',date('Y-m-d',strtotime($end_date))));
    }

    /**
     * @return array
     */
    public function filters(): array
    {
        return [
            'start_date' => Filter::make('Date début')
            ->date([
                'min' => now()->subYear()->format('Y-m-d'), // Optional
                'max' => now()->format('Y-m-d') // Optional
            ]),
            'end_date' => Filter::make('Date fin')
            ->date([
                'min' => now()->subYear()->format('Y-m-d'), // Optional
                'max' => now()->format('Y-m-d') // Optional
            ]),
        ];
    }

    public function columns(): array
    {
        return [
            Column::make(__('Nom'), 'patient_id')
                ->sortable(),
            Column::make(__('Prénom'), 'patient_id')
                ->sortable(),
            Column::make(__('date de naissance'), 'patient_id'),
            Column::make(__('E-mail'), 'patient_id')
                ->sortable(),
            Column::make(__('Numéro de téléphone'), 'patient_id')
                ->sortable(),
            Column::make(__('N° de sécurité sociale'), 'social_security_number'),
            Column::make(__('Adresse'), 'address'),
            Column::make(__('Médecin traitant'), 'doctor'),
            Column::make(__('Tel médecin traitant'), 'doctor_phone'),
            Column::make(__('Antécédents'), 'antecedents'),
            Column::make(__('Date de prise en charge'), 'start_at'),
            // Column::make(__('Number of Users'), 'users_count')
            //     ->sortable(),
            Column::make(__('Actions')),
        ];
    }

    public function rowView(): string
    {
        return 'backend.treatment.includes.row';
    }

    public function exportSelected()
    {
        if ($this->selectedRowsQuery->count() > 0) {
            if($this->treatmentType === 'covid'){
                return (new CovidExport($this->selectedKeys, 'covid_type_treatments'))->download('liste_des_soins.xlsx');
            }else{
                return (new TreatmentsExport($this->selectedKeys))->download('liste_des_soins.xlsx');
            }
        }
    }
}
