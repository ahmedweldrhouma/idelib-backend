<?php

namespace App\Http\Livewire\Backend;

use App\Domains\AdminSubscription\Models\AdminSubscription;
use App\Domains\Auth\Models\User;
use App\Domains\MobileSubscription\Models\MobileSubscription;
use App\Exports\UsersExport;
use Carbon\Carbon;
use Excel;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\Views\Filter;

/**
 * Class UsersTable.
 */
class UsersTable extends DataTableComponent
{
    public string $defaultSortColumn = 'id';
    public string $defaultSortDirection = 'desc';
    /**
     * @var
     */
    public $status;
    public bool $showPerPage = false;

    /**
     * @var array|string[]
     */
    public array $filterNames = [
        'type' => 'User Type',
    ];

    public array $bulkActions = [
        'exportSelected' => 'Export',
    ];



    /**
     * @param string $status
     */
    public function mount($status = 'active'): void
    {

        $this->status = $status;
    }

    /**
     * @return Builder
     */
    public function query(): Builder
    {
        $query = User::query();

        if ($this->status === 'deleted') {
            $query = $query->onlyTrashed();
        } elseif ($this->status === 'deactivated') {
            $query = $query->onlyDeactivated();
        } else {
            $query = $query->onlyActive();
        }

        /*        dd($query
                    ->when($this->getFilter('search'), fn ($query, $term) => $query->search($term))
                    ->when($this->getFilter('type'), fn ($query, $type) => $query->where('type', $type))->get());*/
        //  dd($query->when($this->getFilter('subscription_type'), fn ($query) => $query->all()));
        return $query
            ->when($this->getFilter('search'), fn($query, $term) => $query->search($term))
            ->when($this->getFilter('type'), fn($query, $type) => $query->where('type', $type))
            ->when($this->getFilter('plan'), fn($query, $plan) => $query->byPlan($plan))
            ->when($this->getFilter('subscribed') && $this->getFilter('subscribed') != "", fn($query, $subscriptionType) => $query->bySubscription($subscriptionType));
    }

    /**
     * @return array
     */
    public function filters(): array
    {
        return [
            'type' => Filter::make(__('User Type'))
                ->select([
                    '' => __('All'),
                    User::TYPE_SUPER_ADMIN => 'Super Admin',
                    // User::TYPE_ADMIN => 'Admin',
                    User::TYPE_PATIENT => 'Patient',
                    User::TYPE_NURSE => 'Infirmier',
                    User::TYPE_SUBSTITUTE_NURSE => 'Remplaçant',
                ]),
            'subscribed' => Filter::make('Abonné')
                ->select([
                    "" => 'tous',
                    "adminSubscription" => 'Admin subscription',
                    "mobileSubscription" => 'Mobile subscription',
                ]),
            'plan' => Filter::make('plans')
                ->select([
                    "" => '--',
                    1 => 'lib_2999_1m',
                    2 => 'inf_lib_29999_1y',
                    3 => 'remp_1499_1m',
                    4 => 'remp_14999_1y',
                ]),

        ];
    }

    /**
     * @return array
     */
    public function columns(): array
    {
        return [
            Column::make(__('Id'))
                ->sortable(),
            Column::make(__('Rc Id'))
                ->sortable(),
            Column::make(__('E-mail'), 'email')
                ->sortable(),
            Column::make(__('Name'), 'first_name')
                ->sortable(),
            Column::make(__('Type'))
                ->sortable(),
            Column::make(__('Abonnement'), 'subscription_type')
                ->sortable(),


            Column::make(__('Statut'))
                ->sortable(function(Builder $query, $direction) {

                    return $query
                        ->orderBy(MobileSubscription::select('is_active')->whereColumn('mobile_subscriptions.user_id', 'users.id'), $direction)
                        ->orderBy(AdminSubscription::select('id')
                            ->whereColumn('admin_subscriptions.user_id', 'users.id')
                            ->where('expires_at', '>', Carbon::now()->format('Y-m-d H:i:s')), $direction)
                        ->orderBy('users.id', 'desc')

                        ;
                }),

            Column::make(__('Numéro de téléphone'), 'phone')
                ->sortable(),
            Column::make(__('Date d’inscription'), 'created_at')
                ->sortable(),
            Column::make(__('Souscription via iap'), 'operating_system')
                ->sortable(),
            Column::make(__('Admin subscription'), 'operating_system')
                ->sortable(),
            Column::make(__('Abonné(e) le'), 'subscription_type')
                ->sortable(),
            Column::make(__('Expire le'), 'subscription_type')
                ->sortable(),
            Column::make(__('Équipe'), 'subscription_type')
                ->sortable(),
            Column::make(__('Id associés'), 'active'),
            Column::make(__('Actions'))
            // ->addClass('sticky-col')
        ];
    }

    /**
     * @return string
     */
    public function rowView(): string
    {
        return 'backend.auth.user.includes.row';
    }

    public function exportSelected()
    {
        if ($this->selectedRowsQuery->count() > 0) {
            return (new UsersExport($this->selectedKeys))->download('liste_des_utilisateurs.xlsx');
        }
        return Excel::download(new UsersExport($this->query()->get()), 'users.xlsx');
    }
}
