<?php

namespace App\Http\Livewire\Backend;

use App\Domains\Auth\Models\User;
use App\Domains\Patient\Models\Enums\PatientType;
use App\Domains\Patient\Models\Patient;
use App\Domains\Team\Models\TeamUser;
use Rappasoft\LaravelLivewireTables\Views\Filter;
use App\Exports\CovidExport;
use App\Exports\PatientTreatmentsExport;
use App\Exports\TreatmentsExport;
use App\Exports\UsersExport;
use Auth;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Excel;
use function Clue\StreamFilter\fun;

/**
 * Class PatientsTable.
 */
class PatientsTable extends DataTableComponent
{
    public bool $showPerPage = false;
    public array $bulkActions = [
        'export' => 'Export',
    ];

    public function mount(string $type): void
    {
        $this->type = $type;
    }

    public function query(): Builder
    {
        $user = Auth::user();
        $team = $user->team()->first();

        $query = Patient::query();
        // If the user is a member of a team get the patients of all members of the team
        if ($team) {
            $query = $query->whereHas('nurse', function ($query) use ($team) {
                $query->whereRelation('team', 'teams.id', '=', $team->id);
            });
        } // Otherwise just do a simple query of all the user's patients
        else {
            // PROBLEM: this won't work if the user is a biller
            $query = $query->whereRelation('nurse', 'id', '=', $user->id);
        }
        return $query
            ->when($this->getFilter('search'), fn($query, $term) => $query->search($term))
            ->when(
                $this->getFilter('start_date'), fn($query, $start_date) => $query->whereHas(
                'treatments', function ($trait) {
                $trait->whereBetween('start_at', [date('Y-m-d', strtotime($this->getFilter('start_date'))), date('Y-m-d', strtotime($this->getFilter('end_date')))]);
                $trait->orWhereBetween('end_at', [date('Y-m-d', strtotime($this->getFilter('start_date'))), date('Y-m-d', strtotime($this->getFilter('end_date')))]);
            }
            )
            )->where(function ($query) {
                $query->whereHas('treatments', function ($trait) {
                    $trait->where('classic_type', $this->type);
                })->orWhereDoesntHave('treatments')->where('type', '=', $this->type);
            });
    }

    public function filters(): array
    {
        return [
            'start_date' => Filter::make('Date début')
                ->date([
                    'min' => now()->subYear()->format('Y-m-d'),
                    // Optional
                    'max' => now()->format('Y-m-d') // Optional

                ]),
            'end_date' => Filter::make('Date fin')
                ->date([
                    'min' => now()->subYear()->format('Y-m-d'),
                    // Optional
                    'max' => now()->format('Y-m-d') // Optional

                ]),
        ];
    }

    public function columns(): array
    {
        return [
            Column::make(__('Nom'), 'last_name')
                ->sortable(),
            Column::make(__('Prénom'), 'first_name')
                ->sortable(),
            Column::make(__('date de naissance'), 'birth_date'),
            Column::make(__('E-mail'), 'email')
                ->sortable(),
            Column::make(__('Numéro de téléphone'), 'phone')
                ->sortable(),
            Column::make(__('N° de sécurité sociale'), 'social_security_number'),
            Column::make(__('Adresse'), 'address'),
            Column::make(__('Médecin traitant'), 'doctor'),
            Column::make(__('Antécédents'), 'antecedents'),
            //Column::make(__('Date de prise en charge'), 'doctor_phone'),
            Column::make(__('Actions')),
        ];
    }

    public function rowView(): string
    {
        return 'backend.patient.includes.row';
    }

    public function export()
    {
        /*$patients = $this->query()->with([
            'treatments' => function ($trait) {
                if ($this->getFilter('start_date') != null || $this->getFilter('end_date') != null) {
                    $trait->whereBetween('start_at', [date('Y-m-d', strtotime($this->getFilter('start_date'))), date('Y-m-d', strtotime($this->getFilter('end_date')))]);
                    $trait->orWhereBetween('end_at', [date('Y-m-d', strtotime($this->getFilter('start_date'))), date('Y-m-d', strtotime($this->getFilter('end_date')))]);
                }
            }
        ])->where('type', $this->type)->get();*/
        $user = Auth::user();
        $team = $user->team()->first();
        $query = Patient::query();
        if ($team) {
            $query = $query->whereHas('nurse', function ($query) use ($team) {
                $query->whereRelation('team', 'teams.id', '=', $team->id);
            });
        }
        else {
            $query = $query->whereRelation('nurse', 'id', '=', $user->id);
        }
        $patients = $query
            ->when($this->getFilter('search'), fn($query, $term) => $query->search($term))
            ->when(
                $this->getFilter('start_date'), fn($query, $start_date) => $query->whereHas(
                'treatments', function ($trait) {
                $trait->whereBetween('start_at', [date('Y-m-d', strtotime($this->getFilter('start_date'))), date('Y-m-d', strtotime($this->getFilter('end_date')))]);
                $trait->orWhereBetween('end_at', [date('Y-m-d', strtotime($this->getFilter('start_date'))), date('Y-m-d', strtotime($this->getFilter('end_date')))]);
            }
            )
            )->where(function ($query) {
                $query->whereHas('treatments', function ($trait) {
                    $trait->where('classic_type', $this->type);
                })->orWhereDoesntHave('treatments')->where('type', '=', $this->type);
            })->get();
        $type = PatientType::from($this->type);
        if ($type === PatientType::Chronic) {
            return (
            new PatientTreatmentsExport(
                $patients)
            )->download('liste_des_soins_chroniques.xlsx');
        } else if ($type === PatientType::Punctual) {
            //  return (new CovidExport($this->selectedKeys(), 'covid_type_treatments'))->download(('liste_des_soins.xls'));
            return (
            new PatientTreatmentsExport(
                $patients)
            )->download('liste_des_soins_ponctuelles.xlsx');
        }else if ($type === PatientType::Covid) {
            //  return (new CovidExport($this->selectedKeys(), 'covid_type_treatments'))->download(('liste_des_soins.xls'));
            return (
            new CovidExport(
                $patients)
            )->download('liste_des_soins_covid.xlsx');
        }
    }
}
