<?php

namespace App\Http\Livewire\Backend;

use App\Domains\Patient\Models\Patient;
use App\Domains\Vaccine\Models\Vaccine;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

class PatientVaccinsTable extends DataTableComponent
{
    public function mount(?Patient $patient): void
    {
        $this->patient = $patient;
    }

    public function columns(): array
    {
        return [
            Column::make(__('Id'), 'id'),
            Column::make(__('Name'), 'name'),
            Column::make(__('Date'), 'date'),
            Column::make(__('Doses'), 'doses'),
            Column::make(__('Batch number'), 'batch_number'),
        ];
    }

    public function query(): Builder
    {
        return Vaccine::query()
            ->when(
                $this->patient,
                fn ($query, $patient) => $query->whereBelongsTo($patient)
            );
    }
}












