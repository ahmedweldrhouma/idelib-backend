<?php

namespace App\Http\Livewire\Backend;

use App\Domains\CovidTreatment\Models\CovidTreatment;
use App\Domains\Patient\Models\Patient;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

class PatientCovidTable extends DataTableComponent
{
    public function mount(?Patient $patient): void
    {
        $this->patient = $patient;
    }

    public function columns(): array
    {
        return [
            Column::make(__('Id'), 'id'),
            Column::make(__('Type'), 'type'),
            Column::make(__('Date'), 'date'),
            Column::make(__('Result'), 'result'),
            Column::make(__('Prescription'), 'prescription'),
        ];
    }

    public function query(): Builder
    {
        return CovidTreatment::query()
            ->when(
                $this->patient,
                fn ($query, $patient) => $query->whereBelongsTo($patient)
            );
    }
}
