<?php

namespace App\Http\Livewire\Backend;

use App\Domains\MobileSubscription\Models\MobileSubscription;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

/**
 * Class SubscriptionsTable.
 */
class SubscriptionsTable extends DataTableComponent
{
    /**
     * @return Builder
     */
    public function query(): Builder
    {
        return MobileSubscription::query()
            ->whereHas('user')
            ->when($this->getFilter('search'), fn ($query, $term) => $query->search($term));
    }

    public function columns(): array
    {
        return [
            Column::make(__('Id'), 'id')->sortable(),
            Column::make(__('User'), 'user_id'),
            Column::make(__('système d’exploitation '), 'operating_system'),
            Column::make(__('Original transaction'),'original_transaction_id'),
            Column::make(__('Transaction'), 'transaction_id'),
            Column::make(__('Plan'), 'plan_id'),
            // Column::make(__('Number of Users'), 'users_count')
            //     ->sortable(),
            // Column::make(__('Actions')),
        ];
    }

    public function rowView(): string
    {
        return 'backend.subscription.includes.row';
    }
}
