<?php

namespace App\Http\Livewire\Backend;

use App\Domains\Patient\Models\Patient;
use App\Domains\Treatment\Models\Treatment;
use Carbon\Translator;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

class PatientTreatmentsTable extends DataTableComponent
{



    public function mount(?Patient $patient): void
    {
        $this->patient = $patient;
    }

    public function columns(): array
    {
        return [
            Column::make(__('Id'), 'id')    ,
            Column::make(__('Period'), 'period'),
            Column::make(__('Start at'), 'start_at'),
            Column::make(__('End at'), 'end_at'),
            Column::make(__('Treatment'), 'treatment'),
            Column::make(__('Daily frequency'), 'daily_frequency'),
            Column::make(__('Monthly frequency'), 'monthly_frequency'),
            Column::make(__('Type'), 'type'),
            Column::make(__('Classic type'), 'classic_type'),
        ];
    }


    public function rowView(): string
    {
        return 'backend.patient.view.row';
    }

    public function query(): Builder
    {
        return Treatment::query()
            ->when(
                $this->patient,
                fn ($query, $patient) => $query->whereBelongsTo($patient)
            );
    }
}
