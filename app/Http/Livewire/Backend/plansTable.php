<?php

namespace App\Http\Livewire\Backend;

use App\Domains\Plan\Models\Plan;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

/**
 * Class PlansTable.
 */
class PlansTable extends DataTableComponent
{
    /**
     * @return Builder
     */
    public function query(): Builder
    {
        return Plan::query()
            ->when($this->getFilter('search'), fn ($query, $term) => $query->search($term));
    }

    public function columns(): array
    {
        return [
            Column::make(__('Type d’abonnement'), 'name')
                ->sortable(),
            Column::make(__('Rates'), 'display_price'),
            Column::make(__('Product name'), 'product_name'),
            Column::make(__('Apple Id'), 'apple_id'),
            Column::make(__('Google Id'), 'google_id'),
            Column::make(__('Nombre abonnement actif'), 'google_id'),
            Column::make(__('Nombre abonnement Inactif'), 'google_id'),
            Column::make(__('Nombre des abonnement'), 'google_id'),
            // Column::make(__('Number of Users'), 'users_count')
            //     ->sortable(),
        ];
    }

    public function rowView(): string
    {
        return 'backend.plan.includes.row';
    }
}
