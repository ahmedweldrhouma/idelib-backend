<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class VerifyRcToken
{
    /**
     * Authorization
     */
    const AUTHORIZATION = "Bearer sk_ObmwnYEMdOXFfcWpYVKNwOEHRQIFs";
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->header('Authorization') == self::AUTHORIZATION){
            return $next($request);
        }
    }
}
