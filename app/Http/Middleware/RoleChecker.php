<?php

namespace App\Http\Middleware;

use App\Domains\Auth\Models\User;
use Closure;
use Illuminate\Http\Request;

class RoleChecker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $user = User::ByEmail($request->input('email'))->first();
        if ($user) {
            if ($user->isNurse() && $user->team->first() != null) {
                if ($user->team->first()->pivot->is_admin) {
                    return $next($request);
                }
            } elseif ($user->isSuperAdmin() || $user->isBiller() || $user->team->first() == null) {
                return $next($request);
            }
        }
        return redirect()->route('frontend.auth.login')->withFlashDanger(__('You do not have access to do that.'));
    }
}
