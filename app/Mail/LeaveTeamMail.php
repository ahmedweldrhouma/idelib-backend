<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class LeaveTeamMail extends Mailable
{
    use Queueable, SerializesModels;
    public $team,$user ;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($team,$user)
    {
        $this->user = $user;
        $this->team = $team;
        $this->configName = "leave_team";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(' Vous avez quitter l\'équipe,')->view('frontend.emails.leaveteam');
    }
}
