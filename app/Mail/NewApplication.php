<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class NewApplication extends Mailable
{
    use Queueable, SerializesModels;

    public $user, $applied;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $applied)
    {
        $this->user = $user;
        $this->applied = $applied;
        $this->configName = "new_application";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->subject('Nouveau postulant à votre offre ')->view('frontend.emails.newapplication');
    }
}
