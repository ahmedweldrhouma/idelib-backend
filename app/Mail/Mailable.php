<?php

namespace App\Mail;

use Illuminate\Mail\Mailer as MailerContract;
use Illuminate\Mail\Mailable as BaseMailable;
abstract class Mailable extends BaseMailable
{
    public $configName;
    public function send($mailer)
    {
        //Initializes properties on the Swift Message object
        $this->withSwiftMessage(function ($message) {
            $message->mailable = $this->getConfigName();
        });
        parent::send($mailer);
    }
    protected function getName($class) {
        $path = explode('\\', $class);
        return array_pop($path);
    }
    public function getConfigName(){
        return $this->configName;
    }
}
