<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class TeamInvitationMail extends Mailable
{
    use Queueable, SerializesModels;
    public $team,$email,$sender;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($team,$sender)
    {
        $this->team = $team;
        $this->sender = $sender;
        $this->configName = "team_invitation";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Rejoindre mon équipe')->view('frontend.emails.teamInvitation');
    }
}
